#ifndef TESTING_H_
#define TESTING_H_

#include "useful_definitions.h"

//Entry point for all of the testing that takes place. This can change depending
//on what we want to test. All actual testing should be place in a 
void test( AlgorithmParameters* params ) ;

void test_restriction( AlgorithmParameters* params ) ;

void test_approx_params_set_up( AlgorithmParameters* params ) ;

void test_gradient_recovery( AlgorithmParameters* params ) ;

void test_smoothing( AlgorithmParameters* params ) ;

void test_smooth_to_converge( AlgorithmParameters *params ) ;

void test_smooth_after_mg( AlgorithmParameters* params ) ;

void test_interpolation( AlgorithmParameters *params ) ;

void test_l2_norm_calculation( AlgorithmParameters *params ) ;

void test_h1_seminorm_calculation( AlgorithmParameters *params) ;

void test_h1_norm_calculation( AlgorithmParameters *params ) ;

void test_lapack_solve( AlgorithmParameters *params) ;

void test_connection( AlgorithmParameters *params ) ;

void test_smooth_block( AlgorithmParameters *params ) ;

void print_smooth_func( AlgorithmParameters* params, int type ) ;

void test_approx_set_up( AlgorithmParameters *params ) ;

void test_set_up_rhs( AlgorithmParameters *params ) ;

void test_grid_indices( AlgorithmParameters *params ) ;

void print_row_start_inds( AlgorithmParameters *params ) ;

void print_neighbours( AlgorithmParameters *params ) ;

void test_resid_set_up( AlgorithmParameters *params ) ;

void test_read_func_from_file( AlgorithmParameters *params ) ;

void test_jacobian_calculation( AlgorithmParameters *params ) ;

void test_exact_solve( AlgorithmParameters *params ) ;

void test_fmg( AlgorithmParameters *params ) ;

void test_apply_operator( AlgorithmParameters *params ) ;

void test_hess_to_upper( AlgorithmParameters *params ) ;

void test_back_sub( AlgorithmParameters *params ) ;

void test_least_squares( AlgorithmParameters *params ) ;

void test_factorial( AlgorithmParameters *params ) ;

void test_grid_boundary_types( AlgorithmParameters *params ) ;

void test_comp_exact_numeric_deriv( AlgorithmParameters *params ) ;

void test_tc10_exact_sol_progression( AlgorithmParameters *params ) ;

void print_tc10_sol_at_timestep( AlgorithmParameters *params ) ;

void test_comp_symm_nonsymm_parts( AlgorithmParameters *params ) ;
	
void test_hydr_cond_calc( AlgorithmParameters *params ) ;

void test_hydr_cond_deriv( AlgorithmParameters *params ) ;

void test_theta_calc( AlgorithmParameters *params ) ;

void test_theta_deriv_calc( AlgorithmParameters *params ) ;

//Test that the smoother works for the Richards equation type problems
void test_smooth_richards_eq( AlgorithmParameters *params ) ;

//Test that the time stepping works for the Richards equation type problem. At
//each time step we solve using a smoother only
void test_re_time_step_smooth( AlgorithmParameters *params ) ;

//Test that taking a Newton step followed by solves using smoothing only gives
//a convergent Newton iteration
void test_re_newton_smooth( AlgorithmParameters *params ) ;

//Tests that a multigrid iteration can be performed for the Richards equation
void test_re_newton_mg( AlgorithmParameters *params ) ;

//Tests that we get the right output from calculating the 'next' node on an
//element in terms of the local element numbering
void test_next_node( AlgorithmParameters *params ) ;

//Tests that the soil types are allocated correctly for a Richards equation type
//problem
void test_soil_type_allocation( AlgorithmParameters *params ) ;

//Tests tha the value of the volumetric wetness is calculated correctly for
//different soil types
void test_theta_setup_multiple_soils( AlgorithmParameters *params ) ;

//Tests that the value of the hydraulic conductivity is calculated correctly for
//problems of Richards equation type with multiple soil types
void test_hydr_cond_setup_mult_soils( AlgorithmParameters *params ) ;

//Tries to reproduce the results from Vogel et al, 2001 for a 1-dimensional test
//case
void test_1d_richards_smooth( AlgorithmParameters *params ) ;

//Tests the solution of the Richards equation on a 2D grid with only one column
//of unknowns. There is only flow due to gravity. This is included to test
//against what is contained in Vogel et al, 2001. We solve using a Newton
//iteration with repeated smooths to compare against the case of nonlinear
//smoothing
void test_1d_richards_newton_smooth( AlgorithmParameters *params ) ;

//Tests that the pressure is correctly calculated given the volumetric wetness
void test_calc_pressure_from_theta( AlgorithmParameters *params ) ;

//Tests that the coefficient for the p-Laplacian is set up properly
void test_pc_coeff_set_up( AlgorithmParameters *params ) ;

//Tests that the restriction of a piecewise constant function on a fine grid can
//be restricted to a coarse grid
void test_restrict_pc_coeff( AlgorithmParameters *params ) ;

#endif

















