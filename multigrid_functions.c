#include "multigrid_functions.h"
#include "richards_eq.h"
#include "test_cases.h"

/** Performs two dimensional Simpson's rule (3rd order accurate) on the discrete
 *	function that is passed in over a single element on the grid. It is assumed
 *	that a piecewise linear basis is used
 *	\param [in] nodes	Indices of nodes on an element to consider. These are in
 						the range (0,1,2)
 *	\param [in] elId	The ID of an element on the grid
 *	\param [in] grid	The grid on which the function has been discretized
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] approx	A discrete function. This is often taken to be the
 						approximation to the solution of a discrete system of
 						equations. This is required, for example, when
 						integrating nonlinear terms
 *	\param [in] grid_function	The function to integrate over the given element
 *	\return \p double value of \a grid_function integrated over element with ID
 *	        \a elId on \a grid
 */
double simpsons_rule( const int* nodes, int elId, const GridParameters* grid,
	const AlgorithmParameters* params, const ApproxVars *aVars,
	const double* approx, double (*grid_function)(const int*, int,
	const GridParameters*, const AlgorithmParameters*, const ApproxVars*,
	const double*, double, double ) )
{
	int j ;
	double centreX, centreY ; //Stores the x and y coordinate of the centroid
	int nextV ; //The next vertex in the triangle (in a counter-clockwise
				//direction)
	double retVal ; //The return value from the function
	
	const Element *e ;
	const Node *elNodes[3] ;

	e = &(grid->elements[ elId ]) ;
	elNodes[0] = &(grid->nodes[ e->nodeIDs[0] ]) ;
	elNodes[1] = &(grid->nodes[ e->nodeIDs[1] ]) ;
	elNodes[2] = &(grid->nodes[ e->nodeIDs[2] ]) ;
	
	//Simpson's rule simply returns a weighted average of function evaluations
	//at different points on the triangle
	
	//First we calculate the coordinates for the centroid of the triangle
	centreX = ( elNodes[0]->x + elNodes[1]->x + elNodes[2]->x ) / 3.0 ;
	centreY = ( elNodes[0]->y + elNodes[1]->y + elNodes[2]->y ) / 3.0 ;
	
	retVal = ( 9.0 / 20.0 ) * grid_function( nodes, elId, grid, params, aVars,
		approx, centreX, centreY ) ;
		
	for( j=0 ; j < 3 ; j++ ){
		nextV = (j+1)%3 ;
		//Evaluate the function at each of the grid points and the mid point
		//of an edge
		retVal += grid_function( nodes, elId, grid, params, aVars, approx,
			elNodes[j]->x, elNodes[j]->y ) / 20.0 ;
		retVal += ( 2.0 / 15.0 ) * grid_function( nodes, elId, grid,
			params, aVars, approx, ( elNodes[j]->x + elNodes[nextV]->x ) / 2.0,
			( elNodes[j]->y + elNodes[nextV]->y ) / 2.0 ) ;
	}
	
	retVal *= e->area ;
	
	//Return the appropriate value
	return retVal ;
}

/** Calculates the iteration matrix that is used in the multigrid iteration.
 *	This is taken to be the matrix that multiplies the unknown vector in an
 *	iteration, and can depend on the solution, if the problem is nonlinear.
 *	Using this method allows for the residual calculation to be performed by the
 *	same method irrespective if the problem is linear or nonlinear
 *	\param [in]	grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] approx	The approximation to the solution of the discrete
 *						system of equations to solve. This is required if the
 *						problem is nonlinear
 *	\param [in,out] aVars	Variables associated with the approximation on the
 * 							current grid. The member variable
 *							ApproxVars::iterMat is updated in this method
 */
void calculate_iteration_matrix( const GridParameters* grid,
	const AlgorithmParameters* params, const double* approx,
	ApproxVars *aVars )
{
	int i, j, k ;
	int e;
	const Node *n1, *n2 ;
	
	double *iterMat ; //Placholder for the iteration matrix. Makes the code look
					 //a little neater
					 
	iterMat = aVars->iterMat ;
	
	//I assume that the iteration matrix has not been initialised to zero yet,
	//so do this here, if we are not performing a time dependent solve.
	//Otherwise we set the value initially to be what we have stored in the
	//mass matrix
	if( is_time_dependent( params->testCase ) || params->testCase == 11 )
		copy_vectors( grid->numMatrixEntries, iterMat, aVars->massMat ) ;
	else
		set_dvect_to_zero( grid->numMatrixEntries, iterMat ) ;

	//For each triangle on the grid
	for ( e = 0; e < grid->numElements; e++ ){
		//For each vertex of the triangle
		for ( i = 0; i < 3; i++ ){
			//Get the node on the grid corresponding to vertex i of the element
			n1 = &(grid->nodes[ grid->elements[e].nodeIDs[i] ]) ;
			//If the node is a Dirichlet boundary node we ignore it
			if( n1->type == NODE_DIR_BNDRY ) continue ;
			
			//For each node in the triangle
			for ( j = 0; j < 3; j++ ){
				//Get the node on the grid correpsonding to vertex j of the
				//element
				n2 = &(grid->nodes[ grid->elements[e].nodeIDs[j] ]) ;
				//If the node is not a Dirichlet boundary node count
				//the contribution to the iteration matrix
				if ( n2->type == NODE_DIR_BNDRY ) continue ;
					
				//Find the index into the sparse representation of the
				//matrix where the entry for the contribution of node1 and
				//node2 should go
				//First get the index into the row of the matrix for the
				//first node
				k = grid->rowStartInds[ grid->mapG2A[ n1->id ] ] +
					connected( n1, n2 ) ;
				//Apply the operator on the gird and add the contribution
				//in the correct entry of the sparse representation of the
				//iteration matrix
				if( aVars->recGrads!=NULL ){
					//I am assuming that if a non-null value for 'recGrads'
					//has been passed that the recovery of the values should
					//be performed
					iterMat[k] += recover_stiff_term( i, j,
						&(grid->elements[e]), approx, aVars->recGrads, grid,
						params ) ;
				}
				else{
					//Add the contribution to the iteration matrix in the
					//appropriate position
					iterMat[k] += exact_stiff_term( i, j,
						&(grid->elements[e]), approx, aVars, grid,
						params ) ;
				}
			}
		}
	}
}

/** Constructs the mass matrix for a piecewise linear nodal basis defined on a
 *	given grid
 *	\param [in] grid	The grid on which the basis is defined
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] massMat	Sparse representation of the mass matrix for the
 *							given grid
 */
void calculate_mass_matrix( const GridParameters* grid,
	const AlgorithmParameters* params, double *massMat )
{
	int elCnt, i, j, k ; //Loop counters
	int rowInd ; //Index into the row of the mass matrix for a node on the grid
	const Element *e ; //Placeholder for a triangle
	const Node *n1, *n2 ;
	double fact ;
		
	//For all the elements
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Set a placholder to the current triangle on the grid
		e = &(grid->elements[elCnt]) ;
		//Loop through all of the nodes in the triangle
		for( i=0 ; i<3 ; i++ ){
			n1 = &(grid->nodes[ e->nodeIDs[i] ]) ;
			//If we are at a Dirichlet boundary we move to the next node
			if( n1->type == NODE_DIR_BNDRY ) continue ;
			rowInd = grid->rowStartInds[ grid->mapG2A[ n1->id ] ] ;
				
			//Loop through all of the nodes on the triangle
			for( j=0 ; j<3 ; j++ ){
				n2 = &(grid->nodes[ e->nodeIDs[j] ]) ;
				//Find the connection between the two nodes, if it exists
				k = connected( n1, n2 ) ;
				//If a connection was found then update the appropriate entry
				//in the mass matrix
				if( k >= 0 ){
					fact = (n2->id == n1->id) ? 2.0 : 1.0 ;
					massMat[k+rowInd] += fact * e->area / 12.0 ;
				}
			}
		}
	}
}


/** Constructs a lumped mass matrix for a piecewise linear finite element basis
 *	on a given grid
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] massMat	The mass matrix to update the values for. It is
 *							assumed that any values in this variable are to be
 *							kept and added to
 */
void calc_lumped_mass_matrix( const GridParameters *grid,
	const AlgorithmParameters *params, double *massMat )
{
	int elCnt, i, j, k ; //Loop counters
	const Element *e ; //Placeholder for an element on the grid
	const Node *n1, *n2 ; //Nodes on the grid
	double fact ; //A weighting factor to apply during calculations
	
	//Loop through the elements
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//set a placeholder to the current grid element
		e = &(grid->elements[ elCnt ]) ;
		//Loop through the nodes on the element
		for( i=0 ; i < 3 ; i++ ){
			n1 = &(grid->nodes[ e->nodeIDs[i] ]) ;
			//Check if we are at a Dirichlet boundary
			if( n1->type == NODE_DIR_BNDRY ) continue ;
			
			//Loop through the nodes on the element
			for( j=0 ; j < 3 ; j++ ){
				n2 = &(grid->nodes[ e->nodeIDs[j] ]) ;
				
				//Find if the two nodes are connected on the element
				k = connected( n1, n2 ) ;
				
				//If a connection was found then update the appropriate entry
				//in the mass matrix. For the lumped mass matrix this 
				//corresponds to the diagonal for the column (j) under
				//consideration
				if( k >= 0 ){
					fact = (n2->id == n1->id) ? 2.0 : 1.0 ;
					k = grid->rowStartInds[ grid->mapG2A[ n2->id ] ] ;
					massMat[k] += fact * e->area / 12.0 ;
				}
			}
		}
	}
}

/** If a linear basis function is considered to be of the form \f$a + bx + cy\f$
 *	on each element, then this function returns the coefficient \f$b\f$ scaled
 *	by a factor of twice the area of the element \a e
 *	\param [in] i	The vertex of element \a e at which the basis function is
 *					centred. This is in the range (0,1,2)
 *	\param [in] e	The element on which to find the coefficient \f$b\f$
 *	\param [in] nodes	Global list of nodes on the grid
 *	\return \p double value containing the coefficient \f$b\f$ for a piecewise
 *			linear basis of the form \f$a + bx + cy\f$
 */
double b( int i, const Element *e, const Node *nodes )
{
	int iN1, iN2 ;
	
	iN1 = e->nodeIDs[(i+1)%3] ;
	iN2 = e->nodeIDs[(i+2)%3] ;
	
	return( nodes[iN1].y - nodes[iN2].y ) ;
}

/** If a linear basis function is considered to be of the form \f$a + bx + cy\f$
 *	on each element, then this function returns the coefficient \f$c\f$ scaled
 *	by a factor of twice the area of the element \a e
 *	\param [in] i	The vertex of element \a e at which the basis function is
 *					centred. This is in the range (0,1,2)
 *	\param [in] e	The element on which to find the coefficient \f$c\f$
 *	\param [in] nodes	Global list of nodes on the grid
 *	\return \p double value containing the coefficient \f$c\f$ for a piecewise
 *			linear basis of the form \f$a + bx + cy\f$
 */
double c( int i, const Element *e, const Node *nodes )
{
	int iN1, iN2 ;
	
	iN1 = e->nodeIDs[(i+1)%3] ;
	iN2 = e->nodeIDs[(i+2)%3] ;
	
	return ( nodes[iN2].x - nodes[iN1].x );
}

/** Calculates the area of a given element on a given grid
 *	\param [in] grid	The grid on which to operate
 *	\param [in] e		The element on the grid to calculate the area for
 *	\return \p double value of the area of element \a e on the grid
 */
double element_area( const GridParameters *grid, const Element *e )
{
	const Node *n1, *n2, *n3 ;
	
	n1 = &(grid->nodes[ e->nodeIDs[0] ]) ;
	n2 = &(grid->nodes[ e->nodeIDs[1] ]) ;
	n3 = &(grid->nodes[ e->nodeIDs[2] ]) ;
	
	return fabs( (n2->x - n1->x) * (n3->y - n1->y) - (n1->x - n3->x) *
		(n1->y - n2->y) ) / 2.0 ;
}

/** Performs restriction of a function discretized on a regular fine grid to
 *	a regular coarse grid, assuming that a regular coarsening has been performed
 *	where every second column and row are taken on the coarse grid
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] fineGrid	The fine grid from which to restrict
 *	\param [in] coarseGrid	The coarse grid to restrict onto
 *	\param [in] fineFunc	The function to restrict from the fine grid
 *	\param [in] rType		The type of restriction to perform
 *	\param [in] useWeightFact	Indicator as to whether or not to use an
 *								extra weighting factor when restricting. This
 *								extra weighting factor is used when restricting
 *								integrals, such that the restriction operator
 *								is the transpose of the linear prolongation
 *								operator, rather than a weighted transpose, as
 *								is the case if this variable is not set
 *	\param [out] coarseFunc	The coarse grid function that is the result of
 *							restricting from the fine grid
 */
void restriction( const AlgorithmParameters* params,
	const GridParameters* fineGrid, const GridParameters* coarseGrid,
	const double* fineFunc, enum RESTRICT_TYPE rType,
	int useWeightFact, double* coarseFunc )
{
	int numFNodes, numCNodes ;//The number of nodes in a row for the fine and
							  //coarse grids
							  
	numFNodes = sqrt( fineGrid->numUnknowns ) ;
	numCNodes = sqrt( coarseGrid->numUnknowns ) ;
							  
	if( rType == R_TYPE_FW ){
		//Perform full weighting
		restrict_FW( fineGrid, coarseGrid, useWeightFact, fineFunc,
			coarseFunc ) ;
	}
	else if( rType == R_TYPE_INJECTION ){
		//Perform injection
		restrict_injection( fineGrid, numFNodes, coarseGrid, numCNodes,
			useWeightFact, fineFunc, coarseFunc ) ;
	}	
}

/** Performs restriction of a residual, which may contain a boundary integral,
 *	which needs to be treated differently
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] fGrid	The fine grid from which to restrict
 *	\param [in] cGrid	The coarse grid to restrict onto
 *	\param [in] fResid	The residual on the fine grid
 *	\param [in] fBndTerm	Boundary integral on the fine grid
 *	\param [out] cResid	The residual on the coarse grid. This is populated in
 *						this method
 *	\param [out] cBndTerm	The boundary integral on the coarse grid. This is
 *							populated in this method
 */
void restrict_residual( const AlgorithmParameters *params,
	const GridParameters *fGrid, const GridParameters *cGrid,
	const double *fResid, const double *fBndTerm, double *cResid,
	double *cBndTerm )
{
	int i, j ; //Loop counters
	int cInd, fInd ; //Index of an unknown on the coarse / fine grid
	int fCols, cCols ; //The number of unknown nodes in a row of the fine and
					   //coarse grid, respectively
	double nWeight ; //The weighting given to the nodal value for a particular
					 //node
	double tmp ; //Used to store intermediate values in calculations
	double *tmpResid ; //Used as a placeholder so that we can change the
					   //residual from the fine grid
	int fLOffset ; //The offset from the left hand boundary on the fine grid
	               //where the unknowns start
	int fROffset ; //The offset from the right hand boundary on the fine grid
	               //where the unknowns end
	int increment ; //The number of nodes to skip over on the fine grid when
	                //moving to the next coarse grid row

	const int *fMapG2A, *cMapG2A ; //Placeholder for the maps from nodes on the
	                               //grid to the approximation on the grids
	const int *fMapA2G, *cMapA2G ; //Placeholder for the maps from the
	                               //approximation to nodes on the grid
	                               
	const Node *fNodes, *cNodes ; //Placeholders for the lits of nodes on the
								  //fine and coarse grids
	const Node *fNode, *cNode ; //Placeholder for a single node on the grid

	//Check if we should do any restriction
	if( !(is_richards_eq( params->testCase )) ){
		restrict_FW( fGrid, cGrid, 1, fResid, cResid ) ;
		return ;
	}
	
	//Take away the boundary contribution before we restrict
	tmpResid = (double*)fResid ;
	for( i= 0 ; i < fGrid->numUnknowns ; i++ ){
		tmpResid[i] += fBndTerm[i] ;
	}
	
	//Set all the placeholders
	fMapG2A = fGrid->mapG2A ;
	fMapA2G = fGrid->mapA2G ;
	cMapG2A = cGrid->mapG2A ;
	cMapA2G = cGrid->mapA2G ;
	fNodes = fGrid->nodes ;
	cNodes = cGrid->nodes ;
					   
	//Set up the number of columns (equivalently the number of unknowns) to be
	//the same as the number of columns in the grids
	fCols = fGrid->numCols ;
	cCols = cGrid->numCols ;
	
	//If the left or the right boundary are a Dirichlet boundary reduce the
	//number of unknowns in a row by one for both the fine and coarse grid
	for( i=0 ; i < 3 ; i+=2 ){
		if( fGrid->bndType[i] == NODE_DIR_BNDRY ){
			fCols-- ;
			cCols-- ;
		}
	}
	
	//Figure out from the boundary type on the left boundary if the unknowns are
	//offset from the boundary or not
	fLOffset = ( fGrid->bndType[0] == NODE_DIR_BNDRY ) ? 1 : 0 ;
	fROffset = ( fGrid->bndType[2] == NODE_DIR_BNDRY ) ? 1 : 0 ;
	increment = fLOffset + fROffset + fCols - 1 ;
	
	//Loop through the unknowns on the coarse grid
	cInd = 0 ;
	fInd = fLOffset ;
	//Find the first index into the fine grid 
	cNode = &(cNodes[ cMapA2G[0] ]) ;
	fNode = &(fNodes[ fMapA2G[fInd] ]) ;
	//If the fine grid node is not in the same position as the coarse grid node
	//move up one row on the fine grid. We know that the x-coordinate will be
	//the same so only need to check if we are in the same row or not
	if( fNode->y != cNode->y )
		fInd += fCols ;
	//Now we are at the right starting position we loop through all of the 
	//coarse grid nodes
	while( cInd < cGrid->numUnknowns ){
		//Loop through the row on the coarse grid
		for( i = 0 ; i < cCols ; i++, cInd++, fInd+=2 ){
			//Get the fine grid node
			fNode = &(fNodes[ fMapA2G[fInd] ]) ;
			//Work out the weighting factor to be applied to the node in order
			//that the restriction is the adjoint of the prolongation
			nWeight = 1.0 / (fNode->numNghbrs + fNode->numDirNghbrs + 1) ;
			//We weight the value at the fine grid node that is also on the
			//coarse grid twice as heavily as the others
			tmp = 2.0 * nWeight * fResid[ fInd ] ;
			//Add a contribution from the non-Dirichlet boundary nodes
			for( j=1 ; j < fNode->numNghbrs ; j++ ){
				//For each of the neighbours of the fine grid node add a
				//weighted component of the function value to the coarse grid
				//value
				tmp += nWeight * fResid[ fMapG2A[ fNode->nghbrs[j] ] ] ;
			}

			//Assign the value to the appropriate coarse point, including terms
			//held in the boundary term
			cBndTerm[cInd] = 2.0 * fBndTerm[fInd] ;
			cResid[cInd] = fNode->weightFact * tmp - cBndTerm[cInd];
		}
		//Make sure that the fine grid pointer is pointing to the correct fine
		//grid node
		fInd += increment ;
	}
	
	//Set the residual to what it was before
	for( i=0 ; i < fGrid->numUnknowns ; i++ )
		tmpResid[i] -= fBndTerm[i] ;
}

/** Performs full-weighting restriction. This takes a weighted average of fine
 *	grid neighbours to calculate the coarse grid value, giving twice the weight
 *	to the node that is on both the fine and coarse grids
 *	\param [in] fGrid	The fine grid from which to restrict
 *	\param [in] cGrid	The coarse grid onto which to restrict
 *	\param [in] useWeightFact	Indicator as to whether or not to use an
 *								extra weighting factor when restricting. This
 *								extra weighting factor is used when restricting
 *								integrals, such that the restriction operator
 *								is the transpose of the linear prolongation
 *								operator, rather than a weighted transpose, as
 *								is the case if this variable is not set
 *	\param [in] fFunc	Nodal values of the function on the fine grid
 *	\param [in] cFunc	Nodal values of the function on the coarse grid. These
 *						are populated in this method
 */
void restrict_FW( const GridParameters *fGrid, const GridParameters *cGrid,
	int useWeightFact, const double *fFunc, double *cFunc )
{
	int i, j ; //Loop counters
	int cInd, fInd ; //Index of an unknown on the coarse / fine grid
	int fCols, cCols ; //The number of unknown nodes in a row of the fine and
					   //coarse grid, respectively
	double nWeight ; //The weighting given to the nodal value for a particular
					 //node
	double tmp ; //Used to store intermediate values in calculations
	int fLOffset ; //The offset from the left hand boundary on the fine grid
	               //where the unknowns start
	int fROffset ; //The offset from the right hand boundary on the fine grid
	               //where the unknowns end
	int increment ; //The number of nodes to skip over on the fine grid when
	                //moving to the next coarse grid row

	const int *fMapG2A, *cMapG2A ; //Placeholder for the maps from nodes on the
	                               //grid to the approximation on the grids
	const int *fMapA2G, *cMapA2G ; //Placeholder for the maps from the
	                               //approximation to nodes on the grid
	                               
	const Node *fNodes, *cNodes ; //Placeholders for the lits of nodes on the
								  //fine and coarse grids
	const Node *fNode, *cNode ; //Placeholder for a single node on the grid
	                               
	//Set all the placeholders
	fMapG2A = fGrid->mapG2A ;
	fMapA2G = fGrid->mapA2G ;
	cMapG2A = cGrid->mapG2A ;
	cMapA2G = cGrid->mapA2G ;
	fNodes = fGrid->nodes ;
	cNodes = cGrid->nodes ;
					   
	//Set up the number of columns (equivalently the number of unknowns) to be
	//the same as the number of columns in the grids
	fCols = fGrid->numCols ;
	cCols = cGrid->numCols ;
	
	//If the left or the right boundary are a Dirichlet boundary reduce the
	//number of unknowns in a row by one for both the fine and coarse grid
	for( i=0 ; i < 3 ; i+=2 ){
		if( fGrid->bndType[i] == NODE_DIR_BNDRY ){
			fCols-- ;
			cCols-- ;
		}
	}
	
	//Figure out from the boundary type on the left boundary if the unknowns are
	//offset from the boundary or not
	fLOffset = ( fGrid->bndType[0] == NODE_DIR_BNDRY ) ? 1 : 0 ;
	fROffset = ( fGrid->bndType[2] == NODE_DIR_BNDRY ) ? 1 : 0 ;
	increment = fLOffset + fROffset + fCols - 1 ;
	
	//Loop through the unknowns on the coarse grid
	cInd = 0 ;
	fInd = fLOffset ;
	//Find the first index into the fine grid 
	cNode = &(cNodes[ cMapA2G[0] ]) ;
	fNode = &(fNodes[ fMapA2G[fInd] ]) ;
	//If the fine grid node is not in the same position as the coarse grid node
	//move up one row on the fine grid. We know that the x-coordinate will be
	//the same so only need to check if we are in the same row or not
	if( fNode->y != cNode->y )
		fInd += fCols ;
	//Now we are at the right starting position we loop through all of the 
	//coarse grid nodes
	while( cInd < cGrid->numUnknowns ){
		//Loop through the row on the coarse grid
		for( i = 0 ; i < cCols ; i++, cInd++, fInd+=2 ){
			//Get the fine grid node
			fNode = &(fNodes[ fMapA2G[fInd] ]) ;
			//Work out the weighting factor to be applied to the node in order
			//that the restriction is the adjoint of the prolongation
			nWeight = 1.0 / (fNode->numNghbrs + fNode->numDirNghbrs + 1) ;
			//We weight the value at the fine grid node that is also on the
			//coarse grid twice as heavily as the others
			tmp = 2.0 * nWeight * fFunc[ fInd ] ;
			//Add a contribution from the non-Dirichlet boundary nodes
			for( j=1 ; j < fNode->numNghbrs ; j++ ){
				//For each of the neighbours of the fine grid node add a
				//weighted component of the function value to the coarse grid
				//value
				tmp += nWeight * fFunc[ fMapG2A[ fNode->nghbrs[j] ] ] ;
			}
			//Add a contribution from the Dirichlet boundary nodes
			for( j=0 ; j < fNode->numDirNghbrs ; j++ )
				tmp += nWeight * fFunc[ fMapG2A[ fNode->nghbrs[
					SPARSE - 1 - j] ] ] ;
			//Assign the value to the appropriate coarse point
			if( useWeightFact ) cFunc[cInd] = fNode->weightFact * tmp ;
			else cFunc[cInd] = tmp ;
		}
		//Make sure that the fine grid pointer is pointing to the correct fine
		//grid node
		fInd += increment ;
	}
}

/** Performs injection of the fine grid function onto the coarse grid. This
 *	simply transfers the values from the nodes on the fine grid that are also
 *	on the coarse grid
 *	\param [in] fGrid	The fine grid to restrict from
 *	\param [in] fRNodes	The number of nodes in one row of the fine grid,
 *						\e including Dirichlet boundary nodes
 *	\param [in] cGrid	The coarse grid to restrict onto
 *	\param [in]	cRNodes	The number of nodes in a row on the coarse grid,
 *						\e including Dirichlet boundary nodes
 *	\param [in] useWeightFact	Indicator as to whether or not to use an
 *								extra weighting factor when restricting. This
 *								extra weighting factor is used when restricting
 *								integrals, such that the restriction operator
 *								is the transpose of the prolongation
 *								operator, rather than a weighted transpose, as
 *								is the case if this variable is not set
 *	\param [in] fFunc	The function to restrict from the fine grid
 *	\param [out] cFunc	The function on the coarse grid that is populated with
 *						the restricted values from the fine grid
 */
void restrict_injection( const GridParameters *fGrid, int fRNodes,
	const GridParameters *cGrid, int cRNodes, int useWeightFact,
	const double *fFunc, double *cFunc )
{
	int cInd, i ; //Loop variables
	int fRow ; //Indicates which row on the fine grid we are on
	int fInd ; //The node on the fine grid
	double weightFact ;
	
	//Set the current row on the coarse grid to be zero
	cInd = 0 ;
	fRow = 1 ;
	
	weightFact = 1.0 ;
	
	//We treat one row at a time
	while( cInd < cGrid->numUnknowns ){
		//Find the index of the approximation on the fine grid corresponding to
		//the coarse grid coordinate
		fInd = fRow * fRNodes + 1 ;
		for( i=0 ; i < cRNodes ; i++, cInd++, fInd+=2 )
			//Transfer the value from the fine grid node to the coarse grid
			//node, after multiplying by the weighting factor
			if( useWeightFact ) weightFact =
				fGrid->nodes[ fGrid->mapA2G[ fInd ] ].weightFact ;
			cFunc[cInd] = weightFact * fFunc[ fInd ] ;
		//Increase the count of the row
		fRow += 2 ;
	}	
}

/** Restricts the integral of a piecewise constant coefficient function on the
 *	elements of a fine grid to the elements of the coarse grid. It is assumed
 *	that the fine and coarse grids are rectangular, regular and triangular. The
 *	coarse grid elements are constructed from four fine grid elements
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] fGrid	The fine grid from which to restrict
 *	\param [in] cGrid	The coarse grid to restrict onto
 *	\param [in] fCoeff	The fine grid coefficient function defined elementwise
 *	\param [out] cCoeff	The coarse grid coefficient function defined
 *						elementwise. This is populated in this method
 */
void restrict_pc_coeff( const AlgorithmParameters *params,
	const GridParameters *fGrid, const GridParameters *cGrid,
	const double *fCoeff, double *cCoeff )
{
	int rowCnt ; //Counter for the number of rows on the coarse grid
	int colCnt ; //Counter for the number of columns on the coarse grid
	int fElsPerRow ; //The number of elements per row on the fine grid
	int cElsPerRow ; //The number of elements per row on the coarse grid
	int cEl ; //Index of an element on the coarse grid
	int fEl ; //Index of an element on the fine grid
					 
	if( params->meshType == MESH_DIAG_BR2TL ||
		params->meshType == MESH_DIAG_BR2TL_Q ){
		printf( "Restriction of the piecewise constant coefficient only " ) ;
		printf( "Defined for the case of diagonal running from bottom left " ) ;
		printf( "to top right. Please change the type of mesh used\n" ) ;
		exit( 0 ) ;
	}
	
	//Set the number of elements that there are in a row on the fine grid
	fElsPerRow = 2 * (fGrid->numCols - 1) ;
	cElsPerRow = 2 * (cGrid->numCols - 1) ;
	
	//Loop through all of the rows on the grid
	for( rowCnt = 0 ; rowCnt < cGrid->numRows-1 ; rowCnt ++ ){
		//Set the values for the elements with hypotenuse on the lower side
		//in the current coarse grid row
		fEl = 2 * rowCnt * fElsPerRow ;
		cEl = rowCnt * cElsPerRow ;
		for( colCnt = 0 ; colCnt < cGrid->numCols-1 ; colCnt++, cEl+=2, fEl+=4 )
			cCoeff[cEl] = (fCoeff[fEl] + fCoeff[fEl + fElsPerRow] +
				fCoeff[fEl + fElsPerRow + 1] + fCoeff[fEl + fElsPerRow + 2])
				/ 4.0 ;
			
		//Set the values for the elements with hypotenuse on the upper side
		//in the current coarse grid row
		fEl = 2 * rowCnt * fElsPerRow + 1 ;
		cEl = rowCnt * cElsPerRow + 1 ;
		for( colCnt = 0 ; colCnt < cGrid->numCols-1 ; colCnt++, cEl+=2, fEl+=4 )
			cCoeff[cEl] = (fCoeff[fEl] + fCoeff[fEl+1] + fCoeff[fEl + 2] +
				fCoeff[fEl + 2 + fElsPerRow]) / 4.0 ;
	}
}

/** Performs prolongation of a discrete function from a coarse grid to a fine
 *	grid. It is assumed that the grids are regular rectangular grids, i.e. the
 *	grid spacing in each dimension is constant.
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] cGrid	The coarse grid to prolongate from
 *	\param [in] cFunc	The function to interpolate to the fine grid
 *	\param [in] fGrid	The fine grid to prolongate onto
 *	\param [out] fFunc	The function on the fine grid to be populated
 */
void prolongate( const AlgorithmParameters *params,
	const GridParameters *cGrid, const double *cFunc,
	const GridParameters *fGrid, double *fFunc )
{
	int i ; //Loop counter
	int fInd ; //The index of a node on the fine grid
	int cInds[2] ; //Indices of nodes on the coarse grid from which to take
				   //the contribution to the fine grid
	int cRow ; //Indicates which row we are on on the coarse grid
	int fCols, cCols ; //The number of nodes in a row of the fine / coarse grid
	int addInd ; //Gives the index into 'cInds' for the index to increase at the
				 //next step
	int onCGrid ; //Indicates whether the fine grid row is on the coarse grid
	int lOffset ; //Gives the offset from the left hand boundary where the
				  //unknown nodes start
	int diagBL ; //Indicator to show whether the diagonal runs from the bottom
				 //left to the top right, or not (i.e. from the bottom right to
				 //the top left). A value zero indicates that it does not, else
				 //it does
				 
	//Check if the diagonal runs from the bottom left to the top right of a cell
	diagBL = ( params->meshType == MESH_DIAG_BL2TR ||
		params->meshType == MESH_DIAG_BL2TR_Q ) ;
	
	//Set the index of the node on the fine grid to be zero initially
	fInd = 0 ;
	cRow = 0 ;
	
	cCols = cGrid->numCols ;
	fCols = fGrid->numCols ;
	//The variable fCols should store the number of unknown nodes in a row, not
	//the number of nodes, so we check for Dirichlet boundaries on the left and
	//right of the domain
	if( fGrid->bndType[0] == NODE_DIR_BNDRY ) fCols-- ;
	if( fGrid->bndType[2] == NODE_DIR_BNDRY ) fCols-- ;
	
	//We assume that the nodes are stored in row order from bottom left to top
	//right. We want to find out if the first row of unknowns in the fine grid
	//is on the coarse grid or not
	onCGrid = ( fGrid->bndType[1] != NODE_DIR_BNDRY ) ;
	lOffset = ( fGrid->bndType[0] == NODE_DIR_BNDRY ) ;
	
	//Find the appropriate coarse grid nodes from which to take nodal value
	//contributions for a fine grid node
	if( diagBL ){ //The case that the diagonal runs bottom left to top right
		cInds[0] = 0 ;
		cInds[1] = (!onCGrid) * cCols + lOffset ;
		addInd = !lOffset ;
	}
	else{ //The case that the diagonal runs bottom right to top left
		//Set cInds[0]
		cInds[0] = (!onCGrid && lOffset) ;
		//Set cInds[1]
		cInds[1] = (!onCGrid) ? cCols : lOffset ;
		//Set addInd
		addInd = (!onCGrid && lOffset) || (onCGrid && !lOffset) ;
	}
	
	
	//Perform operations row by row
	while( fInd < fGrid->numUnknowns ){
		//Loop through each node on the fine grid row
		for( i=0 ; i < fCols ; i++, fInd++ ){
			//Assign the value on the fine grid to be the average of the two
			//values on the coarse grid
			fFunc[ fInd ] = (cFunc[ cGrid->mapG2A[ cInds[0] ] ] +
				cFunc[ cGrid->mapG2A[ cInds[1] ] ]) / 2.0 ;
				
			//Increase the appropriate index on the coarse grid
			cInds[addInd] += 1 ;
			addInd = !addInd ;
		}
		//If the previous row was on the coarse grid, the next one is not
		onCGrid = !onCGrid ;
		//Increment the counter of the row on the coarse grid, if applicable
		if( onCGrid )
			cRow += 1 ;
			
		//Calculate the position on the coarse grid at which we should be for
		//the next row on the fine grid
		cInds[0] = cInds[1] = cRow * cCols ;
		if( diagBL ){
			cInds[1] += (!onCGrid) * cCols + lOffset ;
			addInd = !lOffset ;
		}
		else{ //The case that the diagonal runs bottom right to top left
			cInds[0] += (!onCGrid && lOffset) ;
			cInds[1] += (!onCGrid) ? cCols : lOffset ;
			addInd = (!onCGrid && lOffset) || (onCGrid && !lOffset) ;
		}	
	}
}

/** Returns the value of the Dirichlet boundary at the given node on a grid
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] node	The node on the grid for which to get the Dirichlet
 *						boundary value
 *	\return \p double representing the value of the Dirichlet boundary at the
 *			given node
 */
double get_dirichlet_boundary( const AlgorithmParameters *params,
	const Node *node )
{
	//We do not care if it is appropriate to return the value on the boundary
	//here, we just do it as requested and trust that the boundary value is
	//being retrieved only when it is required
		
	//Only test case 14 doesn't have a homogeneous Dirichlet boundary condition
	//at the moment
	if( params->testCase == 14 || params->testCase == 15 ||
		params->testCase == 25 || params->testCase == 27 )
		return params->reCtxt.hOut ;
	else if( params->testCase == 16 && node->x == 100.0 )
		return params->reCtxt.hOut ;
	else if( params->testCase == 16 && node->x == 0.0 ){
		if( params->tdCtxt.currTime >= 2.0 )
			return 0.0 ;
		
		return (2.0 - params->tdCtxt.currTime ) * params->reCtxt.hOut / 2 ;
	}
	else if( params->testCase == 18 || params->testCase == 26 ){
		return 0.0 ;
		//Here the Dirichlet boundary is time dependent
		//If the time is over one day, then simply return the saturated
		//condition
		
//		if( params->tdCtxt.currTime >= 1.0 )
//			return 0.0 ;
			
		//We return a value for the pressure at the boundary that changes
		//linearly with the time between the initial pressure and saturation
//		return (1.0-params->tdCtxt.currTime) * params->reCtxt.hOut ;
	}
	
	//Return zero by default
	return 0.0 ;
}

/** Sets the values in the part of the function vector allocated for Dirichlet
 *	nodes
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are currently operating
 *	\param [out] func	The function to populate with Dirichlet boundary data
 */
void set_dirichlet_boundary( const AlgorithmParameters *params,
	const GridParameters *grid, double *func )
{
	int i ; //Loop counter
	const Node *node ; //A node on the grid
	
	//The Dirichlet values are stored at the end of the vector
	for( i=grid->numUnknowns ; i < grid->numNodes ; i++ ){
		//Get the node on the Dirichlet boundary
		node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
		func[i] = get_dirichlet_boundary( params, node ) ;
	}
}

/** Returns the right hand side function multiplied by a basis function on a
 *	given element on a grid. The parameters follow the structure that is
 *	required for a function template to be passed to simpsons_rule() to be
 *	integrated numerically
 *	\param [in] nodes	The node at which a basis function is centered
 *	\param [in] elId	The ID of an element on the grid
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] nothing	Needed to fit into the function template to use in
 *						method <tt>simpsons_rule</tt>. Does nothing
 *	\param [in] x		The x coordinate at which to analyse the function
 *	\param [in] y		The y coordinate at which to analyse the function
 *	\return \p double value of the right hand side function multiplied by a
 *	basis function on a given element on a grid 
 */
double weak_rhs_function( const int *nodes, int elId,
	const GridParameters* grid, const AlgorithmParameters* params,
	const ApproxVars *aVars, const double* nothing, double x, double y )
{
	//To find the rhs I need the value of the function at the right of the
	//strong formulation multiplied by the basis function. I think that the
	//basis function is identified by 'node', so that I have all of the
	//information that I need
	
	//The rhs from the strong formulation is computed somewhere else, as is the
	//basis function value, so simply call these functions
	
	return rhs_function( params, aVars, x, y, elId ) * linear_bf( grid,
		nodes[0], &(grid->elements[ elId ]), x, y ) ;
}

/** Performs recovery of the gradient function for an approximation on a given
 *	grid
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to operate
 *	\param [in] approx	The function to take as the approximation to the
 *						solution on the current grid level
 *	\param [out] recGrads	The recovered values of the pointwise gradients
 */
void recover_gradients( const AlgorithmParameters* params, 
	const GridParameters* grid, const double* approx, CVector2D *recGrads )
{
	int i,j,k ; //Loop counters
	const Node *node ; //Placholder for a node on the grid
	
	const Element *e ; //Placeholder for the element we are currently working on
	double gradX ;//Stores the x-gradient of the functions on the triangle
	double gradY ;//Stores the y-gradient of the functions on the triangle
	double fVal ; //The value of the function at a given point
								
	//Loop through all of the nodes and make sure that all the elements have
	//been set to zero
	for( i=0 ; i < grid->numNodes ; i++ ){
		recGrads[i].x = recGrads[i].y = 0.0 ;
		recGrads[i].cnt = 0 ;
	}
	
	//First we need to loop through the elements
	for( i=0 ; i < grid->numElements ; i++ ){
		//Now loop through each of the nodes on the element
		e = &(grid->elements[i]) ;
		
		//Now I want to find the gradients of each of the functions defined on
		//the current triangle
		for( j=0 ; j < 3 ; j++ ){
			node = &(grid->nodes[ e->nodeIDs[j] ]) ;
			recGrads[ node->id ].cnt++ ;
			
			//We don't need to check if we are at a boundary node or not, as it
			//is assumed that the approximation contains the boundary data as
			//well
			fVal = approx[ grid->mapG2A[ node->id ]] ;
			
			//Get the value of the gradient at the given node. For this we don't
			//assume any regularity of the grid, but we do assume that the basis
			//is a linear one
			gradX = fVal * linear_bf_gradx( j, e, grid->nodes ) ;
			gradY = fVal * linear_bf_grady( j, e, grid->nodes ) ;
			
			//Now loop through the 	nodes on the triangle, and update the value
			//of the gradient
			for( k=0 ; k < 3 ; k++ ){
				recGrads[ e->nodeIDs[k] ].x += gradX ;
				recGrads[ e->nodeIDs[k] ].y += gradY ;
			}
		}
	}
	
	//Now that I have the appropriate values I want to take an average at all of
	//the nodes
	for( i=0; i < grid->numNodes ; i++ ){
		recGrads[i].x /= recGrads[i].cnt ;
		recGrads[i].y /= recGrads[i].cnt ;
	}
}

/** Calculates the residual on the given grid using the given approximation
 *	variables. This simply performs a matrix-vector multiplication, and assumes
 *	that all appropriate variables have been set
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to operate
 *	\param [in,out] aVars	Variables associated with the approximation on the
 *							grid. The member variable ApproxVars::resid is
 *							populated in this method
 */
void calculate_residual( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars* aVars ) 
{
	int i, j, k, nInd ;
	const Node *node ; //Placholder for a node on the grid
	double temp; //Stores the value of the operator applied to the approximation
	
	//For each node in the approximation
	if( params->currLevel == params->fGridLevel &&
		!is_richards_eq( params->testCase ) ){
		for ( i = 0; i < grid->numUnknowns ; i++ ){
			temp = 0.0;
			//Get a pointer to the current node on the grid
			node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
			//Calculate the values that go into the row for this node
			k = grid->rowStartInds[i] ;
			for ( j = 0; j < node->numNghbrs ; j++ ){
				//Get the index of the node on the grid for the neighbour
				nInd = node->nghbrs[j] ;
				//Multiply the appropriate entry in the iteration matrix by the
				//approximation for the given node
				temp += aVars->iterMat[k+j] *
					aVars->approx[ grid->mapG2A[nInd] ];
			}
			aVars->resid[i] = aVars->rhs[i] + aVars->bndTerm[i] - temp;
		}
	}
	else{
		for( i=0 ; i < grid->numUnknowns ; i++ ){
			temp = 0.0 ;
			//Get a pointer to the current node on the grid
			node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
			//Calculate the values that go into the row for this node
			k = grid->rowStartInds[i] ;
			for( j= 0 ; j < node->numNghbrs ; j++ ){
				//Get the index of the node on the grid for the neighbour
				nInd = node->nghbrs[j] ;
				//Multiply the appropriate entry in the iteration matrix by the
				//approximation for the given node
				temp += aVars->iterMat[k+j] *
					aVars->approx[ grid->mapG2A[nInd] ] ;
			}
			aVars->resid[i] = aVars->rhs[i] - temp ;
		}
	}
}

/** Updates the algorithm parameters using a key-value pair defined by
 *	\e paramName and \e paramValue
 *	\param [in,out] params	The parameters object to update
 *	\param [in] paramName	The string identifier for a parameter
 *	\param [in] paramValue	The value to assign to the given parameter field
 */
int update_params( AlgorithmParameters* params, char* paramName,
	char* paramValue )
{
	RichardsEqCtxt *ctxt ;
	ctxt = &(params->reCtxt) ;
	//Find the value to update in parameters
	if( strcmp( paramName, "TEST_CASE" ) == 0 ){
		params->testCase = atoi( paramValue ) ;
		set_test_case_specific_values( params ) ;
	}
	else if( strcmp( paramName, "NL_METHOD" ) == 0 )
		params->nlMethod = (enum NL_ITERATION_METHOD) atoi( paramValue ) ;
	else if( strcmp( paramName, "SMOOTH_TYPE" ) == 0 )
		params->smoothMethod = (enum SMOOTH_TYPE) atoi( paramValue ) ;
	else if( strcmp( paramName, "PRE_SMOOTH" ) == 0 )
		params->numPreSmooths = atoi( paramValue ) ;
	else if( strcmp( paramName, "POST_SMOOTH" ) == 0 )
		params->numPostSmooths = atoi( paramValue ) ;
	else if( strcmp( paramName, "SMOOTH_WEIGHT" ) == 0 )
		params->smoothWeight = atof( paramValue ) ;
	else if( strcmp( paramName, "NUM_MG_CYCLES" ) == 0 )
		params->numMgCycles = atoi( paramValue ) ;
	else if( strcmp( paramName, "FINEST_GRID" ) == 0 )
		params->fGridLevel = atoi( paramValue ) ;
	else if( strcmp( paramName, "COARSEST_GRID" ) == 0 )
		params->cGridLevel = atoi( paramValue ) ;
	else if( strcmp( paramName, "ALPHA" ) == 0 )
		params->alpha = atof( paramValue ) ;
	else if( strcmp( paramName, "COEFF_WIDTH" ) == 0 ){
		if( params->coeffReg.numRegions > 0 )
			assign_dvect_values( params->coeffReg.width, paramValue ) ;
	}
	else if( strcmp( paramName, "COEFF_HEIGHT" ) == 0 ){
		if( params->coeffReg.numRegions > 0 )
			assign_dvect_values( params->coeffReg.height, paramValue ) ;
	}
	else if( strcmp( paramName, "COEFF_VAL" ) == 0 ){
		if( params->coeffReg.numRegions > 0 )
			assign_dvect_values( params->coeffReg.val, paramValue ) ;
	}
	else if( strcmp( paramName, "COEFF_CORNER" ) == 0 ){
		if( params->coeffReg.numRegions > 0 )
			assign_coord_values( params->coeffReg.cornerPoints, paramValue ) ;
	}
	else if( strcmp( paramName, "SIGMA" ) == 0 )
		params->sigma = atof( paramValue ) ;
	else if( strcmp( paramName, "NEWTON_DAMP" ) == 0 )
		params->newtonDamp = atof( paramValue ) ;
	else if( strcmp( paramName, "SIMPLIFIED_NEWTON" ) == 0 )
		params->isSimplifiedNewt = atoi( paramValue ) == 0 ? false : true ;
	else if( strcmp( paramName, "DEBUG" ) == 0 )
		params->debug = atoi( paramValue ) ;
	else if( strcmp( paramName, "TESTING" ) == 0 )
		params->testing = atoi( paramValue ) ;
	else if( strcmp( paramName, "RECOVER_GRAD" ) == 0 )
		params->recoverGrads = atoi( paramValue ) ;
	else if( strcmp( paramName, "MAX_ITS" ) == 0 )
		params->maxIts = atoi( paramValue ) ;
	else if( strcmp( paramName, "EXACT_COARSE_SOLVE" ) == 0 )
		params->exactCoarseSolve = (atoi( paramValue ) == 0 ) ? false : true ;
	else if( strcmp( paramName, "FMG" ) == 0 )
		params->isFMG = (atoi( paramValue ) == 0) ? false : true ;
	else if( strcmp( paramName, "TIME_DISC") == 0 )
		params->tdCtxt.tDisc = (enum TIME_DISC_TYPE) atoi( paramValue ) ;
	else if( strcmp( paramName, "T_START") == 0 )
		params->tdCtxt.tStart = atof( paramValue ) ;
	else if( strcmp( paramName, "T_STEP_SIZE" ) == 0 )
		params->tdCtxt.tStepSize = atof( paramValue ) ;
	else if( strcmp( paramName, "T_STEPS" ) == 0 )
		params->tdCtxt.numTSteps = atoi( paramValue ) ;
	else if( strcmp( paramName, "ADAPT_T_STEP" ) == 0 )
		params->tdCtxt.adaptTStep = (atoi( paramValue ) == 0) ? false : true ;
	else if( strcmp( paramName, "LUMP_MASS_MAT" ) == 0 )
		params->lumpMassMat = (atoi( paramValue ) == 0 ) ? false : true ;
	else if( strcmp( paramName, "MAX_T_STEP" ) == 0 )
		params->tdCtxt.maxTStep = atof( paramValue ) ;
	else if( strcmp( paramName, "MIN_T_STEP" ) == 0 )
		params->tdCtxt.minTStep = atof( paramValue ) ;
	else if( strcmp( paramName, "INNER_ITS" ) == 0 )
		params->innerIts = atoi( paramValue ) ;
	else if( strcmp( paramName, "NEWTON_INNER_IT" ) == 0 )
		params->innerIt = (enum INNER_IT_TYPE)atoi( paramValue ) ;
	else if( strcmp( paramName, "MIN_NEWT_FACT" ) == 0 )
		params->minNewtFact = atof( paramValue ) ;
	else if( strcmp( paramName, "MAX_NEWT_FACT" ) == 0 )
		params->maxNewtFact = atof( paramValue ) ;
	else if( strcmp( paramName, "GMRES_RESET" ) == 0 )
		params->gmresReset = atoi( paramValue ) ;
	else if( strcmp( paramName, "PREC_SYMM_PART" ) == 0 )
		params->gmresPrecSPart = atoi( paramValue ) ;
	else if( strcmp( paramName, "MESH_TYPE" ) == 0 )
		params->meshType = (enum MESH_TYPE) atoi( paramValue ) ;
	else if( strcmp( paramName, "SOL_X_FREQ" ) == 0 )
		params->solXFreq = atof( paramValue ) ;
	else if( strcmp( paramName, "SOL_Y_FREQ" ) == 0 )
		params->solYFreq = atof( paramValue ) ;
	else if( strcmp( paramName, "SOL_SMOOTH_AMP" ) == 0 )
		params->solSmoothAmp = atof( paramValue ) ;
	else if( strcmp( paramName, "SOL_OSC_AMP" ) == 0 )
		params->solOscAmp = atof( paramValue ) ;
	else if( strcmp( paramName, "P_LAPLACE_EXP" ) == 0 )
		params->plExp = atof( paramValue ) ;
	else if( strcmp( paramName, "INIT_APPROX_FILE" ) == 0 ){
		if( paramValue != NULL )
			sprintf( params->initApproxFile, "%s", paramValue ) ;
		else
			sprintf( params->initApproxFile, "%s", "" ) ;
	}
	else if( strcmp( paramName, "READ_FMG_SOL" ) == 0 )
		params->readFMGSol = atoi( paramValue ) ;
	else if( strcmp( paramName, "FMG_PRE_SMOOTH" ) == 0 )
		params->fmgPreSmooths = atoi( paramValue ) ;
	else if( strcmp( paramName, "FMG_POST_SMOOTH" ) == 0 )
		params->fmgPostSmooths = atoi( paramValue ) ;
	else if( strcmp( paramName, "ADAPT_UPDATE" ) == 0 )
		params->adaptUpdate = atoi( paramValue ) ;
	else if( strcmp( paramName, "POW_U" ) == 0 )
		params->powU = atoi( paramValue ) ;
	else if( strcmp( paramName, "INIT_RAD" ) == 0 )
		params->initRad = atof( paramValue ) ;
	else if( strcmp( paramName, "NUM_SOILS" ) == 0 ){
		ctxt->numSoils = atoi( paramValue ) ;
		assign_re_ctxt_mem( ctxt ) ;
	}
	else if( strcmp( paramName, "RE_USE_CELIA" ) == 0 ){
		ctxt->useCelia = atoi( paramValue ) == 0 ? false : true ;
	}
	else if( strcmp( paramName, "THETA_S" ) == 0 ){
		if( ctxt->numSoils == 0 ){
			printf( "Number of soils has not been specified.\n" ) ;
			return 0 ;
		}
		else
			assign_dvect_values( ctxt->thetaS, paramValue ) ;
	}
	else if( strcmp( paramName, "THETA_R" ) == 0 ){
		if( ctxt->numSoils == 0 ){
			printf( "Number of soils has not been specified.\n" ) ;
			return 0 ;
		}
		else
			assign_dvect_values( ctxt->thetaR, paramValue ) ;
	}
	else if( strcmp( paramName, "SAT_COND" ) == 0 ){
		if( ctxt->numSoils == 0 ){
			printf( "Number of soils has not been specified.\n" ) ;
			return 0 ;
		}
		else
			assign_dvect_values( ctxt->satCond, paramValue ) ;
	}
	else if( strcmp( paramName, "RE_ALPHA" ) == 0 ){
		if( ctxt->numSoils == 0 ){
			printf( "Number of soils has not been specified.\n" ) ;
			return 0 ;
		}
		else
			assign_dvect_values( ctxt->alpha, paramValue ) ;
	}
	else if( strcmp( paramName, "RE_N" ) == 0 ){
		if( ctxt->numSoils == 0 ){
			printf( "Number of soils has not been specified.\n" ) ;
			return 0 ;
		}
		else
			assign_dvect_values( ctxt->n, paramValue ) ;
	}
	else if( strcmp( paramName, "RE_FLOW_IN" ) == 0 )
		ctxt->flowIn = atof( paramValue ) ;
	else if( strcmp( paramName, "RE_FLOW_OUT" ) == 0 )
		ctxt->flowOut = atof( paramValue ) ;
	else if( strcmp( paramName, "RE_PRESS_OUT" ) == 0 )
		ctxt->hOut = atof( paramValue ) ;
	else if( strcmp( paramName, "H_S" ) == 0 ){
		if( ctxt->numSoils == 0 )
			printf( "Number of soils has not been specified.\n" ) ;
		else
			assign_dvect_values( ctxt->hS, paramValue ) ;
	}
	
	//Return success if we get to here
	return 1 ;
}

/** Sets values of parameters that are specific to certain test cases
 *	\param [in,out] params	Parameters defining how the algorithm is to be
 *							executed
 */						
void set_test_case_specific_values( AlgorithmParameters *params )
{
	if( params->testCase >= 53 && params->testCase <= 58 ){
		CoeffRegions *regs = &(params->coeffReg) ;
		//Set the number of regions of set coefficient that are defined on the
		//domain
		regs->numRegions = 4 ;
			
		//Set the memory for the arrays in this region
		regs->cornerPoints = (Vector2D*)calloc( regs->numRegions,
			sizeof( Vector2D ) ) ;
		regs->width = (double*)calloc( regs->numRegions, sizeof( double ) ) ;
		regs->height = (double*)calloc( regs->numRegions, sizeof( double ) ) ;
		regs->val = (double*)calloc( regs->numRegions, sizeof( double ) ) ;
	}
}

/** Assigns values from a comma separated list (no white spaces allowed) to the
 *	elements of a double vector passed in. The values between the commas must
 *	be double values, and no checks are made here that they are. It is assumed
 *	that the end of the string is indicated by a newline character
 *	\param [out] vect	The vector to populate with values
 *	\param [in] values	A comma separated string of double values
 */
void assign_dvect_values( double *vect, char *values )
{
	char *val ; //The string value read from the list of comma separated values
	int i ; //Loop counter
	
	//Split the string at the first occurrence of either a comma or a new line
	val = strtok( values, ",\n" ) ;
	
	i=0 ;
	//Check that a value has been read in, and assign this to the given vector
	while( val != NULL ){
		vect[i++] = atof( val ) ;
		val = strtok( NULL, ",\n" ) ;
	}
}

/** Assigns values from a comma separated list (it is assumed that there are no
 *	white spaces) to the elements of a vector passed in. The values between the
 *	commas are assumed to be double values. The end of the string is assumed to
 *	be delimited by a newline character
 *	\param [out] coords 	The vector of coordinates to populate
 *	\param [in] values	A comma separated string of double values, which come in
 *						pairs (i.e. \f$(x,y)\f$ pairs
 */
void assign_coord_values( Vector2D *coords, char *values )
{
	char *val ; //The string value read from the list of comma separated values
	int i ; //Loop counter
	
	//Split the string at the first occurrence of either a comma or a new line
	val = strtok( values, ",\n" ) ;
	
	i=0 ;
	//Check that a value has been read in, and assign this as the x-value for
	//the coordinate
	while( val != NULL ){
		coords[i].x = atof( val ) ;
		//Assume that if we have read in an x-coordinate that the corresponding
		//y-coordinate exists, and save this as well
		val = strtok( NULL, ",\n" ) ;
		coords[i++].y = atof( val ) ;
		//Read in the x-value of the next coordinate, if it exists
		val = strtok( NULL, ",\n" ) ;
	}
}

/** Validates the parameters object
 *	\param [in] params	The parameters to check for validity
 *	\return \p integer value indicating whether the parameters are valid or not.
 *			A value 0 indicates that the parameters are not valid, else they are
 */
int validate_alg_params( const AlgorithmParameters* params )
{
	if( params->cGridLevel > params->fGridLevel ){
		printf( "Given coarse grid is not coarser than the fine grid\n" ) ;
		return 0 ;
	}
	
	if( params->testCase == 14 || params->testCase == 16 ||
		params->testCase == 17 || params->testCase == 19 ){
		if( params->reCtxt.numSoils != 1 ){
			printf( "Only one soil type is valid for test case %d\n",
				params->testCase ) ;
			return 0 ;
		}
	}
	else if( params->testCase == 15 || params->testCase == 18 ||
		params->testCase == 20 ){
		if( params->reCtxt.numSoils != 3 ){
			printf( "Three soil types must be specified for test case %d\n",
				params->testCase ) ;
			return 0 ;
		}
		if( params->cGridLevel < 3 ){
			printf( "Coarsest grid must be at least 8x8 for test case %d\n",
				params->testCase ) ;
			return 0 ;
		}
	}
	else if( params->testCase == 21 || params->testCase == 22 ||
		params->testCase == 23 || params->testCase == 24 ||
		params->testCase == 25 || params->testCase == 26 ||
		params->testCase == 27 ){
		if( params->reCtxt.numSoils != 2 ){
			printf( "Two soil types must be specified for test case %d\n",
				params->testCase ) ;
			return 0 ;
		}
		if( params->cGridLevel < 3 ){
			printf( "Coarsest grid must be at least 8x8 for test case %d\n",
				params->testCase ) ;
			return 0 ;
		}
	}
	else if( params->testCase == 31 ){
		if( params->fGridLevel < 5 ){
			printf( "Finest grid must be at least 64x64 for test case %d\n",
				params->testCase ) ;
			return 0 ;
		}
	}
	
	//If there are no checks to be made return a success value
	return 1 ;
}

/** Prints out the values of the member variables of the parameters structure to
 *	a file
 *	\param [in] filename	The name of the file to write output to
 *	\param [in] params		The parameters to print out
 */
void print_algorithm_parameters_to_file( const char* filename,
	const AlgorithmParameters* params )
{
	FILE* fout ;
	const TDCtxt *tdCtxt ;
	
	//Open the file for writing
	fout = fopen( filename, "w" ) ;
	
	if( fout == NULL )
		return ;
		
	tdCtxt = &(params->tdCtxt) ;
	
	//File was opened correctly, so print out the member variables
	fprintf( fout, "Parameters used to run the algorithm:\n\t" ) ;
	fprintf( fout, "Test case: %d\n\t", params->testCase ) ;
	fprintf( fout, "Is FMG: " ) ;
	if( params->isFMG )
		fprintf( fout, "Yes\n\t" ) ;
	else
		fprintf( fout, "No\n\t" ) ;
	fprintf( fout, "Number of MG cycles: %d\n\t", params->numMgCycles ) ;
	fprintf( fout, "Finest grid level in the hierarchy: %d\n\t",
		params->fGridLevel ) ;
	fprintf( fout, "Coarsest grid level in the hierarchy: %d\n\t",
		params->cGridLevel ) ;
	fprintf( fout, "Smoothing iteration: " ) ;
	if( params->smoothMethod == SMOOTH_GS )
		fprintf( fout, "Gauss-Seidel\n\t" ) ;
	else if( params->smoothMethod == SMOOTH_JAC )
		fprintf( fout, "Jacobi\n\t" ) ;
	else if( params->smoothMethod == SMOOTH_BLOCK_JAC_SINGLE )
		fprintf( fout, "Block Jacobi with pointwise update\n" ) ;
	else if( params->smoothMethod == SMOOTH_BLOCK_JAC_AVG )
		fprintf( fout, "Block Jacobi with equal updates from neighbours\n" ) ;
	else if( params->smoothMethod == SMOOTH_BLOCK_JAC_DWEIGHT ){
		fprintf( fout, "Block Jacobi with centre node given double " ) ;
		fprintf( fout, "weighting\n" ) ;
	}
	else
		fprintf( fout, "Undefined\n\t" ) ;
	fprintf( fout, "Number of pre-smoothing iterations: %d\n\t",
		params->numPreSmooths ) ;
	fprintf( fout, "Number of post-smoothing iterations: %d\n\t",
		params->numPostSmooths ) ;
	fprintf( fout, "Weighting factor used in the smoothing: %.5f\n\t",
		params->smoothWeight ) ;
	fprintf( fout, "Maximum number of iterations to perform: %d\n\t",
		params->maxIts ) ;
	fprintf( fout, "Alpha: %.5f\n\t", params->alpha ) ;
	fprintf( fout, "Sigma: %.5f\n\t", params->sigma ) ;
	fprintf( fout, "Damping for a Newton step: %.1f\n\t", params->newtonDamp ) ;
	fprintf( fout, "Number of multigrid iterations per Newton step: %d\n\t",
		params->innerIts ) ;
	if( strlen( params->initApproxFile ) > 0 ){
		fprintf( fout, "File from which to read the initial" ) ;
		fprintf( fout, " approximation: %s\n\t",
			params->initApproxFile ) ;
	}
	fprintf( fout, "Time discretization method: " ) ;
	if( tdCtxt->tDisc == TIME_DISC_BE )
		fprintf( fout, "Backward Euler\n\t" ) ;
	fprintf( fout, "Start time: %.3f\n\t", tdCtxt->tStart ) ;
	fprintf( fout, "Time step size: %.3f\n\t", tdCtxt->tStepSize ) ;
	fprintf( fout, "Number of time steps: %d\n\t", tdCtxt->numTSteps ) ;
	if( params->testCase == 6 || params->testCase == 7){
		fprintf( fout, "Amplitude of solution smooth sin component: %.5lf",
			params->solSmoothAmp ) ;
		fprintf( fout, "Amplitdue of solution oscillatory sin component: %.5lf",
			params->solOscAmp ) ;
		fprintf( fout, "Frequency of solution sin component in " ) ;
		fprintf( fout, "x-direction: %.2f\n\t", params->solXFreq ) ;
		fprintf( fout, "Frequency of solution sin component in " ) ;
		fprintf( fout, "y-direction: %.2f\n\t", params->solYFreq ) ;
	}
	fprintf( fout, "Recovering gradients: " ) ;
	if( params->recoverGrads ) fprintf( fout, "True\n\t" ) ;
	else fprintf( fout, "False\n\t" ) ;
	fprintf( fout, "Debugging: " ) ;
	if( params->debug ) fprintf( fout, "True\n\t" ) ;
	else fprintf( fout, "False\n\t" ) ;
	
	fprintf( fout, "Testing: " ) ;
	if( params->testing ) fprintf( fout, "True\n" ) ;
	else fprintf( fout, "False\n" ) ;
	
	//Clean up
	fclose( fout ) ;
}

/** Reads in the parameters required for the algorithm to run from a file
 *	\param [out] params	The parameters to populate
 *	\param [in] fName	The name of the file to read the parameters in from
 *	\return \p int indicating success or failure to read. Zero indicates failure
 *			else success
 */
int read_alg_params( AlgorithmParameters* params, const char* fName )
{
	FILE* paramsFile ;
	size_t strlen ; //The maximum length of a line to be read in from file
	char* line ; //Stores a line read in from file
	char* paramName ; //The name of the parameter to update
	char* paramValue ; //The string representation of the parameter value to
					   //assign
	TDCtxt *tdCtxt ;
	
	//Open the file for reading
	paramsFile = fopen( fName, "r" ) ;
	tdCtxt = &(params->tdCtxt) ;
	
	initialise_re_ctxt_to_null( &(params->reCtxt) ) ;
	initialise_coeff_regs_to_null( &(params->coeffReg) ) ;
	
	//If the file has not been opened properly return
	if( paramsFile == NULL ){
		printf( "Error opening parameters file %s\n", fName ) ;
		return 0 ;
	}
		
	strlen = 100 ;
	line = (char*) calloc( strlen, sizeof( char ) ) ;
	
	//The 'setUpApprox' variable is not read in, and so needs to be initialised
	//explicitly
	params->setUpApprox = 1 ;
	
	//The pre and post-smoothing iterations during the FMG cycle may not be 
	//specified, so we set them here
	params->fmgPreSmooths = 0 ;
	params->fmgPostSmooths = 0 ;
	
	//The file has been opened correctly, so read from here
	//Read a line from the file while we are not at the end of the file
	while( !feof( paramsFile ) ){
		fgets( line, strlen * sizeof(char), paramsFile ) ;

		//Check that the line doesn't start with a hash, and that the
		//line is not empty
		if( *(line) != '#' && *(line) != '\n' ){
			//Split the string about the semi-colon or a newline
			paramName = strtok( line, ":\n" ) ;
			paramValue = strtok( NULL, ":\n" ) ;

			//If we have been able to read both a parameter name
			//and a value then update the parameters struct
			if( paramName != NULL ){
				if( !update_params( params, paramName, paramValue ) )
					return 0 ;
			}
		}
	}
	
	//Clean up unwanted variables
	free( line ) ;
	fclose( paramsFile ) ;
	
	//Set the current level to be the finest level
	params->currLevel = params->fGridLevel ;
	
	if( params->fmgPreSmooths == 0 )
		params->fmgPreSmooths = params->numPreSmooths ;
	if( params->fmgPostSmooths == 0 )
		params->fmgPostSmooths = params->numPostSmooths ;
	
	//Now check whether all of the entries are valid, for example at the moment
	//we only recover the gradient for some test cases, and not at all when
	//performing Newton multigrid
	params->recoverGrads = ( params->recoverGrads &&
		can_recover_grads( params->testCase ) &&
		params->nlMethod != METHOD_NEWTON_MG ) ;

	//We only perform block smoothing for nonlinear smooths, therefore if we
	//are performing Newton Multigrid we also set the smoother to be the
	//Gauss-Seidel smoother
	if( is_block_jacobi_smooth( params ) &&
		//( params->nlMethod == METHOD_NEWTON_MG ||
		//params->testCase == 1 ||
		(params->recoverGrads) )
		params->smoothMethod = SMOOTH_GS ;
		
	if( params->testCase == 10 || params->testCase == 12 )
		tdCtxt->tStart = calculate_t_zero( params ) ;
		
	if( is_time_dependent( params->testCase ) ){
		if( tdCtxt->tDisc == TIME_DISC_BE )
			tdCtxt->weightFact = tdCtxt->tStepSize ;
		else if( tdCtxt->tDisc == TIME_DISC_TRAP )
			tdCtxt->weightFact = tdCtxt->tStepSize / 2.0 ;
	}
	
	//Check whether the parameters are set up so that it makes sense to
	//precondition the symmetric part of the derivative only. This requires that
	//we are performing a Newton preconditioned GMRES iteration, and for the
	//problem to have a symmetric part of the operator
	if( params->gmresPrecSPart != 0 ){
		if( !has_spart( params ) || params->nlMethod != METHOD_NEWTON_MG ||
			params->innerIt != IT_PCGMRES )
			params->gmresPrecSPart = 0 ;
	}
	
	//Check whether the problem is a Richards equation problem or not
	if( is_richards_eq( params->testCase ) ){
		//If we are performing nonlinear multigrid make sure that we are using
		//a nonlinear Jacobi iteration only. We don't consider the case of the
		//block smoothing at the moment
		if( params->nlMethod == METHOD_NLMG )
			params->smoothMethod = SMOOTH_JAC ;
		set_up_re_ctxt( &(params->reCtxt) ) ;
	}
	
	if( params->testCase >= 53 && params->testCase <= 58 ){
		//Update the number of coefficient regions that have been defined
		int i=0 ;
		while( i < params->coeffReg.numRegions &&
			params->coeffReg.width[i] > 0.0 )
			i++ ;
		params->coeffReg.numRegions = i ;
	}
	
	//Everything went well, so return 1
	return 1 ;
}

/** Prints a function discretized on a grid to file, without the boundary term
 *	\param [in] filename	The name of the file to print the function out to
 *	\param [in] grid	The grid on which the function is discretized
 *	\param [in] function	The function to print out to file
 *	\param [in] append	Indicates whether to append to the file or not. A value
 *						zero indicates that the file should be overwritten, else
 *						the file is appended to
 */
void print_grid_func_no_bnd_to_file( const char* filename,
	const GridParameters *grid, const double* function, int append )
{
	FILE* fout ;
	int cnt, index ;
	
	//Open the file for appending or writing, whichever was specified
	if( append ){
		fout = fopen( filename, "a" ) ;
		fprintf( fout, "-1,-1,-1\n" ) ;
	}
	else
		fout = fopen( filename, "w" ) ;
	
	//Check that we have opened the file
	if( fout == NULL )
		return ;
		
	//For all the interior nodes I want to print out the coordinate and the
	//corresponding value
	for( cnt=0 ; cnt < grid->numUnknowns ; cnt++ ){
		index = grid->mapA2G[cnt] ;
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", grid->nodes[index].x,
			grid->nodes[index].y, function[cnt] ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Prints a discretized function, and the Dirichlet boundary data to file. This
 *	assumes that the input function stores both the boundary and non-boundary
 *	data, and the map between nodes on the grid and nodal values is given in
 *	the grid object
 *	\param [in] filename	Name of the file to print output to
 *	\param [in] grid	The grid on which the function is discretized
 *	\param [in] function	The function discretized on the grid, which is to
 *							be written to file
 *	\param [in] append	Indicator as to whether or not the file should be
 *						appended to or not. If not the file is overwritten. A
 *						value zero indicates that the file should be overwritten
 *						else it is appended to
 */
void print_grid_func_to_file( const char *filename,
	const GridParameters *grid, const double *function, int append )
{
	int i ; //Loop counter
	int ind ; //The index mapping from the grid points to the nodal function
			  //values
	const Node *node ; //A node on the grid
	FILE *fout ; //The file to print the output to
	
	//Open the file for appending or writing, whichever was specified
	if( append ){
		fout = fopen( filename, "a" ) ;
		fprintf( fout, "-1,-1,-1\n" ) ;
	}
	else
		fout = fopen( filename, "w" ) ;
		
	//Loop through all of the grid points
	for( i=0 ; i < grid->numNodes ; i++ ){
		ind = grid->mapG2A[i] ;
		node = &(grid->nodes[i]) ;
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", node->x, node->y,
			function[ind] ) ;
	}
	
	//Clean up
	fclose( fout ) ;
}

/** Prints a grid function to file in Paraview format. The function also needs
 *	to be defined on the boundary of the domain
 *	\param [in] filename	The filename to write output to
 *	\param [in] grid		The grid on which the function is defined
 *	\param [in] function	The function to write to file
 */
void print_grid_func_paraview_format( const char* filename,
	const GridParameters *grid, const double *function )
{
	int i ; //Loop counter
	FILE *fout ; //File to print output to
	
	char header[256] ; //Header containing information about the data to be
					   //written to the vtk file

	//Set the text in the header
	sprintf( header, "Point data representing a discrete function on a grid" ) ;
	
	//Print the grid at the start of the file
	print_grid_paraview_format( filename, header, grid ) ;
	
	//Open the file for appending
	fout = fopen( filename, "a" ) ;
	
	fprintf( fout, "\n" ) ;
	fprintf( fout, "POINT_DATA %d\n", grid->numNodes ) ;
	fprintf( fout, "SCALARS approx double 1\n" ) ;
	fprintf( fout, "LOOKUP_TABLE default\n" ) ;
	//Print out the approximation values at each grid point
	for( i=0 ; i < grid->numNodes ; i++ ){
		fprintf( fout, "%.18lf\n",
			function[ grid->mapG2A[ i ] ] ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Prints a vector function to file in Paraview format. The function also needs
 *	to be defined on the boundary of the domain
 *	\param [in] filename	The filename to write output to
 *	\param [in] grid		The grid on which the function is defined
 *	\param [in] vectFunc	The vector function to write to file
 */
void print_vect_field_paraview_format( const char* filename,
	const GridParameters *grid, const Vector2D *vectorFunc )
{
	int i ; //Loop counter
	FILE *fout ; //File to print output to
	
	const Vector2D *vect ;
	
	char header[256] ;
	
	//Write something into the header for the vtk file
	sprintf( header, "Vector field representing the flux on a grid" ) ;
	
	//Print the grid at the start of the file
	print_grid_paraview_format( filename, header, grid ) ;
	
	//Open the file for appending
	fout = fopen( filename, "a" ) ;
	
	fprintf( fout, "\n" ) ;
	fprintf( fout, "POINT_DATA %d\n", grid->numNodes ) ;
	fprintf( fout, "VECTORS flux double\n" ) ;
	//fprintf( fout, "LOOKUP_TABLE default\n" ) ;
	//Print out the approximation values at each grid point
	for( i=0 ; i < grid->numNodes ; i++ ){
		vect = &(vectorFunc[ grid->mapG2A[ i ] ]) ;
		fprintf( fout, "%.18lf %.18lf 0.0\n", vect->x, vect->y ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Prints a piecewise constant function on cells on a grid to file in a format
 *	to be read by Paraview
 *	\param [in] filename	The name of the file to print the function to
 *	\param [in] grid	The grid on which the function is discretized
 *	\param [in] cellFunc	The pieceiwse constant function on the cells of the
 *							given grid
 */
void print_cell_func_paraview_format( const char *filename,
	const GridParameters *grid, const double *cellFunc )
{
	int i ; //Loop counter
	FILE *fout ; //File to write output to
	
	char header[256] ;
	
	sprintf( header, "Piecewise constant cell data" ) ;
	
	//Print the grid at the start of the file
	print_grid_paraview_format( filename, header, grid ) ;
	
	//Open the file for appending
	fout = fopen( filename, "a" ) ;
	
	fprintf( fout, "\n" ) ;
	fprintf( fout, "CELL_DATA %d\n", grid->numElements ) ;
	fprintf( fout, "SCALARS cell_scalars double 1\n" ) ;
	fprintf( fout, "LOOKUP_TABLE default\n" ) ;
	
	//Print out the values of the function on each cell
	for( i=0 ; i < grid->numElements ; i++ )
		fprintf( fout, "%.18lf\n", cellFunc[i] ) ;
	
	//Close the file
	fclose( fout ) ;
}

/** Prints out a grid in paraview format to a file given by the filename. It is
 *	assumed that the file is to be created / overwritten and that this will be
 *	the first bit of information in the file
 *	\param [in] filename	The name of the file to print the grid data to
 *	\param [in] header	A header (maximum 256 characters) to describe the
 *						data to be written to the file
 *	\param [in] grid	The grid to print out to file
 */
void print_grid_paraview_format( const char *filename, const char *header,
	const GridParameters *grid )
{
	int i ; //Loop counter
	const Element *e ; //An element on the grid
	const Node *node ; //A node on the grid
	FILE *fout ; //File to write output to

	//Open the file for writing
	fout = fopen( filename, "w" ) ;
	
	//Print the head for the vtk format file
	fprintf( fout, "# vtk DataFile Version 4.2\n" ) ;
	fprintf( fout, "%s\n", header ) ;
	fprintf( fout, "ASCII\n" ) ;
	//Start the definition of the dataset
	fprintf( fout, "DATASET UNSTRUCTURED_GRID\n" ) ;
	//Print out the information for the points on the grid
	fprintf( fout, "POINTS %d double\n", grid->numNodes ) ;
	for( i=0 ; i < grid->numNodes ; i++ ){
		node = &(grid->nodes[i]) ;
		fprintf( fout, "%.18lf %.18lf 0.0\n", node->x, node->y ) ;
	}
	
	fprintf( fout, "\n" ) ;
	//Print the information for the triangles on the mesh
	fprintf( fout, "CELLS %d %d\n", grid->numElements,
		grid->numElements * 4 ) ;
	for( i=0 ; i < grid->numElements ; i++ ){
		e = &(grid->elements[i]) ;
		fprintf( fout, "3 %d %d %d\n", e->nodeIDs[0], e->nodeIDs[1],
			e->nodeIDs[2] ) ;
	}
	
	fprintf( fout, "\n" ) ;
	//Print the type of polygon we are dealing with for each of the elements
	//on the grid. In my case they are all triangles, identified by a '5' in
	//vtk data format
	fprintf( fout, "CELL_TYPES %d\n", grid->numElements ) ;
	for( i=0 ; i < grid->numElements ; i++ )
		fprintf( fout, "5\n" ) ;
	
	//Close the file
	fclose( fout ) ;
}

/** Prints function values to file, which are weighted by a given constant
 *	factor
 *	\param [in] filename	The name of the file to print output to
 *	\param [in] grid	The grid on which we are currently operating
 *	\param [in] function	The function to print out to file
 *	\param [in] weightFact	The weighting factor to multiply the elements of
 *							the function by before writing to file
 *	\param [in] append	Indicator as to whether to append to an existing file
 *						or to overwrite an existing file
 */
void print_weighted_grid_func_to_file( const char *filename,
	const GridParameters *grid, const double *function, double weightFact,
	int append )
{
	int i ; //Loop counter
	int ind ; //The index mapping from the grid points to the nodal funcion
			  //values
	const Node *node ; //A node on the grid
	FILE *fout ; //The file to print the output to
	
	
	//Open the file for appending or writing, depending on which was specified
	if( append ){
		fout = fopen( filename, "a" ) ;
		fprintf( fout, "-1,-1,-1\n" ) ;
	}
	else
		fout = fopen( filename, "w" ) ;
		
	//Loop through all of the grid points
	for( i=0 ; i < grid->numNodes ; i++ ){
		ind = grid->mapG2A[i] ;
		node = &(grid->nodes[i]) ;
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", node->x, node->y,
			weightFact * function[ind] ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Writes a two dimensional vector field to the file pointed to by \e filename.
 *	For each unknown node on the grid we write a row to file in the format
 *	<em>[x-coord, y-coord, x-value, y-value]</em>.
 *	\param [in] filename	The name of the file to write output to
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] vector	The vector field to write to file
 *	\param [in] append	Indicates whether the vector field should be appended to
 *						the file or whether previous content should be
 *						overwritten
 */
void print_vector_field_to_file( const char *filename,
	const GridParameters *grid, const Vector2D *vector, int append )
{
	FILE *fout ; //The file to write the output to
	int cnt, index ;
	//Open the file for writing
	if( append ){
		fout = fopen( filename, "a" ) ;
		fprintf( fout, "-1,-1,-1,-1\n" ) ;
	}
	else
		fout = fopen( filename, "w" ) ;
		
	if( fout == NULL )
		return ;
		
	//For all the interior nodes I want to print out the coordinate and the
	//corresponding values in the vector
	for( cnt=0 ; cnt < grid->numUnknowns ; cnt++ ){
		index = grid->mapA2G[cnt] ;
		fprintf( fout, "%.18lf, %.18lf, %.18lf, %1.8lf\n", grid->nodes[index].x,
			grid->nodes[index].y, vector[cnt].x, vector[cnt].y ) ;
	}
	
	//Close the file
	fclose( fout ) ;
}

/** Reads a function from file. It is assumed that the file being read has been
 *	written by the function print_grid_function_to_file() and that the function
 *	passed in has enough space to hold all the information in the file. Only the
 *	function values are read from the file
 *	\param [in] filename	The filename of the file to read in the function
 *							values from
 *	\param [out] func	The grid function to populate with the values read in
 */
void read_grid_function_from_file( const char *filename,
	const GridParameters *grid, double *func )
{
	FILE *fin ;
	int cnt ; //A counter
	double x, y ; //Needed to store the x- and y-coordinates read in from file,
				  //but not actually used
	
	cnt = 0 ;
	//Open the file for reading
	fin = fopen( filename, "r" ) ;
	
	if( fin == NULL ){
		printf( "Unable to open file %s.\nExiting ...\n",
			filename ) ;
		exit( 0 ) ;
	}
	
	//For every line in the file read in the coordinate and the function value
	while( fscanf( fin, "%lf, %lf, %lf", &x, &y,
		&(func[ grid->mapG2A[cnt++] ]) ) != EOF ) ;
	
	//Close the file
	fclose( fin ) ;
}

/** Checks to see if there is a connection between two nodes on the grid
 *	\param [in] n1	First node on the grid. The neighbourhood of this node is
 *					searched to find \e n2
 *	\param [in] n2	Second node on the grid
 *	\return \p int value of the index into the neighbourhood of \e n1 of node
 *			\e n2, if there is a connection. Returns -1 if there is no
 *			connection
 */
int connected( const Node *n1, const Node *n2 )
{
	int i ; //Loop counter
	
	//Loop through the neighbours for the current node, and update return the
	//index into the local array of neighbours
	for( i=0 ; i < n1->numNghbrs ; i++ )
		if( n2->id == n1->nghbrs[i] ) return i ;
		
	//Loop through the Dirichlet boundary nodes. We return a value of -2 if the
	//node is connected on the boundary
	for( i=0 ; i < n1->numDirNghbrs ; i++ )
		if( n2->id == n1->nghbrs[ SPARSE-1-i ] ) return -2 ;
		
	//If the connection is not found, then return -1
	return -1 ;
}

/*
 * Points the pointer passed in to the appropriate list of meshes to read from.
 * This allows for different meshes to be used in the experiments
 *
 * INPUT:
 *	const AlgorithmParameters *params: Parameters defining how the algorithm is
 *		to be executed
 *	const char *meshes[]: The pointer to set to the appropriate list of grids
 *
 * OUTPUT:
 *	None. The pointer to the meshes is updated in this method
 */
/** Points the pointer passed in to the appropriate list of meshes to read from.
 *	This allows for different meshes to be used in the experiments
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\return A pointer to a global list of grids, depending on the parameters
 *			passed in
 */
const char** get_grid_hierarchy( const AlgorithmParameters *params )
{
	if( params->meshType == MESH_DIAG_BL2TR ){
//		if( params->testCase == 10 || params->testCase == 11 ||
//			params->testCase == 12 )
//			return exMeshesBL2TR ;
//		else
			return meshesBL2TR ;
	}
	else if( params->meshType == MESH_DIAG_BL2TR_Q )
		return meshesBL2TR_Q ;
	else if( params->meshType == MESH_DIAG_BR2TL )
		return meshesBR2TL ;
	else if( params->meshType == MESH_DIAG_BR2TL_Q )
		return meshesBR2TL_Q ;
	else
		return NULL ;
}

/** Perform a sparse matrix multiply of a matrix stored with sparsity pattern
 *	stored in the grid structure passed in
 *	\param [in] grid	The grid on which the approximation is
 *	\param [in] vec		The vector to multiply the matrix with
 *	\param [in] mat		The matrix to multiply, stored in sparse row format
 *	\param [out] result	The result of the sparse matrix multiplication
 */
void sparse_mat_mult( const GridParameters *grid,
	const double *vec, const double *mat, double *result )
{
	int i, j, k, rInd ;
	const Node *node ;
	for( i = 0 ; i < grid->numUnknowns ; i++ ){
		//Get the current node on the grid
		node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
		//Get the index into the row of the operator for the current node
		rInd = grid->rowStartInds[ i ] ;
		result[i] = vec[i] * mat[ rInd ] ;
		//Loop through the neighbours of the current node
		for( j=1 ; j < node->numNghbrs ; j++ ){
			//Get the index into the approximation for the given neighbour
			k = grid->mapG2A[ node->nghbrs[ j ] ] ;
			//Apply the operator to the appropriate value in the approximation
			result[i] += vec[ k ] * mat[ rInd + j ] ;
		}
	}
}

















