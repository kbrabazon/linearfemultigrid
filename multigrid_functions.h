#ifndef _MULTIGRID_FUNCTIONS_H
#define _MULTIGRID_FUNCTIONS_H

#include "FEM_MG_SparseMatrix.h"
#include "useful_definitions.h"
/** \file multigrid_functions.h
	\brief Functions shared between the linear and nonlinear multigrid
		   iterations
		   
	A collection of functions that are not specific for the linear or nonlinear
	multigrid iterations are put into this file
*/

//Assembles the entries in the 'stiffness matrix'. This term is used in the
//linear and nonlinear cases. The stiffness matrix in the nonlinear case
//contains the values that need to multiply the approximation values such that
//this can be performed as a matrix-vector multiplication
void calculate_iteration_matrix( const GridParameters* grid,
	const AlgorithmParameters* params, const double* approx,
	ApproxVars *aVars ) ;
	
//Assembles the entries in the mass matrix. This is used in the time dependent
//problems
void calculate_mass_matrix( const GridParameters* grid,
	const AlgorithmParameters* params, double *massMat ) ;
	
void calc_lumped_mass_matrix( const GridParameters *grid,
	const AlgorithmParameters *params, double *massMat ) ;

//Returns the boundary function evaluation at the given coordinate
double get_dirichlet_boundary( const AlgorithmParameters *params,
	const Node *node ) ;
void set_dirichlet_boundary( const AlgorithmParameters *params,
	const GridParameters *grid, double *func ) ;

//Various function evaluations
double weak_rhs_function( const int *nodes, int elId,
	const GridParameters* grid, const AlgorithmParameters* params,
	const ApproxVars *aVars, const double* nothing, double x, double y ) ;

//Performs two dimensional Simpson's rule, not assuming any regularity
double simpsons_rule( const int* nodes, int elId, const GridParameters* grid,
	const AlgorithmParameters* params, const ApproxVars *aVars,
	const double* approx, double (*grid_function)(const int*, int,
	const GridParameters*, const AlgorithmParameters*, const ApproxVars *,
	const double*, double, double ) ) ;

//Used in calculating the area of a triangle
double b( int i, const Element *e, const Node *nodes ) ;

double c( int i, const Element *e, const Node *nodes ) ;

//Calculates the residual in an approximation
void calculate_residual( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars ) ;

//Calculates the area of a triangle on the grid
double element_area( const GridParameters *grid, const Element *element ) ;

//Performs restriction from a fine to a coarse grid, assuming the regularity of
//the grid
void restriction( const AlgorithmParameters* params,
	const GridParameters* fineGrid, const GridParameters* coarseGrid,
	const double* fineFunc, enum RESTRICT_TYPE rType,
	int useWeightFact, double* coarseFunc ) ;
	
void restrict_FW( const GridParameters *fGrid, const GridParameters *cGrid,
	int useWeightFact, const double *fFunc, double *cFunc ) ;
	
void restrict_injection( const GridParameters *fGrid, int fRNodes,
	const GridParameters *cGrid, int cRNodes, int useWeightFact,
	const double *fFunc, double *cFunc ) ;
	
void restrict_residual( const AlgorithmParameters *params,
	const GridParameters *fGrid, const GridParameters *cGrid,
	const double *fResid, const double *fBndTerm, double *cResid,
	double *cBndTerm ) ;
	
//Restricts the integral of a function that is piecewise constant on the
//elements of a fine grid to the elements of a coarse grid
void restrict_pc_coeff( const AlgorithmParameters *params,
	const GridParameters *fGrid, const GridParameters *cGrid,
	const double *fCoeff, double *cCoeff ) ;

//Performs prolongation from a coarse grid to a fine grid	
void prolongate( const AlgorithmParameters *params,
	const GridParameters *cGrid, const double *cFunc,
	const GridParameters *fGrid, double *fFunc ) ;
	
//Functions to handle the parameters
int update_params( AlgorithmParameters* params, char* paramName,
	char* paramValue ) ;
int read_alg_params( AlgorithmParameters* params, const char* fName ) ;
void set_test_case_specific_values( AlgorithmParameters *params ) ;
int validate_alg_params( const AlgorithmParameters* params ) ;

//Output functions useful for debugging	
void print_algorithm_parameters_to_file( const char* filename,
	const AlgorithmParameters* params ) ;
	
void print_grid_func_no_bnd_to_file( const char* filename,
	const GridParameters *grid, const double* function, int append ) ;
void print_grid_func_to_file( const char *filename,
	const GridParameters *grid, const double *function, int append ) ;
void print_weighted_grid_func_to_file( const char *filename,
	const GridParameters *grid, const double *function, double weightFact,
	int append ) ;
void read_grid_function_from_file( const char *filename,
	const GridParameters *grid, double *func ) ;

void print_vector_field_to_file( const char *filename,
	const GridParameters *grid, const Vector2D *vector, int append ) ;

//Functions for printing Paraview compatible data
void print_grid_paraview_format( const char *filename, const char *header,
	const GridParameters *grid ) ;
void print_cell_func_paraview_format( const char *filename,
	const GridParameters *grid, const double *cellFunc ) ;
void print_grid_func_paraview_format( const char* filename,
	const GridParameters *grid, const double *function ) ;
void print_vect_field_paraview_format( const char* filename,
	const GridParameters *grid, const Vector2D *vectorFunc ) ;
	
void recover_gradients( const AlgorithmParameters* params, 
	const GridParameters* grid, const double* approx, CVector2D *recGrads ) ;
	
//Checks if node1 is connected to node2 on the grid or not
int connected( const Node *n1, const Node *n2 ) ;

//Get the appropriate list of grids to point to
const char** get_grid_hierarchy( const AlgorithmParameters *params ) ;

//Performs sparse matrix multiplication
void sparse_mat_mult( const GridParameters *grid,
	const double *vec, const double *mat, double *result ) ;
	
//Assigns values from a comma separated list (with not blank spaces) to the
//elements of a double vector passed in
void assign_dvect_values( double *vect, char *values ) ;
void assign_coord_values( Vector2D *coords, char *values ) ;

#endif
