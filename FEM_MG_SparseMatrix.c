#include "FEM_MG_SparseMatrix.h"
#include "test_cases.h"
#include "richards_eq.h"

//This is declared in 'useful_definitions.h', and needs to be defined once in
//another file, so I am doing it before the main method
const char* meshesBL2TR[] = {"./DiagBL/mesh4.out", "./DiagBL/mesh8.out",
	"./DiagBL/mesh16.out", "./DiagBL/mesh32.out", "./DiagBL/mesh64.out",
	"./DiagBL/mesh128.out", "./DiagBL/mesh256.out", "./DiagBL/mesh512.out",
	"./DiagBL/mesh1024.out", "./DiagBL/mesh2048.out" } ;
	
const char* exMeshesBL2TR[] = {"./ExDiagBL/exMesh4.out",
	"./ExDiagBL/exMesh8.out", "./ExDiagBL/exMesh16.out",
	"./ExDiagBL/exMesh32.out", "./ExDiagBL/exMesh64.out",
	"./ExDiagBL/exMesh128.out", "./ExDiagBL/exMesh256.out",
	"./ExDiagBL/exMesh512.out", "./ExDiagBL/exMesh1024.out",
	"./ExDiagBL/exMesh2048.out" } ;
	
const char* meshesBR2TL[] = {"./DiagBR/mesh4.out", "./DiagBR/mesh8.out",
	"./DiagBR/mesh16.out", "./DiagBR/mesh32.out", "./DiagBR/mesh64.out",
	"./DiagBR/mesh128.out", "./DiagBR/mesh256.out", "./DiagBR/mesh512.out",
	"./DiagBR/mesh1024.out", "./DiagBR/mesh2048.out" } ;
	
const char* meshesBL2TR_Q[] = {"./DiagBL/qMesh8.out", "./DiagBL/qMesh16.out",
	"./DiagBL/qMesh32.out", "./DiagBL/qMesh64.out", "./DiagBL/qMesh128.out",
	"./DiagBL/qMesh256.out", "./DiagBL/qMesh512.out", "./DiagBL/qMesh1024.out",
	"./DiagBL/qMesh1024.out", "./DiagBL/qMesh2048.out" } ;

const char* meshesBR2TL_Q[] = {"./DiagBR/qMesh8.out", "./DiagBR/qMesh16.out",
	"./DiagBR/qMesh32.out", "./DiagBR/qMesh64.out", "./DiagBR/qMesh128.out",
	"./DiagBR/qMesh256.out", "./DiagBR/qMesh512.out", "./DiagBR/qMesh1024.out",
	"./DiagBR/qMesh2048.out" } ;

/** Clears the memory used by the grids and the approximation variables on each
 *	grid
 *	\param [in,out] grids	The hierarchy of grids used in the algorithm. The
 *							memory used by each grid in the hierarchy as well
 *							as this pointer is freed in this method
 *	\param [in,out] aVarList	The approximation variables structure stored for
 *								each grid level. The memory used by each
 *								approximation variables structure in the
 *								hierarchy as well as this pointer is freed in
 *								this method
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 */
void clean_up( GridParameters **grids, ApproxVars **aVarList,
	AlgorithmParameters *params )
{
	if( grids != NULL ){
		free_grids( grids, params->fGridLevel ) ;
		free( grids ) ;
		grids = NULL ;
	}
	if( aVarList != NULL ){
		free_approx_var_list( aVarList, params->fGridLevel, params ) ;
		free( aVarList ) ;
		aVarList = NULL ;
	}
	free_re_ctxt_mem( &(params->reCtxt) ) ;
	free_coeff_regs( &(params->coeffReg) ) ;
}

/** Performs Full Multigrid (FMG) to get an initial approximation on the finest
 *	grid level
 *	\param [in]	grids	A hierarchy of grids available to use
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVarsList	A hierarchy of approximation variables available
 *								to use. The approximation on the finest grid
 *								level in here is the desired output of the
 *								function
 *	\param [in] results	Stores results about the execution of the multigrid
 *						iterations performed as part of FMG
 */
void fmg( GridParameters **grids, AlgorithmParameters* params,
	ApproxVars **aVarsList, Results *results )
{
	int lvlCnt ; //Loop counter to keep track of which level we are operating on
	int pre, post ; //The number of pre- and post-smooths (respectively) to
					//perform in the multigrid algorithm. These store the values
					//that are to be used outside of FMG so that the values that
					//are to be used for pre- and post-smoothing in FMG can be
					//used in this method
	
	GridParameters *cGrid, *fGrid ; //Pointers to a grid and the next finest
									//grid, respectively. For a list of member
									//variables see 'useful_definitions.h'
									
	ApproxVars *cVars, *fVars ; //Variables associated with the approximation on
								//a coarse (fine) grid, respectively. For
								//definition of member variables see
							    //'useful_definitions.h'
						
	int cRowNodes, fRowNodes ; //The number of nodes in a row on the coarse
							   //(fine) grid, respectively. This assumes that
							   //a regular square grid is being used
				
	char outFName[100] ; //Value used to store the filename of a file to which
					//to print debugging information. If in doubt about this
					//remove it. It should have no effect on the outcome of the
					//algorithm and removing it could remove output to file,
					//which will save on time
				  
	int finestGrid ; //The finest grid level. This placeholder is needed, as the
					 //finest grid level in the parameters object will be
					 //updated
					 
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
					 
	//Initialise fVars to NULL to stop the compiler complaining
	fVars = NULL ;
		
	finestGrid = params->fGridLevel ;
	pre = params->numPreSmooths ;
	post = params->numPostSmooths ;
	params->numPreSmooths = params->fmgPreSmooths ;
	params->numPostSmooths = params->fmgPostSmooths ;
	
	//Set a placeholder for the current coarse grid that we are using. We assume
	//that the memory has already been assigned and the grid set up
	//appropriately elsewhere
	cGrid = grids[params->cGridLevel-2] ;
	
	//Set a placholder for the coarse grid variables. We assume that the memory
	//has been assigned and the variables set up appropriately elsewhere
	cVars = aVarsList[params->cGridLevel-2] ;
	
	//Set up the approximation on the coarsest grid level
	if( cVars->tmpApprox == NULL )
		cVars->tmpApprox = (double*) calloc( cGrid->numUnknowns,
			sizeof( double ) ) ;
	initialise_approx( params, cGrid, cVars->approx ) ;
		
	//Print out the initial approximation to file for debugging purposes
	if( params->debug ){
		sprintf( outFName, "/tmp/fmgApproxLvl%d.txt",
			params->cGridLevel - 1 ) ;
		print_grid_func_to_file( outFName, cGrid, cVars->approx, 0 ) ;
	}
		
	//After creating the initial approximation I want to solve on the coarsest
	//grid. For this I need a rhs
	initialise_rhs( cGrid, params, cVars, 1.0, cVars->rhs, cVars->bndTerm ) ;
	
	//Calculate the appropriate stiffness matrix. It doesn't matter which method
	//we are running here, as we simply do a solve on the coarsest grid, and
	//don't pay attention to which method is being employed. We do need to
	//consider the formulation of the problem, though, so need to check if
	//recovery was performed
	if( params->recoverGrads )
		recover_gradients( params, cGrid, cVars->approx, cVars->recGrads ) ;
	calculate_iteration_matrix( cGrid, params, cVars->approx, cVars ) ;
	
	//Loop through the grid levels from the coarsest to the finest
	for( lvlCnt = params->cGridLevel-1 ; lvlCnt < finestGrid ; lvlCnt++ ){
		//If we are on the coarsest grid level solve exactly
		if( lvlCnt == params->cGridLevel-1 ){
			if( is_non_linear( params->testCase ) && cVars->jDiags == NULL )
				cVars->jDiags = (double*)calloc( cGrid->numUnknowns,
					sizeof( double ) ) ;
					
			//Now solve on the coarse grid level
			coarse_solve( params, cGrid, cVars, TOL2, 0 ) ;
		}
		else{ //Perform a single multigrid iteration
			params->fGridLevel = params->currLevel = lvlCnt ;
			//We distinguish between running a Newton-Multigrid iteration or
			//the linear / nonlinear methods of multigrid
			if( is_non_linear( params->testCase ) &&
				params->nlMethod == METHOD_NEWTON_MG ) //Newton Multigrid
				newton_mg( grids, params, aVarsList, results ) ;
			else{ //Linear / nonlinear multigrid
				multigrid( grids, params, aVarsList, results ) ;
			}
		}
		
		//For debugging purposes print out the approximation on the current grid
		//level
		if( params->debug ){
			sprintf( outFName, "/tmp/fmgApproxLvl%d.txt", lvlCnt ) ;
			print_grid_func_to_file( outFName, cGrid, cVars->approx, 1 ) ;
		}
		
		//We have got the solution on the finest grid level that we are
		//currently considering. We now interpolate this to the next finest grid
		//level
	
		//Set a placeholder for the current fine grid. We assume that it has
		//already been set up appropriately
		fGrid = grids[lvlCnt] ;
		
		//Set a placeholder for the fine grid approximation variables. We assume
		//that the memory has been assigned and the variables set up
		//appropriately elsewhere
		fVars = aVarsList[lvlCnt] ;
		
		if( fVars->tmpApprox == NULL )
			fVars->tmpApprox = (double*) calloc( fGrid->numUnknowns,
				sizeof( double ) ) ;
	
		//Interpolate the solution from the coarse grid level to the fine grid
		//level. First calculate the number of nodes in a row on each grid. This
		//asssumes that we are working with a regular grid
		fRowNodes = sqrt( fGrid->numNodes ) ;
		cRowNodes = sqrt( cGrid->numNodes ) ;
	
		//Interpolate to the next finest grid
		prolongate( params, cGrid, cVars->approx, fGrid, fVars->approx ) ;
				
		if( params->debug ){
			sprintf( outFName, "/tmp/fmgApproxLvl%d.txt", lvlCnt+1 ) ;
			print_grid_func_to_file( outFName, fGrid, fVars->approx, 0 ) ;
		}
			
		//Set the coarse grid to be the current fine grid before the next
		//iteration
		cVars = fVars ;
		cGrid = fGrid ;
	}
	
	//Restore the finest grid level to the parameters object before we exit
	params->fGridLevel = params->currLevel = finestGrid ;
	params->numPreSmooths = pre ;
	params->numPostSmooths = post ;
}

/** Performs Multigrid (both linear and nonlinear) on the finest grid level
 *	until some stopping criterion is met. For a data flow diagram for the
 *	functionality contained in this function see \ref secMGGraph.
 *	\param [in] grids	A Hierarchy of grids available to use
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVarsList	A hierarchy of approximation variables available
 *								to use. The approximation on the finest grid
 *								is the desired output of the function
 *	\param [out] results	Stores information about the execution of the
 *							multigrid iterations
 */
void multigrid( GridParameters **grids, AlgorithmParameters* params,
	ApproxVars **aVarsList, Results *results )
{
	GridParameters* fineGrid ;   // Stores information about the finest grid
	ApproxVars *fVars ; //Stores information about the approximation on the
						//finest grid
						 
	int performIteration ; //Indicator as to whether a multigrid iteration
						   //should be performed or not
						   
	int solveBySmooths ; //Indicates that instead of performing a multigrid
						 //iteration whether we should simply perform iterations
						 //of the smoother until we get convergence. This occurs
						 //if the number of both the pre- and post-smooths are
						 //set to zero
	
	char debugFName[100] ; //String containing the name of a file to write out
						   //to if we are debugging
	
	params->currLevel = params->fGridLevel ;
						 
	//Find out if we are to solve on the current grid using smooths only,
	//instead of a multigrid iteration
	solveBySmooths = ( params->numPreSmooths == 0 &&
		params->numPostSmooths == 0 ) ;
	
	//Initialise the count of the multigrid iterations to zero
	results->itCnt = 0 ;
	
	//Set a placeholder for the current grid level to make this neater
	fineGrid = grids[params->fGridLevel-1] ;
	
	//Set a placeholder for the approximation variables on the current grid
	fVars = aVarsList[params->fGridLevel-1] ;

	//If the problem is time dependent it is assumed that the right hand side
	//has already been calculated
	if( !is_time_dependent( params->testCase ) )
		initialise_mg_variables( fineGrid, params, fVars ) ;
	
	//Calculate the L2 and the H1 norms of the residual
	results->origResidL2Norm = vect_discrete_l2_norm( fineGrid->numUnknowns,
		fVars->resid ) ;
	//To calculate the H1 norm we need to know the number of rows and columns in
	//the interior of the grid. We use the fact that the grid is regular and
	//square
	results->origResidH1Norm = vect_discrete_h1_norm( fineGrid, fVars->resid ) ;
		
	results->residL2Norm = results->origResidL2Norm ;
	results->residH1Norm = results->origResidH1Norm ;
	//Set the old residual to be larger than the current one, so that we don't
	//get any confusion with the stopping criteria
	results->oldResidL2Norm = results->residL2Norm + 1.0 ;
	
	//If we are not performing FMG print information to the console
	if( !params->isFMG && !is_time_dependent( params->testCase ) ){
		printf( "Original residual L2-norm: %.18f\n",
			results->origResidL2Norm ) ;
		printf( "Original residual H1-norm: %.18lf\n",
			results->origResidH1Norm ) ;
	}
	
	//If we are debugging print out the approximation and the right hand side
	//to file
	if( params->debug ){
		sprintf( debugFName, "/tmp/rhsLvl%d.txt", params->fGridLevel ) ;
		print_grid_func_no_bnd_to_file( debugFName, fineGrid,
			fVars->rhs, 0 ) ;
		sprintf( debugFName, "/tmp/mgApproxLvl%d.txt", params->fGridLevel ) ;
		print_grid_func_to_file( debugFName, fineGrid, fVars->approx, 0 ) ;
		
	}
		
	//Check to see if we want to perform the iteration or not
	if( params->isFMG )
		performIteration = 1 ;
	else if( is_time_dependent( params->testCase ) )
		performIteration = (
			results->residL2Norm / results->origResidL2Norm > TD_TOL &&
			results->itCnt < params->maxIts &&
			results->residL2Norm < MAX_RESID_FACT * results->origResidL2Norm ) ;
	else
		performIteration = (
			results->residL2Norm / results->origResidL2Norm > TOL1 &&
			results->itCnt < params->maxIts &&
			results->residL2Norm < MAX_RESID_FACT * results->origResidL2Norm ) ;
			
	//Check to see if we are suppoosed to perform smoothing iterations until
	//convergence
	if( solveBySmooths ){
		//Check if we are to run the linear case or not
		if( !is_non_linear( params->testCase ) ){
			//direct_lin_solve( fineGrid, fVars ) ;
			//results->itCnt = 1 ;
			results->itCnt = gs_to_converge( params, fineGrid, fVars, TOL2,
				0 ) ;
		}
		else{
			results->itCnt = nl_gauss_seidel_to_converge( fineGrid, params,
				fVars, 0 ) ;
		}
		
		//Set the parameter to perform the multigrid iteration to zero, as we
		//are solving using the smoother instead
		performIteration = 0 ;
	}
			
	while( performIteration ){
        results->oldResidL2Norm = results->residL2Norm ;
        results->oldResidH1Norm = results->residH1Norm ;

		//Perform the appropriate multigrid iteration
		if( !is_non_linear( params->testCase ) )//Linear case
			linear_multigrid( params, grids, aVarsList ) ;
		else//Nonlinear case
			nl_multigrid( params, grids, aVarsList, results->itCnt ) ;
		
		if( is_non_linear( params->testCase ) ){
			//Update the stiffness matrix in the nonlinear case, as this is
			//dependent on the approximation
			if( params->recoverGrads )
				recover_gradients( params, fineGrid, fVars->approx,
					fVars->recGrads ) ;
			calculate_iteration_matrix( fineGrid, params, fVars->approx, 
				fVars ) ;
		}
		
		calculate_residual( params, fineGrid, fVars ) ;
				
		//Calculate the norm of the residual in approximation
		results->residL2Norm = vect_discrete_l2_norm(
			fineGrid->numUnknowns, fVars->resid ) ;
		results->residH1Norm = vect_discrete_h1_norm( fineGrid, fVars->resid ) ;
		
		if( params->debug ){
			sprintf( debugFName, "/tmp/mgApproxLvl%d.txt",
				params->fGridLevel ) ;
			print_grid_func_to_file( debugFName, fineGrid,
				fVars->approx, 1 ) ;
		}

		//Increase the iteration count
		results->itCnt++;
		//If we are not performing FMG then print out the differnces in residual
		if( !params->isFMG && !is_time_dependent( params->testCase ) ){
			printf( "Iteration %d:\n\tNew L2 Residual: %.18lf\tRatio: %.18lf\n",
				results->itCnt, results->residL2Norm,
				results->residL2Norm / results->oldResidL2Norm ) ;
			printf( "\tNew H1 Residual: %.18lf\tRatio: %.18lf\n",
				results->residH1Norm,
				results->residH1Norm / results->oldResidH1Norm ) ;
		}
			
			
		//Check the stopping criteria. If we are performing FMG we only want to
		//perform one iteration, so stop the loop here
		if( params->isFMG )
			performIteration = 0 ;
		else if( is_time_dependent( params->testCase ) )
			performIteration = (
				results->residL2Norm / results->origResidL2Norm > TD_TOL &&
				results->itCnt < params->maxIts &&
				(results->residL2Norm <
				MAX_RESID_FACT * results->origResidL2Norm ) ) ;
		else
//			performIteration = ( results->itCnt < params->maxIts ) ;
			performIteration = (
				results->residL2Norm / results->origResidL2Norm > TOL1 &&
				results->itCnt < params->maxIts &&
				(results->residL2Norm <
				MAX_RESID_FACT * results->origResidL2Norm ) ) ;
	}
}


/** Performs the appropriate operations to set up the variables required before
 *	starting a multigrid iteration
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] aVars	The approximation variables to set up appropriately so
 *						that they can be used in a multigrid iteration
 */
void initialise_mg_variables( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars )
{
	//Set up the initial approximation, unless we have performed FMG, or are
	//performing FMG. If the problem is time dependent it is assumed that the
	//initial approximation has already been calculated
	if( params->setUpApprox )
		initialise_approx( params, grid, aVars->approx ) ;
		
	//Calculate the iteration matrix for the system of equations. This only
	//needs to be calculated once for linear multigrid, and needs to be updated
	//for the nonlinear multigrid.
	if( params->recoverGrads )
		recover_gradients( params, grid, aVars->approx, aVars->recGrads ) ;
		
	//Set up the initial values for the right hand side
	initialise_rhs( grid, params, aVars, 1.0, aVars->rhs, aVars->bndTerm ) ;
	
	calculate_iteration_matrix( grid, params, aVars->approx, aVars ) ;
		
	calculate_residual( params, grid, aVars ) ;
}


/** Performs one iteration of linear multigrid. For a data flow diagram for the
 *	functionality contained in this function see \ref secLinGraph.
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grids	A hierarchy of grids available to use
 *	\param [in,out]	aVarsList	A hierarchy of approximation variables available
 *								to use. The approximation on the finest grid
 *								level is the desired output from the function
 */
void linear_multigrid( AlgorithmParameters* params, GridParameters** grids,
	ApproxVars **aVarsList )
{
	int mgCnt;   // Loop counters
	GridParameters *fGrid, *cGrid ;	//Stores information about the fine (coarse)
									//grid, respectively
						   
	ApproxVars *fVars, *cVars ; //Variables that are related to the
								//approximation on the fine (coarse) grid,
								//respectively
								
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
						  
//	char debugFName[30] ; //Used for debugging 17/11. If in doubt remove this as
						  //it should have no effect on the overall running of
						  //the algorithm
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
	
	//Set the fine grid to be the appropriate grid in the hierarchy of grids
	fGrid = grids[params->currLevel-1] ;
	fVars = aVarsList[params->currLevel-1] ;
	
	//Set the coarse grid to be the appropriate grid in the hierarchy of grids
	cGrid = grids[params->currLevel-2] ;
	
	//Set a placeholder for the coarse grid approximation variables on the
	//current grid
	cVars = aVarsList[params->currLevel-2] ;
	
	//Pre-smooth
	smooth( fGrid, params, params->numPreSmooths, NULL, fVars) ;

	//Calculate the residual
	calculate_residual( params, fGrid, fVars ) ;
	
//	sprintf( debugFName, "/tmp/rhsLvl%d.txt", params->currLevel ) ;
//	print_grid_func_no_bnd_to_file( debugFName, fGrid, fVars->rhs, 1 ) ;

	//Restrict the residual to the coarse grid
	restrict_residual( params, fGrid, cGrid, fVars->resid, fVars->bndTerm,
		cVars->rhs, cVars->bndTerm ) ;

	//Initialise the approximation to zero on the coarse grid
	set_dvect_to_zero( cGrid->numNodes, cVars->approx ) ;
		
	//If we are not performing Newton-Multigrid re-calculate the value of the
	//operator
	if( !(is_non_linear( params->testCase ) &&
		params->nlMethod == METHOD_NEWTON_MG) )
		calculate_iteration_matrix( cGrid, params, cVars->approx, cVars ) ;
	
	if ( params->currLevel == params->cGridLevel ){
		if( cVars->tmpApprox == NULL )
			cVars->tmpApprox = (double*) calloc( cGrid->numUnknowns,
				sizeof( double ) ) ;

		if( params->exactCoarseSolve )
			direct_lin_solve( cGrid, cVars ) ;
		else
			gs_to_converge( params, cGrid, cVars, TOL2, 0 );
	}
	else{
		//Perform multigrid if we are not solving 'exactly'
		for( mgCnt = 0 ; mgCnt < params->numMgCycles ; mgCnt++ ){
			params->currLevel-- ;
			linear_multigrid( params, grids, aVarsList ) ;
			params->currLevel++ ;
		}
	}
	
	if( fVars->tmpApprox == NULL )
		fVars->tmpApprox = (double*) calloc( fGrid->numUnknowns,
			sizeof( double ) ) ;

	//Prolongate the coarse grid approximation, which is the correction to the
	//approximation on the fine grid
	prolongate( params, cGrid, cVars->approx, fGrid, fVars->tmpApprox ) ;

	//Correct the approximation on the fine grid
	add_to_vect( fGrid->numUnknowns, fVars->approx, fVars->tmpApprox );
	
	//Post-smooth
	smooth( fGrid, params, params->numPostSmooths, NULL, fVars ) ;
}

/** Performs iterations of preconditioned conjugate gradients. The algorithm for
 *	conjugate gradients is taken from <em> Newton Methods for Nonlinear
 *	Problems: Affine Invariance and Adaptive Algorithms, Deuflhard P.,
 *	2004, pg. 30 </em>.
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grids	The hierarchy of grids to be used
 *	\param [in,out] aVarsList	Hierarchy of approximation variables. The
 *								approximation on the finest grid level is the
 *								desired output from this method
 *	\param [in] numIts	The number of iterations to perform
 */
void pcg( AlgorithmParameters *params, GridParameters **grids,
	ApproxVars **aVarsList, int numIts )
{
	int i, j, itCnt, rowInd ; //Loop counters
	GridParameters *grid ; //Placeholder to the grid on which we are operating
	ApproxVars *aVars ; //Placeholder to the current grid
	double *tmpRHS ; //Used as a placeholder when swapping pointers
	double *tmpApprox ; //Used as a placeholder when swapping pointers
				 
	CGContext *ctxt ;
				 
	const Node *node ; //Pointer to a node on the grid
	
	enum SMOOTH_TYPE currSmoother ; //Stores the current smoothing iteration
	
	//See the book mentioned in the above comments for a description of what
	//sigma, alpha and beta do
	double sigmaOld, sigmaNew, alpha, beta ;
	
	//Make sure that the current grid level we are on is considered the finest
	//grid level
	params->currLevel = params->fGridLevel ;
	//Make sure that we are performing a multigrid V-cycle
	params->numMgCycles = 1 ;
	//Make sure that we only perform a pointwise Jacobi smooth, as we want a
	//symmetric operator, and so need a symmetric smoother
	currSmoother = params->smoothMethod ;
	params->smoothMethod = SMOOTH_JAC ;
	//Make sure that the number of post smooths is equal to the number of pre
	//smooths, as we require a symmetric operator
	params->numPostSmooths = params->numPreSmooths ;
	
	//Set a placeholder to the fine grid and the approximation variables. We
	//assume that the variables on the finest grid level have all been set up
	//appropriately
	grid = grids[params->fGridLevel-1] ;
	aVars = aVarsList[params->fGridLevel-1] ;
	
	if( aVars->usrCtxt == NULL ){
		aVars->usrCtxt = (CGContext*)calloc( 1, sizeof( CGContext ) ) ;
		initialise_cg_context( grid, (CGContext*)aVars->usrCtxt ) ;
	}

	ctxt = (CGContext*)aVars->usrCtxt ;
	
	//First thing to do is to calculate the residual for the linear system
	calculate_residual( params, grid, aVars ) ;
	
	//The right hand side of the system for multigrid to solve should be the
	//residual, so set this to be the residual
	copy_vectors( grid->numUnknowns, ctxt->resid, aVars->resid ) ;
	//Set a placeholder to the rhs vector, and point the rhs to the residual of
	//the current system
	tmpRHS = aVars->rhs ;
	aVars->rhs = ctxt->resid ;
	
	//Perform linear multigrid with zero initial estimate, with zero Dirichlet
	//boundary conditions
	tmpApprox = aVars->approx ; 
	aVars->approx = ctxt->currApprox ;
	ctxt->currApprox = tmpApprox ;
	set_dvect_to_zero( grid->numNodes, aVars->approx ) ;
	//Multigrid
	linear_multigrid( params, grids, aVarsList ) ;
	
	//Set the right hand side to equal the original right hand side again for
	//the rest of the iteration
	aVars->rhs = tmpRHS ;
	
	//After performing linear multigrid the variable aVars->approx contains
	//the precondtioned residual, which we want to assign to the value p
	tmpApprox = ctxt->p ;
	ctxt->p = aVars->approx ;
	aVars->approx = tmpApprox ;
	
	//Calculate the energy norm in the preconditioned residual
	sigmaNew = vect_inner_product( grid->numUnknowns, ctxt->resid, ctxt->p ) ;
	sigmaOld = sigmaNew ;
	
	for( itCnt = 0 ; itCnt < numIts ; itCnt++ ){
		//Calculate the value Ap
		for( i=0 ; i<grid->numUnknowns ; i++ ){
			//Get a pointer to the current node on the grid
			node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
			ctxt->aP[i] = 0.0 ;
			//Find the index for the start of the row in the iteration matrix
			//corresponding to the current node
			rowInd = grid->rowStartInds[i] ;
			//Apply the multiplication for the current row of the matrix
			for( j=0 ; j < node->numNghbrs ; j++ ){
				ctxt->aP[i] += ctxt->p[grid->mapG2A[ node->nghbrs[j] ] ] *
					aVars->iterMat[ rowInd + j ] ;
			}
		}
	
		alpha = vect_inner_product( grid->numUnknowns, ctxt->p, ctxt->aP ) /
			sigmaOld ;
	
		for( i=0 ; i < grid->numUnknowns ; i++ ){
			//Update the approximation appropriately
			ctxt->currApprox[i] += ctxt->p[i] / alpha ;
			//Update the value of the residual appropriately
			ctxt->resid[i] -= ctxt->aP[i] / alpha ;
		}
	
		if( itCnt != numIts-1 ){
			//Now precondition the system with one iteration of linear multigrid
			aVars->rhs = ctxt->resid ;
			//Set the approximation to zero
			set_dvect_to_zero( grid->numNodes, aVars->approx ) ;
	
			//Precondition with linear multigrid
			linear_multigrid( params, grids, aVarsList ) ;
	
			//Reset the value of the right hand side of the system
			aVars->rhs = tmpRHS ;
	
			sigmaNew = vect_inner_product( grid->numUnknowns, ctxt->resid,
				aVars->approx ) ;
		
			beta = sigmaNew / sigmaOld ;
		
			//Update the value of the search direction
			for( i=0 ; i < grid->numUnknowns ; i++ )
				ctxt->p[i] = beta * ctxt->p[i] + aVars->approx[i] ;
			
			sigmaOld = sigmaNew ;
		}
	}
	
	//Clean up
	params->smoothMethod = currSmoother ;
	
	//We point the residual in the ApproxVars object to the value of the current
	//residual before freeing the other pointer
	tmpRHS = ctxt->resid ;
	ctxt->resid = aVars->resid ;
	aVars->resid = tmpRHS ;
	//free( tmpResid ) ;
	
	//We point the approximation in the ApproxVars structure to the value of the
	//current approximation before freeing the other pointer
	tmpApprox = ctxt->currApprox ;
	ctxt->currApprox = aVars->approx ;
	aVars->approx = tmpApprox ;
	//free( approx ) ;
	
	//free( p ) ;
}

/** Allocates the memory for a conjugate gradients context on a given grid
 *	\param [in] grid	The grid on which to set up the context
 *	\param [out] ctxt	The context to allocate the memory for
 */
void initialise_cg_context( const GridParameters *grid, CGContext *ctxt )
{
	ctxt->resid = (double*) calloc( grid->numUnknowns, sizeof( double ) ) ;
	ctxt->currApprox = (double*) calloc( grid->numNodes, sizeof( double ) ) ;
	ctxt->p = (double*) calloc( grid->numNodes, sizeof( double ) ) ;
	ctxt->aP = (double*) calloc( grid->numUnknowns, sizeof( double ) ) ;
}

/** Frees the memory used by a conjugate gradients context
 *	\param [in] ctxt	The context for which to free the memory
 */
void free_cg_context( CGContext *ctxt )
{
	if( ctxt->resid != NULL ){
		free( ctxt->resid ) ;
		ctxt->resid = NULL ;
	}
	if( ctxt->currApprox != NULL ){
		free( ctxt->currApprox ) ;
		ctxt->currApprox = NULL ;
	}
	if( ctxt->p != NULL ){
		free( ctxt->p ) ;
		ctxt->p = NULL ;
	}
	if( ctxt->aP != NULL ){
		free( ctxt->aP ) ;
		ctxt->aP = NULL ;
	}
}

/** Performs \e numIts iterations of multigrid preconditioned GMRES. The
 *	algorithm for GMRES is taken from pg. 287/8 in Iterative Methods for Sparse
 *	Linear Systems, 2nd Edition, Saad Y., 2003, SIAM.
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grids	The hierarchy of grids to be used
 *	\param [in,out] aVarsList	Hierarchy of approximation variables. The
 *								approximation on the finest grid level is the
 *								desired output from this method
 *	\param [in] numIts	The number of iterations to perform
 */
void pcgmres( AlgorithmParameters *params, GridParameters **grids,
	ApproxVars **aVarsList, int numIts )
{
	int i, j, k, itCnt ; //Loop counters
	int currSmoother ; //A placeholder for the current smoother being used in
					   //the algorithm
	GridParameters *grid ; //The current grid on which we are operating
	ApproxVars *aVars ; //The current approximation variables to use
	FGMResContext *ctxt ; //The context for the GMRES iteration
	LeastSquaresContext *lsCtxt ; //The context for the least squares solve
	double *w ;
	double *origRHS ; //A placeholder for the original right hand side of the
					  //system
	double *origApprox ; //A placeholder for the original approximation of the
						 //system
	const Node *node ; //A node on the grid
	double weightFact ; //Weighting factor used to weight vectors used in the
						//calculations.
	double *swapPtr ;
	
	//I want to implement my own GMRES algorithm here, as inserting my multigrid
	//algorithm into a GMRES implementation does not seem like the easiest thing
	//and I am going to take a hit on converting to and from different formats.
	//The following implementation is taken from pg. 287, Iterative Methods for
	//Sparse Linear Systems, Yousef Sadaaar, 2nd Edition, 2003.
	//I have need of the flexible GMRES implementation as apparently the other
	//implementations are not stable for a varying preconditioner (or at least
	//are not stable in theory)
	
	//Make sure that the current grid level we are on is considered the finest
	//grid level
	params->currLevel = params->fGridLevel ;
	//Make sure that we are performing a multigrid V-cycle
	params->numMgCycles = 1 ;
	//Make sure that we only perform a pointwise Jacobi smooth, as we want a
	//symmetric operator, and so need a symmetric smoother
	currSmoother = params->smoothMethod ;
	//params->smoothMethod = SMOOTH_JAC ;
	//Make sure that the number of post smooths is equal to the number of pre
	//smooths, as we require a symmetric operator
	params->numPostSmooths = params->numPreSmooths ;
	
	//Set a placeholder to the fine grid and the approximation variables. We
	//assume that the variables on the finest grid level have all been set up
	//appropriately
	grid = grids[params->fGridLevel-1] ;
	aVars = aVarsList[params->fGridLevel-1] ;
	
	if( aVars->usrCtxt == NULL ){
		aVars->usrCtxt = (FGMResContext*)calloc( 1, sizeof( FGMResContext) ) ;
		initialise_gmres_context( grid, params, (FGMResContext*)aVars->usrCtxt,
			params->gmresReset ) ;
	}
	
	//Set the GMRES context variable
	ctxt = (FGMResContext*)aVars->usrCtxt ;
	lsCtxt = ctxt->lsCtxt ;
	
	//At this point I assume that the approximation, the right hand side and the
	//iteration matrix have been set up on the current grid level so that I can
	//calculate the residual in approximation. If we assume that the initial
	//approximation is equal to zero (which I do set for the initial
	//approximation to the linear multigrid) then this is going to be simply
	//the right hand side
	//calculate_residual( grid, aVars ) ;
	copy_vectors( grid->numUnknowns, aVars->resid, aVars->rhs ) ;
	
	//Get the norm of the residual
	weightFact = vect_discrete_l2_norm( grid->numUnknowns,
		aVars->resid ) ;
		
//	if( params->debug ){
//		printf( "\tGMRES Iteration: 0   GMRES resid: %.18lf\n", weightFact ) ;
//		fflush( stdout ) ;
//	}

	//Store the value for the original right hand side
	origRHS = aVars->rhs ;
	
	//Set up the memory for the first vector in the subspace
	if( ctxt->subSpaceMat[0] == NULL )
		ctxt->subSpaceMat[0] = (double*)calloc( grid->numUnknowns,
			sizeof( double ) ) ;
	//Point the right hand side for the multigrid iteration to the first
	//subspace vector and populate it with the appropriate data
	aVars->rhs = ctxt->subSpaceMat[0] ;
	for( i=0 ; i < grid->numUnknowns ; i++ )
		aVars->rhs[i] = aVars->resid[i] / weightFact ;
		
	lsCtxt->rhs[0] = weightFact ;
		
	//At this point we assume that the approximation that is set up in the 
	//GMRES context is initialised to zero, as we use 'calloc' to set it up. As
	//we are looking for a zero initial approximation to the system we swap the
	//approximations from the GMRES context to the ApproxVars object
	origApprox = aVars->approx ;
	swapPtr = aVars->iterMat ;
	
	//Loop through the set number of iterations
	for( itCnt = 1 ; itCnt < numIts+1 ; itCnt ++ ){
		ctxt->currIt = itCnt % (ctxt->reset+1) ;
		//If the current iteration is zero then we need to reset the algorithm.
		//However, I don't want to do that here. Maybe this is something to
		//implement in the future. I am going to set up the run so that the 
		//number of iterations to perform is never more that the reset
		
		if( ctxt->precSpaceMat[itCnt-1] == NULL )
			ctxt->precSpaceMat[itCnt-1] = (double*)calloc( grid->numNodes,
				sizeof(double) ) ;
		else
			set_dvect_to_zero( grid->numNodes, ctxt->precSpaceMat[itCnt-1] ) ;
		aVars->approx = ctxt->precSpaceMat[itCnt-1] ;
		
		if( params->gmresPrecSPart || params->testCase == 12 ){
			//Set the iteration matrix to point to the preconditioning matrix
			//for the preconditioning step
			aVars->iterMat = ctxt->precMat ;
		}
		//Perform linear multigrid
		linear_multigrid( params, grids, aVarsList ) ;
		//Reset the iteration matrix to be the full Jacobian, not just the
		//symmetric part
		aVars->iterMat = swapPtr ;
		
		w = aVars->tmpApprox ;
		//Compute the matrix vector multiplication with the preconditioned 
		//approximation and the linear operator
		for( i=0 ; i < grid->numUnknowns ; i++ ){
			//Get the current node on the grid
			node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
			//Get the starting index of the row in the iteration matrix for the
			//current node
			k = grid->rowStartInds[ i ] ;
			//Store the value of the operator applied to the approximation
			w[i] = aVars->approx[i] * aVars->iterMat[k] ;
			for( j=1 ; j < node->numNghbrs ; j++ ){
				w[i] += aVars->approx[ grid->mapG2A[ node->nghbrs[ j ] ] ] *
					aVars->iterMat[k + j] ;
			}
		}
		
		//Perform the orthogonalisation of the vector
		//Loop up to the current iteration number
		for( i=0 ; i < itCnt ; i++ ){
			weightFact = vect_weighted_inner_product( grid->numUnknowns, w,
				ctxt->subSpaceMat[i] ) ;
			lsCtxt->hessMat[itCnt-1][i] = weightFact ;
			for( k=0 ; k < grid->numUnknowns ; k++ )
				w[k] -= weightFact * ctxt->subSpaceMat[i][k] ;
		}
		
		//After the orthogonalizaztion we normalise the vector
		weightFact = vect_discrete_l2_norm( grid->numUnknowns, w ) ;
		lsCtxt->hessMat[itCnt-1][itCnt] = weightFact ;
		if( itCnt != numIts ){
			if( ctxt->subSpaceMat[itCnt] == NULL )
				ctxt->subSpaceMat[itCnt] = (double*)calloc( grid->numUnknowns,
					sizeof( double ) ) ;
			aVars->rhs = ctxt->subSpaceMat[itCnt] ;
			for( i=0 ; i < grid->numUnknowns ; i++ )
				aVars->rhs[i] = w[i] / weightFact ;
		}
		
//		if( params->debug ){
//			//printf( "%s", CLEAR_STR ) ;
//			printf( "\tGMRES Iteration: %d   GMRES Resid: %.18lf   ", itCnt,
//				weightFact ) ;
//			if( itCnt > 1 )
//				printf( "Ratio: %.18lf", weightFact *
//					lsCtxt->hessMat[itCnt-2][itCnt-1] ) ;
//			else
//				printf( "Ratio: %.18lf", weightFact * lsCtxt->rhs[0] ) ;
//			printf( "\n" ) ;
//			//fflush( stdout ) ;
//		}
	}
	
//	if( params->debug ){
//		printf( "%s", CLEAR_STR ) ;
//		fflush( stdout ) ;
//	}
	
	//The least squares should be set up so that all I need to do is to tell it
	//to be solved
	lsCtxt->currSize = ctxt->currIt ;
	least_squares( ctxt->lsCtxt ) ;
	
	//Reset the original approximation
	aVars->approx = origApprox ;
	origApprox = NULL ;
	
	//Apply the preconditioned subspace matrix to the value that we retrieved
	//from the least squares, and update the approximation with the appropriate
	//value
	for( i=0 ; i < grid->numUnknowns ; i++ )
		for( j=0 ; j < lsCtxt->currSize ; j++ )
			aVars->approx[i] += ctxt->precSpaceMat[j][i] * lsCtxt->rhs[j] ;
	
	//Reset the original right hand side
	aVars->rhs = origRHS ;
	origRHS = NULL ;
}

/** Initialises the memory requried for a GMRES context on the given grid
 *	\param [in] grid	The grid on which to discretize
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] ctxt	The GMRES context to initialise
 *	\param [in] reset	The number of iterations of GMRES to perform before
 *						resetting the algorithm
 */
void initialise_gmres_context( const GridParameters *grid,
	const AlgorithmParameters *params, FGMResContext *ctxt, int reset )
{
	int i ;
	
	ctxt->reset = reset ; 
	ctxt->currIt = 0 ; 
	ctxt->origResidNorm = -1.0 ;
	ctxt->subSpaceMat = (double**)calloc( reset+1, sizeof( double* ) ) ;
	ctxt->precSpaceMat = (double**)calloc( reset+1, sizeof( double* ) ) ;
	for( i=0 ; i<reset+1 ; i++ ){
		ctxt->subSpaceMat[i] = NULL ;
		ctxt->precSpaceMat[i] = NULL ;
	}
	if( params->testCase == 12 || params->gmresPrecSPart )
		ctxt->precMat = (double*)calloc( grid->numMatrixEntries,
			sizeof( double ) ) ;
	else ctxt->precMat = NULL ;
	ctxt->lsCtxt = (LeastSquaresContext*)calloc( 1,
		sizeof( LeastSquaresContext ) ) ;
	//Initialise the least squares context
	initialise_least_squares_context( ctxt->lsCtxt, reset ) ;
}

/** Frees the memory allocated to a GMRES context
 *	\param [in] ctxt	The GMRES context for which to free the memory
 */
void free_gmres_context( FGMResContext *ctxt )
{
	int i ;
	if( ctxt->subSpaceMat != NULL ){
		for( i=0 ; i < ctxt->reset ; i++ ){
			if( ctxt->subSpaceMat[i] != NULL ){
				free( ctxt->subSpaceMat[i] ) ;
				ctxt->subSpaceMat[i] = NULL ;
			}
		}
		free( ctxt->subSpaceMat ) ;
		ctxt->subSpaceMat = NULL ;
	}
	if( ctxt->precSpaceMat != NULL ){
		for( i=0 ; i < ctxt->reset ; i++ ){
			if( ctxt->precSpaceMat[i] != NULL ){
				free( ctxt->precSpaceMat[i] ) ;
				ctxt->precSpaceMat[i] = NULL ;
			}
		}
		free( ctxt->precSpaceMat ) ;
		ctxt->precSpaceMat = NULL ;
	}
	if( ctxt->precMat != NULL )
		free( ctxt->precMat ) ;
	if( ctxt->lsCtxt != NULL ){
		free_least_squares_context( ctxt->lsCtxt ) ;
		free( ctxt->lsCtxt ) ;
		ctxt->lsCtxt = NULL ;
	}
}

/** Solves the least squares problem to miminise \f$|| r - Hy ||\f$ for
 *	\f$y\f$ where \f$H\f$ is a Hessenberg matrix. We also make an assumption on
 *	\f$r\f$ according to the algorithm given in 'Iterative Methods for Sparse
 *	Linear Systems, Saad Y., 2003, SIAM', pg. 175. In particular we note that
 *	\f$r\f$ only has non-zero entry in the first position.
 *	\param [in,out] ctxt	The LeastSquares context for which to perform the
 *							least squares solve on. The desired output is the
 *							value of LestSquaresCtxt::rhs, which is the
 *							minimizer \f$y\f$ of \f$|| r - Hy ||\f$
 */
void least_squares( LeastSquaresContext *ctxt )
{
	//First thing to do is to transform the Hessenberg matrix into an upper
	//triangular matrix
	hess_to_upper( ctxt ) ;
	
	//Now that I have got a matrix in upper triangular form I can solve the
	//system by back substitution
	back_sub( (const double**)ctxt->upTMat, ctxt->currSize, ctxt->rhs ) ;
	
	//This is all that there is to the least squares for this application
}

/** Solves a system by back substitution. It is assumed that the matrix supplied
 *	is in upper triangular form, and that the matrix is stored as a set of
 *	column vectors. The solution is written the the vector \e rhs that is passed
 *	in
 *	\param [in] mat		An upper triangular matrix stored as an array of columns
 *	\param [in] dim		The dimension of the upper triangular matrix
 *	\param [in,out] rhs	The right hand side of the system to solve on input, and
 *						as output stores the value of the solution to the upper
 *						triangular system
 */
void back_sub( const double **mat, int dim, double *rhs )
{
	int i, j ; //Loop counter
	double tmp ;
	
	//Loop through the rows
	for( i = dim-1 ; i >= 0 ; i-- ){
		//Find out what contribution should be deducted from the right hand side
		tmp = 0.0 ;
		for( j=i+1 ; j < dim ; j++ )
			tmp += rhs[j] * mat[j][i] ;
		//Calculate the value in the i^th position in the solution
		rhs[i] = (rhs[i] - tmp) / mat[i][i] ;
	}
}

/** This transforms a Hessenberg matrix into an upper triangular matrix. This is
 *	used in the calculation of the least squares approximation to a small system
 *	in the GMRES iteration
 *	\param [in,out] ctxt	The context for the GMRES iteration
 */
void hess_to_upper( LeastSquaresContext *ctxt )
{
	int itCnt, i, j ; //Loop variables
	double c, s ; //The values that are to be in the matrix to multiply the
				  //Hessenberg matrix
	double fact ;
	double *row[2] ; //Stores the updated values to the rows in the
					  //transformation of the Hessenberg matrix into an
					  //upper triangular matrix
	double rhsUpdate[2] ; //Stores the values with which to update the rhs
						  //vector
	
	double **hMat = ctxt->hessMat ;
	double **upTMat = ctxt->upTMat ;
	double *g = ctxt->rhs ;
	
	//Copy all of the values from the Hessenberg matrix into the upper
	//triangular matrix
	//Loop through the columns
	for( j=0 ; j < ctxt->currSize ; j++ ){
		//Loop through the rows
		for( i=0 ; i < j+2 ; i++ ){
			//Copy the values accross
			upTMat[j][i] = hMat[j][i] ;
		}
	}
	
	hMat = upTMat ;
	
	for( i=0 ; i < 2 ; i++ )
		row[i] = (double*)calloc( ctxt->currSize, sizeof( double ) ) ;
	
	//We iterate up to the current iteration number stored in the FGMResContext
	for( itCnt=0 ; itCnt < ctxt->currSize ; itCnt++ ){
		//Calculate the values by which to multiply the Hessenberg matrix
		s = hMat[itCnt][itCnt+1] ;
		c = hMat[itCnt][itCnt] ;
		fact = sqrt( pow( c, 2 ) + pow( s, 2 ) ) ;
		c /= fact ;
		s /= fact ;
		
		//Apply the multiplication of the matrix with the block [c, s; -c, s] on
		//the i^th diagonal to the Hessenberg matrix. We first set the value
		//in the matrix to zero at row 'itCnt+1' and column 'itCnt'
		for( j = itCnt ; j < ctxt->currSize ; j++ ){
			//The only rows that are changed are row 'itCnt' and row 'itCnt+1'
			row[0][j] = hMat[j][itCnt] * c  + hMat[j][itCnt+1] * s ;
			row[1][j] = -hMat[j][itCnt] * s + hMat[j][itCnt+1] * c;
		}
		rhsUpdate[0] = g[itCnt]*c ;
		rhsUpdate[1] = -g[itCnt]*s ;
		
		//Now update the appropriate rows in the matrix and the rhs
		//Loop through the rows
		for( i=0 ; i<2 ; i++ ){
			g[i+itCnt] = rhsUpdate[i] ;
			//Loop through the columns
			for( j=itCnt ; j < ctxt->currSize ; j++ )
				hMat[j][i+itCnt] = row[i][j] ;
		}
	}
	
	//Clean up
	for( i=0 ; i<2 ; i++ )
		free( row[i] ) ;	
}

/** Initialises a least squares context with the maximum size it can be. This is
 *	so that we can re-use the context for least squares problems of size less
 *	than or equal to \e maxSize
 *	\param [in] ctxt	The least squares context to be initialised
 *	\param [in] maxSize	The maximum size of least squares problem that will be
 *						solved using this context
 */
void initialise_least_squares_context( LeastSquaresContext *ctxt, int maxSize )
{
	int i ; //Loop counter
	ctxt->maxSize = maxSize ;
	ctxt->currSize = 0 ;
	//The Hessenberg matrix is going to be of dimension (reset+!,reset), and it
	//is stored as arrays of columns of the matrix
	ctxt->hessMat = (double**)calloc( maxSize, sizeof( double* ) ) ;
	ctxt->upTMat = (double**)calloc( maxSize, sizeof( double* ) ) ;
	ctxt->rhs = (double*)calloc( maxSize+1, sizeof( double ) ) ;
	//The subspace matrix is of maximum dimension (reset,reset), and it is
	//stored as arrays of columns in the matrix
	for( i=0 ; i < maxSize ; i++ ){
		//We know that column i in the Hessenberg matrix has i+2 entries (if i
		//is zero based)
		ctxt->hessMat[i] = (double*)calloc( i+2, sizeof( double ) ) ;
		ctxt->upTMat[i] = (double*)calloc( i+2, sizeof( double ) ) ;
	}
}

/** Frees the memory allocated to the given gmresContext
 *	\param [in] ctxt	The least squares context for which to free the
 *						allocated memory
 */
void free_least_squares_context( LeastSquaresContext *ctxt )
{
	int i ; //Loop counter
	if( ctxt->hessMat != NULL ){
		for( i=0 ; i < ctxt->maxSize ; i++ ){
			if( ctxt->hessMat[i] != NULL ){
				free( ctxt->hessMat[i] ) ;
				ctxt->hessMat[i] = NULL ;
			}
		}
		free( ctxt->hessMat ) ;
		ctxt->hessMat = NULL ;
	}
	if( ctxt->upTMat != NULL ){
		for( i=0 ; i < ctxt->maxSize ; i++ ){
			if( ctxt->upTMat[i] != NULL ){
				free( ctxt->upTMat[i] ) ;
				ctxt->upTMat[i] = NULL ;
			}
		}
		free( ctxt->upTMat ) ;
		ctxt->upTMat = NULL ;
	}
	if( ctxt->rhs != NULL ){
		free( ctxt->rhs ) ;
		ctxt->rhs = NULL ;
	}
}

/** Initialises the hierarchy of grids to null, and sets them to \p NULL
 *	\param [in,out] grids	A hierarhcy of grids to set to \p NULL
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 */
void initialise_grids_to_null( GridParameters **grids,
	const AlgorithmParameters *params )
{
	int i ; //Loop counter
	
	//Set all pointers to NULL. I think that the zero values that are set in
	//calloc should be interpreted as NULL, but it doesn't cost much effort to
	//set the values here
	for( i = 0 ; i < params->fGridLevel ; i++ )
		grids[i] = NULL ;
	
}

/** Sets up a single grid by reading information from a file
 *	\param [out] grid	The grid to set up
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] filename	The name of the file from which to read grid
 *							information
 *	\return \p int value indicating the success or failure of the method. A
 *			value zero indicates that the grid was not set up, else it was
 */
int init_grid( GridParameters *grid, const AlgorithmParameters *params,
	const char *filename )
{
	int i, j, elCnt ; //Loop counters
	int ntype ; //The type of node that is read in. For the different types of
				//nodes see 'useful_definitions.h'
	int numDBnd ; //Counts the number of Dirichlet boundary nodes on the grid
	int bndType[4] ; //Stores the type of boundaries on the grid
				
	Node *node ; //Placeholder for a node on the grid. Used to make the code
				 //more presentable
	Element *e ; //Placeholder for an element on the grid. Used to make the code
				 //more presentable
	Node *n1, *n2 ; //Placeholders for nodes on the grid
				 
	FILE *gridFile ; //The file from which to read the grid information

	//Open the file for reading
	gridFile = fopen( filename, "r" ) ;
	
	//If we couldn't open the file then print an error message and return
	if( gridFile == NULL ){
		perror( "Error opening grid file\n" ) ;
		return 0 ;
	}
	
	//Read from the first line the number of nodes, and the number of elements
	//on the grid
	fscanf( gridFile, "%d %d", &(grid->numNodes), &(grid->numElements) ) ;
	
	//Read from the second line the number of columns and nodes, respectively
	fscanf( gridFile, "%d %d", &(grid->numCols), &(grid->numRows) ) ;
	
	//Read from the third line the type of boundary that we have
	fscanf( gridFile, "%d %d %d %d", &(bndType[0]), &(bndType[1]),
		&(bndType[2]), &(bndType[3]) ) ;
	//Now store the boundary types in the correct format in the grid object
	for( i=0 ; i < 4 ; i++ )
		grid->bndType[i] = (enum NODE_TYPE)bndType[i] ;
	
	//Set the appropriate memory for the nodes on the grid, the mapping from the
	//grid to the approximation, and vice versa
	grid->nodes = (Node*)calloc( grid->numNodes, sizeof( Node ) ) ;
	grid->mapG2A = (int*)calloc( grid->numNodes, sizeof( int ) ) ;
	grid->mapA2G = (int*)calloc( grid->numNodes, sizeof( int ) ) ;
	
	//Initialise the number of 'unknowns' (i.e. non Dirichlet boundary nodes) on
	//the grid to be zero
	grid->numUnknowns = 0 ;
	//Initialise the number of Dirichlet boundary nodes to be zero
	numDBnd = 0 ;
	
	//Now read in the nodes one by one from the file
	for( i=0 ; i < grid->numNodes ; i++ ){
		//Get the placeholder to the current node in the grid
		node = &(grid->nodes[i]) ;
		//Set the ID of the node to be the position in the list of nodes on the
		//grid
		node->id = i ;
		
		//Read the type of node, and the coordinate for the current node
		fscanf( gridFile, "%d %lf %lf", &ntype, &(node->x), &(node->y) ) ;
		//Set the type of node that we have
		node->type = (ntype < 0) ? (enum NODE_TYPE)ntype : NODE_INTERIOR ;
		
		if( node->type != NODE_DIR_BNDRY ){
			//If we are not at a Dirichlet boundary node we put the node into
			//the approximation, and increase the count of the number of
			//unknowns
			grid->mapA2G[ grid->numUnknowns ] = i ;
			grid->mapG2A[i] = grid->numUnknowns++ ;
		}
		else{ //Dirichlet boundary
			//If we are at a Dirichlet boundary node we insert the node at the
			//end of the approximation, such that the Dirichlet boundary nodes
			//are stored in reverse order after the non Dirichlet boundary nodes
			grid->mapA2G[grid->numNodes - (++numDBnd)] = i ;
			grid->mapG2A[i] = grid->numNodes - numDBnd ;
		}
		
		//Set the first element in the neighbourhood of the node to be the
		//current node, and set the size of the neighbourhood to be one
		node->nghbrs[0] = i ;
		node->numNghbrs = 1 ;
		//Set the number of Dirichlet boundary neighbours of the node to zero
		//initially
		node->numDirNghbrs = 0 ;
		node->weightFact = 1.0 ;
	}
	
	//We assume that the grid has been stored in row order, that is the nodes
	//are stored row by row. We also assume that the grid spacing is uniform on
	//the mesh, such that the grid spacing can be calculated by taking the
	//difference between the first and second x-coordinate of the nodes on the
	//grid
	grid->gridSpacing = grid->nodes[1].x - grid->nodes[0].x ;
	
	//We know that the matrix must have at least the diagonals in there, so
	//we set this up to have at least as many entries as there are unknowns
	grid->numMatrixEntries = grid->numUnknowns ;
	
	//Assign the memory for the elements on the grid
	grid->elements = (Element*)calloc( grid->numElements, sizeof( Element ) ) ;
	
//	grid->numBndEls = 0 ; //Set the count of the boundary elements to zero
//	tmpBndEl = (int*)calloc( grid->numElements, sizeof( int ) ) ;
	//Read in the elements from file one by one, and record the connections
	//between the nodes
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Set the placeholder for the current element on the grid
		e = &(grid->elements[elCnt]) ;
		
		//Set the ID of the element to be the value of the counter
		e->id = elCnt ;
		
		//Read in the nodes for the element from file
		fscanf( gridFile, "%d %d %d", &(e->nodeIDs[0]),
			&(e->nodeIDs[1]), &(e->nodeIDs[2]) ) ;
			
		//Calculate the area of the element
		e->area = element_area( grid, e ) ;
			
		//if( isBndEl( e, grid ) ) tmpBndEl[grid->numBndEls++] = elCnt ;
		//Record the connections between the nodes
		for( i=0 ; i < 3 ; i++ ){
			n1 = &(grid->nodes[ e->nodeIDs[i] ]) ;
			//If the current node is a Dirichlet boundary node then we move onto
			//the next one
			if( n1->type == NODE_DIR_BNDRY ) continue ;
			//Loop through the nodes on the element
			for( j=0 ; j < 3 ; j++ ){
				n2 = &(grid->nodes[ e->nodeIDs[j] ]) ;
				//Record a connection if i and j are different, as we have
				//already recorded the connection from each node to itself
				if( n1->id == n2->id ) continue ;
				
				//If the connection does not already exist, record the
				//connection from n1 to n2
				if( connected( n1, n2 ) == -1 ){
					//Increase the count of the number of neighbours of n1, and
					//also increase the count for the number of nonzero
					//elements in the matrix structure
					n1->weightFact += 0.5 ;
					if( n2->type != NODE_DIR_BNDRY ){
						grid->numMatrixEntries++ ;
						n1->nghbrs[ n1->numNghbrs++ ] = n2->id ;
					}
					else
						n1->nghbrs[ SPARSE - 1 - n1->numDirNghbrs++ ] = n2->id ;
				}
			}
		}
	}
	
	//Clean up
	fclose( gridFile ) ;
	
	//We have now stored the nodes, the elements and the connectivity of the
	//nodes. All I have to do now is to store the indices into the sparse
	//matrix for the rows corresponding to the indices
	grid->rowStartInds = (int*)calloc( grid->numUnknowns, sizeof( int ) ) ;
	grid->rowStartInds[0] = 0 ;
	for( i=1 ; i < grid->numUnknowns ; i++ )
		grid->rowStartInds[i] = grid->rowStartInds[i-1] +
			grid->nodes[grid->mapA2G[i-1]].numNghbrs ;
			
	//If we are performing a Richards equation type problem set up the soil
	//types on the domain
	if( is_richards_eq( params->testCase ) )
		set_up_soil_types( grid, params ) ;
	else{
		grid->soilType = NULL ;
	}
	
	//Return 1, indicating success
	return 1 ;
}

/** Sets up the indices at the end of a substrate of soil on the given grid
 *	\param [out] grid	The grid on which we are currently operating
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 */
void set_up_soil_types( GridParameters *grid,
	const AlgorithmParameters *params )
{
	int i, soilCnt ; //Loop counters
	double *zLimits ; //Limits at which the soil type changes
	int numSoils ; //Placeholder for the number of soils under consideration
	const Node* node ; //A node on the grid
	
	
	grid->soilType = (int*)malloc( grid->numNodes * sizeof( int ) ) ;
	
	if( params->testCase == 14 || params->testCase == 16 ||
		params->testCase == 17 || params->testCase == 19 ||
		params->testCase == 101 || params->testCase == 102 ){
		//Set the type of soil to be zero everywhere, as there is only one soil
		//type
		set_ivect_to_zero( grid->numNodes, grid->soilType ) ;
	}
	else if( params->testCase == 15 || params->testCase == 18 ||
		params->testCase == 20 ){
		numSoils = params->reCtxt.numSoils ;
		//Three soil types, which are not changeable at the moment, but this may
		//change in the future
		
		//Here we give away that I am limiting myself to three soil types, but
		//I only want to have this many for the time being. This may change
		//later
		zLimits = (double*)malloc( 2 * sizeof( double ) ) ;
		zLimits[0] = -62.5 ;
		zLimits[1] = -25.0 ;
		i = 0 ;
		while( grid->nodes[i].y < zLimits[0] )
			grid->soilType[i++] = 0 ;

		for( soilCnt = 1 ; soilCnt < numSoils-1 ; soilCnt++ ){
			//While we are in a given stratum increase the count of the nodes
			while( grid->nodes[i].y < zLimits[soilCnt] )
				grid->soilType[i++] = soilCnt ;
		}
		while( i < grid->numNodes )
			grid->soilType[i++] = numSoils-1 ;
		
		//Clean up
		free( zLimits ) ;
	}
	else if( params->testCase == 21 ){
		for( i =0 ; i < grid->numNodes ; i++ ){
			node = &( grid->nodes[i] ) ;
			if( (node->y >= -25.0 && node->x < 75.0) || ( (node->y < -50.0 &&
				node->y >= -75.0) && node->x >= 25.0 ) )
				grid->soilType[i] = 1 ;
			else
				grid->soilType[i] = 0 ;
		}
	}
	else if( params->testCase == 23 ){
		//For this test case we have two rectangles of different type soils that
		//are protruding into the domain, but not all the way across.
		for( i=0 ; i < grid->numNodes ; i++ ){
			node = &( grid->nodes[i] ) ;
			if( ( ( node->y < -23.4375 && node->y >= -50.0 ) &&
				node->x < 60.9375 ) ||
				(node->y < -73.4375 && node->x >= 39.0625) )
				grid->soilType[i] = 1 ;
			else
				grid->soilType[i] = 0 ;
		}
	}
	else if( params->testCase == 22 ){
		for( i= 0 ; i < grid->numNodes ; i++ ){
			node = &(grid->nodes[i]) ;
			if( node->y < -43.75 && node->y >= -93.75 && node->x >= 6.25 &&
				node->x < 56.25 )
				grid->soilType[i] = 1 ;
			else
				grid->soilType[i] = 0 ;
		}
	}
	else if( params->testCase == 24 || params->testCase == 25 ){
		for( i=0 ; i < grid->numNodes ; i++ ){
			node = &(grid->nodes[i]) ;
			if( node->x >= 12.5 && node->x < 21.875 && node->y < -29.6875 )
				grid->soilType[i] = 1 ;
			else if( node->y < -29.6875 && node->y >= -39.0625
				&& node->x >= 21.875 && node->x < 43.75 )
				grid->soilType[i] = 1 ;
			else if( node->y <= -29.6875 && node->y >= -70.3125 &&
				node->x >= 34.375 && node->x < 43.75 )
				grid->soilType[i] = 1 ;
			else if( node->y < -60.9375 && node->y >= -70.3125 &&
				node->x >= 43.75 )
				grid->soilType[i] =1 ;
			else
				grid->soilType[i] = 0 ;
		}
	}
	else if( params->testCase == 26 ){
		double xRemainder, yRemainder ;
		int row ;
		for( i=0 ; i < grid->numNodes ; i++ ){
			//Get the current node on the grid
			node = &(grid->nodes[i]) ;
			if( node->type != NODE_INTERIOR || node->y < -90.0 ||
				node->x > 95.0 ){
				grid->soilType[i] = 0 ;
				continue ;
			}
			
			xRemainder = fmod( node->x, 10.0 ) ;
			yRemainder = fmod( node->y, 10.0 ) ;
//			printf( "Node: %.10f, %.10f\n\tx-remainder: %.10f\t", node->x,
//				node->y, xRemainder ) ;
//			printf( "y-remainder: %.10f\t", yRemainder ) ;
			if( yRemainder > -5.0 ){
//				printf( "\n" ) ;
				grid->soilType[i] = 0 ; 
				continue ;
			}
			
			row = (int)( fabs( node->y - yRemainder ) / 10 ) ;
//			printf( "row: %d\n", row ) ;
			
			if( row % 2 == 0 && xRemainder < 5.0 ){
				grid->soilType[i] = 0 ;
				continue ;
			}
			if( row % 2 == 1 && (xRemainder >= 5.0 || node->x < 10.0)){
				grid->soilType[i] = 0 ;
				continue ;
			}
			
			grid->soilType[i] = 1 ;
		}
	}
	else if( params->testCase == 27 ){
		for( i=0 ; i < grid->numNodes ; i++ ){
			node = &(grid->nodes[i]) ;
			if( node->y >= -25 || node->y < -75 ){
				grid->soilType[i] = 0 ;
			}
			else if( (node->y >= -50 && node->y < -37.5 ) ||
				((node->x < 25 || node->x >=75) && node->y >= -50
				&& node->y < -25) ||
				(node->x >= 25 && node->x < 75 && node->y >= -75 &&
				node->y < -37.5) ){
				grid->soilType[i] = 1 ;
			}
			else{
				grid->soilType[i] = 0 ;
			}
		}
	}
		
}

/** Indicator as to whether the given element on the given grid is a boundary
 *	element or not
 *	\param [in] e	The element to check
 *	\param [in] grid	The grid on which the element exists
 *	\return \p int value indicating whether or not the element has an
 *			intersection with the bondary of non-zero Lebesgue measure (i.e.
 *			that there is more than one point of the element on the boundary)
 */
int isBndEl( const Element *e, const GridParameters *grid )
{
	int i ; //Loop counter
	int intCount ; //Counts the number of interior nodes on the grid
	
	intCount = 0 ;
	for( i=0 ; i<3 ; i++ ){
		if( grid->nodes[ e->nodeIDs[i] ].type == NODE_INTERIOR ) intCount++ ;
	}
	if( intCount < 2 ) return 1 ;
	else return 0 ;
}

/** Frees the memory for all of the grids in the hierarchy
 *	\param [in] grids	A hierarhcy of grids available to use
 *	\param [in] numGrids	The number of grids in the hierarchy
 */
void free_grids( GridParameters **grids, int numGrids )
{
	int i ; //Loop counter
	
	//Loop through all of the grids
	for( i=0 ; i < numGrids ; i++ ){
		//If the grid pointer is not null, free the grid
		if( grids[i] != NULL )
			free_grid( grids[i] ) ;
			free( grids[i] ) ;
	}
		
}

/** Frees the memory for a single grid parameters structure
 *	\param [in] grid	The grid to free memory for
 */
void free_grid( GridParameters* grid )
{
	if( grid->elements != NULL ){
		free( grid->elements ) ;
		grid->elements = NULL ;
	}
	if( grid->nodes != NULL ){
		free( grid->nodes ) ;
		grid->nodes = NULL ;
	}
	if( grid->mapG2A != NULL ){
		free( grid->mapG2A ) ;
		grid->mapG2A = NULL ;
	}
	if( grid->mapA2G != NULL ){
		free( grid->mapA2G ) ;
		grid->mapA2G = NULL ;
	}
	if( grid->rowStartInds != NULL ){
		free( grid->rowStartInds ) ;
		grid->rowStartInds = NULL ;
	}
	if( grid->soilType != NULL ){
		free( grid->soilType ) ;
		grid->soilType = NULL ;
	}
}

/** Initialises the hierarchy of approximation variables to \p NULL
 *	\param [in] aVarsList	A hierarchy of approximation variables to set to
 *							\p NULL
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 */
void initialise_approx_vars_to_null( ApproxVars **aVarsList,
	const AlgorithmParameters *params )
{
	int i ; //Loop counter
	
	//Set all pointers to NULL. I think that the zero values that are set in
	//calloc should be interpreted as NULL, but it doesn't cost much effort to
	//set the values here
	for( i = 0 ; i < params->fGridLevel ; i++ )
		aVarsList[i] = NULL ;
	
}

/** Sets up the approximation variables structure for a given grid
 *	\param [in,out] aVars	The approximation variables structure to update
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 */
void init_approx_vars( ApproxVars* aVars, const GridParameters* grid,
	const AlgorithmParameters* params )
{
	int i ; //Loop counter
	
	//Allocate the correct amount of memory to each of the members of the
	//ApproxParams structure
	aVars->approx = (double*) calloc( grid->numNodes, sizeof( double ) ) ;
	aVars->rhs = (double*) calloc( grid->numUnknowns, sizeof( double ) ) ;
	aVars->bndTerm = (double*) calloc( grid->numUnknowns, sizeof( double ) ) ;
	aVars->resid = (double*) calloc( grid->numUnknowns, sizeof( double ) ) ;
	aVars->iterMat = (double*) calloc( grid->numMatrixEntries,
		sizeof( double ) ) ;
	aVars->tmpApprox = (double*) calloc( grid->numNodes, sizeof( double ) ) ;
		
	if( is_non_linear( params->testCase ) && params->nlMethod == METHOD_NLMG )
		aVars->jDiags = (double*) calloc( grid->numUnknowns,
			sizeof( double ) ) ;
	else
		aVars->jDiags = NULL ;
		
	if( is_time_dependent( params->testCase ) || params->testCase == 11 ){
		aVars->massMat = (double*) calloc( grid->numMatrixEntries,
			sizeof( double ) ) ;
		//The mass matrix is calculated here, as it does not change for the
		//entire running of the algorithm
		if( params->lumpMassMat )
			calc_lumped_mass_matrix( grid, params, aVars->massMat ) ;
		else
			calculate_mass_matrix( grid, params, aVars->massMat ) ;
	}
	else
		aVars->massMat = NULL ;
		
	if( params->recoverGrads ){
		aVars->recGrads = (CVector2D*) calloc( grid->numNodes,
			sizeof( CVector2D ) ) ;
		//As calloc is used above this should mean that the member variables of
		//aVars->recGrads should be initialised to zero.
	}
	else
		aVars->recGrads = NULL ;
		
	if( is_richards_eq( params->testCase ) ){
		//We are solving a Richards equation type problem. We set the memory
		//for the appropriate variables
		aVars->reTheta = (double*)calloc( grid->numNodes, sizeof( double ) ) ;
		aVars->reHCond = (double*)calloc( grid->numNodes, sizeof( double ) ) ;
		aVars->reHCondDeriv = (double*)calloc( grid->numNodes,
			sizeof( double ) ) ;
	}
	else{
		aVars->reTheta = NULL ;
		aVars->reHCond = NULL ;
		aVars->reHCondDeriv = NULL ;
	}
	
	if( has_pc_coeff( params->testCase ) ){
		//If the test case requires a piecewise constant coefficient we
		//calculate the values in the coefficient
		aVars->pcCoeff = (double*)calloc( grid->numElements,
			sizeof( double ) ) ;
		//Only calculate the piecewise constant coefficient on the finest grid
		//level. We restrict to the coarser grid levels, assuming that the
		//coefficient is fully resolved on the fine grid.
		if( params->currLevel == params->fGridLevel )
			set_pc_coeff_vals( params, grid, aVars->pcCoeff ) ;
	}
	else
		aVars->pcCoeff = NULL ;

	//Set the user context to be null, as we don't know if we will be using it
	//or not. The user should initialise this before they want to use it
	aVars->usrCtxt = NULL ;
		
	//Now assign the Dirichlet boundary values to the back of the approximation.
	//This is only done on the fine grid level, or for nonlinear multigrid on
	//every level. This is because in the linear iteration we have that the
	//boundary conditions become homogeneous on the coarse grids. If we do not
	//set the Dirichlet boundary, this means it stays at the value it is
	//initialised to, which is zero, as we use calloc for the memory allocation
	if( params->currLevel == params->fGridLevel ||
		(is_non_linear(params->testCase) && params->nlMethod == METHOD_NLMG) ){
		set_dirichlet_boundary( params, grid, aVars->approx ) ;
	}
	
	if( is_richards_eq( params->testCase ) ){
		//If we are performing a Richards equation type problem we set up the
		//values of the volumetric wetness and the hydraulic conductivity on
		//the boundary
		for( i= grid->numNodes-1 ; i >= grid->numNodes - grid->numUnknowns ;
			i-- ){
			aVars->reTheta[i] = calc_theta( &(params->reCtxt),
				grid->soilType[ grid->mapA2G[ i ] ], aVars->approx[i] ) ;
			aVars->reHCond[i] = calc_hydr_cond( &(params->reCtxt),
				grid->soilType[ grid->mapA2G[ i ] ], aVars->approx[i] ) ;
		}
	}
}

/** Frees the memory in a list of ApproxVars structures
 *	\param [in] aVarList	The list of approximation variables to free the
 *							memory for
 *	\param [in] numGrids	The number of structures in the hierarchy
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 */
void free_approx_var_list( ApproxVars **aVarList, int numGrids,
	const AlgorithmParameters *params )
{
	int i ; //Loop counter
	//Free each of the ApproxVars constructs to which we point
	for( i=0 ; i < numGrids ; i++ ){
		if( aVarList[i] != NULL ){
			free_approx_vars( aVarList[i], params ) ;
			free( aVarList[i] ) ;
			aVarList[i] = NULL ;
		}
	}
}

/** Frees the memory taken by a single ApproxVars structure
 *	\param [in] aVars	The approximation variables structure to free the memory
 *						for
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 */
void free_approx_vars( ApproxVars* aVars, const AlgorithmParameters *params )
{	
	if( aVars->approx != NULL ){
		free( aVars->approx ) ;
		aVars->approx = NULL ;
	}
	if( aVars->rhs != NULL ){
		free( aVars->rhs ) ;
		aVars->approx = NULL ;
	}
	if( aVars->bndTerm != NULL ){
		free( aVars->bndTerm ) ;
		aVars->bndTerm = NULL ;
	}
	if( aVars->resid != NULL ){
		free( aVars->resid ) ;
		aVars->approx = NULL ;
	}
	if( aVars->iterMat != NULL ){
		free( aVars->iterMat ) ;
		aVars->approx = NULL ;
	}
	if( aVars->tmpApprox != NULL ){
		free( aVars->tmpApprox ) ;
		aVars->tmpApprox = NULL ;
	}
	if( aVars->jDiags != NULL ){
		free( aVars->jDiags ) ;
		aVars->jDiags = NULL ;
	}
	if( aVars->recGrads != NULL ){
		free( aVars->recGrads ) ;
		aVars->recGrads = NULL ;
	}
	if( aVars->massMat != NULL ){
		free( aVars->massMat ) ;
		aVars->massMat = NULL ;
	}
	if( aVars->usrCtxt != NULL ){
		if( params->innerIt == IT_PCGMRES )
			free_gmres_context( (FGMResContext*)aVars->usrCtxt ) ;
		else if( params->innerIt == IT_PCG )
			free_cg_context( (CGContext*)aVars->usrCtxt ) ;
		free( aVars->usrCtxt ) ;
		aVars->usrCtxt = NULL ;
	}
	if( aVars->reTheta != NULL ){
		free( aVars->reTheta ) ;
		aVars->reTheta = NULL ;
	}
	if( aVars->reHCond != NULL ){
		free( aVars->reHCond ) ;
		aVars->reHCond = NULL ;
	}
	if( aVars->reHCondDeriv != NULL ){
		free( aVars->reHCondDeriv ) ;
		aVars->reHCondDeriv = NULL ;
	}
	if( aVars->pcCoeff != NULL ){
		free( aVars->pcCoeff ) ;
		aVars->pcCoeff = NULL ;
	}
}

/** Prints the given grid parameters to a specified file
 *	\param [in] grid	The grid to print the parameters to file for
 *	\param [in] filename	The name of the file to print information to
 */
void print_grid_params_to_file( const GridParameters* grid,
	const char* filename )
{
	FILE* outFile ;
	
	//Open the file for writing
	outFile = fopen( filename, "w" ) ;
	
	fprintf( outFile, "GridParameters:\n\t" ) ;
	fprintf( outFile, "Number of nodes on the grid: %d\n\t", grid->numNodes ) ;
	fprintf( outFile, "Number of triangles on the grid: %d\n\t",
		grid->numElements ) ;
	fprintf( outFile, "Number of non Dirichlet boundary nodes on the grid: " ) ;
	fprintf( outFile, "%d\n\t", grid->numUnknowns ) ;
	fprintf( outFile, "Number of nonzero entries in the operator: %d\n\t",
		grid->numMatrixEntries ) ;
	fprintf( outFile, "Not printing grid->boundary_values. Print this " ) ;
	fprintf( outFile, "separately if you would like to look at the values " ) ;
	fprintf( outFile, "in this array\n\t" ) ;
	fprintf( outFile, "Not printing grid->interior_node_index. Print this" ) ;
	fprintf( outFile, " separately if you would like to inspect the " ) ;
	fprintf( outFile, "values\n\t" ) ;
	fprintf( outFile, "Not printing grid->node_index. Print this separately" ) ;
	fprintf( outFile, " if you would like to inpsect the values\n\t" ) ;
	fprintf( outFile, "Not printing grid->row_start_indices. Print this" ) ;
	fprintf( outFile, " separately if you would like to inspect the " ) ;
	fprintf( outFile, "values\n\t" ) ;
	fprintf( outFile, "Not printing grid->connection_matrix. Print this" ) ;
	fprintf( outFile, " separately if you would like to inspect the " ) ;
	fprintf( outFile, "values\n\t" ) ;
	fprintf( outFile, "Not printing grid->triangle_areas. Print this " ) ;
	fprintf( outFile, "separately" ) ;
	fprintf( outFile, " if you would like to inpsect the values\n\t" ) ;
	fprintf( outFile, "Not printing grid->coordinates. Print this " ) ;
	fprintf( outFile, "separately if you would like to inpsect the " ) ;
	fprintf( outFile, "values\n\t" ) ;
	fprintf( outFile, "Not printing grid->triangles. Print this separately" ) ;
	fprintf( outFile, " if you would like to inpsect the values\n\t" ) ;
	fprintf( outFile, "Not printing grid->curr_approx. This is kept as a" ) ;
	fprintf( outFile, " placeholder in the Newton-MG iteration\n" ) ;
	
	//Close the file
	fclose( outFile ) ;
}

/** Performs iterations of a linear Gauss-Seidel iteration
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] numIts 	The number of iterations of Guass-Seidel to perform
 *	\param [in,out] aVars	Variables associated with the approximation. The
 *							member variable ApproxVars::approx is updated in
 *							this method
 */
void linear_gs( const GridParameters *grid, const AlgorithmParameters *params,
	int numIts, ApproxVars* aVars )
{
	int i, j, t;
	double temp;
	const Node *node ; //A node on the grid
	int rowInd ; //Index into the sparse matrix representation for the start of
				 //a row for a given node
	
	if( params->currLevel != params->fGridLevel ||
		is_richards_eq( params->testCase ) ){
		//Perform the given number of GS iterations
		for ( t = 0; t < numIts; t++ ){
			//For each node in the interior of the grid figure out what the
			//value of the operator applied at the node is, and update the
			//approximation using the appropriate value
			for ( i = 0; i < grid->numUnknowns; i++ ){
				temp = 0.0;
				//Get the current node on the grid
				node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
				rowInd = grid->rowStartInds[i] ;
				//Find the index into the column vector for the current node and
				//loop through until the index of the next node
				for ( j = 0 ; j < node->numNghbrs ; j++ )
					temp += aVars->iterMat[rowInd + j] *
						aVars->approx[grid->mapG2A[ node->nghbrs[j] ]];
				
				//Update the value of the approximation at the current node
				aVars->approx[i] += params->smoothWeight * 
					( aVars->rhs[i] - temp ) / aVars->iterMat[ rowInd ] ;
			}
		}
	}
	else{
		for( t=0 ; t < numIts ; t++ ){
			//For each node in the interior of the grid figure out what the
			//value of the operator applied at the node is, and update the
			//approximation using the appropriate value
			for( i=0 ; i < grid->numUnknowns; i++ ){
				temp = 0.0 ;
				//Get the current node on the grid
				node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
				rowInd = grid->rowStartInds[i] ;
				//Find the index into the column vector for the current node and
				//loop through until the index of the next node
				for( j= 0 ; j < node->numNghbrs ; j++ ){
					temp += aVars->iterMat[rowInd + j] *
						aVars->approx[grid->mapG2A[ node->nghbrs[j] ] ] ;
				}
						
				//Update the value of the approximation at the current node,
				//including the effects of the boundary terms
				aVars->approx[i] += params->smoothWeight * 
					(aVars->rhs[i] + aVars->bndTerm[i] - temp) /
					aVars->iterMat[ rowInd ] ;
			}
		}
	}
}

/** Performs iterations of a linear Jacobi iteration
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] numIts 	The number of iterations of Jacobi to perform
 *	\param [in,out] aVars	Variables associated with the approximation. The
 *							member variable ApproxVars::approx is updated in
 *							this method
 */
void linear_jacobi( const GridParameters* grid,
	const AlgorithmParameters* params, int numIts, ApproxVars* aVars )
{
	int i, j, rowInd ;
	int itCnt ;
	double temp;
	const Node *node ;
	
	//Perform a certain number of Jacobi iterations
	if( params->currLevel != params->fGridLevel ||
		is_richards_eq( params->testCase ) ){
		for ( itCnt = 0; itCnt < numIts; itCnt++ ){
			copy_vectors( grid->numUnknowns, aVars->tmpApprox, aVars->approx ) ;
			//For each node in the interior of the grid figure out what the
			//value of the operator applied at the node is, and update the
			//approximation using the appropriate value
			for ( i = 0; i < grid->numUnknowns; i++ ){
				temp = 0.0;
				//Get the current node on the grid
				node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
				//Get the index of the start of the row in the iteration matrix
				//for the current node
				rowInd = grid->rowStartInds[i] ;
				//Find the index into the column vector for the current node and
				//loop through until the index of the next node
				for ( j = 0; j < node->numNghbrs; j++ )
					temp += aVars->iterMat[rowInd + j] *
						aVars->tmpApprox[ grid->mapG2A[ node->nghbrs[j] ] ];
			
				aVars->approx[i] += params->smoothWeight *
					(aVars->rhs[i] - temp ) / aVars->iterMat[rowInd] ;
			}
		}
	}
	else{
		for( itCnt = 0 ; itCnt < numIts ; itCnt++ ){
			copy_vectors( grid->numUnknowns, aVars->tmpApprox, aVars->approx ) ;
			//For each node in the interior of the grid figure out waht the
			//value of the operator applied at the node is, and update the
			//approximation using the appropriate value
			for( i = 0 ; i < grid->numUnknowns ; i++ ){
				temp = 0.0 ;
				//Get the current node on the grid
				node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
				//Get the index of the start of the row in the iteration matrix
				//for the current node
				rowInd = grid->rowStartInds[i] ;
				//Find the index into the column vector for the current node and
				//loop through until the index of the next node
				for( j=0; j < node->numNghbrs; j++ )
					temp += aVars->iterMat[rowInd+j] *
						aVars->tmpApprox[grid->mapG2A[node->nghbrs[j] ] ];
						
				temp = aVars->approx[i] + (aVars->rhs[i] + aVars->bndTerm[i] -
					temp ) / aVars->iterMat[rowInd] ;
				aVars->approx[i] = (1.0 - params->smoothWeight) *
					aVars->approx[i] + params->smoothWeight * temp ;
			}
		}
	}
}

/** Performs smoothing iterations for the given parameters
 *	\param [in] grid	The grid on which to operate
 *	\param [in]	params	Parameters defining how the algorithm is to be executed
 *	\param [in] numIts	The number of smoothing iterations to perform
 *	\param [in] jacobian	The jacobian matrix. This is required for nonlinear
 *							block smoothing iterations
 *	\param [in,out] aVars	Variables associated with the approximation. The
 *							member variable ApproxVarsa::approx is updated in
 *							this method
 */
void smooth( const GridParameters* grid, const AlgorithmParameters* params,
	int numIts, double *jacobian, ApproxVars* aVars )
{
	if( !is_non_linear( params->testCase ) ||
		params->nlMethod == METHOD_NEWTON_MG ){
		if( params->smoothMethod == SMOOTH_GS )
			linear_gs( grid, params, numIts, aVars ) ;
		else if( params->smoothMethod == SMOOTH_JAC )
			linear_jacobi( grid, params, numIts, aVars ) ;
	}
	else{
		if( params->smoothMethod == SMOOTH_GS )
			nl_gauss_seidel( grid, params, numIts, aVars ) ;
		else if( params->smoothMethod == SMOOTH_JAC )
			nl_jacobi( grid, params, numIts, aVars ) ;
		else if( is_block_jacobi_smooth( params ) )
			nl_block_jacobi( grid, params, aVars, numIts, jacobian ) ;
	}
}

/** Performs iterations of liner Gauss-Seidel until a convergence critertion is
 *	met
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to operate
 *	\param [in,out] aVars	Variables associated with the approximation. The
 *							member variable ApproxVars::approx is updated in
 *							this method
 *	\param [in] tol	The tolerance for the absolute difference in iterates
 *					before the iteration executes
 *	\param [in] ignoreMax	If this is non-zero the number of iterations may
 *							surpass the maximum number prescribed by
 *							#MAX_SMOOTHS
 *	\return \p int value indicating the number of smoothing iterations performed
 */
int gs_to_converge( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars, double tol, int ignoreMax )
{
	int i, j, itCnt ;
	double diff, temp ;
	double maxdiff ;
	const Node *node ; //Placeholder for a node on the grid
	int nRow ; //Index into the iteration matrix for the row corresponding to
			   //a given node on the grid
	//Initialize the difference to be larger than the tolerance
	maxdiff = tol + 1.0 ;
	itCnt = 0 ;
	//Loop while the we are above the tolerance
	while( maxdiff > tol && (ignoreMax || itCnt < MAX_LIN_SMOOTHS) ){
		//Initialize the value of the supremum norm to be zero
		maxdiff = 0.0 ;
		//For each node
		for ( i = 0; i < grid->numUnknowns; i++ ){
			//Update the old approximation
			aVars->tmpApprox[i] = aVars->approx[i];
			temp = 0.0;
			node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
			nRow = grid->rowStartInds[i] ;
			//Get the current node on the grid
			//For each neighbour count a contribution
			for ( j = 0; j < node->numNghbrs; j++ ){
				temp += aVars->iterMat[nRow + j] *
					aVars->approx[grid->mapG2A[node->nghbrs[j]]];
			}
			//Update the current approximation and check the maximum error
			aVars->approx[i] += params->smoothWeight *
				( aVars->rhs[i] - temp ) / aVars->iterMat[nRow] ;
			diff = fabs( aVars->tmpApprox[i] - aVars->approx[i] ) ;
			maxdiff = ( maxdiff < diff ) ? diff : maxdiff ;
		}
		
		itCnt++ ;
	}
	
	return itCnt ;
}

/** Performs the solve on the coarse grid. This can be done in different ways
 * 	depending on the parameters that are passed in
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to operate
 *	\param [in,out] aVars	Variables associated with the approximation on the
 *							current grid. The member variable ApproxVars::approx
 *							is the desired output from this function
 *	\param [in] tol		The tolerance to which to solve the sytsem of equations
 *	\param [in] ignoreMax	Indicator as to whether to stop at a specified 
 *							number of iterations (given by MAX_SMOOTHS) or to
 *							carry until convergence. It may be that the
 *							iteration used will not converge, or may take a long
 *							time, so it is recommended that this parameter is
 *							set to zero, such that the maximum number of
 *							iterations is not exceeded.
 */
void coarse_solve( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars, double tol, int ignoreMax )
{
	//If we are to read the solution from file then attempt to do so
	if( params->readFMGSol && params->isFMG ){
		//If we successfully read the function from file then return
		if( read_sol( params, grid, aVars ) ) return ;
	}
	
	//If we didn't successfully read the function from file, or we do not
	//want to read the function then perform smoothing iterations until we reach
	//a fixed point
	if( is_non_linear( params->testCase ) &&
		params->nlMethod != METHOD_NEWTON_MG ) //Nonlinear case
		nl_gauss_seidel_to_converge( grid, params, aVars, ignoreMax ) ;
	else //Linear case
		gs_to_converge( params, grid, aVars, tol, ignoreMax ) ;
}

/** Reads the solution to a set of equations from file. It is assumed that the
 *	filename is in a specific format, and that the correct data is contained in
 *	the file. No check is made that this is a stationary point of a nonlinear
 *	smooth, or that the residual is small
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVars	Variables associated with the approximation. The 
 *							function read in is stored in ApproxVars::approx
 */
int read_sol( const AlgorithmParameters *params, const GridParameters *grid,
	ApproxVars *aVars )
{
	char inFName[100] ; //Stores the name of the file to read data in from
	
	FILE *testFile ;
	
	int hasSmoothComp, hasOscComp ;
	
	//All of the approximation data is saved in the folder './ConvergedSols'
	sprintf( inFName, "./ConvergedSols/" ) ;
	
	if( params->testCase == 1 )
		sprintf( inFName, "%sconvergeLin_lvl%d.txt", inFName,
			params->cGridLevel-1 ) ;
	else if( params->testCase != 6 && params->testCase != 7 )
		sprintf( inFName, "%sconvergeNL_a%.3lflvl%d.txt", inFName,
			params->alpha, params->cGridLevel-1 ) ;
	else{
		hasSmoothComp = (params->solSmoothAmp != 0.0) ;
		hasOscComp = ( params->solOscAmp != 0.0 && params->solXFreq != 0.0 &&
			params->solYFreq != 0.0 ) ;
			
		if( hasSmoothComp && hasOscComp ){
			sprintf( inFName, "%sconvergeNL_a%.3lflvl%dA%.3lfB%.3lfn%.3lf",
				inFName, params->alpha, params->cGridLevel-1,
				params->solSmoothAmp, params->solOscAmp, params->solXFreq ) ;
			sprintf( inFName, "%sm%.3lf.txt", inFName, params->solYFreq ) ;
		}
		else if( hasSmoothComp )
			sprintf( inFName, "%sconvergeNL_a%.3lflvl%dA%.3lf.txt", inFName,
				params->alpha, params->cGridLevel-1, params->solSmoothAmp ) ;
		else
			sprintf( inFName, "%sconvergeNL_a%.3lflvl%dB%.3lfn%.3lfm%.3lf.txt",
				inFName, params->alpha, params->cGridLevel-1, params->solOscAmp,
				params->solXFreq, params->solYFreq ) ;
	}
	
	//Attempt to open the file for reading
	testFile = fopen( inFName, "r" ) ;
	
	//If the file does not exist, or we could not open it for reading then
	//return zero to indicate failure
	if( testFile == NULL )
		return 0 ;
	fclose( testFile ) ;
	
	read_grid_function_from_file( inFName, grid, aVars->approx ) ;
	
	return 1 ;
}

/** Uses a linear solver to solve a system of equations passed in
 *	\param [in] grid	The grid on which to solve a linear system
 *	\param [in,out] aVars	Variables associated with the approximation. The
 *							variable ApproxVars::approx is the desired output
 *							from this method. The matrix in the system
 *							\f$Ax=b\f$ is stored in ApproxVars::iterMat and the
 *							right hand side is stored in ApproxVars::rhs
 */
void direct_lin_solve( const GridParameters *grid, ApproxVars *aVars )
{
	int i, j, k ; //Loop counters
	int iInd ; //The index into the unknowns for a node on the grid
	const Node *node ; //A node on the grid
	int nrhs = 1 ; //The number of right hand sides in the system
	double *mat ; //The dimension of the matrix to use. This is required to copy
				  //the values from the iteration matrix into here, as we
				  //require a different format for the matrices. The entire
				  //matrix is stored, as it is assumed that only small systems
				  //will be solved directly.
	int *pivot ; //Stores the pivoting operations performed by the exact solve
	int mWidth ; //The number of rows / columns in the matrix
	int ok ; //Stores whether the lapack function terminated correctly or not
				  
	mWidth = grid->numUnknowns ;
	//Assing the memory for the matrix, and use calloc to set the values to
	//zero. We don't use a double pointer, and matrix `mat' is stored in a
	//vector where we order the entries by column
	mat = (double*)calloc( pow( mWidth, 2 ), sizeof( double ) ) ;
	pivot = (int*)malloc( mWidth * sizeof( int ) ) ;
	
	//Loop through all of the unknown nodes on the grid
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		//Get a placeholder to the current node on the grid
		node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
		k = grid->rowStartInds[ i ] ;
		//For the node get the neighbouring unknowns
		for( j=0 ; j < node->numNghbrs ; j++ ){
			iInd = grid->mapG2A[ node->nghbrs[j] ] ;
			mat[ iInd * mWidth + i ] = aVars->iterMat[ k + j ] ;
		}
		//We also want to copy the values in the right hand side into the
		//approximation, as this is going to be the input and the output to the
		//lapack function
		aVars->approx[i] = aVars->rhs[i] ;
	}
	
	//Now that we have set up everything appropriately we solve the system
	//directly
	dgesv_( &mWidth, &nrhs, mat, &mWidth, pivot, aVars->approx, &mWidth, &ok ) ;
	
	//Clean up
	free( pivot ) ;
	free( mat ) ;
}

/** Initialises the right hand side of the weak formulation of a system of
 *	equations
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] weightFact	A weighting parameter to be used to multiply the
 *							result. This is required for time dependent problems
 *							where the right hand side is multiplied by the time
 *							step
 *	\param [out] rhs	The right hand side vector to be populated
 *	\param [out] bndTerm	Stores the contribution from the boundary to the
 *							nonlinear operator
 */
void initialise_rhs( const GridParameters* grid,
	const AlgorithmParameters* params, const ApproxVars *aVars,
	double weightFact, double* rhs, double *bndTerm )
{
	int e, i ;
	const Element *els ; //Placeholder for the elements on the grid
	const Node *nodes ; //Placeholder for the nodes on the grid
	const Node *node ; //Placeholder for a single node on the grid

	//Initialise all values to be zero
	set_dvect_to_zero( grid->numUnknowns, rhs ) ;
	set_dvect_to_zero( grid->numUnknowns, bndTerm ) ;
	
	//For each triangle work out the contribution to the right hand side. We
	//either add a value from integrating using Simpson's rule at a given node,
	//or move some value from a neighbouring boundary node
	
	//Set a placeholder for the elements on the grid
	els = grid->elements ;
	//Set a placeholder for the nodes on the grid
	nodes = grid->nodes ;
	
	if( is_richards_eq( params->testCase ) ){
		//For each triangle
		for( e=0 ; e < grid->numElements ; e++ ){
			for( i=0 ; i < 3 ; i++ ){
				node = &(nodes[ els[e].nodeIDs[i] ]) ;
				if( node->type == NODE_NEUM_BNDRY ){
					bndTerm[ grid->mapG2A[ node->id ]] +=
						boundary_rhs_contribution( params, grid, &(els[e]),
							i, node ) ;
				}
			}
		}
	}
	else{
		//For each triangle
		for( e=0 ; e < grid->numElements ; e++ ){
			//Work out the extra contribution from each vertex
			for( i=0 ; i < 3 ; i++ ){
				node = &(nodes[ els[e].nodeIDs[i] ]) ;
				//Check that we don't have a boundary node, and update the rhs
				//at this point
				if( node->type != NODE_DIR_BNDRY ){
					rhs[grid->mapG2A[ node->id ]] += simpsons_rule( &i, e, grid,
						params, aVars, NULL, &weak_rhs_function ) ;
				
					//If we are on a boundary node we calculate the contribution
					//to the boundary term and the derivative boundary term
					if( node->type == NODE_NEUM_BNDRY ){
						bndTerm[ grid->mapG2A[ node->id ]] +=
							boundary_rhs_contribution( params, grid, &(els[e]),
								i, node ) ;
					}
				}
			}
		}
	}
	
	//Now loop through all of the nodes and multiply by the weighting factor
	if( weightFact != 1.0 && !is_richards_eq( params->testCase ) ){
		for( i=0 ; i < grid->numUnknowns ; i++ )
			rhs[i] *= weightFact ;
	}
}




















