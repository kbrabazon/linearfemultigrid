#!/bin/bash

NONE='\x1b[00m'
RED='\x1b[01;31m'
GREEN='\x1b[01;32m'
PURPLE='\x1b[01;35m'
BLUE='\x1b[01;34m'
BLUE_BG='\x1b[34;01;07m'
RED_BG='\x1b[31;01;07m'



make clean ; make all &> make.out
echo -ne ${BLUE}
grep -i -e warning -A 1 -B 1 make.out | grep -i -e function.*: -e warning -
echo -e ${NONE}'****'${RED}
grep -i -e error -e "undefined reference" -A 1 -B 1 make.out | grep -i -e function.*: -e error -e "undefined reference" -
echo -ne ${NONE}
rm make.out
