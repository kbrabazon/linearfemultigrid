#ifndef _NL_MULTIGRID_H
#define _NL_MULTIGRID_H

#include "FEM_MG_SparseMatrix.h"
#include "useful_definitions.h"

/** \file nl_multigrid.h
 *	\brief Functions related to the execution of nonlinear multigrid
 */

//Performs a cycle of nonlinear multigrid on level 'current_level'
void nl_multigrid( AlgorithmParameters* params, GridParameters **grids,
	ApproxVars **aVarsList, const int mgCnt ) ;
	
//Performs newton multigrid iterations until we get convergence / divergence
//defined by given tolerances
void newton_mg( GridParameters **grids, AlgorithmParameters* params,
	ApproxVars **aVarsList, Results *results ) ;
void set_newton_mg_iteration_vars( const AlgorithmParameters *params,
	GridParameters **grids, ApproxVars **aVarsList ) ;

//Calculates the value of the update
double calc_newton_update( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars, const double* approx,
	const double *correction ) ;	
void apply_newton_step( GridParameters *grid, AlgorithmParameters *params,
	ApproxVars *aVars, Results *results, double *currApprox,
	const double *correction ) ;
void set_inner_its( AlgorithmParameters *params, double residNorm ) ;
double get_sigma( const AlgorithmParameters *params, double residNorm ) ;

//Populates the jacobian matrix for a given test case
void calculate_jacobian_matrix( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double *jacobian, int useNDeriv ) ;
	
//Calculates the symmetric part of the jacobian matrix
void calculate_jacobian_spart( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double *jacobian, int useNDeriv ) ;
	
//Sets up the symmetric part of the Jacobian and the Jacobian matrices
void set_gmres_spart_jac( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars ) ;
	
//Sets up the symmetric part of the Jacobian and the Jacobian matrices, where
//mass lumping is performed. This involves putting all the entries from a column
//which are associated with the time derivative term onto the diagonal
void set_gmres_spart_jac_lumped_mass( const AlgorithmParameters *params, 
	const GridParameters *grid, ApproxVars *aVars ) ;

//Calculates the diagonal part of the jacobian matrix
void calculate_jacobian_diagonals( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double* jDiags ) ;
	
//Calculates the diagonal part of the Jacobian matrix for the case when the
//mass matrix is lumped
void calc_jac_diagonals_lumped_mass( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double *jDiags ) ;
	
//Calculates the Jacobian matrix where the mass matrix contributions are lumped
void calc_lumped_mass_mat_jac( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double *jacobian ) ;

//Performs num_its iterations of Gauss-Seidel
void nl_gauss_seidel( const GridParameters* grid,
	const AlgorithmParameters* params, int numIts, ApproxVars* aVars ) ;
	
//Performs num_its iterations of Jacobi
void nl_jacobi( const GridParameters* grid,
	const AlgorithmParameters* params, int numIts, ApproxVars* aVars ) ;	
	
//Performs a single iteration of Gauss-Seidel
void single_nl_gauss_seidel( const GridParameters* grid,
	const AlgorithmParameters* params, const double* jDiags,
	ApproxVars* aVars ) ;

void single_nl_jacobi( const GridParameters* grid,
	const AlgorithmParameters* params, const double* jDiags,
	ApproxVars* aVars ) ;
	
//Block Gauss-Seidel smoothing
void nl_block_jacobi( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars, int numIts,
	double *jacobian ) ;

void single_nl_block_jacobi( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars, double *jacobian ) ;

//Performs a nonlinear Gauss-Seidel iteration until a convergence criterium is
//met
int nl_smooth_to_converge( const GridParameters *grid, double *jacobian,
	AlgorithmParameters *params, ApproxVars *aVars, int ignoreMax ) ;

int nl_gauss_seidel_to_converge( const GridParameters* grid,
	const AlgorithmParameters* params, ApproxVars* aVars,
	int ignoreMax ) ;
	
int block_jacobi_to_converge( const GridParameters *grid, double *jacobian,
	AlgorithmParameters *params, ApproxVars *aVars, int ignoreMax ) ;

#endif





















