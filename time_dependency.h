#ifndef _TIME_DEPENDENCY_H
#define _TIME_DEPENDENCY_H

#include "FEM_MG_SparseMatrix.h"
#include "useful_definitions.h"
/** \file time_dependency.h
 *	\brief Functions with time dependent functionality
 */

void time_dependent_solve( AlgorithmParameters *params,
	GridParameters **grids, ApproxVars **aVarsList ) ;

void initial_condition( const AlgorithmParameters *params,
	const GridParameters *grid, double *initApprox ) ;
	
double initial_func( const AlgorithmParameters *params,
	const GridParameters *grid, const Node *node ) ;

double calculate_t_zero( const AlgorithmParameters *params ) ;
double calculate_mu( const AlgorithmParameters *params ) ;

void calculate_td_rhs( const AlgorithmParameters *params, const double *eqRHS,
	const ApproxVars *aVars, const GridParameters *grid, double *tdRHS ) ;
	
void rhs_backward_euler( const AlgorithmParameters *params,
	const GridParameters *grid, const ApproxVars *aVars, const double *eqRHS,
	double *tdRHS ) ;	
	
void rhs_trap( const AlgorithmParameters *params, const GridParameters *grid,
	const ApproxVars *aVars, const double *eqRHS, double *tdRHS ) ;
	
void update_time_step( TDCtxt *tdCtxt, const AlgorithmParameters *params,
	const Results *results ) ;

#endif
