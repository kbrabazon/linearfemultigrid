#!/usr/local/bin/python3

#Import the different modules and functions that we need
import collections, os, argparse, glob, tables, sys
from experiment import *

#Splits the file 'fname' by the token 'token', and returns an array
def split_file( fname, token ):
	#Open the file for reading
	inFile = open( fname, 'r' )
	#Read the contents of the file
	fileContent = inFile.read()
	#Close the file
	inFile.close()
	
	#Split the file contents by the token and return the list, disregarding the
	#content before the first split
	return fileContent.split( token )[1:]
#### End of function 'split_file'
	
#Given a bit of text attempts to read parameters from it. The assumption is that
#the string will be of a given format, and hence this should only be called from
#within the code at specific points. This code has been put into a function to
#make it more readable, not so that it can be used elsewhere
def read_parameters( text ):
	#Split the section of text that we are supplied at newline characters, and
	#ignore the first line
	sLines = text.splitlines()[1:]
	lCnt = 0
	params = dict()
	#It may or may not be the case that a file contains the indicator to show
	#whether or not FMG was performed. I want to make sure that this is in the
	#parameters dictionary, so I explicitly set the value to false here. This
	#will be overwritten if the FMG parameter is set in the file
	params["FMG"] = "0"
	for line in sLines:
		#If the line stars with some asterisks then break, as we have finished
		#reading the parameters section of the file
		if( line.startswith( '***' ) ):
			#Trim the bit of the lines that we no longer want
			text = '\n'.join( sLines[lCnt:] )	
			break
		lCnt = lCnt + 1
		
		#Now that I am here I simply want to create a dictionary object and
		#update it with the appropriate values
		if( line.count(':') == 1 ):
			data = line.split( ':' )
			params[data[0]] = data[1]
	return text, params
#### End of function 'read_parameters'

#This reads the string representation of the initial approximation from a given
#bit of text. It is assumed that the text is in a certain format, which is gained
#from reading a file with a particular layout and performing some initial
#operations on it. Therefore this should not be called from outside this script
#and the function is only created to make the script more readable
def read_approximation( text ):
	sLines = text.splitlines()
	lCnt = 0
	approxStr = ''
	for line in sLines:
		#If the line starts with either the words 'Coarse' or 'Grid' then we
		#have reached the end of reading the approximation string
		if( line.startswith( "Coarse" ) or line.startswith( "Grid" ) ):
			text = '\n'.join( sLines[lCnt:] )
			break
		#If the line starts with asterisks and isn't the first line then I want
		#to join the lines and extract what is after the ':', as I know there
		#should be only one
		if( line.startswith( '***' ) and lCnt != 0 ):
			approxTxt = ''.join( sLines[1:lCnt] )
			approxStr = approxTxt.split(':')[1].strip()
			approxStr = ''.join( approxStr.split(' ') )
		lCnt = lCnt + 1
	#return the updated text and the string representation of the initial
	#approximation
	return text, approxStr
#### End of function 'read_approximation'

#Reads the coarsest grid level from the text that is given, and returns this
#along with the text without the line that gives the coarsest grid level
def read_coarse_level( text ):
	text = text.lstrip()
	if( text.startswith( "Grid Level:" ) ):
		cLvl = 1
	else:
		index = text.find( "\n" )
		line = text[0:index]
		line = line.split( " " )
		cLvl = int(line[0]) - 1
		if( len( line ) > 1 ):
			innerIts = int( line[len(line)-1] )
		else:
			innerIts = -1
		text = text[index:].lstrip()
	return text, cLvl, innerIts
#### End of function 'read_coarse_level'

#Reads the information about an individual experiment. This includes the
#convergence factors that were recorded, the amount of time taken to run the
#experiment, etc.
def read_experiment( text, params ):
	#First thing to do is to split the text into lines
	sLines = text.splitlines()
	lCnt = 0
	params["MAX_REACHED"] = False
	params["DIVERGED"] = False
	params["INF_NORM"] = -1
	params["L2_NORM"] = -1
	params["CFACS"] = []
	params["H1CFACS"] = []
	params["RESIDS"] = []
	params["H1RESIDS"] = []
	for line in sLines:
		#If we are reading a new line it means that we are at the end of the
		#current section
		lCnt = lCnt+1
		if( line == "" ):
			#Join the remaining lines, with a newline character at the end of
			#each line, so that the remaining file contents are in the same
			#format as what is passed in originally
			text = '\n'.join( sLines[lCnt:] ).rstrip()
			return text
			
		#Check what the line begins with to decide what to do
		if( line.startswith( "Grid Level" ) ):
			params["FINEST_GRID"] = line.split(":")[1].strip()
		elif( line.startswith( "Number of iterations" ) ):
			params["NUM_ITS"] = line.split(":")[1].strip()
		elif( line.startswith( "Maximum number of iterations" ) ):
			params["MAX_REACHED"] = True
		elif( line.startswith( "Approximation is too far" ) ):
			params["DIVERGED"] = True
		elif( line.lstrip().startswith( "Infinity norm" ) ):
			infNorm = line.split(":")[1].strip()
			if( infNorm.find( "nan" ) >= 0 or infNorm.find( "inf" ) >= 0 ):
				params["INF_NORM"] = float( "inf" )
				params["DIVERGED"] = True
			else:
				params["INF_NORM"] = float( infNorm.split()[0].strip() )
		elif( line.lstrip().startswith( "L2 norm" ) ):
			l2Norm = line.split(":")[1].strip()
			if( l2Norm.find( "nan" ) >= 0 or l2Norm.find( "inf" ) >= 0 ):
				params["L2_NORM"] = float( "inf" )
				params["DIVERGED"] = True
			else:
				params["L2_NORM"] = float( l2Norm )
		elif( line.lstrip().startswith( "H1 norm" ) ):
			h1Norm = line.split(":")[1].strip()
			if( h1Norm.find( "nan" ) >= 0 or h1Norm.find( "inf" ) >= 0 ):
				params["H1_NORM"] = float( "inf" )
				#params["DIVERGED"] = True
			else:
				params["H1_NORM"] = float( h1Norm )
		elif( line.startswith( "Execution time" ) ):
			params["EXEC_TIME"] = line.split(":")[1].strip()
		elif( line.lstrip().startswith( "New Residual" ) or
		      line.lstrip().startswith( "New L2 Residual" ) ):
			if( line.lstrip().startswith( "New Residual" ) ):
				token = "New Residual:"
			else:
				token = "New L2 Residual:"
			currResid = line.split( token )[1].lstrip().split()[0]
			ratio = line.split( "Ratio:" )[1].strip()
			if( ratio.find( "nan" ) >= 0 ):
				ratio = float( "inf" )
				params["DIVERGED"] = True
			else:
				ratio = float( ratio )
			if( currResid.find( "nan" ) >= 0 ):
				currResid = float( "inf" )
				params["DIVERGED"] = True
			else:
				currResid = float( currResid )
			params["CFACS"].append( ratio )
			params["RESIDS"].append( currResid )
		elif( line.lstrip().startswith( "New H1 Residual" ) ):
			currResid = line.split( "New H1 Residual:" )[1].lstrip().split()[0]
			ratio = line.split( "Ratio:" )[1].strip()
			if( ratio.find( "nan" ) >= 0 ):
				ratio = float( "inf" )
				params["DIVEREGED"] = True
			else:
				ratio = float(ratio)
			if( currResid.find( "nan" ) >= 0 ):
				currResid = float( "inf" )
				#params["DIVERGED"] = True
			else:
				currResid = float( currResid )
			params["H1CFACS"].append( ratio )
			params["H1RESIDS"].append( currResid )
		elif( line.startswith( "Original residual L2" ) ):
			origResid = line.split( ":" )[1].strip()
			if( origResid.find( "nan" ) >= 0 ):
				origResid = float( "inf" )
			else:
				origResid = float( origResid )
			params["RESIDS"].append( origResid )
		elif( line.startswith( "Original residual H1" ) ):
			origResid = line.split( ":" )[1].strip()
			if( origResid.find( "nan" ) >= 0 ):
				origResid = float( "inf" )
			else:
				origResid = float( origResid )
			params["H1RESIDS"].append( origResid )
		elif( line.startswith( "Original residual" ) ):
			origResid = line.split( ":" )[1].strip()
			if( origResid.find( "nan" ) >= 0 ):
				origResid = float( "inf" )
			else:
				origResid = float( origResid )
			params["RESIDS"].append( origResid )
	#If we have gotten to here it means that we are at the end of the file, as
	#there is not a new line
	return ""
#### End of function 'read_experiment'

#Prepends a given string to every string in a list of strings
def prepend_to_str_list( pre, strList ):
	retList = []
	for item in strList:
		retList.append( "%s/%s" % (pre, item) )
	return retList
#### End of function 'prepend_to_str_list

################################################################################
#################### Start of the script to be executed ########################
################################################################################

#Define how to parse the input arguments, and read appropriate data from the command line
parser = argparse.ArgumentParser(description="Read information from results " +
           "files and output them to a file in LaTeX format" )
#Add a compulsory argument that gives the search string to select files from
#a given folder
parser.add_argument( '--searchString', metavar='STRING', action='store',
	default='*.txt', help='Linux style search string to select files from ' +
	'the results directory' )
parser.add_argument( '--fname', action='store', default='',
    help="A filename for a file containing filenames from which data is to be" +
    " read. Including this argument ignores \033[4mfolder\033[0m and " +
    "\033[4mfinput\033[0m arguments" )
#Add an optional argument that is the folder name of a folder in the directory
#'~/multigridCode/results/'. If no folder is given then we simply look in results
parser.add_argument( '--folders', action='store', nargs='+', default=[''],
    help="Folder names in directory './results' in which to look" +
        " for results files" )
#An optional argument that is an indicator as to whether or not the user will
#supply the names of files to read from 
#parser.add_argument( '--finput', action='store_true',
#    help="Indicator as to whether the user is to input the filenames to be " +
#    "used directly. The directory from which to operate can be given by the " + 
#    "\033[4mfolders\033[0m argument" )
	
#Parse the arguments that are supplied via the command line
args = parser.parse_args()

#Figure out what folder to look in for the results files
home = os.path.expanduser( "~" )
rDir = "%s/multigridCode/results" % home

if( args.fname != '' ):
	if( not os.path.exists( args.fname ) ):
		sys.exit( "File %s does not exist" % args.fname )
	else:
		fList = []
		with open( args.fname.strip() ) as f:
			for line in f:
				if( len( line ) == 0 ): continue
				if( line[0] == '#' or line[0] == '\n' ): continue
				path = "~/multigridCode/results/%s" % line.strip() 
				if( not os.path.exists( path ) ):
					print( "File %s does not exist" % path )
				else:
					fList.append( path )
#elif( args.finput ):
#	if( args.folder != '' ):
#		rDir = '%s/%s' % (rDir, args.folder)
#	print( "Enter a filename to read data from." )
#	print( "The filename should be relative from %s", rDir )
#	print( "Press Ctrl+D to stop input" )
#	#Read data from the command line
#	fList = [] ;
#	for line in sys.stdin:
#		path = "%s/%s" % (rDir, line.strip() )
#		#Check to see if the file exists
#		if( not os.path.exists( path ) ):
#			print( "File %s does not exist" % path )
#		else:
#			fList.append( path )
else:
	#Create an empty list of files
	fList = []
	for folder in args.folders:
		rDir = "%s/multigridCode/results/%s" % (home, folder)
		#Get a list of files in the appropriate directory
		if( os.getcwd() != rDir ):
			os.chdir( rDir )
		#Now get a list of the files in the directory that match the string
		#that is given as the search string
		fList.extend( prepend_to_str_list( rDir,
		    glob.iglob( args.searchString ) ) )

#Create a list to store experiments that are run
experiments = []

#Loop through all of the files that were returned and store the information
#about the experiments in objects to be stored in the above list
for fname in fList:
	#Split a given file by the token 'Algorithm parameters', which is at the start
	#of each test run in between which the initial approximation can change
	fileContents = split_file( fname, 'Algorithm parameters' )

	for section in fileContents:
		#Now that I have got the contents of the file I want to store the parameters
		#that are used for each test case. These are written at the top of the output
		#for each test case, so should be read now
		section, params = read_parameters( section )
	
		#Now that I have read the parameters I should read the initial approximation
		section, approxStr = read_approximation( section )
		params["INIT_APPROX"] = approxStr
	
		#Now that I have got the initial approximation and the parameters I should
		#read the information about the experiments. I will read all of the
		#information that is available, and then give the parameters object to
		#different functions to write out to a file
	
		#First I need to check if information about the coarse grid levels is
		#included in the file or not
		if( section.find( "Coarse Grid Level" ) >= 0 ):
			subSections = section.split( "Coarse Grid Level:" )[1:]
		else:
			subSections = [section]
		
		cLvl = 0
		#For each different section that is identfied with a different coarse grid
		#loop through
		for subSection in subSections:
			#First we find which coarse grid level we are operating on
			subSection, cLvl, innerIts = read_coarse_level( subSection )
			#Update the parameters dictionary with the appropriate coarse grid
			#level
			params["COARSEST_GRID"] = cLvl
			if( innerIts != -1 ):
				params["INNER_ITS"] = innerIts
			while subSection != "":
				#Read each experiment in turn, and add these to the list of
				#experiments
				subSection = read_experiment( subSection, params )
				experiments.append( Experiment( params ) )

#Now I have read all of the experiments that I am interested in I want to print
#the relevant tables. To do this I will simply pass the experiments to a 
#different function which will deal with this
tables.write_output( experiments )

#When I get to here I have read all that is in the file, and should have a nice
#list of experiments. Now I want to check what I do have in there, so will
#print out what is in there
#fout = open( "/tmp/fout", "w" )
#for experiment in experiments:
#	fout.write( str( experiment ) )
#	fout.write( "\n\n*******************\n\n" )
#fout.close()






