#!/bin/bash
paramsFile="parameters.txt"
outFile="/tmp/progOut"

#Replaces a given parameter name with a parameter value.
#This function takes 3 parameters. The first is the identifier for the value
#to replace. The second is the value to insert, and the third is the file in
#which to replace the value.
function replaceParam(){
	#Link file descriptor 10 with stdin
	exec 10<&0
	#Replace stdin with a file, which I am declaring at the top
	exec < "$3"

	#Link file descriptor with stdout
	exec 11>&1
	exec > "tmp.txt"

	#Replace the finest grid variable in the parameters file with the value
	#that is passed to this method
	while read -r line ; do
		if [[ "$line" = "$1:"* ]] ; then
			(( s_len=${#line} ))
			#Get the string to the left of the ':' in line
			(( myChar=${line: $(expr index $line ":")} ))
			echo ${line/"$myChar"/"$2"}
		else
			echo $line
		fi
	done
	
	#Close the file descriptor and de-link from stdout
	exec 1>&11 11>&-
	
	#restore stdin from file descriptor 10 and close file descriptor 10
	exec 0<&10 10<&-
	
	#Replace the parameters file with the temporary one and remove the temporary
	#file
	mv "tmp.txt" "$3"
}

#This function takes two arguments. The first argument is the filename of the
#file to write output to. The second argument is the file from which to read
#the parameters
function writeParams(){
	#Link file descriptor 10 with stdin
	exec 10<&0
	#Replace stdin with a file, which I am declaring at the top
	exec < "$2"
	
	cnt=0
	
	echo "Parameters used: (Note that the finest grid varies, so is not listed here)" > "$1"
	
	while read -r line ; do
		if [[ $line != \n* && "$line" != \#* && $line != "FINEST_GRID"* ]] ; then
			echo $line >> "$1"
		fi
	done
	
	echo >> "$1"
	echo "************************************************" >> "$1"
	echo "Initial approximation: (Insert correct value here)" >> "$1"
	echo >> "$1"
	echo "************************************************" >> "$1"
	echo >> "$1"
	
	#restore stdin from file descriptor 10 and close file descriptor 10
	exec 0<&10 10<&-
}

#Check that the input to the script is appropriate
if [[ $# -lt 2 || $1 -lt 2 || $1 -gt $2 || $2 -gt 10 ]] ; then
	echo "    Usage: $0 a b [outFile] [parametersFile]"
	echo "    for integers 2 <= a <= b <= 10 and optional file to write to and parameters file to read from"
	exit 0
fi

if [[ $# -ge 4 ]] ; then
	outFile=$3
	paramsFile=$4
elif [[ $# -ge 3 ]] ; then
	outFile=$3
fi

writeParams $outFile $paramsFile

#Loop through from the first argument to the second
for (( i=$1 ; i<=$2 ; i++ )) ; do
	echo Grid level\: $i
	#Set the number of grid levels to what we want it to be
	replaceParam "FINEST_GRID" $i $paramsFile
	
	echo "Number of grid levels: $i" >> "$outFile"
	./mgExec $paramsFile >> "$outFile"
	#Add a blank line after the output so we can see where the different
	#executions start/ end
	echo >> "$outFile"
done

