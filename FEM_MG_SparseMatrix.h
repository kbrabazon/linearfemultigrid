#ifndef _FEM_MG_SPARSEMATRIX_H
#define _FEM_MG_SPARSEMATRIX_H

#include "useful_definitions.h"
#include "multigrid_functions.h"
#include "linear_basis.h"
#include "nl_multigrid.h"
#include "time_dependency.h"
#include "testing.h"

/** \file FEM_MG_SparseMatrix.h
 *	\brief Entry point for the program and contains high level functions
 */
	
//Applies a number of Gauss-Seidel iterations to the approximation. The 'CR'
//indicates that the matrix is stored in compressed row format
void linear_gs( const GridParameters *grid, const AlgorithmParameters *params,
	int numIts, ApproxVars* aVars ) ;
	
void linear_jacobi( const GridParameters* grid,
	const AlgorithmParameters* params, int numIts, ApproxVars* aVars ) ;
	
//Method called to perform the smoothing. Depending on the parameters defined
//in the AlgorithmParameters structure we can perform different forms of
//smoothing
void smooth( const GridParameters* grid, const AlgorithmParameters* params,
	int numIts, double *jacobian, ApproxVars* aVars ) ;


void coarse_solve( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars, double tol, int ignoreMax ) ;
int read_sol( const AlgorithmParameters *params, const GridParameters *grid,
	ApproxVars *aVars ) ;
int gs_to_converge( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars, double tol, int ignoreMax ) ;
void direct_lin_solve( const GridParameters *grid, ApproxVars *aVars ) ;

//Multigrid functions
void fmg( GridParameters **grids, AlgorithmParameters* params,
	ApproxVars **aVarsList, Results *results ) ;
	
void multigrid( GridParameters **grids, AlgorithmParameters* params,
	ApproxVars **aVarsList, Results *results ) ;
void linear_multigrid( AlgorithmParameters* params, GridParameters** grids,
	ApproxVars **aVarsList ) ;
	
//Alternative iterative methods to linear multigrid
void pcg( AlgorithmParameters *params, GridParameters **grids,
	ApproxVars **aVarsList, int numIts ) ;
void pcgmres( AlgorithmParameters *params, GridParameters **grids,
	ApproxVars **aVarsList, int numIts ) ;
void least_squares( LeastSquaresContext *ctxt ) ;
void hess_to_upper( LeastSquaresContext *ctxt ) ;
void back_sub( const double **mat, int dim, double *rhs ) ;
	
//Functions dealing with construction and destruction of a CG context
void initialise_cg_context( const GridParameters *grid, CGContext *ctxt ) ;
void free_cg_context( CGContext *ctxt ) ;
//Functions dealing with construction and destruction of a GMRES context
void initialise_gmres_context( const GridParameters *grid,
	const AlgorithmParameters *params, FGMResContext *ctxt, int reset ) ;
void free_gmres_context( FGMResContext *ctxt ) ;
//Functions dealing with construction and destruction of a least squares context
void initialise_least_squares_context( LeastSquaresContext *ctxt, int reset ) ;
void free_least_squares_context( LeastSquaresContext *ctxt ) ;
	
void initialise_mg_variables( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars ) ;

//Initialising functions
void initialise_grids_to_null( GridParameters **grids,
	const AlgorithmParameters *params ) ;
int init_grid( GridParameters *grid, const AlgorithmParameters *params,
	const char *filename ) ;
int isBndEl( const Element *e, const GridParameters *grid ) ;
void initialise_rhs( const GridParameters* grid,
	const AlgorithmParameters* params, const ApproxVars *aVars,
	double weightFact, double* rhs, double *bndTerm ) ;
void set_up_soil_types( GridParameters *grid,
	const AlgorithmParameters *params ) ;
	
//Initialise ApproxVars structures
void initialise_approx_vars_to_null( ApproxVars **aVarsList,
	const AlgorithmParameters *params ) ;
void init_approx_vars( ApproxVars* aVars, const GridParameters* grid,
	const AlgorithmParameters* params ) ;
//Destroy ApproxVars structures
void free_approx_var_list( ApproxVars **aVarList, int numGrids,
	const AlgorithmParameters *params ) ;
void free_approx_vars( ApproxVars* aVars, const AlgorithmParameters *params ) ;

//Destruction functions
void free_grids( GridParameters **grids, int numGrids ) ;
void free_grid( GridParameters* grid ) ;
		
//Printing functions
void print_grid_params_to_file( const GridParameters* grid,
	const char* filename ) ;
	
//Clean up
void clean_up( GridParameters **grids, ApproxVars **aVarList,
	AlgorithmParameters *params ) ;

#endif

//Here we write the output for the main page of the Doxygen documentation
/** \mainpage Documentation index for the Multigrid project
 *
 *	For details of the implementation please see the functions and descriptions
 *	of different components detailed in the above tabs. The files tabs gives a
 *	good place to start, or if you are looking for a specific function please
 *	see the `Globals' tab under the `Files' tab.
 */















