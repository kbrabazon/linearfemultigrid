#$@ is the item to the left of ':'
#$< is the first item to the right of ':'
#$^ is the entire string appearing to the right of ':'

# Debugging and extra warnings
DEBUG = on
USEGDB = off
PROFILE = off
WARNINGS = on

CC=cc #Use the c compiler from mpi for compliance with PETSC

#The definition of the following compile flags can be found at
#'http://gcc.gnu.org/onlinedocs/gcc/'
#
#-c: Do not link
#-Wall: This enables all the warnings about constructions that some users
#	consider questionable, and that are easy to avoid
#-pedantic: Issue all the warnings demanded by strict ISO C and ISO C++
#-std=: Determine the language standard
#-g: Produce debugging information in the operating system's native format
#-l: Link a given library
#-o: Generate the output file with the given name
#-O3: Third level optimization. I am not sure what this does exactly, but at the
#	moment it simply makes my code run very fast, and gives the same output. I
#	will keep it in, but need to bear in mind that this can cause some issues
#	as it is trying to do some fancy stuff
# Compilation mode
ifeq ($(DEBUG), on)
ifeq ($(USEGDB), on)
  COMPFLAGS = -ggdb
else
  COMPFLAGS = -O3 -g
endif
else
  COMPFLAGS = -O3
endif

# Extra warnings
ifeq ($(WARNINGS), on)
 WARNFLAGS = -Wall -std=c99 -pedantic
endif

ifeq ($(PROFILE), on)
#  CFLAGS = $(WARNFLAGS) $(COMPFLAGS) -pg
  CFLAGS = $(WARNFLAGS) -pg
else
  CFLAGS = $(WARNFLAGS) $(COMPFLAGS)
endif

LIBS=-lm -llapack -lblas -lncurses
OBJECTS= linear_basis.o FEM_MG_SparseMatrix.o nl_multigrid.o \
    multigrid_functions.o time_dependency.o test_cases.o testing.o \
    richards_eq.o

#Determine the default target all
all: mgExec
	
#Define the rules for compiling the different object files
useful_definitions.o: useful_definitions.c useful_definitions.h
	$(CC) $(CFLAGS) -c $< -o $@
	
FEM_MG_SparseMatrix.o: FEM_MG_SparseMatrix.c FEM_MG_SparseMatrix.h \
	useful_definitions.o multigrid_functions.o linear_basis.o nl_multigrid.o
	$(CC) $(CFLAGS) -c $< -o $@
	
multigrid_functions.o: multigrid_functions.c multigrid_functions.h \
	useful_definitions.o linear_basis.o
	$(CC) $(CFLAGS) -c $< -o $@
	
linear_basis.o: linear_basis.c linear_basis.h
	$(CC) $(CFLAGS) -c $< -o $@

nl_multigrid.o: nl_multigrid.c nl_multigrid.h useful_definitions.o \
	linear_basis.o multigrid_functions.o
	$(CC) $(CFLAGS) -c $< -o $@
	
time_dependency.o: time_dependency.c time_dependency.h useful_definitions.o
	$(CC) $(CFLAGS) -c $< -o $@

test_cases.o: test_cases.c test_cases.h useful_definitions.o linear_basis.o
	$(CC) $(CFLAGS) -c $< -o $@
	
testing.o: testing.c testing.h useful_definitions.o linear_basis.o \
	multigrid_functions.o FEM_MG_SparseMatrix.o
	$(CC) $(CFLAGS) -c $< -o $@
	
richards_eq.o: richards_eq.c richards_eq.h useful_definitions.o
	$(CC) $(CFLAGS) -c $< -o $@
	
mgExec: mgExec.c $(OBJECTS) useful_definitions.o 
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

#Remove all .o files and the executable as well
clean::
	rm -rf *o mgExec

