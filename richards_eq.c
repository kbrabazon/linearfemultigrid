#include "richards_eq.h"
#include "linear_basis.h"
#include "nl_multigrid.h"

/** Performs a solve of the Richards equation using Newton-Multigrid defined by
*	the parameters that are passed in
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grids	The hierarchy of grids to use in the multigrid iteration
 *	\param [in] aVarsList	Hierarchy of approximation variables on each grid
 *							level. The approximation ApproxVars::approx on the
 *							finest grid level is the desired output of this
 *							function
 */
void solve_richards_eq( AlgorithmParameters *params, GridParameters **grids,
	ApproxVars **aVarsList )
{
	int tCnt ; //Loop counters
	const char **meshes ; //Points to the filenames of meshes to use in the
						  //multigrid iteration
						  
	GridParameters *grid ; //Placeholder for the grid on the finest level
	ApproxVars *aVars ; //Placeholder for the approximation variables on the
						//finest grid level
						  
	TDCtxt *tdCtxt ; //Context containing information regarding the time
					 //time dependent behaviour of the algorithm
	RichardsEqCtxt *reCtxt ; //Context containing information regarding the
							 //Richards equation set up
	char outFName[30] ; //Filename to print output to
	char fluxFOut[30] ; //Filename to print the flux out to
	char thetaFOut[30] ; //Filename to print volumetric wetness out to
	char thetaDFout[30] ; //Filename to print the derivative of the volumetric
						  //wetness wrt the pressure to file
	char parViewFName[30] ;//Filename to print data in paraview format to
//	char hCondDFout[30] ; //Filename to print the derivative of the hydraulic
//						  //conductivity wrt the pressure to file
	
	double *eqRHS ; //The right hand side of the nonlinear system of equations
					//without a contribution from the time derivative. This is
					//always going to be zero for the Richards equation, but
					//this variable is required to conform with the rest of the
					//code
	double *prevApprox ; //The approximation from the previous time step. This
						 //is required to be able to check for convergence of
						 //the time dependent solution
	double steadyStateDiff ; //The difference in successive approximations that
							 //an approximation needs to reach for the problem
							 //to be considered to have reached a steady state
	Vector2D maxMin ; //Stores the maximum and minimum values in a vector
						 
	Results results ; //Object to store information about the running of the
					  //multigrid iteration
	double maxTStep ; //The maximum time step we are allowed to take
	Vector2D *flux = NULL ; //The flux for the given approximations
						  
	meshes = get_grid_hierarchy( params ) ;
	
	tdCtxt = &(params->tdCtxt) ;
	reCtxt = &(params->reCtxt) ;
	
	//Set a placeholder to the finest grid in the hierarchy. We assume that the
	//memory has been assigned and the grid set up appropriately elsewhere
	grid = grids[params->fGridLevel-1] ;
	//Set a placeholder for the approximation variables on the finest level. We
	//assume that the memory has been assigned and the variables set up
	//appropriately elsewhere
	aVars = aVarsList[params->fGridLevel-1] ;
	
	//Set the current time
	tdCtxt->currTime = tdCtxt->tStart ;
	
	//Set up the initial condition
	initial_condition( params, grid, aVars->approx ) ;
	//Set the value on the Dirichlet boundary
	set_dirichlet_boundary( params, grid, aVars->approx ) ;
	if( params->debug ){
		//Write the approximation at time t_{0} to file
		sprintf( outFName, "/tmp/re_approxLvl%d.txt", params->fGridLevel ) ;
		print_grid_func_to_file( outFName, grid, aVars->approx, 0 ) ;
	
		//Calculate and print the flux to file
		sprintf( fluxFOut, "/tmp/re_fluxLvl%d.txt", params->fGridLevel ) ;
		flux = (Vector2D*)malloc( grid->numUnknowns * sizeof( Vector2D ) ) ;
		set_hydr_cond( params, grid, aVars->approx, aVars->reHCond ) ;
		set_flux( params, grid, aVars->approx, aVars->reHCond, flux ) ;
		print_vector_field_to_file( fluxFOut, grid, flux, 0 ) ;
		
		sprintf( thetaDFout, "/tmp/re_thetaDLvl%d.txt", params->fGridLevel ) ;
		set_theta_deriv( params, grid, aVars->approx, aVars->reTheta ) ;
		print_grid_func_to_file( thetaDFout, grid, aVars->reTheta, 0 ) ;
	
		//Calculate and print the volumetric wetness to file
		sprintf( thetaFOut, "/tmp/re_thetaLvl%d.txt", params->fGridLevel ) ;
		set_theta( params, grid, aVars->approx, aVars->reTheta ) ;
		print_grid_func_to_file( thetaFOut, grid, aVars->reTheta, 0 ) ;
		
		//Calculate and print the hydraulic conductivity to file
//		sprintf( hCondDFout, "/tmp/re_hCondDLvl%d.txt", params->fGridLevel ) ;
//		set_hydr_cond_deriv( params, grid, aVars->approx,
//			aVars->reHCondDeriv ) ;
//		print_grid_func_to_file( hCondDFout, grid, aVars->reHCondDeriv, 0 ) ;
	}
	
	//Set the right hand side of the nonlinear equation without a contribution
	//from the time derivative to be equal to zero
	eqRHS = (double*)calloc( grid->numUnknowns, sizeof( double ) ) ;
	//Call the next function just to get the boundary term
	initialise_rhs( grid, params, aVars, 1.0, eqRHS, aVars->bndTerm ) ;
	
	//Set the memory for the previous approximation. We do not need to assign
	//enough memory to cover the boundary conditions as well, as we are
	//interested only in the difference at the unknown points of the
	//approximation
	prevApprox = (double*)malloc( grid->numUnknowns * sizeof( double ) ) ;
	
	//Initialise some variables in the resutls object
	results.residL2Norm = -1.0 ;
	results.itCnt = 0 ;
		
	//Start the time stepping
	tdCtxt->currTime = tdCtxt->tStart ;
	tdCtxt->tEnd = tdCtxt->tStart + tdCtxt->tStepSize * tdCtxt->numTSteps ;
	tCnt = 0 ;
	maxTStep = tdCtxt->tStepSize ;
	vect_max_min_val( grid->numNodes, aVars->approx, &maxMin ) ;
	steadyStateDiff = STEADY_STATE_DIFF ;
	if( maxMin.x - maxMin.y >= 1.0 ) steadyStateDiff *= maxMin.x - maxMin.y ;
	for( tCnt=0 ; tCnt < tdCtxt->numTSteps ; ){
//	while( tdCtxt->currTime < tdCtxt->tEnd - 1e-14 ){
		//Calculate the right hand side for the current time step and time
		//stepping scheme
		calculate_td_rhs( params, eqRHS, aVars, grid, aVars->rhs ) ;
		
		if( params->debug ){
			//Print out the approximation in a format able to be read by
			//Paraview
			sprintf( parViewFName, "/tmp/approxLvl%d_%d.vtk",
				params->fGridLevel, tCnt+1 ) ;
			print_grid_func_paraview_format( parViewFName, grid,
				aVars->approx ) ;
				
			set_hydr_cond( params, grid, aVars->approx, aVars->reHCond ) ;
			set_flux( params, grid, aVars->approx, aVars->reHCond, flux ) ;
			sprintf( parViewFName, "/tmp/fluxLvl%d_%d.vtk",
				params->fGridLevel, tCnt+1 ) ;
			print_vect_field_paraview_format( parViewFName, grid,
				flux ) ;
		}
		
		//Calculate the residual in the approximation for the system of
		//nonlinear equations
		calc_richards_resid( params, grid, aVars, aVars->approx,
			aVars->resid ) ;
			
		//Calculate the residual at the current time step
		results.residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
			aVars->resid ) ;
		//Print out the residual at the current time step
		printf( "(%d) L2-norm of the residual at time %.5lf: %.18lf", tCnt,
			tdCtxt->currTime, results.residL2Norm ) ;
		printf( "  with %d iterations performed\n", results.itCnt ) ;
		
		//Copy the current approximation unknowns into the previous
		//approximation
		copy_vectors( grid->numUnknowns, prevApprox, aVars->approx ) ;
		
		//Set the current time
		tCnt++ ;
		tdCtxt->currTime = tdCtxt->tStart + tdCtxt->tStepSize * tCnt ;
//		tdCtxt->currTime += tdCtxt->tStepSize ;
		
		if( params->nlMethod == METHOD_NEWTON_MG ){
			//Perform a Newton multigrid iteration with everything that we have
			//set up
			richards_newton_mg( grids, aVarsList, params, &results ) ;
		}
		else
			richards_multigrid( grids, aVarsList, params, &results ) ;
		
		if( params->debug ){
			//Print the updated approximation to file
			print_grid_func_to_file( outFName, grid, aVars->approx, 1 ) ;
		
			set_hydr_cond( params, grid, aVars->approx, aVars->reHCond ) ;
			set_flux( params, grid, aVars->approx, aVars->reHCond, flux ) ;
			print_vector_field_to_file( fluxFOut, grid, flux, 1 ) ;
			
			set_theta_deriv( params, grid, aVars->approx, aVars->reTheta ) ;
			print_grid_func_to_file( thetaDFout, grid, aVars->reTheta, 1 ) ;
		
			set_theta( params, grid, aVars->approx, aVars->reTheta ) ;
			print_grid_func_to_file( thetaFOut, grid, aVars->reTheta, 1 ) ;
			
//			set_hydr_cond_deriv( params, grid, aVars->approx,
//				aVars->reHCondDeriv ) ;
//			print_grid_func_to_file( hCondDFout, grid,
//				aVars->reHCondDeriv, 1 ) ;
		}
//		else if( (tCnt+1)%1000 == 0 ){
//			print_grid_func_to_file( outFName, grid, aVars->approx, 1 ) ;
//			set_hydr_cond( params, grid, aVars->approx, aVars->reHCond ) ;
//			set_flux( params, grid, aVars->approx, aVars->reHCond, flux ) ;
//			print_vector_field_to_file( fluxFOut, grid, flux, 1 ) ;
//			set_theta( params, grid, aVars->approx, aVars->reTheta ) ;
//			print_grid_func_to_file( thetaFOut, grid, aVars->reTheta, 1 ) ;
//		}
		
		if( (tCnt)%10 == 0 ){
			vect_max_min_val( grid->numNodes, aVars->approx, &maxMin ) ;
			steadyStateDiff = STEADY_STATE_DIFF ;
			if( maxMin.x - maxMin.y >= 1.0 )
				steadyStateDiff *= maxMin.x - maxMin.y ;
		}
		
		//Update the time step, if required
		if( tdCtxt->adaptTStep )
			update_time_step( tdCtxt, params, &results ) ;
			
		//Get the maximum difference between the approximations at subsequent
		//timesteps
		if( vect_diff_infty_norm( grid->numUnknowns, aVars->approx,
			prevApprox ) < steadyStateDiff ){
			printf( "Steady state reached at time %.5lf\n", tdCtxt->currTime ) ;
			break ;
		}
	}
	
	if( params->debug ){
		//Print the final approximation to file in paraview format
		sprintf( parViewFName, "/tmp/approxLvl%d_%d.vtk", params->fGridLevel,
			params->tdCtxt.numTSteps+1 ) ;
		print_grid_func_paraview_format( parViewFName, grid, aVars->approx ) ;
		
		set_hydr_cond( params, grid, aVars->approx, aVars->reHCond ) ;
		set_flux( params, grid, aVars->approx, aVars->reHCond, flux ) ;
		sprintf( parViewFName, "/tmp/fluxLvl%d_%d.vtk",
			params->fGridLevel, tCnt+1 ) ;
		print_vect_field_paraview_format( parViewFName, grid,
			flux ) ;
	}
	
	//calculate the residual
//	calc_richards_resid( params, grid, aVars, aVars->approx, aVars->resid ) ;
//	results.residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
//		aVars->resid ) ;
	
	//Print out information at the end of the time stepping
	printf( "L2-norm of the residual at final time %.5lf: %.18lf",
		tdCtxt->currTime, results.residL2Norm ) ;
	printf( "  with %d iterations performed\n", results.itCnt ) ;
	
	printf( "Test case: %d\n", params->testCase ) ;
	
	//Print the final approximation to its own file
	sprintf( outFName, "/tmp/finalApproxLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_to_file( outFName, grid, aVars->approx, 0 ) ;
	
	//Calculate and print the flux at the final time
	if( flux == NULL )
		flux = (Vector2D*)malloc( grid->numUnknowns * sizeof( Vector2D ) ) ;
	set_hydr_cond( params, grid, aVars->approx, aVars->reHCond ) ;
	set_flux( params, grid, aVars->approx, aVars->reHCond, flux ) ;
	sprintf( fluxFOut, "/tmp/finalFluxLvl%d.txt", params->fGridLevel ) ;
	print_vector_field_to_file( fluxFOut, grid, flux, 0 ) ;
	
	//Clean up
	free( eqRHS ) ;
	free( prevApprox ) ;
	free( flux ) ;
}

/** Sets all pointer variables in a Richards equation context to NULL
 *	\param [in,out] ctxt	Context storing necessary information for a Richards
 *							equation type problem
 */
void initialise_re_ctxt_to_null( RichardsEqCtxt *ctxt )
{
	ctxt->numSoils = 0 ;
	ctxt->thetaR = NULL ;
	ctxt->thetaS = NULL ;
	ctxt->satCond = NULL ;
	ctxt->n = NULL ;
	ctxt->m = NULL ;
	ctxt->hS = NULL ;
	ctxt->thetaM = NULL ;
	ctxt->alpha = NULL ;
	ctxt->satScaleFact = NULL ;
	ctxt->cHydrCond = NULL ;
}

/** Sets the required amount of memory to store information about the number of
 *	soils that we wish to use in solving the Richards equation
 *	\param [in,out] ctxt	Context storing necessary information for a Richards
 *							equation type problem
 */
void assign_re_ctxt_mem( RichardsEqCtxt *ctxt )
{
	//Assume that the number of soils has been set up, and that we need to
	//simply assign the memory for the different variables
	ctxt->thetaR = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
	ctxt->thetaS = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
	ctxt->satCond = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
	ctxt->n = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
	ctxt->m = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
	ctxt->hS = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
	ctxt->thetaM = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
	ctxt->alpha = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
	ctxt->satScaleFact = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
	ctxt->cHydrCond = (double*)calloc( ctxt->numSoils, sizeof( double ) ) ;
}

/** Free the memory assigned to the Richards equation context
 *	\param [in,out] ctxt	Context storing necessary information for a Richards
 *							equation type problem
 */
void free_re_ctxt_mem( RichardsEqCtxt *ctxt )
{
	//Check that the context is not null
	if( ctxt == NULL ) return ;
	
	if( ctxt->thetaR != NULL ){
		free( ctxt->thetaR ) ;
		ctxt->thetaR = NULL ;
	}
	if( ctxt->thetaS != NULL ){
		free( ctxt->thetaS ) ;
		ctxt->thetaS = NULL ;
	}
	if( ctxt->satCond != NULL ){
		free( ctxt->satCond ) ;
		ctxt->satCond = NULL ;
	}
	if( ctxt->n != NULL ){
		free( ctxt->n ) ;
		ctxt->n = NULL ;
	}
	if( ctxt->m != NULL ){
		free( ctxt->m ) ;
		ctxt->m = NULL ;
	}
	if( ctxt->hS != NULL ){
		free( ctxt->hS ) ;
		ctxt->hS = NULL ;
	}
	if( ctxt->thetaM != NULL ){
		free( ctxt->thetaM ) ;
		ctxt->thetaM = NULL ;
	}
	if( ctxt->alpha != NULL ){
		free( ctxt->alpha ) ;
		ctxt->alpha = NULL ;
	}
	if( ctxt->satScaleFact != NULL ){
		free( ctxt->satScaleFact ) ;
		ctxt->satScaleFact = NULL ;
	}
	if( ctxt->cHydrCond != NULL ){
		free( ctxt->cHydrCond ) ;
		ctxt->cHydrCond = NULL ;
	}
}

/** Initialises values in the Richards equation context that depend on values
 *	read in from file
 *	\param [in,out] ctxt	Context storing necessary information for a Richards
 *							equation type problem
 */
void set_up_re_ctxt( RichardsEqCtxt *ctxt )
{
	int i ; //Loop counter
	
	for( i=0 ; i < ctxt->numSoils ; i++ ){
		//First calculate m, as this is needed in the calculation of other constants
		ctxt->m[i] = 1.0 - 1.0/ctxt->n[i] ;
		//Now get the fictitious theta value
		ctxt->thetaM[i] = get_theta_m( ctxt, i ) ;
		//Set the scaling factor between the effective saturation and the scaled
		//effective saturation
		ctxt->satScaleFact[i] = (ctxt->thetaS[i] - ctxt->thetaR[i]) /
			(ctxt->thetaM[i] - ctxt->thetaR[i]) ;
		//Get the scaling factor for calculations of the hydraulic conductivity
		ctxt->cHydrCond[i] = 1.0 - hydr_cond_sub( ctxt, i, 1.0 ) ;
	}
}

/** Calculates the value of \f$\theta_{m}\f$ which is the fictional value of
 *	volumetric wetness which would be achieved at zero hydraulic head, if the
 *	Vogel, et al model would allow for the pressure to drop to zero
 *	\param [in] ctxt	Context storing necessary information for a Richards
 *						equation type problem
 *	\param [in] soilID	Identifier for the soil type for which to calculate the
 *						value of \f$\theta_{m}\f$
 *	\return \p double value of the value of \f$\theta_{m}\f$ for the modified
 *			van Genuchten-Mualem model by Vogel, et al
 */
double get_theta_m( const RichardsEqCtxt *ctxt, int soilID )
{
	return ctxt->thetaR[soilID] + ( ctxt->thetaS[soilID] -
		ctxt->thetaR[soilID] ) * pow ( 1.0 +  pow( -ctxt->alpha[soilID] *
		ctxt->hS[soilID], ctxt->n[soilID] ), ctxt->m[soilID] ) ;
}

/** Calculates the volumetric wetness \f$\theta(h)\f$ given the hydraulic head
 *	\f$h\f$. This is using the modified van Genuchten-Mualem model as proposed
 *	by Vogel, et al
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which the volumetric wetness is to be
 *						discretized
 *	\param [in] pressure	The hydraulic head over the domain
 *	\param [out] theta	The volumetric wetness, which is populated in this
 *						method
 */
void set_theta( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure,	double *theta )
{
	int i ; //Loop counters
	const RichardsEqCtxt *ctxt ;
	int ind ; //Index into the approximation for a node on the grid
	
	ctxt = (const RichardsEqCtxt*)(&(params->reCtxt)) ;
	
	//For each node on the grid calculate volumetric wetness given the pressure
	//at the node
	for( i=0 ; i < grid->numNodes ; i++ ){
		ind = grid->mapG2A[ i ] ;
		theta[ind] = calc_theta( ctxt, grid->soilType[i], pressure[ind] ) ;
	}
}

/** Calculates the pointwise gradient of the volumetric wetness \f$\theta\f$
 *	given the hydraulic head \f$h\f$. This is using the van Genuchten-Mualem
 *	model as proposed by Vogel, et al
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which the derivative of the volumetric
 *						wetness is to be discretized
 *	\param [in] pressure	The hydraulic head over the domain
 *	\param [out] gradTheta	The gradient of the volumetric wetness with respect
 *							to the hydraulic head. This is populated in this
 *							method
 */
void set_theta_deriv( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure,
	double *gradTheta )
{
	int i ; //Loop counter
	const RichardsEqCtxt *ctxt ;
	int ind ; //The index into the approximation for a node on the grid
	
	ctxt = (const RichardsEqCtxt*)(&(params->reCtxt)) ;
	
	//For each point on the grid calculate the derivative of the volumetric
	//wetness given the pressure at the point
	for( i=0 ; i < grid->numNodes ; i++ ){
		ind = grid->mapG2A[ i ] ;
		gradTheta[ind] = calc_theta_deriv( ctxt, grid->soilType[i],
			pressure[ind] ) ;
	}
}

/** Calculates the value of the volumetric wetness \f$\theta\f$ at a point given
 *	the pressure at that point
 *	\param [in] ctxt	Context storing necessary information for a Richards
 *						equation type problem
 *	\param [in] soilID	Identifier for the soil type for which to calculate the
 *						value of the volumetric wetness
 *	\param [in] pressure	The pointwise pressure given in terms of hydraulic
 *							head
 *	\return \p double value of the volumetric wetness at a point given the
 *			pressure at the point
 */
double calc_theta( const RichardsEqCtxt *ctxt, int soilID, double pressure )
{
	//If the pressure is larger than the pressure at saturation return the
	//saturated water volume
	if( pressure >= ctxt->hS[soilID] )
		return ctxt->thetaS[soilID] ;
	else
		return ctxt->thetaR[soilID] + (ctxt->thetaM[soilID] -
			ctxt->thetaR[soilID]) /	pow( 1.0 + pow( -ctxt->alpha[soilID] *
			pressure, ctxt->n[soilID] ), ctxt->m[soilID] ) ;
}

/** Calculates the analytic derivative of the volumetric wetness function at
 *	a given pressure
 *	\param [in] ctxt	Context storing necessary information for a Richards
 *						equation type problem
 *	\param [in] soilID	Identfier for the soil type for which to calculate the
 *						derivative of the volumetric wetness for
 *	\param [in] pressure	The pointwise pressure given in terms of hydraulic
 *							head
 *	\return \p double value of the derivative of the volumetric wetness function
 *			at a given value of the pressure
 */
double calc_theta_deriv( const RichardsEqCtxt *ctxt, int soilID,
	double pressure )
{
	//If the pressure is high enough such that we are at saturation then the
	//derivative of the volumetric wetness as a function of the pressure is zero
	if( pressure >= ctxt->hS[soilID] ) return 0.0 ;
	
	return (ctxt->thetaM[soilID] - ctxt->thetaR[soilID]) * ctxt->alpha[soilID] *
		(ctxt->n[soilID] -1) * pow( -ctxt->alpha[soilID] * pressure,
		ctxt->n[soilID]-1 ) / pow( 1 + pow(-ctxt->alpha[soilID] * pressure,
		ctxt->n[soilID]), ctxt->m[soilID]+1 ) ;
}

/** Calculates the value of the effective saturation given a pointwise pressure
 *	\param [in] ctxt	Context storing necessary information for a Richards
 *						equation type problem
 *	\param [in] soilID	Identifier for the soil type for which to calculate the
 *						effective saturation
 *	\param [in] pressure	The pressure (given in terms of hydraulic head) at
 *							which to evaluate the relative saturation
 *	\return \p double value of the relative saturation for the given pressure
 *			head
 */
double effective_saturation( const RichardsEqCtxt *ctxt, int soilID,
	double pressure )
{
	//If we are at full saturation (i.e. higher than the pressure ctxt->hs) then
	//the relative saturation is given by one
	if( pressure >= ctxt->hS[soilID] ) return 1.0 ;

//	return ctxt->satScaleFact[soilID] / pow( 1.0 + pow( -ctxt->alpha[soilID] *
//			pressure, ctxt->n[soilID] ), ctxt->m[soilID] ) ;
			
	return 1.0 / (ctxt->satScaleFact[soilID] *
		pow( 1.0 + pow( -ctxt->alpha[soilID] * pressure,
		ctxt->n[soilID] ), ctxt->m[soilID] ) ) ;
}

/** Calculates the hydraulic conductivity \f$K\f$ given the hydraulic head
 *	\f$h\f$
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which the hydraulic conductivity is to be
 *						discretized
 *	\param [in] pressure	The pressure in terms of hydraulic head on the
 *							interior points of the domain
 *	\param [out] hydrCond	The hydraulic conductivity at each point on the
 *							interior of the domain. This is updated in this
 *							function
 */
void set_hydr_cond( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure, double *hydrCond )
{
	int i ; //Loop counters
	const RichardsEqCtxt *ctxt ;
	int ind ; //Index into the approximation for a node on the grid
	
	ctxt = &(params->reCtxt) ;
	
	//For each node on the grid calculate the hydraulic conductivity given
	//the pressure at the node
	for( i=0 ; i < grid->numNodes ; i++ ){
		ind = grid->mapG2A[ i ] ;
		hydrCond[ind] = calc_hydr_cond( ctxt, grid->soilType[i],
			pressure[ind] ) ;
	}
}

/** Calcualtes the derivative of the hydraulic conductivity with respect to the
 *	pressure (given in terms of hydraulic head)
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] pressure	The pressure on the points of the domain
 *	\param [out] gradHCond	The derivative of the hydraulic conductivity in
 *							terms of the hydraulic head. This is populated in
 *							this function
 */
void set_hydr_cond_deriv( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure, double *gradHCond )
{
	int i ; //Loop counters
	const RichardsEqCtxt *ctxt ;
	int ind ;
	
	ctxt = &(params->reCtxt) ;
	
	//For each node on the grid calculate the value of the derivative of the
	//hydraulic conductivity given the pressure at the node
	for( i=0 ; i < grid->numNodes ; i++ ){
		ind = grid->mapG2A[ i ] ;
		gradHCond[ind] = calc_hydr_cond_deriv( ctxt, grid->soilType[i],
			pressure[ind] ) ;
	}
}

/** Calcualtes the hydraulic head at a point given the pressure (in terms of
 *	hydraulic head) at that point
 *	\param [in] ctxt	Context storing necessary information for a Richards
 *						equation type problem
 *	\param [in] soilID	Identifier for the soil for which to calculate the
 *						hydraulic conductivity
 *	\param [in] pressure	The pressure at a given point
 *	\return \p double value containing the value of the hydraulic conductivity
 *			given the pressure at a point
 */
double calc_hydr_cond( const RichardsEqCtxt *ctxt, int soilID, double pressure )
{
	double effSat ; //The effective saturation for the given pressure
	
	//If we are above the pressure at which we assume saturation, simply return
	//the saturated hydraulic conductivity
	if( pressure >= ctxt->hS[soilID] ) return ctxt->satCond[soilID] ;
	
	//Get the effective saturation for the given pressure
	effSat = effective_saturation( ctxt, soilID, pressure ) ;
	
	return ctxt->satCond[soilID] * sqrt( effSat ) *
		pow( (1 - hydr_cond_sub( ctxt, soilID, effSat )) /
		ctxt->cHydrCond[soilID], 2 ) ;
}

/** Calcualtes the derivative of the hydraulic conductivity with respect to
 *	the pressure (given in terms of hydraulic head) at a given presure
 *	\param [in] ctxt	Context storing necessary information for a Richards
 *						equation type problem
 *	\param [in] soilID	Identifier for the soil type for which to calculate the
 *						derivative of the hydraulic conductivity
 *	\param [in] pressure	The pressure at a given point
 *	\return \p double value containing the value of the derivative of the
 *			hydraulic conductivity at the given pressure
 */
double calc_hydr_cond_deriv( const RichardsEqCtxt *ctxt, int soilID,
	double pressure )
{
	double retVal ; //Value to return
	double effSat ; //The effective saturation given the pressure
	double fSe ; //The value of the function 1 - F(S_{e}) as defined in
				 //Vogel, et al., 2001
	double gH ; //The value of a function g, dependent on the pressure, that
				//is required a few times in the calculation, and so prevents
				//multiple calculations
	
	//If the pressure is high enough such that we are at saturation then the
	//derivative of the hydraulic conductivity as a function of the pressure
	//is zero
	if( pressure >= ctxt->hS[soilID] ) return 0.0 ;

	//Set the derivative of the hydraulic conductivity, and return it
	effSat = effective_saturation( ctxt, soilID, pressure ) ;
	fSe = 1.0 - hydr_cond_sub( ctxt, soilID, effSat ) ;
	gH = 1 + pow( -ctxt->alpha[soilID] * pressure, ctxt->n[soilID] ) ;
	
	retVal = ctxt->alpha[soilID] * (ctxt->n[soilID] - 1.0) *
		pow( -ctxt->alpha[soilID] * pressure, ctxt->n[soilID] - 1.0 ) * fSe *
		sqrt( effSat ) * ( pow( gH, ctxt->m[soilID] - 1.0 ) *
		fSe / ( 2.0 * ctxt->satScaleFact[soilID] * effSat ) + 2 *
		pow( 1.0 - 1.0 / gH, ctxt->m[soilID] - 1 ) / pow( gH, 2.0 ) ) /
		pow( ctxt->cHydrCond[soilID], 2.0 ) ;
		
	retVal *= ctxt->satCond[soilID] ;
		
	return retVal ;
}

/** Used within the calculation of the unsaturated hydraulic conductivity, and
 *	is put in it's own function to make everything a little more modular
 *	\param [in] ctxt	Context storing necessary information for a Richards
 *						equation type problem
 *	\param [in] soilID	Identifier for the soil type for which to calculate the
 *						given function
 *	\param [in] effSat	Effective saturation at a given point
 *	\return \p double value containing a value required for the calculation of
 *			the unsaturated hydraulic conductivity
 */
double hydr_cond_sub( const RichardsEqCtxt *ctxt, int soilID, double effSat )
{
	return pow( 1 - pow( ctxt->satScaleFact[soilID] * effSat,
		1.0/ctxt->m[soilID] ), ctxt->m[soilID] ) ;
}

/** Sets the value of the flux given the pressure on a grid
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are currently operating
 *	\param [in] pressure	The pressure at the grid nodes
 *	\param [in] hCond		The hydraulic conductivity at the nodes on the grid
 *	\param [out] flux	The flux term, with flux in the x- and z-direction
 */
void set_flux( const AlgorithmParameters *params, const GridParameters *grid,
	const double *pressure, const double *hCond, Vector2D *flux )
{
	int i ; //Loop counter
	const Node *node ; //A node on the grid
	
	//Estimate the derivative of the pressure using divided differences on the
	//current grid
	func_grad_regular_mesh( grid, pressure, flux ) ;
		
	//Now loop through the nodes on the grid and updated the flux accordingly
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		//Get a placeholder to the appropriate node on the grid
		node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
		flux[i].x *= -hCond[i] ;
		flux[i].y = -hCond[i] * (flux[i].y + 1.0) ;
	}
}

/** Returns the pressure as a function of the volumetric wetness
 *  \param [in] ctxt	Context storing necessary information for a Richards
 *						equation type problem
 *	\param [in] soilID	Identifier for which soil type to use
 *	\param [in] theta	The value of the volumetric wetness for which to
 *						calculate the pressure
 *	\return \p double value containing the pressure calculated for the given
 *			volumetric wetness value
 */
double get_pressure_for_theta( const RichardsEqCtxt *ctxt, int soilID,
	double theta )
{
	//If we are at saturation, all we know is that the pressure is at least hS,
	//so we return this value
	if( theta >= ctxt->thetaS[soilID] ) return ctxt->hS[soilID] ;
	
	//Calculate the inverse of the volumetric wetness function for when the soil
	//is not saturated
	return -pow( pow( (ctxt->thetaM[soilID] - ctxt->thetaR[soilID]) /
		(theta - ctxt->thetaR[soilID]),
		1.0 / ctxt->m[soilID] ) - 1.0, 1.0 / ctxt->n[soilID] ) /
			ctxt->alpha[soilID] ;
}

/** Sets the value of the pressure given the volumetric wetness on a grid
 *  \param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to calculate the volumetric wetness
 *	\param [in] theta	The volumetric wetness for which to calculate the
 *						pressure
 *	\param [out] pressure	The pressure on the grid. This is populated in this
 *							method
 */
void set_pressure_for_theta( const AlgorithmParameters *params,
	const GridParameters *grid, const double *theta, double *pressure )
{
	int i ; //Loop counters
	const RichardsEqCtxt *ctxt ; //Placeholder for the context storing
								 //information regarding the Richards equation
	int ind ; //The index into the approximation for a node on the grid
	
	ctxt = &(params->reCtxt) ;
	
	for( i=0 ; i < grid->numNodes ; i++ ){
		ind = grid->mapG2A[ i ] ;
		pressure[ind] = get_pressure_for_theta( ctxt, grid->soilType[i],
			theta[ind] ) ;
	}
}

/** Calculates the residual for the Richards equation given the value of the
 *	pressure (in terms of hydraulic head) at the nodes on the grid
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which the pressure is discretized
 *	\param [in] aVars	Variables associated with the approximation on the
 *						current grid level. Note that in the execution of this
 *						method the variables ApproxVars::reTheta and
 *						ApproxVars::hCond are updated
 *	\param [in] pressure	The pressure (in terms of hydraulic head) at the
 *							nodes on the grid
 *	\param [out] resid		The residual to be populated in this method
 */
void calc_richards_resid( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars,
	const double *pressure, double *resid )
{
	int i, j, elCnt ; //Loop counters
	const RichardsEqCtxt *ctxt ; //Placeholder for the Richards equation context
								 //on the current grid
	const Node *node ; //A node on the grid
	double tmp ; //Used to store intermediate values in calculations
	int ind ; //Index into the residual vector
	Element *e ; //Placeholder for an element on the grid
	
	//Set the placeholder to the Richards equation context
	ctxt = &(params->reCtxt) ;

	//Copy the right hand side vector into the residual, then take off the
	//contribution from applying the operator to the current approximation
	copy_vectors( grid->numUnknowns, resid, aVars->rhs ) ;
	
	//Calculate the pointwise values of the volumetric wetness and the hydraulic
	//conductivity, to avoid repeated calculation
	set_theta( params, grid, pressure, aVars->reTheta ) ;
	set_hydr_cond( params, grid, pressure, aVars->reHCond ) ;
	
	if( params->lumpMassMat ){
		for( i=0 ; i < grid->numUnknowns ; i++ ){
			//Get the index into the mass matrix 
			ind = grid->rowStartInds[i] ;
			//Update the residual with the value of the mass matrix
			resid[i] = -resid[i] + aVars->reTheta[i] * aVars->massMat[ind] ;
		}
	}
	else{
		for( i=0 ; i < grid->numUnknowns ; i++ ){
			//Get the current node on the grid
			node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
			//Perform the matrix / vector multiply for the current row in the
			//mass matrix
			ind = grid->rowStartInds[i] ;
			tmp = aVars->reTheta[i] * aVars->massMat[ ind ] ;
			for( j=1 ; j < node->numNghbrs ; j++ ){
				tmp += aVars->reTheta[ grid->mapG2A[ node->nghbrs[j] ] ] *
					aVars->massMat[ind+j] ;
			}
			
			//Update the residual
			resid[i] = -resid[i] + tmp ;
		}
	}
	
	//Update the residual using the values of the hydraulic conductivity
	//Loop over all the elements on the grid
	
	for( elCnt=0 ; elCnt < grid->numElements ; elCnt++ ){
		//Set a placeholder to the current element on the grid
		e = &(grid->elements[elCnt]) ;
		//Loop over all the nodes on the element
		for( i=0 ; i < 3 ; i++ ){
			ind = grid->mapG2A[ e->nodeIDs[ i ] ] ;
			//Check that we are not at a Dirichlet boundary node
			if( ind >= grid->numUnknowns ) continue ;
			//Add a contribution from the integral of the volumetric wetness
			//function to the residual
		
			resid[ind] += params->tdCtxt.weightFact *
				int_richards_h_cond( params, grid, aVars->reHCond, e, i,
				pressure ) ;
		
//			resid[ind] -= int_richards_theta( params, grid, aVars->reTheta,
//				e, i, pressure ) +
//				params->tdCtxt.weightFact *
//				int_richards_h_cond( params, grid, aVars->reHCond, e, i,
//					pressure ) ;
		}
	}
}

/** Performs integration of the volumetric wetness function \f$\theta\f$
 *	multiplied by a linear basis function centered at node \e node on element
 *	\e e
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] theta	The pointwise values of theta for the current
 *						approximation. These are stored to prevent multiple
 *						calculation
 *	\param [in] e	The element on the grid over which to integrate
 *	\param [in] node	The index of the node on the element at which the basis
 *						function is centered. This is in the set {0,1,2}
 *	\param [in] pressure	The current approximation to the pointwise pressure
 *							given in terms of hydraulic head
 *	\return \p double value containing an approximation to the integral of the
 *			volumetric wetness function multiplied by a linear basis function
 *			centered at node \e node on element \e e
 */
double int_richards_theta( const AlgorithmParameters *params,
	const GridParameters *grid, const double *theta, const Element *e, int node,
	const double *pressure )
{
	double retVal = 0.0 ; //The value to return
//	int next ; //Stores the next index in an anti-clockwise direction from the
//			   //given node on the element
//	int prev ; //Stores the previous index in an anti-clockwise direction from
//			   //the given node on the element
//	double evalPnts[4] ; //Stores the values at the evaluation points of the
//						 //pressure in terms of the hydraulic head. The way in
//						 //which this is stored is [ node, (node+next)/2,
//						 //(node+prev)/2, centroid ]
//	int soilType[4] ; //Stores the soil type at each of the evaluation nodes
//	int aInd ; //Index into the approximation for the node specified
//	const RichardsEqCtxt *ctxt ; //Placeholder for the Richards equation context
//	int nonIntBnd ; //Index of a node on the element which is not on an interior
//					//boundary. Only required for test case 15 at the moment
//	
//	ctxt = &(params->reCtxt) ;
//	
//	//Set the indices for the next and previous nodes on the grid
//	next = (node+1)%3 ;
//	prev = (next+1)%3 ;
//	
//	aInd = grid->mapG2A[ e->nodeIDs[ node ] ] ;
//	
//	if( params->testCase == 14 || params->testCase == 16 ||
//		params->testCase == 17 || params->testCase == 101 ||
//		params->testCase == 102 ){
//		//Get the value of the pressure at the node at which to evaluate the
//		//integral
//		evalPnts[0] = pressure[ aInd ] ;
//		//Get the value at the midpoint between the next node on the element
//		evalPnts[1] = ( evalPnts[0] +
//			pressure[ grid->mapG2A[ e->nodeIDs[ next ] ] ] ) / 2.0 ;
//		//Get the value of the pressure at the midpoint between the previous
//		//node on the element
//		evalPnts[2] = ( evalPnts[0] +
//			pressure[ grid->mapG2A[ e->nodeIDs[ prev ] ] ] ) / 2.0 ;
//		//Get the value of the pressure at the centroid of the element
//		evalPnts[3] = ((evalPnts[1] + evalPnts[2]) * 2.0 - evalPnts[0]) / 3.0 ;
//	
//		//Return the value of the approximate integral using a Simpson's rule
//		//for the integration
//		retVal = theta[ aInd ] / 20.0 + (2.0 / 30.0) *
//			( calc_theta( ctxt, 0, evalPnts[1] ) +
//			calc_theta( ctxt, 0, evalPnts[2] ) ) + (9.0 / 60.0) *
//			calc_theta( ctxt, 0, evalPnts[3] ) ;
//		
//		//Multiply by the area of the element and then return
//		retVal *= e->area ;
//	}
//	else if( params->testCase == 15 || params->testCase == 18 ){
//		//Get the value of the pressure at the node at which to evaluate the
//		//integral
//		evalPnts[0] = pressure[ aInd ] ;
//		soilType[0] = grid->soilType[ e->nodeIDs[ node ] ] ;
//		//Get the value at the midpoint between the next node on the element
//		evalPnts[1] = ( evalPnts[0] +
//			pressure[ grid->mapG2A[ e->nodeIDs[ next ] ] ] ) / 2.0 ;
//		//Get the value of the pressure at the midpoint between the previous
//		//node on the element
//		evalPnts[2] = ( evalPnts[0] +
//			pressure[ grid->mapG2A[ e->nodeIDs[ prev ] ] ] ) / 2.0 ;
//		if( grid->nodes[ e->nodeIDs[node] ].isIntBnd ){
//			soilType[1] = grid->soilType[ e->nodeIDs[ next ] ] ;
//			soilType[2] = grid->soilType[ e->nodeIDs[ prev ] ] ;
//			nonIntBnd = (grid->nodes[ e->nodeIDs[ next ] ].isIntBnd) ? prev :
//				next ;
//			soilType[3] = grid->soilType[ e->nodeIDs[ nonIntBnd ] ] ;
//		}
//		else{
//			soilType[1] = soilType[2] = soilType[3] = soilType[0] ;
//		}
//		//Get the value of the pressure at the centroid of the element
//		evalPnts[3] = ((evalPnts[1] + evalPnts[2]) * 2.0 - evalPnts[0]) / 3.0 ;
//	
//		//Return the value of the approximate integral using a Simpson's rule
//		//for the integration
//		retVal = theta[ aInd ] / 20.0 + (2.0 / 30.0) *
//			( calc_theta( ctxt, soilType[1], evalPnts[1] ) +
//			calc_theta( ctxt, soilType[2], evalPnts[2] ) ) + (9.0 / 60.0) *
//			calc_theta( ctxt, soilType[3], evalPnts[3] ) ;
//		
//		//Multiply by the area of the element and then return
//		retVal *= e->area ;
//	}
	
	return retVal ;
}

/** Performs integration of the hydraulic conductivity function multiplied by
 *  the gradient of a linear basis function centered at node \e node on element
 *	\e e
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] hCond	The hydraulic conductivity at the nodes of the grid.
 *						This is stored to prevent multiple recalculation
 *	\param [in] e		The element over which to integrate
 *	\param [in] node	The node on the element at which the basis function is
 *						centered. This is in the set \f$\{0,1,2\}\f$
 *	\param [in] pressure	The current approximation to the pointwise pressure
 *							given in terms of hydraulic head
 *	\return \p double value of the integral of the hydraulic conductivity
 *			multiplied by the gradient of a linear basis function centered at
 *			node \e node on element \e e
 */
double int_richards_h_cond( const AlgorithmParameters *params,
 	const GridParameters *grid, const double *hCond, const Element *e, int node,
 	const double *pressure )
 {
 	int i ; //Loop counter
 	int aInd ; //Index into the approximation for a node on the grid
 	double retVal = 0.0 ; //Return value
 	double sumGrad[2] ; //Placeholder for the sum of the gradients of the basis
 					 	//functions on the element, multiplied by the nodal
 					 	//values of the pressure, and scaled by the element area
	double sumHCond ; //Placeholder for the sum of the hydraulic conductivity
					  //at the nodes of the element
 						
	//store the values of the gradients of the basis functions and the hydraulic
	//conductivity at the nodes of the current element
	sumHCond = 0.0 ;
	sumGrad[0] = 0.0 ;
	//We set the gradient in the z-direction to be equal to one initially, as 
	//this takes into account the gravity term
	sumGrad[1] = 2*e->area ;
	for( i=0 ; i < 3 ; i++ ){
		aInd = grid->mapG2A[ e->nodeIDs[ i ] ] ;
		sumHCond += hCond[ aInd ] ;
		sumGrad[0] += pressure[ aInd ] * b( i, e, grid->nodes ) ;
		sumGrad[1] += pressure[ aInd ] * c( i, e, grid->nodes ) ;
	}
	
	retVal = ( sumGrad[0] * b( node, e, grid->nodes ) + sumGrad[1] *
		c( node, e, grid->nodes ) ) * sumHCond / (12 * e->area) ;
		
	return retVal ;
 }

/** Performs \e numIts iterations of a Jacobi type nonlinear smooth for the
 *	Richards equation
 *	\param [in] grid	The grid on which we are currently operating
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] numIts	The number of smoothing iterations to perform
 *	\param [in] recalculate	Indicator as to whether the nonlinear operator
 * 							should be recalculated in the execution of the
 *							smoothing
 *	\param [in,out] aVars	Variables associated with the approximation on the
 *							current grid level. The desired output from this
 *							method is ApproxVars::approx. To calculate this
 *							the parameters ApproxVars::resid and
 *							ApproxVars::jDiags are also updated
 */
void re_smooth_nl_jacobi( const GridParameters* grid,
	const AlgorithmParameters* params, int numIts, bool recalculate,
	ApproxVars* aVars )
{
	int i, itCnt ; //Loop counters
	
	//To perform smoothing what I want to do is to perform a single Newton
	//step for a single equation in the system. This involves updating the
	//approximation at each point by adding a correction that is the residual
	//at the point divided by the derivative at the point. Hence the first
	//thing to do is to calculate the residual and the diagonals of the Jacobian
	//matrix
	
	for( itCnt=0 ; itCnt < numIts ; itCnt++ ){
		//Calculate the residual
		calc_richards_resid( params, grid, aVars, aVars->approx,
			aVars->resid ) ;
	
		//Calculate the diagonals of the Jacobian matrix
		if( recalculate ){
			set_theta_deriv( params, grid, aVars->approx, aVars->reTheta ) ;
			calculate_jacobian_diagonals( grid, aVars, params, aVars->jDiags ) ;
		}
	
		//Loop over all the unknowns
		for( i=0 ; i < grid->numUnknowns ; i++ ){
			//Update the approximation at the current grid point, using the 
			//smoothing weight stored in the parameters object
			aVars->approx[i] = aVars->approx[i] - params->smoothWeight * 
				( aVars->resid[i] / aVars->jDiags[i] ) ;
		}
	}
}

/** Performs nonlinear Jacobi smoothing iterations until a convergence criterion
 *	is met, or a set number of smoothing iterations has been performed
 *	\param [in] grid	The grid on which smoothing is to be performed
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation on the
 *						current grid level
 */
void re_smooth_nl_jacobi_to_converge( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars )
{
	int itCnt ; //Loop counter
	double maxDiff ;
	
	maxDiff = TOL2 + 1.0 ;
	
	//Set the counter for the number of iterations performed to zero initially
	itCnt = 0 ; 
	while( maxDiff > TOL2 && itCnt < MAX_NL_SMOOTHS ){
		//Set the old approximation equal to the new one
		copy_vectors( grid->numUnknowns, aVars->tmpApprox, aVars->approx ) ;
		
		//Perform a smoothing step
		re_smooth_nl_jacobi( grid, params, 1, true, aVars ) ;
		
		//Find the maximum difference between the approximations
		maxDiff = vect_diff_infty_norm( grid->numUnknowns, aVars->approx,
			aVars->tmpApprox ) ;
		if( isnan( maxDiff ) || isinf( maxDiff ) ){
			printf( "Smoother diverged when iterating on grid level %d\n",
				params->currLevel -1 ) ;
		}
		//Increase the iteration counter
		itCnt++ ;
	}
}

/** Performs nonlinear smooths until some convergence criterion is met, or until
 *	a maximum number of smoothing iterations has been performed. The nonlinear
 *	operator is not updated in every iteration to save on computing time
 *	\param [in]	grid	The grid on which we are currently operating
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation on the
 *						current grid
 */
void re_smooth_nl_simp_jac_to_converge( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars )
{
	int itCnt ; //Loop counter
	double maxDiff ; //The maximum difference between successive approximations
	int numItsPerUpdate = 10 ; //The number of iterations to perform before
						//updating the nonlinear operators
	
	maxDiff = TOL2 + 1.0 ;
	
	itCnt = 0 ;
	while( maxDiff > TOL2 && itCnt < MAX_NL_SMOOTHS ){
		//Set the old approximation equivalent to the new one
		copy_vectors( grid->numUnknowns, aVars->tmpApprox, aVars->approx ) ;
		
		//Perform a smoothing step with an update of the operator
		re_smooth_nl_jacobi( grid, params, 1, true, aVars ) ;
		//Perform smoothing steps without updating the operator
		re_smooth_nl_jacobi( grid, params, numItsPerUpdate - 1, false, aVars ) ;
		
		maxDiff = vect_diff_infty_norm( grid->numUnknowns, aVars->approx,
			aVars->tmpApprox ) ;
		if( isnan( maxDiff ) || isinf( maxDiff ) ){
			printf( "Smoother diverged when iterating on grid level %d\n",
				params->currLevel - 1 ) ;
		}
		//Increase the iteration counter
		itCnt += numItsPerUpdate ;
	}
}

/** Performs a single Newton iteration with multiple inner solves for the
 *	Richards equation
 *	\param [in] grids	The hierarchy of grids to use in the multigrid iteration
 *	\param [in,out] aVarsList	The approximation variables for each grid in the
 *								multigrid hierarchy. The desired output from
 *								this function is ApproxVars::approx on the
 *								finest grid level
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] results	Stores results on how the Newton-Multigrid iteration
 *							performed
 */
void richards_newton_mg( GridParameters **grids, ApproxVars **aVarsList,
	AlgorithmParameters *params, Results *results )
{
	int i, mgCnt ; //Loop counters
	
	GridParameters *grid ; //Placeholder for the current grid on which we are
						   //operating. This is the finest grid level for the
						   //current run
	ApproxVars *aVars ; //Placeholder for the approximation variables on the
						//current grid
						
	double *approx, *rhs ; //Stores the values of the approximation and the
						   //right hand side for the nonlinear problem
	double *tmpApprox ; //Used as a placeholder for the approximation pointer
						//in the apprxoimation variables object, so that this
						//can be changed
	double *tmpRHS ; //Used as a placeholder for the right hand side pointer
					 //in the approximation variables object, so that this
					 //can be changed
	InfNorm infNorm ; //Used to store the value of the infinity norm
	
//	double resid_d, oldResid_d, origResid_d; //Used for debugging purposes 01/10. If in doubt
			//remove these values as they should have no effect on the running
			//of the program
						   
	//Set the grid to point to the correct grid in the hierarchy
	grid = grids[params->fGridLevel-1] ;
	aVars = aVarsList[params->fGridLevel-1] ;
						   
	//Assign the appropriate memory to store copies of the approximation and
	//the right hand side for the nonlinear system of equations
	approx = (double*)calloc( grid->numNodes, sizeof( double ) ) ;
	rhs = (double*)calloc( grid->numUnknowns, sizeof( double ) ) ;
	
	//Store the approximation to the nonlinear system of equations
	copy_vectors( grid->numNodes, approx, aVars->approx ) ;
	
	//Store the right hand side for the nonlinear system of equations
	copy_vectors( grid->numUnknowns, rhs, aVars->rhs ) ;
	
	//Assume that the residual has already been calculated before coming into
	//this method, so that now we simply store the residual norm without
	//recalculating the residual
	results->origResidL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
		aVars->resid ) ;
	results->residRatio = 1.0 ;
		
	//Set the current and old residual norms to some initial value
	results->residL2Norm = results->origResidL2Norm ;
	results->oldResidL2Norm = results->residL2Norm + 1.0 ;
	
	if( params->debug ){
		//printf( "%s", CURS_INVIS_STR ) ;
		printf( "Original residual norm: %.18lf\n", results->origResidL2Norm ) ;
		//fflush( stdout ) ;
	}
		
	//Now copy the values of the residual in to the right hand side vector,
	//as the residual is the right hand side for the linear system of equations
	copy_vectors( grid->numUnknowns, aVars->rhs, aVars->resid ) ;
	
	//Set the count for the number of multigrid iterations performed to zero
	results->itCnt = 0 ;
	
	tmpApprox = aVars->approx ;
	tmpRHS = aVars->rhs ;
	results->maxCorrection = 1.0 ;
	
	//Perform linear multigrid iterations
	while( results->residL2Norm / results->origResidL2Norm > TD_TOL &&
		results->itCnt < params->maxIts &&
		results->residL2Norm < MAX_RESID_FACT * results->origResidL2Norm &&
		results->maxCorrection > 1e-10){
		//Set the old residual norm to be equal to the current residual norm
		results->oldResidL2Norm = results->residL2Norm ;
		
		if( results->itCnt == 0 || !params->isSimplifiedNewt ||
			results->residRatio > 0.9 ){
			aVars->approx = approx ;
			set_newton_mg_iteration_vars( params, grids, aVarsList ) ;
		}
		
		//Set the correction term to zero as an initial guess
		aVars->approx = tmpApprox ;
		set_dvect_to_zero( grid->numNodes, aVars->approx ) ;
		
		//Store the approximation on the current grid in the grid variable.
		//This means that we can access this to recalculate the Jacobian
		//on coarser grid levels
		//grid->currApprox = approx ;
		
		//Perform an iteration to solve the linear system arising from taking a
		//Newton step
		if( params->innerIt == IT_MULTIGRID ){
//			calculate_residual( params, grid, aVars ) ;
//			origResid_d = resid_d = vect_discrete_l2_norm( grid->numUnknowns,
//				aVars->resid ) ;
			for( mgCnt = 0 ; mgCnt < params->innerIts ; mgCnt++ ){
//			while( resid_d / origResid_d > 1e-6 ){
//				oldResid_d = resid_d ;
				linear_multigrid( params, grids, aVarsList ) ;
//				calculate_residual( params, grid, aVars ) ;
//				resid_d = vect_discrete_l2_norm( grid->numUnknowns,
//					aVars->resid ) ;
//				printf( "%.18lf, ", resid_d / oldResid_d ) ;
			}
//			printf( "\n" ) ;
		}
		else if( params->innerIt == IT_PCG ){
			printf( "Preconditioned conjugate gradients not set up for the " ) ;
			printf( "Richards equation yet\n" ) ;
		}
		else if( params->innerIt == IT_PCGMRES ){
//			if( params->debug ){
//				calculate_residual( params, grid, aVars ) ;
//				oldResid_d = resid_d = vect_discrete_l2_norm( grid->numUnknowns,
//					aVars->resid ) ;
//			}
			pcgmres( params, grids, aVarsList, params->innerIts ) ;
//			if( params->debug ){
//				calculate_residual( params, grid, aVars ) ;
//					resid_d = vect_discrete_l2_norm( grid->numUnknowns,
//						aVars->resid ) ;
//					printf( "%.18lf\n", resid_d / oldResid_d ) ;
//			}
		}
		
		//Calculate the infinity norm of the approximation
		vect_infty_norm( grid->numUnknowns, aVars->approx, &infNorm ) ;
		results->maxCorrection = infNorm.val ;
		
		aVars->rhs = rhs ;
		
		if( params->adaptUpdate ){
			//Try and take a full Newton-Step
//			params->newtonDamp = 1.0 ;
			params->newtonDamp = calc_richards_newton_update( params, grid,
				aVars, approx, aVars->approx ) ;
				
//			printf( "Newton update: %.18lf\n", params->newtonDamp ) ;
			
			//Update the approximation
			for( i=0 ; i < grid->numUnknowns ; i++ )
				approx[i] -= params->newtonDamp * aVars->approx[i] ;
				
			//calculate the residual and the norm in the residual
			calc_richards_resid( params, grid, aVars, approx, aVars->resid ) ;
			results->residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
				aVars->resid ) ;
				
//			//If we have not had a decrease in the residual norm decrease the
//			//size of the step
//			while( results->residL2Norm > results->oldResidL2Norm &&
//				params->newtonDamp > params->minNewtFact ){
//				//Halve the Newton damping parameter
//				params->newtonDamp /= 2.0 ;
//				//Update the approximation
//				for( i=0 ; i < grid->numUnknowns ; i++ )
//					approx[i] += params->newtonDamp * aVars->approx[i] ;
//				//Calculate the residual and the norm in residual
//				calc_richards_resid( params, grid, aVars, approx,
//					aVars->resid ) ;
//				results->residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
//					aVars->resid ) ;
//			}
		}
		else{
			//Update the approximation using the correction. We don't do anything
			//fancy here, just use the damping parameter to update the
			//approximation
			for( i=0 ; i < grid->numUnknowns ; i++ )
				approx[i] -= params->newtonDamp * aVars->approx[i] ;
		
			//Calculate the residual for the nonlinear problem
			calc_richards_resid( params, grid, aVars, approx, aVars->resid ) ;
			//Calculate the current residual norm for the nonlinear problem
			results->residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
				aVars->resid ) ;
		}
		
		//Set the maximum correction to be appropriately scaled
		results->maxCorrection *= params->newtonDamp ;
		
		//Set the ratio between the current and previous residual norms
		results->residRatio = results->residL2Norm /
				results->oldResidL2Norm ;
			
		aVars->approx = approx ;
			
		//Set up the right hand side for the next iteration
		aVars->rhs = tmpRHS ;
		copy_vectors( grid->numUnknowns, aVars->rhs, aVars->resid ) ;
		
		//Increase the iteration count in the results
		results->itCnt++ ;
		
		if( params->debug ){
			//move_cursor_pos_up( 1 ) ;
			//printf( "%s", CLEAR_STR ) ;
			printf( "Iteration: %d\tResid Norm: %.18lf\tRatio: %.18lf\n",
				results->itCnt, results->residL2Norm,
				results->residL2Norm / results->oldResidL2Norm ) ;
			//fflush( stdout ) ;
		}
	}
	
//	if( params->debug ){
//		printf( "%s%s", CLEAR_STR, CURS_VIS_STR ) ;
//		fflush( stdout ) ;
//	}
	
	//Set the right hand side and the approximation to point to the right hand
	//side and the approximation for the nonlinear system of equations
	aVars->rhs = rhs ;
	aVars->approx = approx ;
	
	//Clean up
	free( tmpRHS ) ;
	free( tmpApprox ) ;
}


/** Calculates the update parameter for a Newton method by fitting a quadratic
 *	function \f$ f(\omega) \f$ to points corresponding to using different
 *	update parameters \f$ \omega \f$ in the Newton step
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are performing the Newton iteration
 *	\param [in] aVars	Variables associated with the approximation on the
 * 	                 	current grid level. These may be used for different
 *	                 	things inside this function. See the source code for
 *	                 	more details
 *	\param [in] approx	The approximation before the current Newton step
 *	\param [in] correction	The correction to the approximation calculated
 *							during a Newton step
 *	\return \p double value containing the `optimal' value of the update step
 *			step size for the Newton iteration
 */
double calc_richards_newton_update( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars, const double* approx,
	const double *correction )
{
	int i, j, pntCnt ; //Loop counters
	
	double retVal ; //The return value from the method
	
	int len = 3 ; //Needed for the LAPACK solve
	int nrhs = 1 ; //Needed for the LAPACK solve
	
	double evalPoints[3] ; //The values of the update parameter at which to
						   //sample the residual
	double residVals[3] ; //The values of the residual norm a the evaluation
						  //points
	double subMat[9] ; //The matrix of values to use in the subsystem that is
					   //solved to fit the quadratic
	double rhs[3] ; //The right hand side of values used in the subsytem that is
					//solved to fit the quadratic
	int pivot[3] = {0} ; //Used to pass to LAPACK, but not used explicitly in
						 //this code
	int ok ; //Used to pass to LAPACK, but not used explicitly in this code
	double* trialApprox ; //The updated approximation
	double weightFact ; //The weighting factor to use to update the
						//approximation
	bool validMin = false ; //Indicates whether the minimiser that is calculated
							//is valid or not
	int updateInd = 0 ; //Indicates the index of the value to calculate the
						//residual for
	//Indicates the direction that the window of values was shifted the last
	//time it was moved
	enum SHIFT_DIRECTION prevShift = SHIFT_UNKNOWN ;
	
	//Initialise the values of the update parameter at which to sample the
	//residual norm
	evalPoints[0] = 0.5 ;
	evalPoints[1] = 1.0 ;
	evalPoints[2] = 2.0 ;
	
	trialApprox = aVars->tmpApprox ;
	
	//Set the Dirichlet boundary nodes in the approximation
	set_dirichlet_boundary( params, grid, trialApprox );
	
	//Calculate the value of the residual for each update parameter
	for( pntCnt = 0 ; pntCnt < 3 ; pntCnt++ ){
		weightFact = evalPoints[pntCnt] ;
		//Update the approximation using the appropriate parameter
		for( i=0 ; i < grid->numNodes ; i++ )
			trialApprox[i] = approx[i] - weightFact * correction[i] ;
			
		//Calculate the residual for the approximation
		calc_richards_resid( params, grid, aVars, trialApprox, aVars->resid ) ;
		
		//Calculate the residual norm
		rhs[pntCnt] = residVals[pntCnt] = vect_discrete_l2_norm( grid->numNodes,
			aVars->resid ) ;
			
		//Set up the row in the matrix with which to perform a solve
		for( i=0 ; i < 3 ; i++ )
			subMat[i*3 + pntCnt] = pow( weightFact, 2.0-i ) ;
	}
	
	//Now I have all of the information that I need I just need to call the
	//Lapack function to solve the system of equations
	dgesv_( &(len), &(nrhs), subMat, &(len), pivot, rhs, &(len), &(ok) ) ;
	
	//Now calculate the minimiser using the result
	retVal = -rhs[1] / (2.0*rhs[0]) ;
	
	//If we have had a divide by zero we will end up with an infinite answer.
	//We use the fact that if this is the case the function is linear in
	//the weighting parameter, and return one of the boundaries of the
	//interval depending on the sign of the gradient
	if( isinf( retVal ) || isnan( retVal ) ){
		if( rhs[1] == 0 ) retVal = evalPoints[2] ;
		else if( rhs[1] < 0 ) retVal = evalPoints[0] - 1.0 ;
		else if( rhs[1] > 0 ) retVal = evalPoints[2] + 1.0 ;
	}
//	printf( "Curr Approx: %.10lf\n", retVal ) ;
//	if( isnan( retVal ) ){
//		printf( "retVal is NaN\n" ) ;
//		//Print out the values in the matrix
//		for( pntCnt = 0 ; pntCnt < 3 ; pntCnt++ ){
//			weightFact = evalPoints[pntCnt] ;
//			//Set the values for the row in the matrix
//			for( i=0; i < 3 ; i++ )
//				printf( "%.5lf, ", pow( weightFact, 2.0-i ) ) ;
//			printf( "\n" ) ;
//		}
//		//Print out the right hand side
//		for( i=0 ; i < 3 ; i++ )
//			printf( "%.5lf, ",  residVals[i] ) ;
//	}
	
	//Check that the update is valid
	if( evalPoints[0] < params->minNewtFact && retVal < params->minNewtFact ){
		//If we are below the allowed minimum then set the value to the allowed
		//minimum
		retVal = params->minNewtFact ;
		validMin = true ;
	}
	else if( evalPoints[2] > params->maxNewtFact &&
		retVal > params->maxNewtFact ){
		//If we are above the allowed maximum then set the value to the allowed
		//maximum
		retVal = params->maxNewtFact ;
		validMin = true ;
	}
	else if( retVal < evalPoints[0] ){
		//If the value of the update is below the range of values that were
		//specified then move the range of values down
		for( i=1 ; i>=0 ; i-- ){
			evalPoints[i+1] = evalPoints[i] ;
			rhs[i+1] = residVals[i+1] = residVals[i] ;
		}
		
		evalPoints[0] /= 2.0 ;
		updateInd = 0 ;
		prevShift = SHIFT_DOWN ;
	}
	else if( retVal > evalPoints[2] ){
		//If the value of the update is above the range of values that were
		//specified then move the range of values up
		for( i=0 ; i < 2 ; i++ ){
			evalPoints[i] = evalPoints[i+1] ;
			rhs[i] = residVals[i] = residVals[i+1] ;
		}
		
		evalPoints[2] *= 2.0 ;
		updateInd = 2 ;
		prevShift = SHIFT_UP ;
	}
	else{
		if( retVal < params->minNewtFact )
			retVal = params->minNewtFact ;
		else if( retVal > params->maxNewtFact )
			retVal = params->maxNewtFact ;
		validMin = true ;
	}
			
	//While we have not obtained a valid minimiser
	while( !validMin ){
		//Set the values in the matrix for the fitting of the quadratic
		for( pntCnt = 0 ; pntCnt < 3 ; pntCnt++ ){
			weightFact = evalPoints[pntCnt] ;
			//Set the values for the row in the matrix
			for( i=0; i < 3 ; i++ )
				subMat[i*3 + pntCnt] = pow( weightFact, 2.0-i ) ;
		}
		//Calculate the value at the new evaluation point
		weightFact = evalPoints[updateInd] ;
		
		//Update the approximation using the appropriate parameter
		for( i=0 ; i < grid->numNodes ; i++ )
			trialApprox[i] = approx[i] - weightFact * correction[i] ;
			
		//Calculate the residual for the approximation
		calc_richards_resid( params, grid, aVars, trialApprox, aVars->resid ) ;
		
		//Calculate the residual norm
		rhs[updateInd] = residVals[updateInd] = vect_discrete_l2_norm(
			grid->numNodes, aVars->resid ) ;
			
		//Set up the row in the matrix with which to perform a solve
		for( i=0 ; i < 3 ; i++ )
			subMat[i*3 + updateInd] = pow( weightFact, 2-i ) ;
			
		//Now I have all of the information that I need I just need to call the
		//Lapack function to solve the system of equations
		dgesv_( &(len), &(nrhs), subMat, &(len), pivot, rhs, &(len), &(ok) ) ;
	
		//Now calculate the minimiser using the result
		retVal = -rhs[1] / (2.0*rhs[0]) ;
		
		//If we have had a divide by zero we will end up with an infinite answer
		//We use the fact that if this is the case the function is linear in
		//the weighting parameter, and return one of the boundaries of the
		//interval depending on the sign of the gradient
		if( isinf( retVal ) || isnan( retVal ) ){
			if( rhs[1] == 0 ) retVal = evalPoints[2] ;
			else if( rhs[1] < 0 ) retVal = evalPoints[0] - 1.0 ;
			else if( rhs[1] > 0 ) retVal = evalPoints[2] + 1.0 ;
		}
		
//		printf( "Curr Approx: %.5lf\n", retVal ) ;
	
		//Check that the update is valid
		if( evalPoints[0] < params->minNewtFact &&
			retVal < params->minNewtFact ){
			//If we are below the allowed minimum then set the value to the
			//allowed minimum
			retVal = params->minNewtFact ;
			validMin = true ;
		}
		else if( evalPoints[2] > params->maxNewtFact &&
			retVal > params->maxNewtFact ){
			//If we are above the allowed maximum then set the value to the
			//allowed maximum
			retVal = params->maxNewtFact ;
			validMin = true ;
		}
		else if( retVal < evalPoints[0] ){
			if( prevShift == SHIFT_UP ){
				//If we shifted the window of values up last time, and now are
				//switching back down we choose the value that was in both of
				//the intervals as the value to return
				retVal = evalPoints[0] ;
//				printf( "Returned Value: %.5lf\n", retVal ) ;
				return retVal ;
			}
			//If the value of the update is below the range of values that were
			//specified then move the range of values down
			for( i=1 ; i>=0 ; i-- ){
				evalPoints[i+1] = evalPoints[i] ;
				rhs[i+1] = residVals[i+1] = residVals[i] ;
			
				//Now update the row in the submatrix
				for( j=0 ; j < 3 ; j++ )
					subMat[j*3 + i+1] = subMat[j*3 + i] ;
			}
		
			evalPoints[0] /= 2.0 ;
			updateInd = 0 ;
			prevShift = SHIFT_DOWN ;
		}
		else if( retVal > evalPoints[2] ){
			if( prevShift == SHIFT_DOWN ){
				//If we shifted the window of values down last time, and now
				//we are switching back up we choose the value that was in both
				//intervals as the value to return
				retVal = evalPoints[2] ;
//				printf( "Returned Value: %.5lf\n", retVal ) ;
				return retVal ;
			}
			//If the value of the update is above the range of values that were
			//specified then move the range of values up
			for( i=0 ; i < 2 ; i++ ){
				evalPoints[i] = evalPoints[i+1] ;
				rhs[i] = residVals[i] = residVals[i+1] ;
			
				//Now update the row in the submatrix
				for( j=0 ; j < 3 ; j++ )
					subMat[j*3 + i] = subMat[j*3 + i + 1] ;
			}
		
			evalPoints[2] *= 2.0 ;
			updateInd = 2 ;
			prevShift = SHIFT_UP ;
		}
		else{
			if( retVal < params->minNewtFact )
				retVal = params->minNewtFact ;
			else if( retVal > params->maxNewtFact )
				retVal = params->maxNewtFact ;
			validMin = true ;
		}
	}
	
//	printf( "Returned Value: %.5lf\n", retVal ) ;
	return retVal ;
}

/** Perform a solve of a nonlinear system of equations using a nonlinear
 *	multigrid iteration	
 *	\param [in] grids	The hierarchy of grids to use in the multigrid method
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVarsList	Variables associated with the approximation on
 *								each grid level in the hierarchy. The desired
 *								output from this method is the
 *								ApproxVars::approx variable on the finest grid
 *								level
 *	\param [out] results	Stores results regarding the execution of the
 *							algorithm
 */
void richards_multigrid( GridParameters **grids, ApproxVars **aVarsList,
	AlgorithmParameters *params, Results *results )
{
	GridParameters *fGrid ; //Points to the fine grid on the finest grid level
	ApproxVars *fVars ; //Variables associated with the approximation on the
						//finest grid level
						
	fGrid = grids[ params->currLevel-1 ] ;
	fVars = aVarsList[ params->currLevel-1 ] ;

	//Set some values in the results variable
	results->origResidL2Norm = vect_discrete_l2_norm( fGrid->numUnknowns,
		fVars->resid ) ;
	//Set the H1 norm variables to be negative, which indicates that we do not
	//use this (for now)
	results->residH1Norm = results->oldResidH1Norm =
		results->origResidH1Norm = -1.0 ;
	
	results->residL2Norm = results->origResidL2Norm ;
	results->oldResidL2Norm = results->residL2Norm + 1.0 ;
	
	results->itCnt = 0 ;
	//Loop while the convergence criterion has not been met
	while( results->residL2Norm / results->origResidL2Norm > TD_TOL &&
		results->itCnt < params->maxIts &&
		results->residL2Norm < MAX_RESID_FACT * results->origResidL2Norm ){
		
		if( params->numPreSmooths == params->numPostSmooths &&
			params->numPreSmooths == 0 ){
			results->oldResidL2Norm = results->residL2Norm ;
			//Perform smoothing on the current grid level
			re_smooth_nl_jacobi( fGrid, params, 1, true, fVars ) ;
			re_smooth_nl_jacobi( fGrid, params, 9, false, fVars ) ;
			
			calc_richards_resid( params, fGrid, fVars, fVars->approx,
				fVars->resid ) ;
			
			results->residL2Norm = vect_discrete_l2_norm( fGrid->numUnknowns,
				fVars->resid ) ;
			printf( "Resid: %.18lf\tRatio: %.18lf\n", results->residL2Norm,
				results->residL2Norm /results->oldResidL2Norm ) ;
			
			continue ;
		}
		
		//Set the norm in residual of the old approximation to be equal to the
		//norm in residual of the current approximation
		results->oldResidL2Norm = results->residL2Norm ;
		
		//Perform an iteration of nonlinear multigrid
		richards_nlmg( grids, aVarsList, params, results->itCnt ) ;
		
		//Calculate the residual in approximation
		calc_richards_resid( params, fGrid, fVars, fVars->approx,
			fVars->resid ) ;
			
		//Calculate the norm in residual
		results->residL2Norm = vect_discrete_l2_norm( fGrid->numUnknowns,
			fVars->resid ) ;
			
		results->itCnt++ ;
		if( params->debug ){
			printf( "Iteration: %d\tResidual norm: %.18lf\tRatio: %.18lf\n",
				results->itCnt, results->residL2Norm, results->residL2Norm /
				results->oldResidL2Norm ) ;
		}
	}
}

/** Performs a single cycle of a given nonlinear multigrid iteration
 *	\param [in] grids	A hierarchy of grids on which to perform nonlinear
 *						multigrid
 *	\param [in] aVarsList	Variables associated with the approximation on each
 *							grid in the hierarchy
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] callCnt	Indicates how many times the multigrid algorithm has
 *						been called on the current grid level
 */
void richards_nlmg( GridParameters **grids, ApproxVars **aVarsList,
	AlgorithmParameters *params, const int callCnt )
{
	int i, mgCnt ; //Loop counters
	GridParameters *fGrid, *cGrid ; //Parameters defining the fine and coarse
									//grid, respectively
	ApproxVars *fVars, *cVars ; //Variables associated with the approximation on
								//the fine grid and coarse grid, respectively
								 
	double *rApprox ; //The approximation from the fine grid restricted to the
					  //coarse grid
	double *cCorrection ; //The correction term on the coarse grid		  
					  
	double residNorm ;
	double currSigma ;
	
	//Set a placeholder for the fine grid and the variables associated with the
	//approximation on that grid
	fGrid = grids[params->currLevel-1] ;
	fVars = aVarsList[ params->currLevel-1 ] ;
	
	//Set a placeholder for the coarse grid
	cGrid = grids[params->currLevel-2] ;
	
	//Assign the appropriate memory to the restricted approximation
	rApprox = (double*)calloc( cGrid->numNodes, sizeof( double ) ) ;
	cVars = aVarsList[ params->currLevel-2 ] ;
		
	//Loop through the number of multigrid cycles we need to perform
	for( mgCnt=0 ; mgCnt < params->numMgCycles ; mgCnt++ ){
		//Perform pre-smoothing on the fine grid
		if( params->isSimplifiedNewt ){
			//Perform one iteration in which the operator is calculated
			re_smooth_nl_jacobi( fGrid, params, 1, true, fVars ) ;
			//Perform the rest of the pre-smooths without updating the nonlinear
			//operator
			re_smooth_nl_jacobi( fGrid, params, params->numPreSmooths - 1,
				false, fVars ) ;
		}
		else{
			re_smooth_nl_jacobi( fGrid, params, params->numPreSmooths, true,
				fVars ) ;
		}
		
		//Calculate the residual on the fine grid
		calc_richards_resid( params, fGrid, fVars, fVars->approx,
			fVars->resid ) ;
			
		//Calculate the residual in approximation, and use this to calculate
		//the weighting parameter to use in the coarse grid correction step
		residNorm = vect_discrete_l2_norm( fGrid->numUnknowns, fVars->resid ) ;
		currSigma = get_sigma( params, residNorm ) ;
			
		//Restrict the approximation to the coarse grid
		restriction( params, fGrid, cGrid, fVars->approx, R_TYPE_FW, 0,
			rApprox ) ;
		set_dirichlet_boundary( params, cGrid, rApprox ) ;
			
		//Set the initial approximation on the coarse grid to be the restriction
		//of the fine grid approximation
		copy_vectors( cGrid->numNodes, cVars->approx, rApprox ) ;
			
		//Restrict the residual to the coarse grid
		restrict_residual( params, fGrid, cGrid, fVars->resid,
			fVars->bndTerm, cVars->resid, cVars->bndTerm ) ;
			
		//Calculate the coarse grid right hand side. For this we need to make
		//sure that the volumetric wetness and the hydraulic conductivity have
		//been appropriately set on the coarse grid
		set_theta( params, cGrid, cVars->approx, cVars->reTheta ) ;
		set_hydr_cond( params, cGrid, cVars->approx, cVars->reHCond ) ;
		calc_richards_coarse_rhs( params, cGrid, cVars->approx, currSigma,
			cVars ) ;
		
		if( params->currLevel == params->cGridLevel ){
			//The next grid level is the coarsest, so we perform and 'exact'
			//solve
			if( params->isSimplifiedNewt )
				re_smooth_nl_simp_jac_to_converge( cGrid, params, cVars ) ;
			else
				re_smooth_nl_jacobi_to_converge( cGrid, params, cVars ) ;
		}
		else{
			//Perform nonlinear multigrid on the next grid level down
			params->currLevel-- ;
			richards_nlmg( grids, aVarsList, params, callCnt ) ;
			params->currLevel++ ;
		}
		
		//Calculate the correction to interpolate to the fine grid
		cCorrection = cVars->tmpApprox ;
		for( i=0 ; i < cGrid->numUnknowns ; i++ )
			cCorrection[i] = (cVars->approx[i] - rApprox[i]) / currSigma ;
		for( i= cGrid->numUnknowns ; i < cGrid->numNodes ; i++ )
			cCorrection[i] = 0.0 ;
				
		//Interpolate the correction to the fine grid
		prolongate( params, cGrid, cCorrection, fGrid, fVars->tmpApprox ) ;
		
		//Update the approximation on the fine grid
		for( i=0 ; i < fGrid->numUnknowns ; i++ ){
			fVars->approx[i] += fVars->tmpApprox[i] ;
		}
		
		//Smooth the approximation on the fine grid
		if( params->isSimplifiedNewt ){
			//Perform a single iteration of nonlinear smoothing, where the
			//nonlinear operator is calculated
			re_smooth_nl_jacobi( fGrid, params, 1, true, fVars ) ;
			//Perform the rest of the post smoothing iterations without updating
			//the nonlinear operator
			re_smooth_nl_jacobi( fGrid, params, params->numPostSmooths -1,
				false, fVars ) ;
		}
		else{
			//Perform post-smoothing iterations
			re_smooth_nl_jacobi( fGrid, params, params->numPostSmooths, true,
				fVars ) ;
		}
	}

	//Clean up
	free( rApprox ) ;
}

/** Calculates the right hand side for the nonlinear multigrid method on the
 *	grid given for a Richards equation type problem. This is simply the residual
 *	restricted from the fine grid perturbed by the operator applied on the
 *	coarse grid approximation
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to calculate the perturbed right hand
 *						side
 *	\param [in] pressure	The current approximation of the pressure profile
 *	\param [in,out] aVars	Variables associated with the approximation on the
 *							current grid level. The desired output from this
 *							method is the variable ApproxVars::rhs
 */
void calc_richards_coarse_rhs( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure, double sigma,
	ApproxVars *aVars )
{
	int i, j, elCnt ; //Loop counters
	int rowInd ; //The index into the mass matrix for the start of the row
				 //associated with a given node
	double tmp ; //An intermediate value used in the calculations
	const Node *node ; //A node on the grid
	const Element *e ; //An element on the grid
	
	//We assume that both the volumetric wetness and the hydraulic conductivity
	//have been calculated for the current value of the pressure
	
	//Copy the residual into the right hand side
	for( i=0 ; i < grid->numUnknowns ; i++ )
		aVars->rhs[i] = -sigma * aVars->resid[i] ;
	
	//Add a contribution from the term associated with the volumetric wetness
	if( params->lumpMassMat ){
		for( i=0 ; i < grid->numUnknowns ; i++ ){
			aVars->rhs[i] += aVars->reTheta[i] *
				aVars->massMat[ grid->rowStartInds[i] ] ;
		}
	}
	else{
		for( i=0 ; i < grid->numUnknowns ; i++ ){
			//get the current node on the grid
			node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
			rowInd = grid->rowStartInds[i] ;
			tmp = aVars->reTheta[i] * aVars->massMat[ grid->rowStartInds[i] ] ;
			for( j=1 ; j < node->numNghbrs ; j++ ){
				tmp += aVars->reTheta[ grid->mapG2A[ node->nghbrs[ j ] ] ] *
					aVars->massMat[ rowInd + j ] ;
			}
			
			//Add a contribution onto the right hand side
			aVars->rhs[i] += tmp ;
		}
	}
	
	//Add a contribution to the right hand side from the term associated with
	//the hydraulic conductivity
	//Loop through all the elements on the grid
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a placeholder to the current element
		e = &(grid->elements[ elCnt ]) ;
		//Loop through the nodes on the element
		for( i=0 ; i < 3 ; i++ ){
			j = grid->mapG2A[ e->nodeIDs[i] ] ;
			//If we are at a Dirichlet boundary node we continue
			if( j >= grid->numUnknowns ) continue ;
			
			aVars->rhs[j] += params->tdCtxt.weightFact *
				int_richards_h_cond( params, grid, aVars->reHCond, e, i,
					pressure ) ;
		}
	}
}




















