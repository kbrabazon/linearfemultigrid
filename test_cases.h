#ifndef _TEST_CASES_H
#define _TEST_CASES_H

#include "useful_definitions.h"

/** \file test_cases.h
 *	\brief Functions with functionality dependent on which test case is being
 *		   performed
 */

//Initialises the approximation
void initialise_approx( const AlgorithmParameters *params,
	const GridParameters *grid,	double* approx ) ;
double approx_function( const AlgorithmParameters *params,
	double x, double y, double gridSpacing ) ;
	
//Sets up the right hand side
double rhs_function( const AlgorithmParameters* params, const ApproxVars *aVars,
	double x, double y, int elId ) ;

//Indicator functions for a given test case
bool is_non_linear( int testCase ) ;
bool can_recover_grads( int testCase ) ;
bool exists_analytic_deriv( const AlgorithmParameters* params ) ;
bool is_time_dependent( int testCase ) ;
bool is_block_jacobi_smooth( const AlgorithmParameters *params ) ;
bool has_spart( const AlgorithmParameters *params ) ;

bool is_solution_known( int testCase ) ;
void calculate_exact_solution( const AlgorithmParameters *params,
	const GridParameters* grid, double* sol ) ;
double solution_function( const AlgorithmParameters *params, const double x,
	const double y ) ;
void solution_as_string( const AlgorithmParameters *params, char* string ) ;
	
//Gives the contribution to the stiffness matrix for the basis functions at the
//given nodes on the given element. This uses a global numbering for the
//approximation on an element
double exact_stiff_term( int node1, int node2, const Element *e,
	const double* approx, const ApproxVars *aVars, const GridParameters* grid,
	const AlgorithmParameters* params ) ;
	
//Gives the contribution to the stiffness matrix for the basis functions at the
//given nodes on the given element. This uses a global numbering for the
//approximation on an element and assumes that recovery of the gradient function
//has been performed
double recover_stiff_term( int node1, int node2, const Element *e,
	const double* approx, const CVector2D* recGrads, const GridParameters* grid,
	const AlgorithmParameters* params ) ;
	
//Gives the contribution to the stiffness matrix for the basis functions at the
//given nodes on the given element. This uses a local numbering for the
//approximation on an element and assumes that recovery of the gradient function
//has been performed
double recover_stiff_term_ln( int node1, int node2, const Element *e,
	const double* approx, const CVector2D* recGrads, const GridParameters* grid,
	const AlgorithmParameters* params )	;
	
//Calculates the exact value of the derivative of the operator for the given
//test case on the given element for the basis functions centred at the given
//nodes
double exact_deriv_term( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars ) ;
//Calculates the contribution to the symmetric part of the derivative of the
//operator for a given test case on the given element for the basis functions
//centered at the given nodes
double exact_deriv_spart( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars ) ;
	
//Calculates the contribution to the symmetric part of the derivative of an
//operator for a given test case on the given element for the basis functions
//centered at the given nodes
double numeric_deriv_spart( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars ) ;
	
//Calculates and sets the numeric derivative and the symmetric part of a numeric
//derivative of an operator
void set_numeric_deriv_and_spart( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars, double *jac, double *spart ) ;
	
//Calculates the contribution to the symmetric part of the Jacobian, and to the
//Jacobian, of an operator over a given element. This includes mass matrix
//lumping (i.e. putting all the time derivative terms from a column on the
//diagonal)
void set_numeric_deriv_and_spart_lumped_mass( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars, double *jac, double *spart ) ;
	
//Calculates the contribution to the Jacobian matrix, where lumping of the mass
//matrix is performed
void set_lumped_mass_mat_jac( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars, double *jac ) ;
	
void set_lumped_mass_mat_jac_diags( int ni, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars, double *jDiags ) ;
	
//Calculates an approximation to the derivative of the operator for the given
//test case on the given element for the basis functions centered at the given
//nodes
double numeric_deriv_term( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars ) ;
	
//Calculates a contribution from a boundary term to the right hand side.
double boundary_rhs_contribution( const AlgorithmParameters *params,
	const GridParameters *grid, const Element *e, int nodeInd,
	const Node *node ) ;
	
//Checks whether the grid boundary types match with the expectation of the
//boundary type for a given test case, and prints a warning message if they
//don't
int check_boundary_type( const GridParameters *grid,
	const AlgorithmParameters *params ) ;
	
//Checks whether a given test case identifier is for a Richards equation type
//problem
bool is_richards_eq( int testCase ) ;

//Checks whether a given test case identifier is for a \f$p\f$-Laplacian type
//problem
bool is_p_laplacian( int testCase ) ;

//Checks whether a given test case uses a piecewise continuous coefficient on
//the domain
bool has_pc_coeff( int testCase ) ;

//Returns the value of the piecewise constant coefficient function for a test
//case
double get_coeff_val( const AlgorithmParameters *params, double x, double y ) ;

//Sets the values of a piecewise constant coefficient on the domain
void set_pc_coeff_vals( const AlgorithmParameters *params,
	const GridParameters *grid, double *coeff ) ;

#endif
