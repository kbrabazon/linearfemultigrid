#include "nl_multigrid.h"
#include "test_cases.h"
#include "richards_eq.h"

/** Performs one iteration of nonlinear multigrid. For a data flow diagram for
 *	the functionality contained in this function see \ref secNLGraph.
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grids	A hierarchy of grids available to use
 *	\param [in,out]	aVarsList	A hierarchy of approximation variables available
 *								to use. The approximation on the finest grid
 *								level is the desired output from the function
 *	\param [in]	callCnt	Indicates how many times the multigrid algorithm has
 *						been called on the current grid level
 */
void nl_multigrid( AlgorithmParameters* params, GridParameters **grids,
	ApproxVars **aVarsList, const int callCnt )
{
	int i, j, mgCnt ; //Loop variables
	
	const Node *cNode ; //A node on the coarse grid
	int rowInd ; //The index into the sparse representation of the matrix for
				 //the start of a row corresponding to a node on the grid
	double residNorm, currSigma ;

	double* rApprox ;	 //The approximation from the fine grid restricted to
						 //the coarse grid
	double* cCorrection ; //The correction on the coarse grid
	double *jacobian ; //Required when smoothing, if we are using a block
					   //smoother
	
	GridParameters *fGrid, *cGrid ;//Parameters defining the fine (coarse) grid,
								   //respectively
	ApproxVars *fVars, *cVars ; //Variables related to the approximation on the
								//fine (coarse) grid, respectively
										  
	double tmp ;//Temporary variable used as a placeholder in several cases.
				//It is explained what this is used for when it is used
	char debugFName[100] ; //Filename for writing output to when debugging
	
	//Set a placeholder for the fine grid we are operating on
	fGrid = grids[params->currLevel-1] ;
	fVars = aVarsList[params->currLevel-1] ;
	
	//Set a placeholder for the coarse grid that we are operating on. We assume
	//that the memory has been assigned and the grid set up appropriately
	//elsewhere
	cGrid = grids[params->currLevel-2] ;
	
	//Set a placeholder for the variables related to the approximation on the
	//coarse grid. We assume that the memory has been assigned and the grid set
	//up appropriately elsewhere
	cVars = aVarsList[params->currLevel-2] ;
	
	//The jacobian is set to NULL by default, as this is only needed if we are
	//performing a block smooth
	jacobian = NULL ;
	if( is_block_jacobi_smooth( params ) )
		jacobian = (double*) malloc( fGrid->numMatrixEntries * 
			sizeof( double ) ) ;
	
	//Assign the required memory for the approximation restricted to the coarse
	//grid.
	rApprox = (double*) calloc( cGrid->numNodes, sizeof( double ) ) ;
	
	//Set the values at the boundary nodes in the restricted approximation
	copy_vectors( cGrid->numNodes - cGrid->numUnknowns,
		rApprox + cGrid->numUnknowns, cVars->approx + cGrid->numUnknowns ) ;
	
	for( mgCnt=0 ; mgCnt<params->numMgCycles ; mgCnt++ ){
		//Pre-smooth
		smooth( fGrid, params, params->numPreSmooths, jacobian, fVars ) ;
		
		//Update the stiffness matrix
		if( params->recoverGrads )
			recover_gradients( params, fGrid, fVars->approx, fVars->recGrads ) ;
		calculate_iteration_matrix( fGrid, params, fVars->approx, fVars ) ;
		
		//Calculate the residual on the fine grid
		calculate_residual( params, fGrid, fVars ) ;
		
		//Calcualte the norm in the residual
		residNorm = vect_discrete_l2_norm( fGrid->numUnknowns, fVars->resid ) ;
		currSigma = get_sigma( params, residNorm ) ;
		
		//printf( "Sigma: %.18lf\n", currSigma ) ;
		
		if( cVars->tmpApprox == NULL )
			cVars->tmpApprox = (double*) calloc( cGrid->numUnknowns,
				sizeof( double ) ) ;

		//Restrict the residual to the coarse grid
		restriction( params, fGrid, cGrid, fVars->resid, R_TYPE_FW,
			1, cVars->tmpApprox ) ;

		//Restrict the approximation to the coarse grid
		restriction( params, fGrid, cGrid, fVars->approx, R_TYPE_FW,
			0, rApprox ) ;
	
		//Calculate the stiffness matrix on the coarse grid
		if( params->recoverGrads )
			recover_gradients( params, cGrid, rApprox, cVars->recGrads ) ;
		calculate_iteration_matrix( cGrid, params, rApprox, cVars ) ;
			
		
		//Find the coarse grid right hand side, and set up the initial
		//approximation for the perturbed equation
		for( i=0 ; i < cGrid->numUnknowns ; i++ ){
			tmp = 0.0 ;
			cNode = &(cGrid->nodes[ cGrid->mapA2G[i] ]) ;
			rowInd = cGrid->rowStartInds[i] ;
			for( j=0 ; j < cNode->numNghbrs ; j++ )
				tmp += cVars->iterMat[rowInd + j] *
					rApprox[ cGrid->mapG2A[ cNode->nghbrs[j] ] ] ;
			cVars->rhs[i] = currSigma * cVars->tmpApprox[i] + tmp ;
			cVars->approx[i] = rApprox[i] ;
		}
		//Next grid is the coarsest, so perform an 'exact' solve
		if( params->currLevel == params->cGridLevel ){
			nl_gauss_seidel_to_converge( cGrid, params, cVars, 0 ) ;
		}
		else{
			//Perform a multigrid iteration on the next grid level down
			params->currLevel-- ;
			nl_multigrid( params, grids, aVarsList, callCnt ) ;
			params->currLevel++ ;
		}
	
		//This is now a bit of a cheat. We point the correction term at the same
		//array as the solution to the perturbed coarse system to prevent us
		//from performing unneccessary memory allocation
		cCorrection = cVars->approx ;
		for( i=0 ; i < cGrid->numUnknowns ; i++ )
			cCorrection[i] = ( cVars->approx[i] - rApprox[i] ) / currSigma ;	
			
		//If we are debugging print the correction from the coarse grid
		//to file
		if( params->debug ){
			//Generate an appropriate file name
			sprintf( debugFName, "/tmp/correctLvl%d.txt",
				params->currLevel-1 ) ;
			print_grid_func_no_bnd_to_file( debugFName, cGrid, cCorrection,
				callCnt ) ;
		}
		
		if( fVars->tmpApprox == NULL )
			fVars->tmpApprox = (double*) calloc( fGrid->numUnknowns,
				sizeof( double ) ) ;
		
		//Prolongate the correction term
		prolongate( params, cGrid, cCorrection, fGrid, fVars->tmpApprox ) ;
		
		//Update the approximation
		for( i=0 ; i < fGrid->numUnknowns ; i++ )
			fVars->approx[i] += fVars->tmpApprox[i] ;
		
		//Calculate the stiffness matrix
		if( params->recoverGrads )
			recover_gradients( params, fGrid, fVars->approx, fVars->recGrads ) ;
		calculate_iteration_matrix( fGrid, params, fVars->approx, fVars ) ;
	
		//Post-smooth
		smooth( fGrid, params, params->numPostSmooths, jacobian, fVars ) ;
		
		//If we are debugging we print out the approximation on the fine grid
		if( params->debug ){
			//Create the appropriate filename
//			sprintf( debugFName, "/tmp/approxLvl%d.txt", params->currLevel ) ;
//			//print the residual to file
//			print_grid_func_to_file( debugFName, fGrid, fVars->approx,
//				callCnt ) ;
			sprintf( debugFName, "/tmp/mgApproxLvl%d_%d.vtk",
				params->fGridLevel, callCnt ) ;
			print_grid_func_paraview_format( debugFName, fGrid, fVars->approx ) ;
			//Now print out the residual in approximation
			//Create the appropriate filename
			sprintf( debugFName, "/tmp/residLvl%d.txt", params->currLevel ) ;
			//print the residual to file
			print_grid_func_no_bnd_to_file( debugFName, fGrid, fVars->resid,
				callCnt ) ;
		}
	}
	
	//Clean up
	free( rApprox ) ;
	if( jacobian != NULL )
		free( jacobian ) ;
}

/** Returns the value of sigma to use in the nonlinear multigrid algorithm. This
 *	is calculated using information from Note 9.3.2, pg. 186 and Remark 9.5.2,
 *	pg. 193 of <em>Multi-Grid Methods and Applications</em>, Hackbusch, 1985.
 *	Note that the exact values described are not used here as their calculation
 *	would be costly. Instead constants weighted using the grid levels are used
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] residNorm	The norm of the residual in the current
 *							approximation. This is used to calculate an
 *							appropriate sigma
 *	\return \p double value containing a value deemed to be 'suitable' for the 
 *			scaling parameters sigma in the nonlinear multigrid method
 */
double get_sigma( const AlgorithmParameters *params, double residNorm )
{
	double retVal ;
	//If we don't want to update the parameter simply use the one that is read
	//in
	if( !params->adaptUpdate )
		return params->sigma ;
		
	//Now we want to calculate an appropriate value
	retVal = 1.0 / (pow( 2, params->currLevel ) * residNorm ) ;
	
	return fmin( fmax( retVal, MIN_SIGMA ), 1.0 ) ;
}

/** Performs Newton-Multigrid iterations on the finest grid level until some
 *	stopping criterion is met. For a data flow diagram for the functionality
 *	contained in this function see \ref secNewtGraph.
 *	\param [in] grids	A Hierarchy of grids available to use
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVarsList	A hierarchy of approximation variables available
 *								to use. The approximation on the finest grid
 *								is the desired output of the function
 *	\param [out] results	Stores information about the execution of the
 *							multigrid iterations
 */
void newton_mg( GridParameters **grids, AlgorithmParameters* params,
	ApproxVars **aVarsList, Results *results )
{
	int mgCnt, i ; //Loop counters
	double* approx ; //The approximation of the exact solution
	double* rhs ; //The right hand side to converge to
	GridParameters *grid ; //The grid on which we are currently operating
	ApproxVars *fVars ; //Variables associated with the approximation on the
						//current grid
	
	int performIteration ;//Indicator indicating whether or not a multigrid
						  //iteration should be performed
	
	double* tmpRHS ; //This is used as a placeholder to switch the rhs and resid
				     //vectors to prevent from having to copy values in memory,
				     //which is an expensive operation
	double *tmpApprox ; //This is used as a placeholder to switch the
						//approximation in the ApproxVars structure between
						//pointing to the approximation to the nonlinear and
						//linear problem. This is used to save on copying
						//values in memory
			  
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
	char debugFName[100] ; //The filename of a file to which to write debugging
						   //information
	InfNorm infNorm ; //Stores information about the infimum norm of the
					  //correction term
						   
	double resid_d, oldResid_d, origResid_d; //Used for debugging purposes 01/10. If in doubt
			//remove these values as they should have no effect on the running
			//of the program
						   
	//Make sure that the grid on which we are operating is the finest one
	params->currLevel = params->fGridLevel ;
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
	
	//We assume that the grids in the hierarchy have already been set up, and
	//simply set a placeholder to make the code look a little neater
	grid = grids[params->fGridLevel-1] ;
	
	//We assume that the approximation variables for the grids in the hierarchy
	//have already been set up, and we simply set a placeholder to make the code
	//look a little neater
	fVars = aVarsList[params->fGridLevel-1] ;
	
	//If we are not performing FMG and the problem is not time dependent then
	//print output to console
	if( !params->isFMG && !is_time_dependent( params->testCase ) )
		printf( "Newton Multigrid:\n" ) ;
	
	//Initialise the required variables
	approx = (double*) calloc( grid->numNodes, sizeof( double ) ) ;
	rhs = (double*) calloc( grid->numUnknowns, sizeof( double ) ) ;
	
	//Initialise the approximation, unless we have performed FMG or the problem
	//is time dependent
	if( params->setUpApprox && !is_time_dependent( params->testCase ) )
		initialise_approx( params, grid, fVars->approx ) ;
		
	//Copy the values that are originally stored in the approximation into a
	//different variable
	copy_vectors( grid->numNodes, approx, fVars->approx ) ;
	
	//Initialise the RHS if the problem is not time dependent. Otherwise it is
	//assumed that the RHS has already been set up
	if( !is_time_dependent( params->testCase ) )
		initialise_rhs( grid, params, fVars, 1.0, rhs, fVars->bndTerm ) ;
	else
		copy_vectors( grid->numUnknowns, rhs, fVars->rhs ) ;
	
	tmpRHS = fVars->rhs ;
	fVars->rhs = rhs ;
		
	//Calculate the nonlinear iteration matrix so that we can calculate the
	//residual in approximation
	calculate_iteration_matrix( grid, params, fVars->approx, fVars ) ;
	
	//Calculate the residual. This is also the right hand side for the linear
	//multigrid iteration
	calculate_residual( params, grid, fVars ) ;
	
	//Calculate the original residual norm, and initialise the other residual
	//norm variables in both the L2 and the H1 norm
	results->origResidL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
		fVars->resid ) ;
	
	fVars->rhs = tmpRHS ;
	copy_vectors( grid->numUnknowns, fVars->rhs, fVars->resid ) ;
	
	//Calculate the H1 norm of the approximation
	results->origResidH1Norm = vect_discrete_h1_norm( grid, fVars->resid ) ;
		
	results->residL2Norm = results->origResidL2Norm ;
	results->residH1Norm = results->origResidH1Norm ;
	
	results->oldResidL2Norm = results->residL2Norm + 1.0 ;
	
	//If we are not performing FMG print output to the console
	if( !params->isFMG && !is_time_dependent( params->testCase ) ){
		printf( "Original residual L2-norm: %.18f\n",
			results->origResidL2Norm ) ;
		printf( "Original residual H1-norm: %.18lf\n",
			results->origResidH1Norm ) ;
	}
	
	//Print out the initial function to file, if we are performing a debugging
	//operation
	if( params->debug && !is_time_dependent( params->testCase ) ){
		sprintf( debugFName, "/tmp/rhsLvl%d.txt", params->fGridLevel ) ;
		print_grid_func_no_bnd_to_file( debugFName, grid, rhs, 0 ) ;
		sprintf( debugFName, "/tmp/mgApproxLvl%d.txt", params->fGridLevel ) ;
		print_grid_func_to_file( debugFName, grid, fVars->approx, 0 ) ;
		sprintf( debugFName, "/tmp/mgApproxLvl%d_0.vtk", params->fGridLevel ) ;
		print_grid_func_paraview_format( debugFName, grid, fVars->approx ) ;
		sprintf( debugFName, "/tmp/residLvl%d.txt", params->fGridLevel ) ;
		print_grid_func_no_bnd_to_file( debugFName, grid, fVars->resid, 0 ) ;
	}
	
//	sprintf( debugFName, "/tmp/rhsLvl%d.txt", params->currLevel ) ;
//	print_grid_func_no_bnd_to_file( debugFName, grid, fVars->rhs, 0 ) ;
//	for( i=params->currLevel-1 ; i >= params->cGridLevel-1 ; i-- ){
//		sprintf( debugFName, "/tmp/rhsLvl%d.txt", i ) ;
//		debugFile = fopen( debugFName, "w" ) ;
//		fclose( debugFile ) ;
//	}
	
	//Initialise the count of the iterations to zero
	results->itCnt = 0 ;
		
	if( params->isFMG )
		performIteration = 1 ;
	else{
		if( is_time_dependent( params->testCase ) ){
			performIteration = (
			results->residL2Norm / results->origResidL2Norm > TD_TOL
			&& results->itCnt < params->maxIts &&
			results->residL2Norm < MAX_RESID_FACT * results->origResidL2Norm ) ;
		}
		else{
			performIteration = (
				results->residL2Norm / results->origResidL2Norm > TOL1
				&& results->itCnt < params->maxIts &&
				results->residL2Norm < MAX_RESID_FACT *
				results->origResidL2Norm ) ;
		}
	}
	
	results->residRatio = 1.0 ;
			
	tmpApprox = fVars->approx ;
			
	while( performIteration ){
		//Set the value of the old residual to be the value of the current
		//residual, before we recalculate the current residual
        results->oldResidL2Norm = results->residL2Norm ;
        results->oldResidH1Norm = results->residH1Norm ;
        
        //Calculate the necessary information for the Newton multigrid iteration
        if( results->itCnt == 0 || !params->isSimplifiedNewt ||
        	results->residRatio > 0.9 )
	        set_newton_mg_iteration_vars( params, grids, aVarsList ) ;
		        
        //Set the correction term to zero initially
        fVars->approx = tmpApprox ;
        set_dvect_to_zero( grid->numUnknowns, fVars->approx ) ;
       	
       	if( params->innerIt == IT_MULTIGRID ){
       		calculate_residual( params, grid, fVars ) ;
			origResid_d = resid_d = vect_discrete_l2_norm( grid->numUnknowns,
				fVars->resid ) ;
		   	//Perform a linear multigrid cycle for the given number of times
			for( mgCnt = 0 ; mgCnt < params->innerIts ; mgCnt++ ){
//			mgCnt = 0 ;
//			while( resid_d / origResid_d > LIN_TOL && mgCnt < MAX_INNER_ITS ){
				oldResid_d = resid_d ;
				linear_multigrid( params, grids, aVarsList ) ;
				calculate_residual( params, grid, fVars ) ;
				resid_d = vect_discrete_l2_norm( grid->numUnknowns,
					fVars->resid ) ;
				printf( "%.18lf, ", resid_d / oldResid_d ) ;
//				mgCnt++ ;
			}
//			printf( "\nInner iterations: %d\n", mgCnt ) ;
			printf( "\n" ) ;
		}
		else if( params->innerIt == IT_PCG){
//			calculate_residual( params, grid, fVars ) ;
//			oldResid_d = resid_d = vect_discrete_l2_norm( grid->numUnknowns,
//				fVars->resid ) ;
			pcg( params, grids, aVarsList, params->innerIts ) ;
//			calculate_residual( params, grid, fVars ) ;
//			resid_d = vect_discrete_l2_norm( grid->numUnknowns,
//				fVars->resid ) ;
//			printf( "%.18lf\n", resid_d / oldResid_d ) ;
		}
		else if( params->innerIt == IT_PCGMRES ){
			calculate_residual( params, grid, fVars ) ;
			oldResid_d = resid_d = vect_discrete_l2_norm( grid->numUnknowns,
				fVars->resid ) ;
			pcgmres( params, grids, aVarsList, params->innerIts ) ;
			calculate_residual( params, grid, fVars ) ;
			resid_d = vect_discrete_l2_norm( grid->numUnknowns,
				fVars->resid ) ;
			printf( "%.18lf\n", resid_d / oldResid_d ) ;
		}
       		
		tmpRHS = fVars->rhs ;
		fVars->rhs = rhs ;
		
		//Calculate the infimum norm of the correction
		vect_infty_norm( grid->numUnknowns, fVars->approx, &infNorm ) ;
		results->maxCorrection = infNorm.val ;

		//Apply the Newton step. In this method we update the approximation,
		//calculate the appropriate iteration matrix, residual and residual norm
		//as well
		if( params->adaptUpdate ){
			params->newtonDamp = calc_newton_update( params, grid,
				fVars, approx, fVars->approx ) ;
				
//			printf( "Newton update: %.18lf\n", params->newtonDamp ) ;
			
			//Update the approximation
			for( i=0 ; i < grid->numUnknowns ; i++ )
				approx[i] += params->newtonDamp * fVars->approx[i] ;
				
			//Recalculate the iteration matrix
			calculate_iteration_matrix( grid, params, approx, fVars ) ;
		
			//Calculate the residual
			fVars->approx = approx ;
			calculate_residual( params, grid, fVars ) ;
		
			//Reset the pointer
			fVars->approx = tmpApprox ;
			results->residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
				fVars->resid ) ;
		}
		else{
			//Update the approximation using the correction. We don't do anything
			//fancy here, just use the damping parameter to update the
			//approximation
			for( i=0 ; i < grid->numUnknowns ; i++ )
				approx[i] += params->newtonDamp * fVars->approx[i] ;
		
			//Recalculate the iteration matrix
			calculate_iteration_matrix( grid, params, approx, fVars ) ;
		
			//Calculate the residual
			fVars->approx = approx ;
			calculate_residual( params, grid, fVars ) ;
		
			//Reset the pointer
			fVars->approx = tmpApprox ;
			//Calculate the current residual norm for the nonlinear problem
			results->residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
				fVars->resid ) ;
		}
		
		results->maxCorrection *= params->newtonDamp ;
		
		//printf( "Newton Damping Factor: %.5lf\n", params->newtonDamp ) ;
				
		//Set the number of iterations of the linear solver to be used in the
		//inner iteration of the Newton method
	    set_inner_its( params, results->residL2Norm ) ;
	    	
	    if( params->debug && !is_time_dependent( params->testCase ) ){
	    	sprintf( debugFName, "/tmp/correctionLvl%d.txt",
	    		params->fGridLevel ) ;
	    	print_grid_func_to_file( debugFName, grid, fVars->approx,
	    		results->itCnt ) ;
	    }
		
		//Instead of copying the values across simply set the approximation to
		//point to the approximation
		fVars->approx = approx ;
		
		//Print the new approximation to file, if we are debugging
		if( params->debug && !is_time_dependent( params->testCase ) ){
//			sprintf( debugFName, "/tmp/mgApproxLvl%d.txt",
//				params->fGridLevel ) ;
//			print_grid_func_to_file( debugFName, grid, fVars->approx, 1 ) ;
			sprintf( debugFName, "/tmp/mgApproxLvl%d_%d.vtk",
				params->fGridLevel, results->itCnt+1 ) ;
			print_grid_func_paraview_format( debugFName, grid, fVars->approx ) ;
			sprintf( debugFName, "/tmp/residLvl%d.txt",
				params->fGridLevel ) ;
			print_grid_func_no_bnd_to_file( debugFName, grid,
				fVars->resid, 1 ) ;
		}
		
		fVars->rhs = tmpRHS ;
		copy_vectors( grid->numUnknowns, fVars->rhs, fVars->resid ) ;
		
		//Calculate the residual norm
		results->residH1Norm = vect_discrete_h1_norm( grid, fVars->resid ) ;
		//Increment the iteration count
		results->itCnt++ ;
		
		results->residRatio = results->residL2Norm / results->oldResidL2Norm ;
		
		//If we are not performing FMG print information to the console
		if( !params->isFMG && !is_time_dependent( params->testCase ) ){
			printf( "Iteration %d:\n\tNew L2 Residual: %.18lf\tRatio: %.18lf\n",
				results->itCnt, results->residL2Norm,
				results->residL2Norm / results->oldResidL2Norm ) ;
			printf( "\tNew H1 Residual: %.18lf\tRatio: %.18lf\n",
				results->residH1Norm, 
				results->residH1Norm / results->oldResidH1Norm ) ;
		}
			
		if( params->isFMG )
			performIteration = 0 ;
		else if( is_time_dependent( params->testCase ) ){
			performIteration = (
				results->residL2Norm / results->origResidL2Norm > TD_TOL
				&& results->itCnt < params->maxIts &&
				results->residL2Norm < MAX_RESID_FACT * results->origResidL2Norm
				&& results->maxCorrection > 1e-13) ;
		}
		else{
//			performIteration = (
//				results->residL2Norm <
//				MAX_RESID_FACT * results->origResidL2Norm &&
//				results->itCnt < params->maxIts ) ;
			performIteration = (
				results->residL2Norm / results->origResidL2Norm > TOL1
				&& results->itCnt < params->maxIts &&
				(results->residL2Norm <
				MAX_RESID_FACT * results->origResidL2Norm ) ) ;
		}
	}
	
	//Set the appropriate rhs before we return
	fVars->rhs = rhs ;
	fVars->approx = approx ;
	
	//Free the memory allocated. Note that the rhs vector is not deleted, as
	//this is now part of the approximation variables structure, and will be
	//free'd in the destructor for the approximation variables
	free( tmpApprox ) ;
	free( tmpRHS ) ;
}

/** Sets up the iteration matrices in the Newton-Multigrid hiearchy, so that
 *	these do not need to be recalculated for repeated multigrid iterations
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] grids	The hierarchy of grids to use. The member variable
 *							GridParameters::currApprox is updated on each grid
 *							in the hierarchy in this method
 *	\param [in,out] aVarsList	The list of approximation varibales on the grids
 *								in the multigrid hierarchy. The member variable
 *								ApproxVars::iterMat is set on each grid level
 *								in this method
 */
void set_newton_mg_iteration_vars( const AlgorithmParameters *params,
	GridParameters **grids, ApproxVars **aVarsList )
{
	int i ; //Loop counter
	GridParameters *fGrid, *cGrid ; //Placeholders for a grids in the multigrid
									//hierarchy
	ApproxVars *fVars, *cVars ; //Placeholder for approximation variables in the
								//multigrid hierarchy
	
	//Set the placeholders for the grid and approximation variables to point to
	//the finest grid level
	fGrid = grids[params->fGridLevel-1] ;
	fVars = aVarsList[params->fGridLevel-1] ;
	cGrid = grids[params->fGridLevel-2] ;
	cVars = aVarsList[params->fGridLevel-2] ;
	
	//Make sure the Dirichlet boundary terms are set properly on the fine grid
	set_dirichlet_boundary( params, fGrid, fVars->approx ) ;
	//Calculate the value of the hydraulic conductivity and the volumetric
	//wetness if we are performing test case 14 or 15
	if( is_richards_eq( params->testCase ) ){
		//Set the values of the hydraulic conductivity and the volumetric
		//wetness at the current approximation
		set_theta_deriv( params, fGrid, fVars->approx, fVars->reTheta ) ;
		set_hydr_cond( params, fGrid, fVars->approx, fVars->reHCond ) ;
		set_hydr_cond_deriv( params, fGrid, fVars->approx,
			fVars->reHCondDeriv ) ;
	}
	
	//If we are performing preconditioned GMRES and preconditioning the
	//symmetric part of the matrix only we calculate the iteration matrix given
	//by the symmetric part of the Jacobian
	if( params->innerIt == IT_PCGMRES &&
		(params->testCase == 12 || params->gmresPrecSPart) ){
		//Set the symmetric part of the Jacobian and the Jacobian in the same
		//method to save recalculating information in separate methods
		set_gmres_spart_jac( params, fGrid, fVars ) ;
	}
	else{
		//Set the iteration matrix to be equal to the Jacobian matrix
		calculate_jacobian_matrix( fGrid, fVars, params,
			fVars->iterMat, 0 ) ;
	}
	
	//Restrict the approximation to the next coarsest grid level
	restriction( params, fGrid, cGrid, fVars->approx, R_TYPE_FW, 0,
		cVars->approx ) ;
		
	//Set the values at the Dirichlet boundary on the coarse grid
	set_dirichlet_boundary( params, cGrid, cVars->approx ) ;
	
	//Calculate the value of the hydraulic conductivity and the volumetric
	//wetness if we are performing test case 14 or 15
	if( is_richards_eq( params->testCase ) ){
		//Set the values of the hydraulic conductivity and the volumetric
		//wetness at the current approximation
		set_theta_deriv( params, cGrid, cVars->approx, cVars->reTheta ) ;
		set_hydr_cond( params, cGrid, cVars->approx, cVars->reHCond ) ;
		set_hydr_cond_deriv( params, cGrid, cVars->approx,
			cVars->reHCondDeriv ) ;
	}
	
	//If we are performing preconditioned GMRES and preconditioning the
	//symmetric part of the matrix only we calculate the iteration matrix given
	//by the symmetric part of the Jacobian
	if( params->innerIt == IT_PCGMRES &&
		(params->testCase == 12 || params->gmresPrecSPart) ){
		//Set the symmetric part of the Jacobian and the Jacobian in the same
		//method to save recalculating information in separate methods
		set_gmres_spart_jac( params, cGrid, cVars ) ;
	}
	else{
		//Set the iteration matrix to be equal to the Jacobian matrix
		calculate_jacobian_matrix( cGrid, cVars, params,
			cVars->iterMat, 0 ) ;
	}
	
	//Loop through the remaining grids
	for( i = params->fGridLevel-2 ; i > params->cGridLevel - 2 ; i-- ){
		//Set the placeholders for the current grid and the approximation
		//variables
		fGrid = grids[i] ;
		fVars = aVarsList[i] ;
		cGrid = grids[i-1] ;
		cVars = aVarsList[i-1] ;
				
		//Restrict the approximation from the fine to the coarse grid
		//Restrict the approximation to the next coarsest grid level
		restriction( params, fGrid, cGrid, fVars->approx, R_TYPE_FW, 0,
			cVars->approx ) ;
		
		//Set the values at the Dirichlet boundary on the coarse grid
		set_dirichlet_boundary( params, cGrid, cVars->approx ) ;
						
		//Calculate the value of the hydraulic conductivity and the volumetric
		//wetness if we are performing test case 14 or 15
		if( is_richards_eq( params->testCase ) ){
			//Set the values of the hydraulic conductivity and the volumetric
			//wetness at the current approximation
			set_theta_deriv( params, cGrid, cVars->approx, cVars->reTheta ) ;
			set_hydr_cond( params, cGrid, cVars->approx, cVars->reHCond ) ;
			set_hydr_cond_deriv( params, cGrid, cVars->approx,
				cVars->reHCondDeriv ) ;
		}
	
		//If we are performing preconditioned GMRES and preconditioning the
		//symmetric part of the matrix only we calculate the iteration matrix
		//given by the symmetric part of the Jacobian
		if( params->innerIt == IT_PCGMRES &&
			(params->testCase == 12 || params->gmresPrecSPart) ){
			//Set the symmetric part of the Jacobian and the Jacobian in the same
			//method to save recalculating information in separate methods
			set_gmres_spart_jac( params, cGrid, cVars ) ;
		}
		else{
			calculate_jacobian_matrix( cGrid, cVars, params,
				cVars->iterMat, 0 ) ;
		}
	}
}

/** Sets the iteration matrices for the symmetric part of the Jacobian and the
 *	Jacobian without repeating caluclations
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to calculate the derivative of the
 *						operator
 *	\param [in,out] aVars	Variables associated with the approximation on the
 *							current grid level. The desired output from this
 *							method is ApproxVars::iterMat, in which the Jacobian
 *							matrix is stored, and the FGMResCtxt::precMat which
 *							stores the symmetric part of the Jacobian matrix
 */
void set_gmres_spart_jac( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars )
{
	int i, j, elCnt ; //Loop counters
	int ind ; //An index of a grid node in the approximation
	const Element *e ; //Element on the grid
	FGMResContext *gmresCtxt ; //Placeholder for the preconditioned GMRES
							   //context
	double *jacSPart ; //Placeholder for the symmetric part of the Jacobian
					   //matrix
					   
	if( is_richards_eq( params->testCase ) && params->lumpMassMat ){
		set_gmres_spart_jac_lumped_mass( params, grid, aVars ) ;
		return ;
	}
					   
	//If the GMRES context has not been initialised, initialise it
	if( aVars->usrCtxt == NULL ){
		aVars->usrCtxt = (FGMResContext*)calloc( 1,
			sizeof( FGMResContext) ) ;
		initialise_gmres_context( grid, params,
			(FGMResContext*)aVars->usrCtxt, params->gmresReset ) ;
	}
	//Set the preconditioning matrix to be equal to the symmetric part of
	//the Jacobian
	gmresCtxt = (FGMResContext*)aVars->usrCtxt ;
	jacSPart = gmresCtxt->precMat ;
	
	//Set the symmetric part of the Jacobian and the Jacobian to zero initially
	if( !is_time_dependent( params->testCase ) ||
		is_richards_eq( params->testCase ) ){
		set_dvect_to_zero( grid->numMatrixEntries, jacSPart ) ;
		set_dvect_to_zero( grid->numMatrixEntries, aVars->iterMat ) ;
	}
	else{
		copy_vectors( grid->numMatrixEntries, aVars->iterMat, aVars->massMat ) ;
		copy_vectors( grid->numMatrixEntries, jacSPart, aVars->massMat ) ;
	}
	
	//Loop through the elements
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get the current element on the grid
		e = &(grid->elements[elCnt]) ;
		//Loop through each of the nodes on the element
		for( i=0 ; i < 3 ; i++ ){
			//Get the index into the approximation for the current node
			ind = grid->mapG2A[ e->nodeIDs[i] ] ;
			//If we are at a Dirichlet boundary we continue
			if( ind >= grid->numUnknowns ) continue ;
			//Loop through the nodes on the element
			for( j=0 ; j < 3 ; j++ ){
				//Check that we are not on a Dirichlet boundary node
				ind = grid->mapG2A[ e->nodeIDs[j] ] ;
				if( ind >= grid->numUnknowns ) continue ;
				//Set the contribution to the matrices
				set_numeric_deriv_and_spart( i, j, e, grid, params, aVars,
					aVars->iterMat, jacSPart ) ;
			}
		}
	}
	
//	for( i = 0 ; i < grid->numUnknowns ; i++ ){
//		if( jacSPart[ grid->rowStartInds[ i ] ] == 0.0 ){
//			printf( "Zero entry for symmetric part at node %d\n", i ) ;
//			printf( "Next diagonal entry: %.18lf\n",
//				jacSPart[ grid->rowStartInds[ i + 1 ] ] ) ;
//			exit( 0 ) ;
//		}
//	}
}

/** Calculates and sets the appropriate entry in the symmetric part of the
 *	Jacobian matrix for the case in which mass matrix lumping is being applied
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are currently operating
 *	\param [in,out] aVars	Variables associated with the approximation on the
 *							current grid level. The desired output from this
 *							function is FGMResContext::precMat, where the
 *							context is stored in ApproxVars::usrCtxt.
 */
void set_gmres_spart_jac_lumped_mass( const AlgorithmParameters *params, 
	const GridParameters *grid, ApproxVars *aVars )
{
	int i, j, elCnt ; //Loop counters
	int ind ; //An index of a grid node in the approximation
	const Element *e ; //Element on the grid
	FGMResContext *gmresCtxt ; //Placeholder for the preconditioned GMRES
							   //context
	double *jacSPart ; //Placeholder for the symmetric part of the Jacobian
					   //matrix
					   
	//If the GMRES context has not been initialised, initialise it
	if( aVars->usrCtxt == NULL ){
		aVars->usrCtxt = (FGMResContext*)calloc( 1,
			sizeof( FGMResContext) ) ;
		initialise_gmres_context( grid, params,
			(FGMResContext*)aVars->usrCtxt, params->gmresReset ) ;
	}
	//Set the preconditioning matrix to be equal to the symmetric part of
	//the Jacobian
	gmresCtxt = (FGMResContext*)aVars->usrCtxt ;
	jacSPart = gmresCtxt->precMat ;
	
	//Set the symmetric part of the Jacobian and the Jacobian to zero initially
	set_dvect_to_zero( grid->numMatrixEntries, jacSPart ) ;
	set_dvect_to_zero( grid->numMatrixEntries, aVars->iterMat ) ;
	
	//Loop through the elements
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get the current element on the grid
		e = &(grid->elements[elCnt]) ;
		//Loop through each of the nodes on the element
		for( i=0 ; i < 3 ; i++ ){
			//Get the index into the approximation for the current node
			ind = grid->mapG2A[ e->nodeIDs[i] ] ;
			//If we are at a Dirichlet boundary we continue
			if( ind >= grid->numUnknowns ) continue ;
			//Loop through the nodes on the element
			for( j=0 ; j < 3 ; j++ ){
				//Check that we are not on a Dirichlet boundary node
				ind = grid->mapG2A[ e->nodeIDs[j] ] ;
				if( ind >= grid->numUnknowns ) continue ;
				//Set the contribution to the matrices
				set_numeric_deriv_and_spart_lumped_mass( i, j, e, grid, params,
					aVars, aVars->iterMat, jacSPart ) ;
			}
		}
	}
}

/** Calculates the update parameter for a Newton method by fitting a quadratic
 *	function \f$ f(\omega) \f$ to points corresponding to using different
 *	update parameters \f$ \omega \f$ in the Newton step
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are performing the Newton iteration
 *	\param [in] aVars	Variables associated with the approximation on the
 * 	                 	current grid level. These may be used for different
 *	                 	things inside this function. See the source code for
 *	                 	more details
 *	\param [in] approx	The approximation before the current Newton step
 *	\param [in] correction	The correction to the approximation calculated
 *							during a Newton step
 *	\return \p double value containing the `optimal' value of the update step
 *			step size for the Newton iteration
 */
double calc_newton_update( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars, const double* approx,
	const double *correction )
{
	int i, j, pntCnt ; //Loop counters
	
	double retVal ; //The return value from the method
	
	int len = 3 ; //Needed for the LAPACK solve
	int nrhs = 1 ; //Needed for the LAPACK solve
	
	double evalPoints[3] ; //The values of the update parameter at which to
						   //sample the residual
	double residVals[3] ; //The values of the residual norm a the evaluation
						  //points
	double subMat[9] ; //The matrix of values to use in the subsystem that is
					   //solved to fit the quadratic
	double rhs[3] ; //The right hand side of values used in the subsytem that is
					//solved to fit the quadratic
	int pivot[3] = {0} ; //Used to pass to LAPACK, but not used explicitly in
						 //this code
	int ok ; //Used to pass to LAPACK, but not used explicitly in this code
	double* trialApprox ; //The updated approximation
	double weightFact ; //The weighting factor to use to update the
						//approximation
	bool validMin = false ; //Indicates whether the minimiser that is calculated
							//is valid or not
	int updateInd = 0 ; //Indicates the index of the value to calculate the
						//residual for
	//Indicates the direction that the window of values was shifted the last
	//time it was moved
	enum SHIFT_DIRECTION prevShift = SHIFT_UNKNOWN ;
	
	//Initialise the values of the update parameter at which to sample the
	//residual norm
	evalPoints[0] = 0.5 ;
	evalPoints[1] = 1.0 ;
	evalPoints[2] = 2.0 ;
	
	trialApprox = aVars->tmpApprox ;
	
	//Set the Dirichlet boundary nodes in the approximation
	set_dirichlet_boundary( params, grid, trialApprox );
	
	//Calculate the value of the residual for each update parameter
	for( pntCnt = 0 ; pntCnt < 3 ; pntCnt++ ){
		weightFact = evalPoints[pntCnt] ;
		//Update the approximation using the appropriate parameter
		for( i=0 ; i < grid->numNodes ; i++ )
			trialApprox[i] = approx[i] + weightFact * correction[i] ;
			
		//Calculate the residual for the approximation
		double *swapPtr = aVars->approx ;
		//Recalculate the iteration matrix
		calculate_iteration_matrix( grid, params, trialApprox, aVars ) ;
	
		//Calculate the residual
		aVars->approx = trialApprox ;
		calculate_residual( params, grid, aVars ) ;
	
		//Reset the pointer
		aVars->approx = swapPtr ;
		
		//Calculate the residual norm
		rhs[pntCnt] = residVals[pntCnt] = vect_discrete_l2_norm( grid->numNodes,
			aVars->resid ) ;
			
//		printf( "Value: %.3lf\tNorm: %.18lf\n", weightFact,
//			residVals[pntCnt] ) ;
			
		//Set up the row in the matrix with which to perform a solve
		for( i=0 ; i < 3 ; i++ )
			subMat[i*3 + pntCnt] = pow( weightFact, 2.0-i ) ;
	}
	
	//Now I have all of the information that I need I just need to call the
	//Lapack function to solve the system of equations
	dgesv_( &(len), &(nrhs), subMat, &(len), pivot, rhs, &(len), &(ok) ) ;
	
	//Now calculate the minimiser using the result
	retVal = -rhs[1] / (2.0*rhs[0]) ;
	
	//If we have had a divide by zero we will end up with an infinite answer.
	//We use the fact that if this is the case the function is linear in
	//the weighting parameter, and return one of the boundaries of the
	//interval depending on the sign of the gradient
	if( isinf( retVal ) || isnan( retVal ) ){
		if( rhs[1] == 0 ) retVal = evalPoints[2] ;
		else if( rhs[1] < 0 ) retVal = evalPoints[0] - 1.0 ;
		else if( rhs[1] > 0 ) retVal = evalPoints[2] + 1.0 ;
	}
//	printf( "Curr Approx: %.10lf\n", retVal ) ;
//	if( isnan( retVal ) ){
//		printf( "retVal is NaN\n" ) ;
//		//Print out the values in the matrix
//		for( pntCnt = 0 ; pntCnt < 3 ; pntCnt++ ){
//			weightFact = evalPoints[pntCnt] ;
//			//Set the values for the row in the matrix
//			for( i=0; i < 3 ; i++ )
//				printf( "%.5lf, ", pow( weightFact, 2.0-i ) ) ;
//			printf( "\n" ) ;
//		}
//		//Print out the right hand side
//		for( i=0 ; i < 3 ; i++ )
//			printf( "%.5lf, ",  residVals[i] ) ;
//	}
	
	//Check that the update is valid
	if( evalPoints[0] < params->minNewtFact && retVal < params->minNewtFact ){
		//If we are below the allowed minimum then set the value to the allowed
		//minimum
		retVal = params->minNewtFact ;
		validMin = true ;
	}
	else if( evalPoints[2] > params->maxNewtFact &&
		retVal > params->maxNewtFact ){
		//If we are above the allowed maximum then set the value to the allowed
		//maximum
		retVal = params->maxNewtFact ;
		validMin = true ;
	}
	else if( retVal < evalPoints[0] ){
		//If the value of the update is below the range of values that were
		//specified then move the range of values down
		for( i=1 ; i>=0 ; i-- ){
			evalPoints[i+1] = evalPoints[i] ;
			rhs[i+1] = residVals[i+1] = residVals[i] ;
		}
		
		evalPoints[0] /= 2.0 ;
		updateInd = 0 ;
		prevShift = SHIFT_DOWN ;
	}
	else if( retVal > evalPoints[2] ){
		//If the value of the update is above the range of values that were
		//specified then move the range of values up
		for( i=0 ; i < 2 ; i++ ){
			evalPoints[i] = evalPoints[i+1] ;
			rhs[i] = residVals[i] = residVals[i+1] ;
		}
		
		evalPoints[2] *= 2.0 ;
		updateInd = 2 ;
		prevShift = SHIFT_UP ;
	}
	else{
		if( retVal < params->minNewtFact )
			retVal = params->minNewtFact ;
		else if( retVal > params->maxNewtFact )
			retVal = params->maxNewtFact ;
		validMin = true ;
	}
			
	//While we have not obtained a valid minimiser
	while( !validMin ){
		//Set the values in the matrix for the fitting of the quadratic
		for( pntCnt = 0 ; pntCnt < 3 ; pntCnt++ ){
			weightFact = evalPoints[pntCnt] ;
			//Set the values for the row in the matrix
			for( i=0; i < 3 ; i++ )
				subMat[i*3 + pntCnt] = pow( weightFact, 2.0-i ) ;
		}
		//Calculate the value at the new evaluation point
		weightFact = evalPoints[updateInd] ;
		
		//Update the approximation using the appropriate parameter
		for( i=0 ; i < grid->numNodes ; i++ )
			trialApprox[i] = approx[i] + weightFact * correction[i] ;
			
		//Calculate the residual for the approximation
		if( is_richards_eq( params->testCase ) ){
			calc_richards_resid( params, grid, aVars, trialApprox,
				aVars->resid ) ;
		}
		else{
			double *swapPtr = aVars->approx ;
			//Recalculate the iteration matrix
			calculate_iteration_matrix( grid, params, trialApprox, aVars ) ;
		
			//Calculate the residual
			aVars->approx = trialApprox ;
			calculate_residual( params, grid, aVars ) ;
		
			//Reset the pointer
			aVars->approx = swapPtr ;
		}
		
		//Calculate the residual norm
		rhs[updateInd] = residVals[updateInd] = vect_discrete_l2_norm(
			grid->numNodes, aVars->resid ) ;
			
		//Set up the row in the matrix with which to perform a solve
		for( i=0 ; i < 3 ; i++ )
			subMat[i*3 + updateInd] = pow( weightFact, 2-i ) ;
			
		//Now I have all of the information that I need I just need to call the
		//Lapack function to solve the system of equations
		dgesv_( &(len), &(nrhs), subMat, &(len), pivot, rhs, &(len), &(ok) ) ;
	
		//Now calculate the minimiser using the result
		retVal = -rhs[1] / (2.0*rhs[0]) ;
		
		//If we have had a divide by zero we will end up with an infinite answer
		//We use the fact that if this is the case the function is linear in
		//the weighting parameter, and return one of the boundaries of the
		//interval depending on the sign of the gradient
		if( isinf( retVal ) || isnan( retVal ) ){
			if( rhs[1] == 0 ) retVal = evalPoints[2] ;
			else if( rhs[1] < 0 ) retVal = evalPoints[0] - 1.0 ;
			else if( rhs[1] > 0 ) retVal = evalPoints[2] + 1.0 ;
		}
		
//		printf( "Curr Approx: %.5lf\n", retVal ) ;
	
		//Check that the update is valid
		if( evalPoints[0] < params->minNewtFact &&
			retVal < params->minNewtFact ){
			//If we are below the allowed minimum then set the value to the
			//allowed minimum
			retVal = params->minNewtFact ;
			validMin = true ;
		}
		else if( evalPoints[2] > params->maxNewtFact &&
			retVal > params->maxNewtFact ){
			//If we are above the allowed maximum then set the value to the
			//allowed maximum
			retVal = params->maxNewtFact ;
			validMin = true ;
		}
		else if( retVal < evalPoints[0] ){
			if( prevShift == SHIFT_UP ){
				//If we shifted the window of values up last time, and now are
				//switching back down we choose the value that was in both of
				//the intervals as the value to return
				retVal = evalPoints[0] ;
//				printf( "Returned Value: %.5lf\n", retVal ) ;
				return retVal ;
			}
			//If the value of the update is below the range of values that were
			//specified then move the range of values down
			for( i=1 ; i>=0 ; i-- ){
				evalPoints[i+1] = evalPoints[i] ;
				rhs[i+1] = residVals[i+1] = residVals[i] ;
			
				//Now update the row in the submatrix
				for( j=0 ; j < 3 ; j++ )
					subMat[j*3 + i+1] = subMat[j*3 + i] ;
			}
		
			evalPoints[0] /= 2.0 ;
			updateInd = 0 ;
			prevShift = SHIFT_DOWN ;
		}
		else if( retVal > evalPoints[2] ){
			if( prevShift == SHIFT_DOWN ){
				//If we shifted the window of values down last time, and now
				//we are switching back up we choose the value that was in both
				//intervals as the value to return
				retVal = evalPoints[2] ;
//				printf( "Returned Value: %.5lf\n", retVal ) ;
				return retVal ;
			}
			//If the value of the update is above the range of values that were
			//specified then move the range of values up
			for( i=0 ; i < 2 ; i++ ){
				evalPoints[i] = evalPoints[i+1] ;
				rhs[i] = residVals[i] = residVals[i+1] ;
			
				//Now update the row in the submatrix
				for( j=0 ; j < 3 ; j++ )
					subMat[j*3 + i] = subMat[j*3 + i + 1] ;
			}
		
			evalPoints[2] *= 2.0 ;
			updateInd = 2 ;
			prevShift = SHIFT_UP ;
		}
		else{
			if( retVal < params->minNewtFact )
				retVal = params->minNewtFact ;
			else if( retVal > params->maxNewtFact )
				retVal = params->maxNewtFact ;
			validMin = true ;
		}
	}
	
//	printf( "Returned Value: %.5lf\n", retVal ) ;
	return retVal ;
}

/** Applies a (damped) Newton update to the approximation using the given
 *	approximation. The result of the update is stored in \e currApprox and
 *	the update is in the direction of \e correction. The variables
 *	ApproxVars::iterMat, ApproxVars::resid and Results::residL2Norm are also
 *	updated in this method. Depending on whether we should use a set value
 *	for the Newton iteration or not different codes are executed. If the
 *	Newton damping factor that is passed in (in \e params) is to be used then
 *	the update is applied using this factor. If a more optimal parameter is to
 *	be chosen a line search is performed on the Newton damping factor between
 *	AlgorithmParameters::minNewtFact and 1, starting at 1, and halving the
 *	spacing each time
 *	\param [in] grid	The grid on which to execute
 *	\param [in,out] params	Parameters defining how the algorithm is to be
 *							executed
 *	\param [in,out] aVars	Variables associated with the approximation. The
 *							members ApproxVars::iterMat and ApproxVars::resid
 *							are updated in this method
 *	\param [in,out] results	Stores information about the execution of the
 *							algorithm. The member Results::residL2Norm is
 *							updated in this method
 *	\param [in,out] currApprox	The current approximation to the solution. This
 *								is updated in this method
 *	\param [in] correction	The correction term to add on to the current
 *							approximation
 */
void apply_newton_step( GridParameters *grid, AlgorithmParameters *params,
	ApproxVars *aVars, Results *results, double *currApprox,
	const double *correction )
{
	int i ;
	double *swapPtr ;
	double diff ;
	InfNorm infNorm ;
	
	swapPtr = aVars->approx ;
	
	vect_infty_norm( grid->numUnknowns, correction, &infNorm ) ;
	results->maxCorrection = infNorm.val ;
	
	if( !params->adaptUpdate ){
		//Simply perform a Newton step using the factor in the algorithm
		//parameters passed in
		for( i=0 ; i < grid->numUnknowns ; i++ )
			currApprox[i] += params->newtonDamp * correction[i] ;
			
		//Recalculate the iteration matrix
		calculate_iteration_matrix( grid, params, currApprox, aVars ) ;
		
		//Calculate the residual
		aVars->approx = currApprox ;
		calculate_residual( params, grid, aVars ) ;
		
		//Reset the pointer
		aVars->approx = swapPtr ;
		
		//calculate the norm in the residual
		results->residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
			aVars->resid ) ;
		results->maxCorrection *= params->newtonDamp ;
		return ;
	}
	
	//Now we know that we are to perform the Newton update adaptively. We first
	//set the Newton factor to be equal to one, and if this does not give a good
	//update then we select points that are along the line between zero and one
	//for the update. This is a simple method, and is just for a proof of
	//concept rather than something that should be used
	params->newtonDamp = 1.0 ;
	//perform a Newton step
	for( i=0 ; i < grid->numUnknowns ; i++ )
		currApprox[i] += correction[i] ;
		
	//Recalculate the iteration matrix
	calculate_iteration_matrix( grid, params, currApprox, aVars ) ;
	
	//Calculate the residual
	aVars->approx = currApprox ;
	calculate_residual( params, grid, aVars ) ;
	
	aVars->approx = swapPtr ;
	
	//Get the residual norm
	results->residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
		aVars->resid ) ;
		
	do{
		if( results->residL2Norm < results->oldResidL2Norm ) break ;
		
		diff = fmin( params->newtonDamp / 2.0, 0.25 ) ;
		params->newtonDamp -= diff ;
		for( i=0 ; i < grid->numUnknowns ; i++ )
			currApprox[i] -= diff * correction[i] ;
			
		//Calculate the iteration matrix
		calculate_iteration_matrix( grid, params, currApprox, aVars ) ;
		
		//Calculate the residual
		aVars->approx = currApprox ;
		calculate_residual( params, grid, aVars ) ;
		
		aVars->approx = swapPtr ;
		
		//Get the norm in residual
		results->residL2Norm = vect_discrete_l2_norm( grid->numUnknowns,
			aVars->resid ) ;
	}while( params->newtonDamp > params->minNewtFact ) ;
	results->maxCorrection *= params->newtonDamp ;
}

/** Sets the number of inner iterations of a linear method to perform per
 *	Newton step. This is dependent on the size of the residual norm that is
 *	passed in
 *	\param [in,out] params	Parameters defining how the algorithm is to be
 *							executed
 *	\param [in] residNorm	The norm of the residual in approximation. This
 *							is used to decide how many inner iterations to run
 */
void set_inner_its( AlgorithmParameters *params, double residNorm )
{
	int nIts ;
	return ;
	nIts = params->innerIts + 1 ;
	if( residNorm > 100 )
		params->innerIts = 1 ;
	else if( residNorm > 10 )
		params->innerIts = (nIts < 4) ? nIts : 4 ;
	else if( residNorm > 1 )
		params->innerIts = (nIts < 8) ? nIts : 8 ;
	else
		params->innerIts = (nIts < 10 ) ? nIts : 10 ;
}

/** Populates the Jacobian matrix on a given grid level using the parameters
 *	given
 *	\param [in] grid	The grid on which to operate
 *	\param [in] aVars	Variables associated with the approximation, which may
 *						be used in the calculation of the Jacobian
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] jacobian	The jacobian matrix to populate. This is stored in a
 *							sparse matrix format
 *	\param [in] useNDeriv	Indicator as to whether to calculate the numerical
 *							derivative, regardless if the exact derivative is
 *							known or not
 */
void calculate_jacobian_matrix( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double *jacobian, int useNDeriv )
{
	int i, j, elId, k ; //Loop counters
	const Node *n1, *n2 ; //Placeholders for nodes on the grid
	
	if( is_richards_eq( params->testCase ) && params->lumpMassMat ){
		calc_lumped_mass_mat_jac( grid, aVars, params, jacobian ) ;
		return ;
	}
	
	//Initialise the jacobian to be equal to zero, if we are not solving a time
	//dependent problem. Otherwise we initialise the value to be that that is
	//stored in the mass matrix.
	if( (is_time_dependent( params->testCase ) || params->testCase == 11) &&
		!is_richards_eq( params->testCase ) )
		copy_vectors( grid->numMatrixEntries, jacobian, aVars->massMat ) ;
	else
		set_dvect_to_zero( grid->numMatrixEntries, jacobian ) ;
	
	//For each element on the grid
	for( elId=0 ; elId < grid->numElements ; elId++ ){
		//For each vertex of the triangle
		for( i=0 ; i < 3 ; i++ ){
			//Get the node on the grid corresponding to vertex i of the element
			n1 = &(grid->nodes[ grid->elements[elId].nodeIDs[i] ]) ;
			
			//If we are at a boundary node ignore it and move on
			if( n1->type == NODE_DIR_BNDRY ) continue ;
			
			//For each node in the triangle
			for( j=0 ; j < 3 ; j++ ){
				//Get the node on the grid corresponding to vertex j of the
				//element
				n2 = &(grid->nodes[ grid->elements[elId].nodeIDs[j] ]) ;

				//Move to the next node if we are at a boundary node
				if( n2->type == NODE_DIR_BNDRY ) continue ;
				
				
				//Find the index into the connection matrix of the connection
				//between node_1 and node_2, as this will be the index into
				//the Jacobian matrix. This assumes that the Jacobian matrix
				//has the same sparse structure as the iteration matrix
				k = grid->rowStartInds[ grid->mapG2A[ n1->id ] ] +
					connected( n1, n2 ) ;
					
				//Calculate the contribution to the entry in the Jacobian matrix
				if( !useNDeriv && exists_analytic_deriv( params ) ){
					//Get the exact contribution for the Jacobian on the current
					//element
					jacobian[k] += exact_deriv_term( i, j,
						&(grid->elements[elId]), grid, params, aVars ) ;
				}
				else{
					//Get the numerical contribution for the Jacobian on the
					//current element
					jacobian[k] += numeric_deriv_term( i, j,
						&(grid->elements[elId]), grid, params, aVars ) ;
				}
			}
		}
	}
}

/** Calculates the Jacobian matrix where the mass matrix contribution is lumped,
 *	(i.e. all contributions are put onto the diagonal)
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] aVars	Variables associated with the approximation on the
 *						current grid level
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] jacobian	The Jacobian matrix, which is to be populated in
 *							this method
 */
void calc_lumped_mass_mat_jac( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double *jacobian )
{
	int i, j, elId ; //Loop counters
	const Node *n1, *n2 ; //Placeholders for nodes on the grid
	const Element *e ; //Placeholder for an element on the grid
	
	//Initialise the jacobian to be equal to zero
	set_dvect_to_zero( grid->numMatrixEntries, jacobian ) ;
	
	//For each element on the grid
	for( elId=0 ; elId < grid->numElements ; elId++ ){
		e = &(grid->elements[ elId ]) ;
		//For each vertex of the triangle
		for( i=0 ; i < 3 ; i++ ){
			//Get the node on the grid corresponding to vertex i of the element
			n1 = &(grid->nodes[ e->nodeIDs[i] ]) ;
			
			//If we are at a boundary node ignore it and move on
			if( n1->type == NODE_DIR_BNDRY ) continue ;
			
			//For each node in the triangle
			for( j=0 ; j < 3 ; j++ ){
				//Get the node on the grid corresponding to vertex j of the
				//element
				n2 = &(grid->nodes[ e->nodeIDs[j] ]) ;

				//Move to the next node if we are at a boundary node
				if( n2->type == NODE_DIR_BNDRY ) continue ;
				
				//Set the contribution to the jacobian, taking into account
				//that we are lumping the mass matrix contribution
				set_lumped_mass_mat_jac( i, j, e, grid, params, aVars,
					jacobian ) ;
			}
		}
	}
}

/** Calculates the symmetric part of the Jacobian matrix
 **	\param [in] grid	The grid on which to operate
 *	\param [in] aVars	Variables associated with the approximation, which may
 *						be used in the calculation of the Jacobian
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] jacobian	The jacobian matrix to populate. This is stored in a
 *							sparse matrix format
 *	\param [in] useNDeriv	Indicator as to whether to calculate the numerical
 *							derivative, regardless if the exact derivative is
 *							known or not
 */
void calculate_jacobian_spart( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double *jacobian, int useNDeriv )
{
	int i, j, k ; //Loop counters
	int elCnt ;
	const Element *e ; //An element on the grid
	const Node *n1, *n2 ; //Nodes on the grid
	
	if( is_time_dependent( params->testCase ) &&
		!is_richards_eq( params->testCase ) )
		copy_vectors( grid->numMatrixEntries, jacobian, aVars->massMat ) ;
	else
		set_dvect_to_zero( grid->numMatrixEntries, jacobian ) ;
	
	//Loop through each element on the grid
	for( elCnt = 0 ; elCnt<grid->numElements ; elCnt++ ){
		//Get a placeholder to the current element on the grid
		e = &(grid->elements[elCnt]) ;
		//For each vertex of the triangle
		for( i=0 ; i < 3 ; i++ ){
			//Get the node on the grid corresponding to vertex i of the element
			n1 = &(grid->nodes[ e->nodeIDs[i] ]) ;
			
			//If we are at a boundary node ignore it and move on
			if( n1->type == NODE_DIR_BNDRY ) continue ;
			
			//For each node in the triangle
			for( j=0 ; j < 3 ; j++ ){
				//Get the node on the grid corresponding to vertex j of the
				//element
				n2 = &(grid->nodes[ e->nodeIDs[j] ]) ;

				//Move to the next node if we are at a boundary node
				if( n2->type == NODE_DIR_BNDRY ) continue ;
				
				
				//Find the index into the connection matrix of the connection
				//between node_1 and node_2, as this will be the index into
				//the Jacobian matrix. This assumes that the Jacobian matrix
				//has the same sparse structure as the iteration matrix
				k = grid->rowStartInds[ grid->mapG2A[ n1->id ] ] +
					connected( n1, n2 ) ;
					
				//Calculate the contribution to the entry in the Jacobian matrix
				if( !useNDeriv && exists_analytic_deriv( params ) ){
					//Get the exact contribution for the Jacobian on the current
					//element
					jacobian[k] += exact_deriv_spart( i, j, e, grid, params,
						aVars ) ;
				}
				else{
					//Approximate the contirbution to the entry in the symmetric
					//part of the Jacobian matrix
					jacobian[k] += numeric_deriv_spart( i, j, e, grid, params,
						aVars ) ;
				}
			}
		}
	}
}

/** Calculates the diagonal entries of the Jacobian matrix. These are required
 *	for the nonlinear smoothing iterations
 *	\param [in] grid	The grid on which to operate
 *	\param [in] aVars	Variables associated with the approximation, which may
 *						be used in the calculation of the derivative
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] jDiags	The diagonals of the Jacobian matrix to populate
 */
void calculate_jacobian_diagonals( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double* jDiags )
{
	int elId ; //Counter for the elements on the grid
	int i ; //Counter for the nodes in an element
	const Node *node ; //Placeholder for a node on the grid
	const Element *e ; //Placehodler for an element on the grid
	
	
	if( is_richards_eq( params->testCase ) && params->lumpMassMat ){
		calc_jac_diagonals_lumped_mass( grid, aVars, params, jDiags ) ;
		return ;
	}
	
	//I will not assume that jDiags has been initialised, so this is the first
	//thing that I do
	if( is_time_dependent( params->testCase ) &&
		!is_richards_eq( params->testCase ) ){
		//Copy the values from the diagonals of the mass matrix into the vector
		//jDiags
		for( i=0 ; i < grid->numUnknowns ; i++ )
			jDiags[i] = aVars->massMat[ grid->rowStartInds[i] ] ;
	}
	else
		set_dvect_to_zero( grid->numUnknowns, jDiags ) ;
	
	//Loop through for every element
	for( elId=0 ; elId < grid->numElements ; elId++ ){
		//Get a placeholder for the current node on the grid
		e = &(grid->elements[elId]) ;
		//For each node in the element
		for( i=0 ; i < 3 ; i++ ){
			//Get a placeholder for vertex i of the current element
			node = &(grid->nodes[ e->nodeIDs[i] ]) ;
			//If we are at a Dirichlet boundary node then continue
			if( node->type == NODE_DIR_BNDRY ) continue ;
			
			//Update the diagonal term for the jacobian, by taking the
			//derivative over the element with respect to itself
			if( exists_analytic_deriv( params ) ){
				jDiags[ grid->mapG2A[ node->id ] ] += exact_deriv_term( i,
					i, e, grid, params, aVars ) ;
			}
			else{
				jDiags[ grid->mapG2A[ node->id ] ] += numeric_deriv_term(
					i, i, e, grid, params, aVars ) ;
			}
		}
	}
}

/** Calculates the diagonals of the Jacobian matrix in the case when mass matrix
 *	lumping is being performed
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] aVars	Variables associated with the approximation on the
 *						current grid level
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] jDiags	The diagonals of the Jacobian matrix. This is populated
 *						in this method
 */
void calc_jac_diagonals_lumped_mass( const GridParameters *grid,
	const ApproxVars *aVars, const AlgorithmParameters *params,
	double *jDiags )
{
	int elCnt, i ; //Loop counters
	const Element *e ; //Placeholder for an element on the grid
	int aInd ; //The index into the approximation for a node on the grid
	
	//Set the diagonal part of the Jacobian to be equal to zero initially
	set_dvect_to_zero( grid->numUnknowns, jDiags ) ;
	
	//Loop over all of the elements
	for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
		//Get a placeholder to the current element on the grid
		e = &(grid->elements[ elCnt ]) ;
		//Loop over all of the nodes in the element
		for( i=0 ; i < 3 ; i++ ){
			aInd = grid->mapG2A[ e->nodeIDs[i] ] ;
			
			//If we are at a Dirichlet boundary node we continue
			if( aInd >= grid->numUnknowns ) continue ;
			
			set_lumped_mass_mat_jac_diags( i, e, grid, params, aVars,
				jDiags ) ;
		}
	}
}

/** Performs iterations of nonlinear Gauss-Seidel smoothing
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] numIts	The number of smoothing iterations to perform
 *	\param [in,out] aVars	Variables related to the approximation. The member
 *							variable ApproxVars::approx is updated in this
 *							method
 */
void nl_gauss_seidel( const GridParameters* grid,
	const AlgorithmParameters* params, int numIts, ApproxVars* aVars )
{
	int gsCnt ; //Counts the number of Gauss-Seidel iterations that are
				//performed
	
	//Calcualte the stiffness matrix for the current approximation
	if( params->recoverGrads )
		recover_gradients( params, grid, aVars->approx, aVars->recGrads ) ;
	calculate_iteration_matrix( grid, params, aVars->approx, aVars ) ;
	//Calculate the diagonals of the jacobian matrix for the current
	//approximation
	calculate_jacobian_diagonals( grid, aVars, params, aVars->jDiags ) ;
	
	//Now that we have all the information we need we can perform the
	//Gauss-Seidel iteration. We don't update the stiffness or jacobian
	//matrices, as it is likely that these will be quite similar, and this saves
	//on computational time (this is my theory - let's see if it works)
	for( gsCnt = 0 ; gsCnt < numIts ; gsCnt++ )
		single_nl_gauss_seidel( grid, params, aVars->jDiags, aVars ) ;
}

/** Performs a single iteration of nonlinear Gauss-Seidel smoothing
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] jDiags	The diagonals of the Jacobian matrix to use
 *	\param [in,out] aVars	Variables related to the approximation. The member
 *							variable ApproxVars::approx is updated in this
 *							method
 *	\return \p int value indicating the success or failure of the method. If
 *			the approximation contains \e Inf or \e NaN then zero is returned
 *			to indicate divergence
 */
void single_nl_gauss_seidel( const GridParameters* grid,
	const AlgorithmParameters* params, const double* jDiags,
	ApproxVars* aVars )
{
	int i, j ; //Loop counters
	int rowInd ; //The starting index of the row in the iteration matrix for a
				 //node on the grid
	double tmp ; //Used to store an intermediate value in the Gauss-Seidel
				 //iteration
	const Node *node ; //Placeholder for a node on the grid
	double diag ;
	
	//For every unknown
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		diag = jDiags[i] ; 
		if( fabs( diag ) < 1e-16 ) continue ; //Nothing to do
		//Get the current node on the grid
		node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
		rowInd = grid->rowStartInds[ i ] ;
		//Apply the operator pointwise
		tmp = 0.0 ;
		for( j=0 ; j < node->numNghbrs ; j++ )
			tmp += aVars->iterMat[rowInd + j] *
				aVars->approx[ grid->mapG2A[ node->nghbrs[j] ] ] ;
		//Find the pointwise correction
		tmp = aVars->approx[i] - (tmp - aVars->rhs[i]) / jDiags[i] ;
		//Update the approximation using the appropriate weighting factor
		aVars->approx[i] = params->smoothWeight * tmp +
			(1.0 - params->smoothWeight ) * aVars->approx[i] ;
		//If the calculated value is infinity or NaN then set the return value
		//to zero
	}
}

/** Performs iterations of nonlinear block Jacobi smoothing
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVars	Variables related to the approximation. The member
 *							variable ApproxVars::approx is updated in this
 *							method
 *	\param [in] numIts	The number of smoothing iterations to perform
 *	\param [in] jacobian	The Jacobian matrix to use in the smoothing process
 */
void nl_block_jacobi( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars, int numIts,
	double *jacobian )
{
	int smoothCnt ;
	
	for( smoothCnt = 0 ; smoothCnt < numIts ; smoothCnt++ )
		single_nl_block_jacobi( grid, params, aVars, jacobian ) ;
}

/** Performs a single iteration of nonlinear block Jacobi smoothing
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVars	Variables related to the approximation. The member
 *							variable ApproxVars::approx is updated in this
 *							method
 *	\param [in] jacobian	The Jacobian matrix to use in the smoothing process
 */
void single_nl_block_jacobi( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars, double *jacobian )
{
	int i, j ; //Loop counters
	int row, col ;//Loop counters
	int k ; //used as an index into the jacobian matrix
					
	SmoothBlock sBlock ; //Stores information about the block which is extracted
						 //from the matrix to solve for
						 
	const Node *node, *rowNode, *colNode ; //Placeholders for nodes on the grid

	double *correction ;//Placholder for the correction to the nodal
						//approximations
	int nrhs = 1 ; //The number of right hand sides that we have to pass
				   //to the fortran Lapack code. This will always be set
				   //to one, but I need to define it so that I can pass a
				   //pointer to Lapack
	double weight ;
	
	//We point the correction term to the temporary variable stored in the 
	//approximation variables parameter
	correction = aVars->tmpApprox ;
	
	//Calcualte the stiffness matrix and the recovered gradient, if necessary
	if( params->recoverGrads )
		recover_gradients( params, grid, aVars->approx, aVars->recGrads ) ;
		
	calculate_iteration_matrix( grid, params, aVars->approx, aVars ) ;
	
	//We want to store the values of the Jacobian matrix for the current
	//approximation
	calculate_jacobian_matrix( grid, aVars, params, jacobian, 0 ) ;
	
	//Calculate the residual
	calculate_residual( params, grid, aVars ) ;
	
	//Set the correction to be zero initially, if we are to calculate the
	//average of updates
	if( params->smoothMethod != SMOOTH_BLOCK_JAC_SINGLE )
		set_dvect_to_zero( grid->numUnknowns, correction ) ;
			   
	//The first thing to do is to loop through the interior nodes on the current
	//grid
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		//Get the current node on the grid
		node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
		//Get the number of non-boundary nodes in neighbourhood of node 'i'
		sBlock.len = node->numNghbrs ;
		
		//I want to use a LAPACK solve to calculate the correction term for me
		//from the little block that I retrieve
		//The size of the nxn block is given by 'sBlock.len'
		//The number of rhs vectors is 1, and is given by 'nrhs'
		//Construct the matrix to pass to the Lapack code
		for( row=0 ; row < sBlock.len ; row++ ){
			//Get the node on the grid for which we are to construct a row in 
			//the smooth block
			rowNode = &(grid->nodes[ node->nghbrs[row] ]) ;
			
			//Get the rhs for the given index. This is the value of the residual
			//at the interior point on the grid
			sBlock.rhs[row] = aVars->resid[ grid->mapG2A[ rowNode->id ] ] ;
			
			//set the column in the matrix which is to be inverted (i.e. take
			//the appropriate values from the jacobian matrix)
			for( col=0 ; col < sBlock.len ; col++ ){
				//Get the node which represents the column in the current row
				colNode = &(grid->nodes[ node->nghbrs[col] ]) ;
				
				//Check whether we have a connection between the two values
				k = connected( rowNode, colNode ) ;
				if( k != -1 ){
					k += grid->rowStartInds[ grid->mapG2A[ rowNode->id ] ] ;
					//Get the appropriate entry from the Jacobian matrix
					sBlock.deriv[col*sBlock.len + row] = jacobian[k] ;
				}
				else
					sBlock.deriv[col*sBlock.len + row] = 0.0 ;
			}
		}
		
		//Now I have all of the information that I need I just need to call the
		//Lapack function to solve the system of equations
		dgesv_( &(sBlock.len), &nrhs, sBlock.deriv, &(sBlock.len), sBlock.pivot,
			sBlock.rhs, &(sBlock.len), &(sBlock.ok) ) ;
			
		//Now I update the correction with the calculated values
		if( params->smoothMethod == SMOOTH_BLOCK_JAC_AVG ){
			for( j=0 ; j < node->numNghbrs ; j++ )
				correction[ grid->mapG2A[ node->nghbrs[j] ] ] +=
					sBlock.rhs[ j ] ;
		}
		else if( params->smoothMethod == SMOOTH_BLOCK_JAC_DWEIGHT ){
			//For a weighted contribution we calculate the contribution to each
			//node from the current one
			for( j=0 ; j < node->numNghbrs ; j++ ){
				weight = 1.0 / (grid->nodes[ node->nghbrs[j] ].numNghbrs + 1) ;
				if( j == 0 )
					weight *= 2.0 ;
				correction[ grid->mapG2A[ node->nghbrs[j] ] ] +=
					sBlock.rhs[ j ] * weight ;
			}
		}
		else if( params->smoothMethod == SMOOTH_BLOCK_JAC_HWEIGHT ){
			//For a weighted contribution we calculate the contribution to each
			//node in the neighbourhood from the current solve
			for( j=0 ; j < node->numNghbrs ; j++ ){
				if( j == 0 ) weight = 0.5 ;
				else weight = 1.0 /
					(2*(grid->nodes[ node->nghbrs[j] ].numNghbrs-1)) ;
				correction[ grid->mapG2A[ node->nghbrs[j] ] ] +=
					sBlock.rhs[ j ] * weight ;
			}
			
		}
		else if( params->smoothMethod == SMOOTH_BLOCK_JAC_SINGLE ){
			//Only update the value at the node at which we are centered
			correction[ i ] = sBlock.rhs[0] ;
		}
	}
	
	//Now that I have solved a system at every time step and calculated the
	//appropriate correction term, I need to add this correction term to the
	//approximation. I weight the correction term by the size of the
	//neighbourhood of the given node
	if( params->smoothMethod == SMOOTH_BLOCK_JAC_AVG ){
		for( i=0 ; i < grid->numUnknowns ; i++ )
			aVars->approx[i] += correction[ i ] /
				grid->nodes[ grid->mapA2G[i] ].numNghbrs ;
	}
	else{
		for( i=0 ; i < grid->numUnknowns ; i++ )
			aVars->approx[i] += correction[i] ;
	}
}

/** Performs iterations of the current smoother until some convergence criterion
 *	is met
 *	\param [in] grid	The grid on which to operate
 *	\param [in] jacobian	The jacobian matrix needed for the block smoothing.
 *							It is assumed that the appropriate memory has been
 *							assigned to this variable
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVars	Variables related to the approximation. The member
 * 							variables ApproxVars::approx is updated in this
 *							method
 *	\param [in] ignoreMax	If this is non-zero the number of iterations may
 *							surpass the maximum number prescribed by
 *							#MAX_SMOOTHS
 *	\return The count of the number of smoothing iterations performed
 */
int nl_smooth_to_converge( const GridParameters *grid, double *jacobian,
	AlgorithmParameters *params, ApproxVars *aVars, int ignoreMax )
{
	//If we are not performing a block Jacobi smoother then 'solve' using the
	//Gauss-Seidel smoother, as this converges when Jacobi does, converges
	//better, and my implementation is more efficient
	if( !is_block_jacobi_smooth( params ) )
		return nl_gauss_seidel_to_converge( grid, params, aVars, ignoreMax ) ;
		
	//If we are using a block jacobi smoother then perform this until a
	//convergenc criterion is met
	return block_jacobi_to_converge( grid, jacobian, params, aVars,
		ignoreMax ) ;
}

/** Performs iterations of nonlinear Guass-Seidel smoothing until some
 *	convergence criterion is met
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVars	Variables related to the approximation. The member
 *							variable ApproxVars::approx is updated in this
 *							method
 *	\param [in] ignoreMax	If this is non-zero the number of iterations may
 *							surpass the maximum number prescribed by
 *							#MAX_SMOOTHS
 *	\return The count of the number of smoothing iterations performed
 */
int nl_gauss_seidel_to_converge( const GridParameters* grid,
	const AlgorithmParameters* params, ApproxVars* aVars, int ignoreMax )
{
	int itCnt ; //Loop counter
	double maxDiff ; //The maximum difference in subsequent approximations

	bool success ;//Indicates success/ failure of the smoother that we use. This
				  //is set to true by default, and set to false if a value of
				  //NaN or inf is obtained in the smoothing iteration
	
	//Set the difference greater than the required tolerance
	maxDiff = TOL2 + 1.0 ;
	
	if( aVars->tmpApprox == NULL )
		aVars->tmpApprox = (double*) calloc( grid->numNodes,
			sizeof( double ) ) ;
	if( aVars->jDiags == NULL )
		aVars->jDiags = (double*) malloc( grid->numUnknowns *
			sizeof( double ) ) ;
			
	//Initialise the value of success, so that we don't get any unexpected
	//behaviour
	success = true ; 
			
	itCnt = 0 ;
	while( maxDiff > TOL2 && (ignoreMax || itCnt < MAX_NL_SMOOTHS) && success ){
		//Set the old approximation equal to the new one
		copy_vectors( grid->numUnknowns, aVars->tmpApprox, aVars->approx ) ;
		
		//Calculate the stiffness matrix
		if( params->recoverGrads )
			recover_gradients( params, grid, aVars->approx, aVars->recGrads ) ;
		calculate_iteration_matrix( grid, params, aVars->approx, aVars ) ;
		
		//Calculate the diagonals of the jacobian matrix
		calculate_jacobian_diagonals( grid, aVars, params, aVars->jDiags ) ;
		
		single_nl_gauss_seidel( grid, params, aVars->jDiags,
			aVars ) ;
				
		//Now check the difference between this approximation and the old
		//approximation
		maxDiff = vect_diff_infty_norm( grid->numUnknowns, aVars->approx,
			aVars->tmpApprox ) ;
		if( isnan( maxDiff ) || isinf( maxDiff ) ){
			success = false ;
			printf( "Smoother diverged to infinity when iterating to " ) ;
			printf( "convergence on grid level %d\n", params->currLevel-1 ) ;
		}
		itCnt++ ;
	}
	
	return itCnt ;
}

/** Performs block jacobi iterations to 'convergence'. The type of block jacobi
 *	performed uses a weighting such that the centre node is weighted as much
 *	as the other nodes added together
 *	\param [in] grid	The grid on which to operate
 *	\param [in] jacobian	The jacobian matrix which is to be used. This is
 *							updated in this method, as it depends on the value
 *							of the approximation ApproxVars::approx
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in,out] aVars	Variables associated with the approximation. The
 *							variable ApproxVars::approx is the desired output
 *							from this method
 *	\param [in] ignoreMax	If this is non-zero the number of iterations may
 *							surpass the maximum number prescribed by
 *							#MAX_SMOOTHS
 *	\return The count of the number of smoothing iterations performed
 */
int block_jacobi_to_converge( const GridParameters *grid, double *jacobian,
	AlgorithmParameters *params, ApproxVars *aVars, int ignoreMax )
{
	enum SMOOTH_TYPE currMethod ;
	int itCnt ; //Loop counter
	double maxDiff ; //The maximum difference in subsequent approximations

	int success ; //Indicates success/ failure of the smoother that we use. This
				  //is set to true (1) by default, and set to false (0) if a
				  //value of NaN or inf is obtained in the smoothing iteration
	
	//Set the difference greater than the required tolerance
	maxDiff = TOL2 + 1.0 ;
	
	//Set the smoothing method to be the one with the weighting as we would like
	//to have it. This is the one where we weight the node at the centre of the
	//block to have the same amount of weighting as the sum of the weighting of
	//the remaining nodes
	currMethod = params->smoothMethod ;
	params->smoothMethod = SMOOTH_BLOCK_JAC_HWEIGHT ;
	
	if( aVars->tmpApprox == NULL )
		aVars->tmpApprox = (double*) calloc( grid->numNodes,
			sizeof( double ) ) ;
			
	//Initialise the value of success, so that we don't get any unexpected
	//behaviour
	success = 1 ; 
			
	itCnt = 0 ;
	while( maxDiff > TOL2 && (ignoreMax || itCnt < MAX_NL_SMOOTHS) && success ){
		//Set the old approximation equal to the new one
		copy_vectors( grid->numUnknowns, aVars->tmpApprox, aVars->approx ) ;
		
		//Calculate the stiffness matrix
		if( params->recoverGrads )
			recover_gradients( params, grid, aVars->approx, aVars->recGrads ) ;
		calculate_iteration_matrix( grid, params, aVars->approx, aVars ) ;
		
		//Calculate the jacobian matrix
		calculate_jacobian_matrix( grid, aVars, params, jacobian, 0 ) ;
		
		single_nl_block_jacobi( grid, params, aVars, jacobian ) ;
				
		//Now check the difference between this approximation and the old
		//approximation
		maxDiff = vect_diff_infty_norm( grid->numUnknowns, aVars->approx,
			aVars->tmpApprox ) ;
		if( isnan( maxDiff ) || isinf( maxDiff ) ) success = 0 ;
		itCnt++ ;
	}
	
	//Reset the smooth method
	params->smoothMethod = currMethod ;
	
	return itCnt ;
}

/** Performs iterations of nonlinear Jacobi smoothing
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] numIts	The number of smoothing iterations to perform
 *	\param [in,out] aVars	Variables related to the approximation. The member
 *							variable ApproxVars::approx is updated in this
 *							method
 */
void nl_jacobi( const GridParameters* grid,
	const AlgorithmParameters* params, int numIts, ApproxVars* aVars )
{
	int cnt ; //Counts the number of Gauss-Seidel iterations that are performed
	
	//Calcualte the stiffness matrix for the current approximation
	if( params->recoverGrads )
		recover_gradients( params, grid, aVars->approx, aVars->recGrads ) ;
	calculate_iteration_matrix( grid, params, aVars->approx, aVars ) ;
		
	calculate_jacobian_diagonals( grid, aVars, params, aVars->jDiags ) ;
	
	//Now that we have all the information we need we can perform the
	//Gauss-Seidel iteration. We don't update the stiffness or jacobian
	//matrices, as it is likely that these will be quite similar, and this saves
	//on computational time (this is my theory - let's see if it works)
	for( cnt = 0 ; cnt < numIts ; cnt++ )
		single_nl_jacobi( grid, params, aVars->jDiags, aVars ) ;
}

/** Performs a single iteration of nonlinear Jacobi smoothing
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] jDiags	The diagonals of the Jacobian matrix to use in the 
 *						smoothing procedure
 *	\param [in,out] aVars	Variables related to the approximation. The member
 *							variable ApproxVars::approx is updated in this
 *							method
 */
void single_nl_jacobi( const GridParameters* grid,
	const AlgorithmParameters* params, const double* jDiags,
	ApproxVars* aVars )
{
	int i, j ; //Loop counters
	double tmp ; //Used to store an intermediate value in the Gauss-Seidel
				 //iteration
	const Node *node ; //Placeholder for a node on the grid
	int rowInd ; //The starting index of a row in the iteration matrix for a
				 //node on the grid
				 
	//Copy the approximation into the old approximation
	copy_vectors( grid->numUnknowns, aVars->tmpApprox, aVars->approx ) ;
	
	//For every unknown
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		//Get the current node on the grid
		node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
		//Get the starting index of the row in the iteration matrix for the
		//current node
		rowInd = grid->rowStartInds[ i ] ;
		//Apply the operator pointwise
		tmp = 0.0 ;
		for( j=0 ; j < node->numNghbrs ; j++ )
			tmp += aVars->iterMat[rowInd + j] *
				aVars->tmpApprox[ grid->mapG2A[ node->nghbrs[j] ] ] ;
		//Find the pointwise correction
		tmp = aVars->tmpApprox[i] - (tmp - aVars->rhs[i]) / jDiags[i] ;
		//Update the approximation using the appropriate weighting factor
		aVars->approx[i] = params->smoothWeight * tmp +
			(1.0 - params->smoothWeight ) * aVars->approx[i] ;
	}
}




















