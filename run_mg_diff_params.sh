#!/bin/bash
paramsFile="parameters.txt"

#replaces a given parameter name with a parameter value
function replaceParam(){
	#Link file descriptor 10 with stdin
	exec 10<&0
	#Replace stdin with a file, which I am declaring at the top
	exec < "$paramsFile"

	#Link file descriptor with stdout
	exec 11>&1
	exec > "tmp.txt"

	#Replace the finest grid variable in the parameters file with the value
	#that is passed to this method
	while read -r line ; do
		if [[ "$line" = "$1:"* ]] ; then
			(( s_len=${#line} ))
			#Get the string to the left of the ':' in line
			(( myChar=${line: $(expr index $line ":")} ))
			echo ${line/"$myChar"/"$2"}
		else
			echo $line
		fi
	done
	
	#Close the file descriptor and de-link from stdout
	exec 1>&11 11>&-
	
	#restore stdin from file descriptor 10 and close file descriptor 10
	exec 0<&10 10<&-
	
	#Replace the parameters file with the temporary one and remove the temporary
	#file
	mv "tmp.txt" "$paramsFile"
}

#Make sure we are performing a nonlinear method
replaceParam "TEST_CASE" 2
for (( j=0 ; j<=1 ; j++ )) do
	#First loop through the different multigrid methods
	if [[ $j -eq 0 ]] ; then
		echo "NMLM" > "/tmp/progOut"
		echo >> "/tmp/progOut"
	else
		echo "Newton-MG" >> "/tmp/progOut"
		echo >> "/tmp/progOut"
	fi
	replaceParam "NL_METHOD" $j
	#Loop through all the grid levels
	for (( i=2 ; i<=10 ; i++ )) ; do
		#Set the number of grid levels to what we want it to be
		replaceParam "FINEST_GRID" $i
		#If we are not performing the first iteration concatenate to the file
		echo "Number of grid levels: $i" >> "/tmp/progOut"
		./mgExec >> "/tmp/progOut"
		#Add a blank line after the output so we can see where the different
		#executions start/ end
		echo >> "/tmp/progOut"
	done
	echo "**********" >> "/tmp/progOut"
	echo >> "/tmp/progOut"
done







