#ifndef RICHARS_EQ_H_
#define RICHARDS_EQ_H_

#include "useful_definitions.h"

/** \file richards_eq.h
 *	\brief Implements functions specifically for Richards equation type problems
 */

//Performs time stepping of the Richards equation using Newton-Multigrid defined
//by the parameters that are passed in
void solve_richards_eq( AlgorithmParameters *params, GridParameters **grids,
	ApproxVars **aVarsList ) ;

//Performs a solve of a nonlinear system of equations representing a Richards
//equation type problem	
void richards_newton_mg( GridParameters **grids, ApproxVars **aVarsList,
	AlgorithmParameters *params, Results *results ) ;
	
//Calculates the update parameter for a Newton iteration
double calc_richards_newton_update( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars, const double* approx,
	const double *correction ) ;

//Perform a solve of a nonlinear system of equations using a nonlinear multigrid
//iteration	
void richards_multigrid( GridParameters **grids,  ApproxVars **aVarsList,
	AlgorithmParameters *params, Results *results ) ;
	
//Performs a single cycle of a given nonlinear multigrid iteration
void richards_nlmg( GridParameters **grids, ApproxVars **aVarsList,
	AlgorithmParameters *params, const int callCnt ) ;
	
//Assigns the appropriate memory for a Richards equation context
void assign_re_ctxt_mem( RichardsEqCtxt *ctxt ) ;
//Frees the memory assigned to a Richards equation context
void free_re_ctxt_mem( RichardsEqCtxt *ctxt ) ;
//Sets all pointer variables to null in the Richards equation context
void initialise_re_ctxt_to_null( RichardsEqCtxt *ctxt ) ;
//Initialises the context for a Richards equation type problem
void set_up_re_ctxt( RichardsEqCtxt *ctxt ) ;

//Calculates the value of the volumetric water wetness (theta) given the
//pressure in terms of the hyrdraulic head
void set_theta( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure,	double *theta ) ;
	
//Calculates the value of the derivative of the volumetric water content at
//a given pressure (given in terms of hydraulic head)
void set_theta_deriv( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure,
	double *gradTheta ) ;
	
//Calculates the value of the volumetric wetness at a given pressure (given in
//terms of hydraulic head)
double calc_theta( const RichardsEqCtxt *ctxt, int soilID, double pressure ) ;

//Calculates the analytic derivative of the volumetric wetness with respect to
//the pressure (given in terms of hydraulic head) at a given pressure
double calc_theta_deriv( const RichardsEqCtxt *ctxt, int soilID,
	double pressure ) ;

//Calcualtes the hyrdarulic conductivity given the pressure in terms of the
//hydraulic head
void set_hydr_cond( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure, double *hydrCond ) ;
	
void set_hydr_cond_deriv( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure, double *gradHCond ) ;
	
//Functions required for the calculation of the hydraulic conductivity
double calc_hydr_cond( const RichardsEqCtxt *ctxt, int soilID,
	double pressure ) ;
double hydr_cond_sub( const RichardsEqCtxt *ctxt, int soilID, double effSat ) ;

//Calculates the analytic derivative of the hydraulic conductivity with respect
//to the pressure (given in terms of hydraulic head) at a given pressure
double calc_hydr_cond_deriv( const RichardsEqCtxt *ctxt, int soilID,
	double pressure ) ;

//Calculates the flux per unit area given the pressure in terms of the hydraulic
//head
void set_flux( const AlgorithmParameters *params, const GridParameters *grid,
	const double *pressure, const double *hCond, Vector2D *flux ) ;

//Calculates the value of volumetric wetness at which saturation would occur
//for pressure equal to zero, given some non-positive constant pressure, which
//is stored in the algorithm parameters
double get_theta_m( const RichardsEqCtxt *ctxt, int soilID ) ;

//Calculates the value of the pressure given the volumetric wetness
double get_pressure_for_theta( const RichardsEqCtxt *ctxt, int soilID,
	double theta ) ;
	
//Sets the pressure given the volumetric wetness function on a grid
void set_pressure_for_theta( const AlgorithmParameters *params,
	const GridParameters *grid, const double *theta, double *pressure ) ;

//Calculates the relative saturation given a pointwise pressure
double effective_saturation( const RichardsEqCtxt *ctxt, int soilID,
	double pressure ) ;

void calc_richards_resid( const AlgorithmParameters *params,
	const GridParameters *grid, ApproxVars *aVars,
	const double *pressure, double *resid ) ;
		
//Performs integration of the theta function multiplied by a basis function
//centered at the node given by node 'node' on element 'e'
double int_richards_theta( const AlgorithmParameters *params,
	const GridParameters *grid, const double *theta, const Element *e, int node,
	const double *pressure ) ;
	
//Performs integration of the theta function multiplied by a basis function
//centered at the node given by node 'node' on element 'e'
double int_richards_h_cond( const AlgorithmParameters * params,
	const GridParameters *grid, const double *hCond, const Element *e, int node,
	const double *pressure ) ;
	
//Performs 'numIts' iterations of a nonlinear Jacobi smooth for the Richards
//equation
void re_smooth_nl_jacobi( const GridParameters* grid,
	const AlgorithmParameters* params, int numIts, bool recalculate,
	ApproxVars* aVars ) ;
	
//Performs nonlinear Jacobi smoothing iterations until a convergence criterion
//is met, or a set number of smoothing iterations has been performed
void re_smooth_nl_jacobi_to_converge( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars ) ;
	
//Performs nonlinear Jacobi smoothing iterations until a convergence criterion
//is met, or a set number of smoothing iterations has been performed. The
//nonlinear operator is not updated every iteration to save on computing time
void re_smooth_nl_simp_jac_to_converge( const GridParameters *grid,
	const AlgorithmParameters *params, ApproxVars *aVars ) ;
	
//Calculates the appropriate right hand side for the nonlinear multigrid
//algorithm for the Richards equation
void calc_richards_coarse_rhs( const AlgorithmParameters *params,
	const GridParameters *grid, const double *pressure, double sigma,
	ApproxVars *aVars ) ;

#endif
