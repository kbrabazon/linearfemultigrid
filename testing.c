#include "testing.h"
#include "FEM_MG_SparseMatrix.h"
#include "multigrid_functions.h"
#include "nl_multigrid.h"
#include "linear_basis.h"
#include "test_cases.h"
#include "richards_eq.h"
#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
	
/*
 * Used to run tests using different testing methods
 *
 * INPUT
 *	AlgorithmParameters* params: The parameters which can be used during the
 *		testing
 *
 * OUTPUT:
 *	None.
 */
void test( AlgorithmParameters* params )
{	
	//Now run the tests that we want to
	
	//Test whether the derivative of the recovered gradients is performed
	//correctly or not. The value of the derivative should approach that of the
	//formulation where recovery is not performed
	//testRecovDerivCalculation( params ) ;
	
	//Test if the operator is being applied correctly to the approximation
	//test_apply_operator( params ) ;
	
	//Test if the smoothing converges as expected
	//test_smoothing( params ) ;
	
	//Test the smoothing to convergence
	//test_smooth_to_converge( params ) ;
	
	//Print out the errors in approximation after each smoothing step for the
	//test cases where the exact solution is known
	//print_smooth_func( params, 0 ) ;
	
	//Tests if the smoothing block is put together correctly or not
	//test_smooth_block( params ) ;
	
	//Tests the effect of the smoothing operator on the approximation after
	//a multigrid iteration on a finer grid level
	//test_smooth_after_mg( params ) ;
	
	//Test if the gradients are being recovered correctly
	//test_gradient_recovery( params ) ;
	
	//Test whether or not we are leaking memory when we set up the approximation
	//variables
	//test_approx_params_set_up( params ) ;
	
	//Test whether the algorithm parameters are being read in correctly
	//print_algorithm_parameters_to_file( "/tmp/progOut", params ) ;
	
	//Test if the restriction works as expected
	//test_restriction( params ) ;
	
	//testInterpolation( params ) ;
	//test_interpolation( params ) ;
	
	//Test that the calculation of the discrete L2 norm is as expected
	//test_l2_norm_calculation( params ) ;
	
	//Test that the calculation of the discrete H1 seminorm is as expected
	//test_h1_seminorm_calculation( params ) ;
	
	//Test that the calculation of the discrete H1 norm is as expected
	//test_h1_norm_calculation( params ) ;
	
	//Tests whether the Lapack solver is doing what I think it should be doing
	//test_lapack_solve( params ) ;
	
	//Tests whether a system is being solved exactly
	//test_exact_solve( params ) ;
	
	//Tests whether the calculation of the neighbourhood of elements for a given
	//node is as expected
	//testGetNodeElements() ;
	
	//Tests whether the connectivity stored in the nodes is correct or not
	//print_row_start_inds( params ) ;
	//Prints out the non Dirichlet boundary neighbourhood of each of the non
	//Dirichlet boundary nodes in turn
	//print_neighbours( params ) ;
	
	//Tests whether we find the correct connectivity
	//test_connection( params ) ;
	
	//test_resid_set_up( params ) ;
	
	//Tests whether the approximation is being set up appropriately
	//test_approx_set_up( params ) ;

	//Tests whethre the right hand side is being set up as expected
	//test_set_up_rhs( params ) ;
	
	//test_read_func_from_file( params ) ;
	
	//test_grid_indices( params ) ;
	
	//test_smooth_block( params ) ;
	
	//Test if the Jacobian matrix is set up correctly
	//test_jacobian_calculation( params ) ;
	
	//Test if a Hessenberg matrix is successfully transformed into an upper
	//triangular matrix or not
	//test_hess_to_upper( params ) ;
	
	//Tests if the back substitution solves an upper triangular system
	//test_back_sub( params ) ;
	
	//Tests if the least squares solution is a minimizer
	//test_least_squares( params ) ;
	
	//Test if the factorial function returns the correct value
	//test_factorial( params ) ;
	
	//Output the boundary types that are on the grids in a hierarchy
	//test_grid_boundary_types( params ) ;
	
	//Compares the numerical derivative to the exact derivative (if the exact
	//derivative is known)
	//test_comp_exact_numeric_deriv( params ) ;
	
	//Prints the progression of the exact solution for time dependent test case
	//10 to file
	//test_tc10_exact_sol_progression( params ) ;
	
	//Prints out the symmetric, non-symmetric and full 
	//test_comp_symm_nonsymm_parts( params ) ;
	
	//Test whether the hydraulic conductivity is calculated correctly as a
	//function of the pressure head
	//test_hydr_cond_calc( params ) ;
	
	//Test whether the derivative of the hydraulic conductivity is calcualted
	//correctly as a function of the pressure head
	//test_hydr_cond_deriv( params ) ;
	
	//Test whether the volumetric water content is calculated correctly as a
	//function of the pressure head
	//test_theta_calc( params ) ;
	
	//Test whether the derivative of the volumetric wetness is calculated
	//correctly as a function of the pressure head
	//test_theta_deriv_calc( params ) ;
	
	//Test that the smoother works for the Richards equation type problems
//	test_smooth_richards_eq( params ) ;
	
	//Test that the time stepping works for Richards equation type problems. At
	//each time step we solve using a smoother only
	//test_re_time_step_smooth( params ) ;
	
	//Test that taking a Newton step followed by solves using smoothing only
	//gives a convergent iteration
	//test_re_newton_smooth( params ) ;
	
	//Test that a multigrid iteration can be performed for the Richards equation
	//test_re_newton_mg( params ) ;
	
	//Test the the 'next' node on an element is calculated properly
	//test_next_node( params ) ;
	
	//Test that the soil types are allocated correctly for a Richards equation
	//type problem with multiple soil types
	//test_soil_type_allocation( params ) ;
	
	//Test that the volumetic wetness can be calculated correctly for a Richards
	//equation type problem with multiple soil types
	//test_theta_setup_multiple_soils( params ) ;
	
	//Tests that the value of the hydraulic conductivity is calculated correctly
	//for problems of Richards equation type with multiple soil types
	//test_hydr_cond_setup_mult_soils( params ) ;
	
	//Tries to reproduce the results from Vogel et al, 2001 for a one
	//dimensional test case
	//test_1d_richards_smooth( params ) ;
	
	//Tries to reproduce the results from Vogel et al, 2001 for a one
	//dimensional test case using a Newton iteration with linear smooths
	//test_1d_richards_newton_smooth( params ) ;
	
	//Tests that the pressure is correctly calculated given the volumetric
	//wetness
	//test_calc_pressure_from_theta( params ) ;
	
	//Tests that the coefficient for the p-Laplacian is set up properly
	//test_pc_coeff_set_up( params ) ;
	
	//Tests that the restriction of a piecewise constant function on a fine grid 
	//can be restricted to a coarse grid
	test_restrict_pc_coeff( params ) ;
}

/*
 * TODO: Insert appropriate comment
 */
void test_restriction( AlgorithmParameters* params )
{
	GridParameters fineGrid, coarseGrid ;
	int fineLevel ;
	double *fApprox, *cApprox ;
	
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
	
	fineLevel = params->fGridLevel ;
	
	//Set up the grids
	init_grid( &fineGrid, params, meshes[fineLevel-1] ) ;
	init_grid( &coarseGrid, params, meshes[fineLevel-2] ) ;
	
	//Set up the memory for the different approximations, and also initialise
	//the values in the fine approximation
	fApprox = (double*) calloc( fineGrid.numUnknowns, sizeof( double ) ) ;
	cApprox = (double*) calloc( coarseGrid.numUnknowns, sizeof( double ) ) ;
	
	initialise_approx( params, &fineGrid, fApprox ) ;
		
	//Now restrict the approximation to the coarse grid

	restrict_FW( &fineGrid, &coarseGrid, 0, fApprox, cApprox ) ;
		
	//Now print out the different approximations
	print_grid_func_no_bnd_to_file( "/tmp/fApproxOut", &fineGrid, fApprox,
		0 ) ;
	print_grid_func_no_bnd_to_file( "/tmp/cApproxOut", &coarseGrid, cApprox,
		0 ) ;
		
	//Clean up
	free( fApprox ) ;
	free( cApprox ) ;
	free_grid( &fineGrid ) ;
	free_grid( &coarseGrid ) ;
}

/*
 * TODO: Insert appropriate comment
 */
void test_interpolation( AlgorithmParameters *params )
{
	const char **meshes ; //The grids to use
	GridParameters fGrid, cGrid ; //The fine and coarse grids
	ApproxVars fVars, cVars ;
	
	meshes = get_grid_hierarchy( params ) ;
	
	init_grid( &fGrid, params, meshes[params->fGridLevel-1] ) ;
	init_grid( &cGrid, params, meshes[params->fGridLevel-2] ) ;
	
	init_approx_vars( &fVars, &fGrid, params ) ;
	init_approx_vars( &cVars, &cGrid, params ) ;
	
	//Set up the approximation on the coarse grid
	initialise_approx( params, &cGrid, cVars.approx ) ;
	
	//Interpolate from the coarse to the fine grid
	prolongate( params, &cGrid, cVars.approx, &fGrid, fVars.approx ) ;
	
	//Print the function on the coarse and fine grids out to file for inspection
	print_grid_func_to_file( "/tmp/origOut.txt", &cGrid, cVars.approx, 0 ) ;
	print_grid_func_to_file( "/tmp/interpOut.txt", &fGrid,
		fVars.approx, 0 ) ;
	
	//Clean up
	free_approx_vars( &fVars, params ) ;
	free_approx_vars( &cVars, params ) ;
	free_grid( &fGrid ) ;
	free_grid( &cGrid ) ;
}

/*
 * Tests whether the approximation parameters are set up and destroyed
 * appropriately. When running this test case compile with the debug flag, and
 * run with valgrind
 *
 * INPUT:
 *	AlgorithmParameters* params: Parameters to define how to run the algorithm
 *
 * OUTPUT:
 *	None. If this test works then there will be no leaks when running with
 *	valgrind
 */
void test_approx_params_set_up( AlgorithmParameters* params )
{
	ApproxVars aVars ;
	GridParameters gParams ;
	
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
	
	//We need to set up a grid so that we can set up the approximation
	//parameters
	init_grid( &gParams, params, meshes[5] ) ;
	
	//Now we set up the approximation parameters
	init_approx_vars( &aVars, &gParams, params ) ;
	
	//Now free the approximation parameters, as this is simply supposed to test
	//if the creation and destruction of this object works without a problem
	free_approx_vars( &aVars, params ) ;
	
	//Now free the grid parameters object as well
	free_grid( &gParams ) ;
}

/*
 * Tests whether or not the gradients are recovered correctly or not. Output is
 * printed to file '/tmp/fout'
 *
 * INPUT:
 *	AlgorithmParameters* params: Parameters that can be used to define how the
 *		algorithm is to be run
 *
 * OUTPUT:
 *	None. Text output is written to file '/tmp/fout'
 */
void test_gradient_recovery( AlgorithmParameters* params )
{
	int i ; //Loop counter
	
	FILE* fout ; //Output file
	
	GridParameters grid ; //The grid which will be used in this test
	ApproxVars vars ; //The approximation variables that will be used in this
					  //test
	double* approx ; //The approximation to use for this test
	
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
	
	//Open the file for writing
	fout = fopen( "/tmp/fout", "w" ) ;
	
	//We only operate on the coarsest grid, as this is small enough to check
	//by hand if the appropriate values are being stored
	init_grid( &grid, params, meshes[0] ) ;
	
	//Now that we have set up the grid I want to set up the approximation
	//variables
	init_approx_vars( &vars, &grid, params ) ;
	
	//Now I need an approximation
	approx = (double*) calloc( grid.numUnknowns, sizeof( double ) ) ;
	
	//Populate the approximation with values
	for( i=0 ; i < grid.numUnknowns ; i++ )
		approx[i] = i+1 ;
	
	//Recover the gradients
	recover_gradients( params, &grid, approx, vars.recGrads ) ;
	
	//Print some useful information about the grid
	fprintf( fout, "Number of nodes in total: %d\n", grid.numNodes ) ;
	fprintf( fout, "Number of interior nodes: %d\n", grid.numUnknowns ) ;
	fprintf( fout, "Number of triangles: %d\n", grid.numElements ) ;
	fprintf( fout, "Grid spacing: %.10f\n\n",
		grid.nodes[1].x - grid.nodes[0].x ) ;
	//Print the result out to file to see if we are getting the correct values
	for( i=0 ; i < grid.numNodes ; i++ )
		fprintf( fout, "Node %d:\n\tx: %.10f,\ty: %.10f\n", i,
			vars.recGrads[i].x, vars.recGrads[i].y ) ;
	
	//Free the memory allocated to the approximation
	free( approx ) ;
	//Close the file
	fclose( fout ) ;
	//Free the grid parameters
	free_grid( &grid ) ;
	//Free the approximation variables
	free_approx_vars( &vars, params ) ;
}


/*
 * TODO: Insert appropriate comment
 */
void test_smoothing( AlgorithmParameters* params )
{
	GridParameters grid ;
	ApproxVars aVars ;
	
	int numSmooths ;
	double prevRNorm, currRNorm ;
	
	double *jacobian ; //Needed if we are performing block smoothing, in the
					   //nonlinear case
	double *eqRHS = NULL ;
	const Node *node ;
	int i, j, k ;
	double tmp ;
	
	char outFName[100] ;
	InfNorm infNorm ;
	
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
	TDCtxt *tdCtxt = NULL ;
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
	
	numSmooths = 10 ;
	
	//Set up the grid. I am using the smallest grid here so that the entire
	//approximation can be considered in one go.
	init_grid( &grid, params, meshes[params->fGridLevel-1] ) ;
	
	jacobian = NULL ;
	if( is_block_jacobi_smooth( params ) )
		jacobian = (double*) malloc( grid.numMatrixEntries *
			sizeof( double ) ) ;
	
	sprintf( outFName, "/tmp/testSmoothLvl%d.txt", params->fGridLevel-1 ) ;
	
	//Set up the information needed for the approximation variables
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Now that I have the grid I want to initialise the right hand side and the
	//approximation
	if( is_time_dependent( params->testCase ) ){
		tdCtxt = &(params->tdCtxt) ;
		eqRHS = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
		if( params->testCase != 10 )
			initialise_rhs( &grid, params, &aVars, tdCtxt->tStepSize, eqRHS,
				aVars.bndTerm ) ;
		initial_condition( params, &grid, aVars.approx ) ;
 		
		//Set up the appropriate right hand side now
	for( i=0 ; i<grid.numUnknowns ; i++ ){
			//Calculate the value of the mass matrix for the current row times
			//the current approximation
			tmp = 0.0 ;
			node = &(grid.nodes[ grid.mapA2G[ i ] ]) ;
			k = grid.rowStartInds[ i ] ;
			for( j=0 ; j < node->numNghbrs ; j++ )
				tmp += aVars.massMat[k+j] *
					aVars.approx[ grid.mapG2A[ node->nghbrs[j] ] ] ;
			//Set the correct value for the right hand side for the current row
			aVars.rhs[i] = eqRHS[i] + tmp ;
		}
	}
	else{
		initialise_rhs( &grid, params, &aVars, 1.0, aVars.rhs, aVars.bndTerm ) ;
		initialise_approx( params, &grid, aVars.approx ) ;
	}
	
	//Print out the right hand side
	print_grid_func_no_bnd_to_file( "/tmp/testRHSOut.txt", &grid,
		aVars.rhs, 0 ) ;
		
	//Initialise the stiffness matrix - come back to the case of the recovered
	//gradient when I have sorted out the normal case
	if( params->recoverGrads )
		recover_gradients( params, &grid, aVars.approx, aVars.recGrads ) ;
		
	calculate_iteration_matrix( &grid, params, aVars.approx, &aVars ) ;
	
	calculate_residual( params, &grid, &aVars ) ;
		
	currRNorm = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
		
	//Print the L2 norm of the residual in original approximation
	printf( "Residual in original approximation: %.18lf\n", currRNorm ) ;
	
	//Print the initial approximation to file
	print_grid_func_to_file( outFName, &grid, aVars.approx, 0 ) ;
		
	//Now I want to smooth a given number of times
	i = 0 ;
	while( currRNorm > 1e-14 && i < numSmooths){
		prevRNorm = currRNorm ;
		
		//Perform the smoothing
		smooth( &grid, params, 1, jacobian, &aVars ) ;
		
		//Calculate the residual
		calculate_residual( params, &grid, &aVars ) ;
			
		//Print the information about the residual norm and the ratio with the
		//previous iteration
		currRNorm = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
		vect_infty_norm( grid.numUnknowns, aVars.approx, &infNorm ) ;
		
		printf( "Current residual: %.18lf\t |\tRatio: %.18lf\n",
			currRNorm, currRNorm / prevRNorm ) ;
		printf( "Largest value of approximation: %.18lf\n\t", infNorm.val ) ;
		node = &(grid.nodes[ grid.mapA2G[ infNorm.ind ] ]) ;
		printf( "at point %d (%.10lf, %.10lf )\n\t", infNorm.ind, node->x,
			node->y ) ;
		printf( "with radius %.18lf\n",
			sqrt( pow(node->x,2) + pow(node->y,2)) ) ;
		
		i++ ;//if( (i++)%100 == 0 )
			print_grid_func_to_file( outFName, &grid, aVars.approx, 1 ) ;
	}
	
	
	printf( "Smoothing method: %d\n", (int)params->smoothMethod ) ;
	printf( "Number of iterations performed: %d\n", i ) ;
	printf( "File written to: %s\n", outFName ) ;
	
	//Clean up
	free_approx_vars( &aVars, params ) ;
	free_grid( &grid ) ;
	if( jacobian != NULL )
		free( jacobian ) ;
	if( eqRHS != NULL )
		free( eqRHS ) ;
}

/*
 * TODO: Insert appropriate comment
 */
void test_smooth_to_converge( AlgorithmParameters *params )
{
	const char **meshes ; //The grid hierarchy we are using
	GridParameters grid ; //The grid on which we are working
	ApproxVars aVars ; //Variables associated with the approximatinog on a given
					   //grid level
	char outFName[100] ; //Filename to print output to
	int numIts ;
	int hasSmoothComp, hasOscComp ;
					   
	if( !is_non_linear( params->testCase ) ||
		params->nlMethod == METHOD_NEWTON_MG ){
		printf( "Test not set up to deal with Newton-Multigrid\n" ) ;
		return ;
	}
	
	meshes = meshesBL2TR ;
	//Initialise the grid
	init_grid( &grid, params, meshes[ params->fGridLevel-1 ] ) ;
	
	//Set up the approximation variables
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Initialise the approximation
	initialise_approx( params, &grid, aVars.approx ) ;
	
	//Initialise the right hand side
	initialise_rhs(&grid, params, &aVars, 1.0, aVars.rhs, aVars.bndTerm ) ;
	
	if( params->recoverGrads )
		recover_gradients( params, &grid, aVars.approx, aVars.recGrads ) ;
	
	//Calculate the iteration matrix
	calculate_iteration_matrix( &grid, params, aVars.approx, &aVars ) ;
	
	//Now that we have everything we need we want to 'solve' to convergence
	if( !is_non_linear( params->testCase ) )
		numIts = gs_to_converge( params, &grid, &aVars, TOL2, 0 ) ;
	else
		numIts = nl_gauss_seidel_to_converge( &grid, params, &aVars, 0 ) ;
	
	if( params->testCase == 1 )
		sprintf( outFName, "/tmp/convergeLin_lvl%d.txt", params->fGridLevel ) ;
	else if( params->testCase != 6 && params->testCase != 7 )
		sprintf( outFName, "/tmp/convergeNL_a%.3lflvl%d.txt", params->alpha,
			params->fGridLevel ) ;
	else{
		hasSmoothComp = (params->solSmoothAmp != 0.0) ;
		hasOscComp = ( params->solOscAmp != 0.0 && params->solXFreq != 0.0 &&
			params->solYFreq != 0.0 ) ;
			
		if( hasSmoothComp && hasOscComp ){
			sprintf( outFName, "/tmp/convergeNL_a%.3lflvl%dA%.3lfB%.3lfn",
				params->alpha, params->fGridLevel, params->solSmoothAmp,
				params->solOscAmp ) ;
			sprintf( outFName, "%s%.3lfm%.3lf.txt", outFName, params->solXFreq,
				params->solYFreq ) ;
		}
		else if( hasSmoothComp )
			sprintf( outFName, "/tmp/convergeNL_a%.3lflvl%dA%.3lf.txt",
				params->alpha, params->fGridLevel, params->solSmoothAmp ) ;
		else{
			sprintf( outFName, "/tmp/convergeNL_a%.3lflvl%dB%.3lfn%.3lf",
				params->alpha, params->fGridLevel, params->solOscAmp,
				params->solXFreq ) ;
			sprintf( outFName, "%sm%.3lf.txt", outFName, params->solYFreq ) ;
		}
	}
	print_grid_func_to_file( outFName, &grid, aVars.approx, 0 ) ;
	
	printf( "Number of iterations for convergence: %d\n", numIts ) ;
	printf( "Result written to file: %s\n", outFName ) ;
	
	free_approx_vars( &aVars, params ) ;
	free_grid( &grid ) ;
}

/*
 * Prints out to file after each smoothing iteration some function related to
 * the approximation. This can be the approximation itself, the error in
 * approximation or the residual in approximation. This is useful is we want to
 * see what components there are in the respective grid functions
 *
 * INPUT:
 *	AlgorithmParameters *params: Parameters defining how the algorithm is to be
 *		executed
 *	int type: The type of function to print out. Value zero prints the
 *		approximation, 1 the error in approximation, and 2 the residual in
 *		approximation
 *
 * OUTPUT:
 *	None. Any output from this method is printed to the console or to file
 */
void print_smooth_func( AlgorithmParameters* params, int type )
{
	GridParameters grid ;
	ApproxVars aVars ;
	
	int numSmooths, i ;
	double prevRNorm, currRNorm ;
	
	double *jacobian ; //Needed if we are performing block smoothing, in the
					   //nonlinear case
	double *exactSol ; //The exact solution discretized on the current grid. To
					   //be able to use this in this function we assume that the
					   //user has given a test case for which the exact solution
					   //is known
	double *error ; //The error between the current approximation and the exact
					//solution
					
	int recover ;
	
	char outFName[100] ;
	
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
					
	//Initialise the exact solution and the error here so that the compiler
	//doesn't complain
	exactSol = error = NULL ;
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
	
	numSmooths = 30 ;
	
	recover = (params->recoverGrads && can_recover_grads( params->testCase ) &&
		params->nlMethod != METHOD_NEWTON_MG ) ;
	
	//Set up the grid. I am using the smallest grid here so that the entire
	//approximation can be considered in one go.
	init_grid( &grid, params, meshes[params->fGridLevel-1] ) ;
	
	jacobian = NULL ;
	if( is_block_jacobi_smooth( params ) ){
		//We have only implemented the block smoothing for the recovered
		//formulation, so make sure that we only do it in this case
		jacobian = (double*) malloc( grid.numMatrixEntries *
			sizeof( double ) ) ;
	}
	
	sprintf( outFName, "/tmp/testSmoothLvl%d.txt", params->fGridLevel-1 ) ;
	
	//Set up the information needed for the approximation variables
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Now that I have the grid I want to initialise the right hand side and the
	//approximation
	initialise_rhs( &grid, params, &aVars, 1.0, aVars.rhs, aVars.bndTerm ) ;
	
	initialise_approx( params, &grid, aVars.approx ) ;
		
	//Also initialise the exact soultion on the current grid
	if( type == 1 ){
		exactSol = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
		error = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
		calculate_exact_solution( params, &grid, exactSol ) ;
	}
	
		
	//Initialise the stiffness matrix
	if( recover )
		recover_gradients( params, &grid, aVars.approx, aVars.recGrads ) ;
		
	calculate_iteration_matrix( &grid, params, aVars.approx, &aVars ) ;
		
	//Calculate the residual for the initial approximation
	calculate_residual( params, &grid, &aVars ) ;
		
	currRNorm = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
		
	//Print the L2 norm of the residual in original approximation
	printf( "Residual in original approximation: %.18lf\n", currRNorm ) ;
	
	if( type == 0 ){
		//Print the approximation to file
		print_grid_func_to_file( outFName, &grid, aVars.approx, 0 ) ;
	}
	else if( type == 1 ){
		//Calculate the error
		vect_diff( grid.numUnknowns, exactSol, aVars.approx, error ) ;
	
		//Print the error to file
		print_grid_func_no_bnd_to_file( outFName, &grid, error, 0 ) ;
	}
	else{
		//print the residual to file
		print_grid_func_no_bnd_to_file( outFName, &grid, aVars.resid, 0 ) ;
	}
		
	//Now I want to smooth a given number of times
	i = 0 ;
	while( currRNorm > 1e-14 && i < numSmooths){
		prevRNorm = currRNorm ;
		
		//Perform the smoothing
		smooth( &grid, params, 1, jacobian, &aVars ) ;
		calculate_residual( params, &grid, &aVars ) ;
			
		//Print the information about the residual norm and the ratio with the
		//previous iteration
		currRNorm = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
		
		printf( "Current residual: %.18lf\t |\tRatio: %.18lf\n",
			currRNorm, currRNorm / prevRNorm ) ;
//		if( (i+1)%100 == 0 )

		if( type==0 ){
			//Print the approximation to file
			print_grid_func_to_file( outFName, &grid, aVars.approx, 1 ) ;
		}
		else if( type==1 ){
			//Calculate the error
			vect_diff( grid.numUnknowns, exactSol, aVars.approx, error ) ;
			
			//Print the error to file
			print_grid_func_no_bnd_to_file( outFName, &grid, error, 1 ) ;
		}
		else{
			//Print the residual
			print_grid_func_no_bnd_to_file( outFName, &grid, aVars.resid, 1 ) ;
		}
		i++ ;
	}
	
	printf( "Number of non Dirichlet boundary nodes on the grid: %d\n",
		grid.numUnknowns ) ;
	printf( "Number of iterations performed: %d\n", i ) ;
	
	//Clean up
	if( type==1 ){
		free( exactSol ) ;
		free( error ) ;
	}
	free_approx_vars( &aVars, params ) ;
	free_grid( &grid ) ;
	if( jacobian != NULL )
		free( jacobian ) ;
}

/*
 * Looks at the performance of the smoother on the given fine grid level, after
 * having performed a multigrid iteration on the previous grid level
 *
 * INPUT:
 *	AlgorithmParameters* params: Parameters defining how the algorithm should be
 *		executed
 *
 * OUTPUT:
 *	None. Any information that is required is printed either to screen or to a
 *	file
 */
void test_smooth_after_mg( AlgorithmParameters* params )
{
	int itCnt ; //Counter variable
	
	GridParameters **gridHierarchy ; //The grids in the multigrid hierarchy
	ApproxVars **aVarsList ; //The variables associated with the apprxomation
							 //in the grid hierarchy
	ApproxVars *cVars ; //Variables associated with the approximation on the
						//fine grid level
	ApproxVars *fVars ; //Variables associated with the approximation on the
					   //coarse grid level
	Results results ; //Stores information about the results from the multigrid
					  //iteration
					   
	GridParameters *fGrid ; //The fine grid
	GridParameters *cGrid ; //The coarse grid
	
	double cRowNodes, fRowNodes ; //The number of nodes in a row of the coarse/
								  //fine grids, respectively. This is assuming
								  //a regular mesh
								  
	double *jacobian ; //Needed if we are performing a block smooth
								  
	int recover ; //Indicator showing whether or not recovery was performed
	
	double origResid, currResid, oldResid ; //The original / current / old
											//residual in approximation
											
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
					
	//Set up the variable to store the grids   
	gridHierarchy = (GridParameters**) malloc( sizeof( GridParameters* ) *
		params->fGridLevel ) ;
	//set all of the grids to be null pointers
	initialise_grids_to_null( gridHierarchy, params ) ;
	
	//Set up the variable to store the approximations on the grids in the
	//hierarchy
	aVarsList = (ApproxVars**) malloc( sizeof( ApproxVars* ) *
		params->fGridLevel ) ;
	initialise_approx_vars_to_null( aVarsList, params ) ;
	
	//We want to perform multigrid on the next grid level down, set the finest
	//grid in the parameters object to be one less than it is at the moment
	params->fGridLevel-- ;
	
	//Now set up the finest grid for the multigrid iteration
	gridHierarchy[params->fGridLevel - 1] = (GridParameters*) malloc( 
		sizeof( GridParameters ) ) ;
	init_grid( gridHierarchy[params->fGridLevel -1], params,
		meshes[params->fGridLevel - 1] )  ;
		
	//Set a pointer to the finest grid to make the code look neater
	cGrid = gridHierarchy[params->fGridLevel - 1] ;
	
	aVarsList[params->fGridLevel-1] = (ApproxVars*) malloc(
		sizeof( ApproxVars ) ) ;
	//Initialize the approximation variables on the finest grid
	init_approx_vars( aVarsList[params->fGridLevel-1], cGrid, params ) ;
	
	cVars = aVarsList[params->fGridLevel-1] ;
	
	//Check if we are to perform FMG to get an initial approximation
	if( params->isFMG ){
		//This is to tell the multigrid functions not to set up an initial
		//approximation, as it will be given by the result of FMG
		params->setUpApprox = 0 ;
		fmg( gridHierarchy, params, aVarsList, &results ) ;
		//We now no longer want to perform FMG
		params->isFMG = false ;
		
//		//Print the approximation gained from FMG to file to see what we got
//		print_grid_function_to_file( "/tmp/testApprox",
//			gridHierarchy[params->fGridLevel-1], cVars.approx, 0 ) ;
		
		//The initial approximation is stored in 'aVars', which will be used in
		//the multigrid iterations below
	}
			
	//Perform the appropriate multigrid method
	if( !is_non_linear(params->testCase) || params->nlMethod == METHOD_NLMG )
		multigrid( gridHierarchy, params, aVarsList, &results ) ;
	else
		newton_mg( gridHierarchy, params, aVarsList, &results ) ;
		
	//We are now interested in the finer grid level again, so increase the
	//grid level in the parameters
	params->fGridLevel++ ;
		
	//Now we have got an approximation on the grid level below where we are
	//interested in. What we do now is to interpolate onto the current grid
	//level
	
	//Set up the fine grid
	gridHierarchy[params->fGridLevel -1] = (GridParameters*)malloc(
		sizeof( GridParameters ) ) ;
	init_grid( gridHierarchy[params->fGridLevel -1], params,
		meshes[params->fGridLevel - 1] )  ;
		
	fGrid = gridHierarchy[params->fGridLevel -1] ;
		
	//Set up the approximation variables on the fine grid
	aVarsList[params->fGridLevel-1] = (ApproxVars*) malloc(
		sizeof( ApproxVars ) ) ;
	init_approx_vars( aVarsList[params->fGridLevel-1], fGrid, params ) ;
	
	fVars = aVarsList[params->fGridLevel-1] ;
	
	cRowNodes = sqrt( cGrid->numNodes ) ;
	fRowNodes = sqrt( fGrid->numNodes  ) ;
	
	//Now I want to inerpolate the approximation from the coarse grid to the
	//fine grid
	prolongate( params, cGrid,cVars->approx, fGrid, fVars->approx ) ;
		
	print_grid_func_to_file( "/tmp/testApprox",
		gridHierarchy[params->fGridLevel-1], fVars->approx, 0 ) ;
	
	//Now I need to set up everything for the smoother. I need the rhs and the
	//stiffness matrix
	recover = (params->recoverGrads && can_recover_grads( params->testCase ) &&
		params->nlMethod != METHOD_NEWTON_MG) ;
		
	//Set up the jacobian matrix, if we are performing a block smooth
	jacobian = NULL ;
	if( is_block_jacobi_smooth( params ) ){
		//We have only implemented the block smoother for the case of the
		//recovered formulation, so we check that we are performing recovery
		if( recover )
			jacobian = (double*) malloc( fGrid->numMatrixEntries *
				sizeof( double ) ) ;
		else
			params->smoothMethod = SMOOTH_GS ;
	}
		
	//First we set up the right hand side
	initialise_rhs( fGrid, params, fVars, 1.0, fVars->rhs, fVars->bndTerm ) ;
	
	//Now we calculate the stiffness matrix
	if( recover )
		recover_gradients( params, fGrid, fVars->approx, fVars->recGrads ) ;
	calculate_iteration_matrix( fGrid, params, fVars->approx, fVars ) ;
	
	//Calculate the residual in approximation
	calculate_residual( params, fGrid, fVars ) ;
	
	//Get the L2 norm of the residual
	origResid = vect_discrete_l2_norm( fGrid->numUnknowns, fVars->resid ) ;
	
	//Print to console the value of the residual in approximation
	if( params->setUpApprox )
		printf( "Original residual norm: %.18lf\n", origResid ) ;
	else
		printf( "Residual after FMG: %.18lf\n", origResid ) ;
	
	//Initialise the current residual
	currResid = origResid ;
	
	//Now we smooth
	itCnt = 0 ;
	while( itCnt < 30 ){
		//Set the old residual to be the current residual
		oldResid = currResid ;
		//Perform one smoothing iteration
		smooth( fGrid, params, 1, jacobian, fVars ) ;
		
		//Calculate the new residual
		calculate_residual( params, fGrid, fVars ) ;
				
		//calculate the norm in the residual
		currResid = vect_discrete_l2_norm( fGrid->numUnknowns, fVars->resid ) ;
		
		//Print out the ratio in successive norms
		printf( "Current Residual: %.18lf   |    Ratio: %.10lf\n", currResid,
			currResid / oldResid ) ;
			
		//Print the function out to file, appending to what is in there
		print_grid_func_to_file( "/tmp/testApprox",
			gridHierarchy[params->fGridLevel-1], fVars->approx, 1 ) ;
			
		//Increase the iteration count
		itCnt++ ;
	}
	
	//Clean up
	if( jacobian != NULL )
		free( jacobian ) ;
}

/*
 * Tests the L2 norm calculation by checking the values returned for a
 * discretization of 'sin(pi x)sin(pi y)' are converging to the value 1/2, as
 * this is the exact value
 *
 * INPUT:
 *	AlgorithmParameters *params: Parameters defining how the algorithm is to be
 *	executed
 *
 * OUTPUT:
 *	None. Any output is printed out to the console
 */
void test_l2_norm_calculation( AlgorithmParameters *params )
{
	int i ;//Loop counter
	
	//First thing is to set up the points for which we are to evaluate the given
	//function. The easiest way to do this would be to set up some grids. I want
	//to set up the coarsest grid, and the 3 grid levels above.
	GridParameters* grids ;
	GridParameters* grid ;
	double* approx ;
	double currL2Norm, prevL2Norm ;
	double currDiff, prevDiff ;
	
	const int numGrids = 10 ;
	
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
						  
	params->testCase = 2 ;
						  
	//Set the meshes that we want to set up
	meshes = meshesBL2TR ;
	
	//Initialise some variables here so tha the compiler doesn't complain
	prevL2Norm = prevDiff = 0.0 ;
	
	grids = (GridParameters*) calloc( numGrids, sizeof( GridParameters ) ) ;
	
	//For each of the meshes in our hierarchy
	for( i=0 ; i < numGrids ; i++ ){
		//Set up the mesh
		grid = &(grids[i]) ;
		init_grid( grid, params, meshes[i] ) ;
		
		//Set up the approximation on the current mesh
		approx = (double*) calloc( grid->numUnknowns, sizeof( double ) ) ;
			
		//For each of the grids that I have set up, discretize the function
		//'sin(pi x)sin(pi y)'
			
		//Set up the value of the approximation. Make sure that this is what you
		//think it is. The value of this is determined by the function
		//'approx_function' in the file 'multigrid_functions.c'
		initialise_approx( params, grid, approx ) ;
			
		//Now calculate the L2 norm of the approximation, and print this to the
		//console
		currL2Norm = vect_discrete_l2_norm( grid->numUnknowns, approx ) ;
		
		
		//For each of the approximations on the grid calculate the discrete
		//L2 norm, and print out the value. The error with the exact value (1/2)
		//should be proportional to 1/h, so the approximation should be twice
		//as good as we halve the grid spacing
		printf( "Number of points on the grid: %d,\tL2-norm: %.18lf",
			grid->numUnknowns, currL2Norm ) ;
			
		//If we are not on the coarsest grid calculate the difference in the
		//norms
		if( i!=0 ){
			currDiff = prevL2Norm - currL2Norm ;
			if( i != 1 ){
				printf( ",\tRatio of diff: %.18lf", prevDiff / currDiff ) ;
			}
			printf( "\n" ) ;
			prevDiff = currDiff ;
		}
		else
			printf( "\n" ) ;
		
		//Set the value of the previous L2 norm to be the current norm
		prevL2Norm = currL2Norm ;
			
		//Free the approximation
		free( approx ) ;
	}
	//Print the exact expected value
	printf( "Exact L2 norm for sin( pi x )sin( pi y ): %lf\n", 0.5 ) ;
	
	//Free any memory that was allocated
	for( i=0 ; i< numGrids ; i++ )
		free_grid( &(grids[i]) ) ;
	
	free( grids ) ;
}

/*
 * Tests the calculation of the H1 seminorm by checking that the value returned
 * for the norm is converging to 'pi/ sqrt(2)' for the case of the function
 * 'sin(pi x)sin(pi y)'
 *
 * INPUT:
 *	AlgorithmParameters *params: Parameters defining how the algorithm is to be
 *		executed
 *
 * OUTPUT:
 *	None. Any output from this function is printed either to the console or to a
 *	file
 */
void test_h1_seminorm_calculation( AlgorithmParameters *params )
{
	int i ; //Loop counter
	
	GridParameters *grids ; //The grids on which we want to have our
							 //approximation
	GridParameters *grid ; //A placeholder for the current grid
	
	double *func ; //The function on the current grid level
	int rNodes ; //The number of points in a row on the current grid level
	double h ; //The grid spacing on the current grid level
	double currNorm, prevNorm ; //The H1 seminorm
	double currDiff, prevDiff ; //The difference in approximations
	
	const int numGrids = 10 ;
	
	const char **meshes ; //The appropriate mesh hierarchy from which we want
						  //grids
						  
	params->testCase = 2 ;
	
	meshes = meshesBL2TR ;
	
	//Initialise variables here so that the compiler doesn't complain
	prevNorm = prevDiff = 0.0 ;
	
	//Set up the meshes
	grids = (GridParameters*) calloc( numGrids, sizeof( GridParameters ) ) ;
	
	//Loop through each of the grids
	for( i=0 ; i < numGrids ; i++ ){
		grid = &(grids[i]) ;
		init_grid( grid, params, meshes[i] ) ;
		
		//Calculate the grid spacing, assuming that this is a regular grid, and
		//that the nodes are ordered in rows
		h = grid->nodes[1].x - grid->nodes[0].x ;
		
		//Find the number of points in one row of the approximation. Assuming
		//a square grid this is also the number of rows on the grid
		rNodes = sqrt( grid->numUnknowns ) ;
		
		//Now we set up the function on the current grid. Note that the
		//calculation of the function is done in the function 'approx_function'
		//in 'multigrid_functions.c', so make sure this is set to the value
		//that we are expecting
		func = (double*) malloc( grid->numUnknowns * sizeof( double ) ) ;
		
		initialise_approx( params, grid, func ) ;
			
		//Now calculate the H1 seminorm of the function that we have set up
		currNorm = vect_discrete_h1_seminorm( grid, func ) ;
			
		//Print out the number of points on the grid, and the norm that we have
		//calculated
		printf( "Number of points on the grid: %d,\tH1 seminorm: %.18lf",
			grid->numUnknowns, currNorm ) ;
		
		if( i!= 0 ){
			currDiff = currNorm - prevNorm ;
			if( i != 1 )
				printf( "\tRatio of diff: %.18lf", prevDiff / currDiff ) ;
			
			//Set the old difference in norms to be the current difference in
			//norms
			prevDiff = currDiff ;
		}
		
		printf( "\n" ) ;
			
		//Set the old norm to be the value of the current norm
		prevNorm = currNorm ;
		
		//Clean up
		free( func ) ;
	}
	//Print the expected value
	printf( "Exact H1 seminorm of sin( pi x )sin( pi y ): %.18lf\n",
		PI / sqrt(2.0) ) ;
	
	//Clean up
	for( i=0 ; i < numGrids ; i++ )
		free_grid( &(grids[i]) ) ;
	free( grids ) ;
}

/*
 * Tests the calculation of the H1 norm by checking that the value returned
 * for the norm is converging to 'sqrt(0.25 + 0.5 pi^2)' for the case of the
 * function 'sin(pi x)sin(pi y)'
 *
 * INPUT:
 *	None
 *
 * OUTPUT:
 *	None. Any output from this function is printed either to the console or to a
 *	file
 */
void test_h1_norm_calculation( AlgorithmParameters *params )
{
	int i ; //Loop counter
	
	GridParameters *grids ; //The grids on which we want to have our
							 //approximation
	GridParameters *grid ; //A placeholder for the current grid
	
	double *func ; //The function on the current grid level
	int rNodes ; //The number of points in a row on the current grid level
	double h ; //The grid spacing on the current grid level
	double currNorm, prevNorm ; //The H1 seminorm
	double currDiff, prevDiff ; //The difference in approximations
	
	const int numGrids = 10 ;
	
	const char **meshes ; //The appropriate mesh hierarchy from which we want
						  //grids
	char outFName[100] ;
						  
	params->testCase = 2 ;
	
	meshes = meshesBL2TR ;
	
	//Initialise variables here
	prevNorm = prevDiff = 0.0 ;
	
	//Set up the meshes
	grids = (GridParameters*) calloc( numGrids, sizeof( GridParameters ) ) ;
	
	//Loop through each of the grids
	for( i=0 ; i < numGrids ; i++ ){
		grid = &(grids[i]) ;
		init_grid( grid, params, meshes[i] ) ;
		
		//Calculate the grid spacing, assuming that this is a regular grid, and
		//that the nodes are ordered in rows
		h = grid->nodes[1].x - grid->nodes[0].x ;
		
		//Find the number of points in one row of the approximation. Assuming
		//a square grid this is also the number of rows on the grid. For this
		//calculate we disregard the boundary nodes
		rNodes = sqrt( grid->numUnknowns ) ;
		
		//Now we set up the function on the current grid. Note that the
		//calculation of the function is done in the function 'approx_function'
		//in 'multigrid_functions.c', so make sure this is set to the value
		//that we are expecting
		func = (double*) malloc( grid->numUnknowns * sizeof( double ) ) ;
		
		initialise_approx( params, grid, func ) ;
		
		if( i < 6 ){
			sprintf( outFName, "/tmp/approxOutLvl%d.txt", i ) ;
			print_grid_func_no_bnd_to_file( outFName, grid, func, 0 ) ;
		}
			
		//Now calculate the H1 seminorm of the function that we have set up
		currNorm = vect_discrete_h1_norm( grid, func ) ;
			
		//Print out the number of points on the grid, and the norm that we have
		//calculated
		printf( "Number of points on the grid: %d,\tH1 norm: %.18lf",
			grid->numUnknowns, currNorm ) ;
		
		if( i!= 0 ){
			currDiff = currNorm - prevNorm ;
			if( i != 1 )
				printf( "\tRatio of diff: %.18lf", prevDiff / currDiff ) ;
			
			//Set the old difference in norms to be the current difference in
			//norms
			prevDiff = currDiff ;
		}
		
		printf( "\n" ) ;
			
		//Set the old norm to be the value of the current norm
		prevNorm = currNorm ;
		
		//Clean up
		free( func ) ;
	}
	printf( "Exact H1 norm for sin( pi x )sin( pi y ): %.18lf\n",
		sqrt( 0.25 + PI * PI / 2.0 ) ) ;
	
	//Clean up
	for( i=0 ; i < numGrids ; i++ )
		free_grid( &(grids[i]) ) ;
	free( grids ) ;
}

/*
 * Tests that the Lapack solver returns the correct value. For this a
 * pre-defined matrix and rhs are used
 *
 * INPUT:
 *	None
 *
 * OUTPUT:
 *	None. Any output is printed to the screen
 */
void test_lapack_solve( AlgorithmParameters *params )
{
	int j ; //Loop counters
	int mDim, nrhs ; //The dimension of the matrix, and the number of rhs
					 //vectors
	int pivot[3], ok; //Vector to store the value of the rows that are pivoted.
					  //These are needed to be able to call the Lapack function
	double A[9], b[3] ;


	A[0] = 10.0; A[3] = 1.0; A[6] = 8.0;
	A[1] = 5.0; A[4] = 4.0; A[7] = 10.0;
	A[2] = 8.0; A[5] = 9.0; A[8]= 7.0;

	b[0]=7.0;			/* if you define b as a matrix then you */
	b[1]=8.0;			/* can solve multiple equations with */
	b[2]=7.0;			/* the same A but different b */ 						

	mDim=3;			/* and put all numbers we want to pass */
	nrhs=1;    			/* to the routine in variables */

	/* find solution using LAPACK routine SGESV, all the arguments have to */
	/* be pointers and you have to add an underscore to the routine name */
	dgesv_(&mDim, &nrhs, A, &mDim, pivot, b, &mDim, &ok);      

	/*
	 parameters in the order as they appear in the function call
		order of matrix A, number of right hand sides (b), matrix A,
		leading dimension of A, array that records pivoting, 
		result vector b on entry, x on exit, leading dimension of b
		return value */ 
		 						
	for (j=0; j<3; j++) printf("%e\n", b[j]);	/* print vector x */
	
	//The output should be:
	//	1.443737e-01
	//	1.210191e-01
	//	6.794055e-01
}

/*
 * TODO: Insert appropriate comment here
 */
void test_connection( AlgorithmParameters *params )
{
	int connect ;
	GridParameters grid ; //The grid to consider
	
	const char **meshes ; //The appropriate mesh hierarchy from which we want
						  //grids
	
	meshes = meshesBL2TR ;
	
	//Set up the grid. We take the grid with grid spacing 0.25 (i.e. the
	//coarsest one). This is so that we can easily check that the returned
	//values are the correct ones
	init_grid( &grid, params, meshes[0] ) ;
	
	printf( "Number of connections recorded: %d\n", grid.numMatrixEntries ) ;
	
	//Check different connections
	//First check that we can recognise when there are no connections
	printf( "Checking connection between (interior) node 5 and (interior)" ) ;
	printf( " node 7: No connection expected\n" ) ;
	connect = connected( &(grid.nodes[grid.mapA2G[5]]),
		&(grid.nodes[grid.mapA2G[7]]) ) ;
	if( connect == -1 )
		printf( "No connection found\n" ) ;
	else
		printf( "Connection at index %d\n", grid.rowStartInds[5] + connect ) ;
		
	printf( "***\n" ) ;
		
	printf( "Checking connection between (interior) node 2 and (interior)" ) ;
	printf( " node 0: No connection expected\n" ) ;
	connect = connected( &(grid.nodes[grid.mapA2G[2]]),
		&(grid.nodes[grid.mapA2G[0]]) ) ;
	if( connect == -1 )
		printf( "No connection found\n" ) ;
	else
		printf( "Connection at index %d\n", grid.rowStartInds[2] + connect ) ;
		
	printf( "***\n" ) ;
		
	printf( "Checking connection between (interior) node 4 and (interior)" ) ;
	printf( " node 6: No connection expected\n" ) ;
	connect = connected( &(grid.nodes[grid.mapA2G[4]]),
		&(grid.nodes[grid.mapA2G[6]]) ) ;
	if( connect == -1 )
		printf( "No connection found\n" ) ;
	else
		printf( "Connection at index %d\n", grid.rowStartInds[4] + connect ) ;
		
	printf( "***\n" ) ;
		
	//Now check that we can find a connection
	printf( "Checking connection between (interior) node 4 and (interior)" ) ;
	printf( " node 7: Connection expected\n" ) ;
	connect = connected( &(grid.nodes[grid.mapA2G[4]]),
		&(grid.nodes[grid.mapA2G[7]]) ) ;
	if( connect == -1 )
		printf( "No connection found\n" ) ;
	else
		printf( "Connection at index %d. Value at index: %d\n",
			grid.rowStartInds[4] + connect,
			grid.mapG2A[ grid.nodes[grid.mapA2G[4]].nghbrs[connect] ] ) ;
			
	printf( "***\n" ) ;
		
	printf( "Checking connection between (interior) node 3 and (interior)" ) ;
	printf( " node 7: Connection expected\n" ) ;
	connect = connected( &(grid.nodes[grid.mapA2G[3]]),
		&(grid.nodes[grid.mapA2G[7]]) ) ;
	if( connect == -1 )
		printf( "No connection found\n" ) ;
	else
		printf( "Connection at index %d. Value at index: %d\n",
			grid.rowStartInds[3] + connect,
			grid.mapG2A[ grid.nodes[grid.mapA2G[3]].nghbrs[connect] ] ) ;
	
	printf( "***\n" ) ;
		
	printf( "Checking connection between (interior) node 1 and (interior)" ) ;
	printf( " node 0: Connection expected\n" ) ;
	connect = connected( &(grid.nodes[grid.mapA2G[1]]),
		&(grid.nodes[grid.mapA2G[0]]) ) ;
	if( connect == -1 )
		printf( "No connection found\n" ) ;
	else
		printf( "Connection at index %d. Value at index: %d\n",
			grid.rowStartInds[1] + connect,
			grid.mapG2A[ grid.nodes[grid.mapA2G[1]].nghbrs[connect] ] ) ;
			
	//Clean up
	free_grid( &grid ) ;
}

/*
 * TODO: Insert appropriate comment
 */
void test_smooth_block( AlgorithmParameters *params )
{
	int i, k, row, col ; //Loop counter
	GridParameters grid ; //The grid on which we are operating
	ApproxVars aVars ; //Approximation variables needed
	
	const Node *node, *rowNode, *colNode ; //Placeholders for nodes on the grid
	
	SmoothBlock sBlock ;
	
	double *jacobian ; //The jacobian matrix
	
	const char **meshes ; //The appropriate mesh hierarchy from which we want
						  //grids
	
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
	
	//Make sure that the parameters are set up in the way in which we want them
	params->smoothMethod = SMOOTH_BLOCK_JAC_AVG ;
	params->testCase = 2 ;
	params->nlMethod = METHOD_NLMG ;
	params->alpha = 1.0 ;
	params->recoverGrads = 0 ;
	
	//Set up the grid to be the coarsest grid, so that we can check by hand
	//whether the output is the correct one
	init_grid( &grid, params, meshes[0] ) ;
	
	//Initialise the approximation variables
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Allocate the memory for the jacobian
	jacobian = (double*) calloc( grid.numMatrixEntries, sizeof( double ) ) ;
	
	//Set up the approximation on the grid, and a rhs
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		aVars.approx[i] = i+1.0 ;
		aVars.rhs[i] = 1.0 ;
	}
	
	//Calculate the stiffness matrix
	calculate_iteration_matrix( &grid, params, aVars.approx, &aVars ) ;
		
	//Calculate the jacobian matrix
	calculate_jacobian_matrix( &grid, &aVars, params, jacobian, 0 ) ;	

	for( i=0 ; i < grid.numUnknowns ; i++ ){
		//Get the node on the grid corresponding to the current node in the
		//approximation
		node = &(grid.nodes[ grid.mapA2G[i] ]) ;
		//Set the length of the smooth block to be the number of non Dirichlet
		//boundary neighbours of the node
		sBlock.len = node->numNghbrs ;
		
		//Print out the index of the nodes that we take the derivative wrt
		printf( "Nodes in matrix:\n\t" ) ;
		//Construct the matrix to pass to the Lapack code
		for( row=0 ; row < sBlock.len ; row++ ){
			//Get the node for which we construct a row in the block
			rowNode = &(grid.nodes[ node->nghbrs[row] ]) ;
		
			//Set the columns one by one
			for( col=0 ; col < sBlock.len ; col++ ){
				colNode = &(grid.nodes[ node->nghbrs[col] ]) ;
			
				printf( "%d", colNode->id ) ;
				if( col != sBlock.len -1 )
					printf( ", " ) ;
				//Check whether we have a connection between the two values
				k = connected( rowNode, colNode ) ;
				if( k != -1 ){
					//Get the appropriate entry from the Jacobian matrix
					k += grid.rowStartInds[ grid.mapG2A[rowNode->id] ] ;
					sBlock.deriv[col*sBlock.len + row] = jacobian[k] ;
				}
				else
					sBlock.deriv[col*sBlock.len + row] = 0.0 ;
			}
			printf( "\n\t" ) ;
		}
		printf( "\n" ) ;
	
		//Now print out what I have retrieved
		//I expect the entries in the first row to be [628, -65, -353, -96],
		//where the only entry of which I am sure of the position is the first
		//one
		printf( "Actual output:\n\t" ) ;
		for( row=0 ; row < sBlock.len ; row++ ){
			for( col = 0 ; col < sBlock.len ; col++ ){
				//Get the entries for the row
				printf( "%.2lf", sBlock.deriv[ col*sBlock.len + row ] ) ;
				if( col != sBlock.len - 1 )
					printf( ", " ) ;
			}
			printf( "\n\t" ) ;
		}
		printf( "\n***\n\n" ) ;
	}
	
	
	//Clean up
	free_grid( &grid ) ;
}

/*
 * TODO: Insert appropriate comment here
 */
void test_approx_set_up( AlgorithmParameters *params )
{
	const char **meshes ;
	double *approx ;
	GridParameters grid ;
	
	meshes = get_grid_hierarchy( params ) ;
	//Work with the finest grid only
	init_grid( &grid, params, meshes[params->fGridLevel-1] ) ;
	
	approx = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	
	//Initialise the approximation
	initialise_approx( params, &grid, approx ) ;
	
	print_grid_func_no_bnd_to_file( "/tmp/approxOut", &grid, approx, 0 ) ;
	
	//Clean up
	free_grid( &grid ) ;
	free( approx ) ;
}

/*
 * This test should be deleted. I am including it to see if I get the same rhs
 * using the updated GridParameters structure as I do with the old one. As I am
 * planning to delete the old structure I won't be able to use this, so delete
 * it
 */
void test_set_up_rhs( AlgorithmParameters *params )
{
	const char **meshes ;
	double *rhs, *bndryTerm ;
	GridParameters grid ;
	ApproxVars aVars ;
	char outFName[100] ;
	
	meshes = meshesBL2TR ;
	init_grid( &grid, params, meshes[params->fGridLevel-1] ) ;
	init_approx_vars( &aVars, &grid, params ) ;
	
	rhs = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	bndryTerm = (double*) calloc( grid.numUnknowns, sizeof( double ) ) ;
	
	//Set up the right hand side using the new method
	initialise_rhs( &grid, params, &aVars, 1.0, rhs, bndryTerm ) ;
	
	sprintf( outFName, "/tmp/rhsLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_no_bnd_to_file( outFName, &grid, rhs, 0 ) ;
	
	sprintf( outFName, "/tmp/bndLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_no_bnd_to_file( outFName, &grid, bndryTerm, 0 ) ;
	
	//Clean up
	free( rhs ) ;
	free( bndryTerm ) ;
	free_grid( &grid ) ;
	free_approx_vars( &aVars, params ) ;
}

/*
 * TODO: Insert appropriate comment
 */
void test_resid_set_up( AlgorithmParameters *params )
{
	const char **meshes ;
	GridParameters grid ;
	ApproxVars aVars ;
	char outFName[100] ;
	
	//Set up the grid, according to the parameters that are passed in
	meshes = meshesBL2TR ;
	init_grid( &grid, params, meshes[params->fGridLevel-1] ) ;
	
	printf( "Number of entries in the matrix: %d\n", grid.numMatrixEntries ) ;
	
	//Now that I have set up the grid, I want to set up an approximation
	//First set up the appropriate storage
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Now set up the approximation
	initialise_approx( params, &grid, aVars.approx ) ;
	
	sprintf( outFName, "/tmp/approxNewLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_to_file( outFName, &grid, aVars.approx, 0 ) ;
	
	//Now set up the rhs
	initialise_rhs( &grid, params, &aVars, 1.0, aVars.rhs, aVars.bndTerm ) ;
	
	sprintf( outFName, "/tmp/rhsNewLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_no_bnd_to_file( outFName, &grid, aVars.rhs, 0 ) ;
	
	if( params->recoverGrads )
		recover_gradients( params, &grid, aVars.approx, aVars.recGrads ) ;
	
	//Calculate the iteration matrix
	calculate_iteration_matrix( &grid, params, aVars.approx, &aVars ) ;
	
	//Now we calculate the residual
	calculate_residual( params, &grid, &aVars ) ;
	
	//Print out the norm of the residual
	printf( "L2 norm of the residual: %.18lf\n", vect_discrete_l2_norm(
		grid.numUnknowns, aVars.resid ) ) ;
	printf( "H1 norm of the residual: %.18lf\n", vect_discrete_h1_norm(
		&grid, aVars.resid ) ) ;
		
	sprintf( outFName, "/tmp/residLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_no_bnd_to_file( outFName, &grid, aVars.resid, 0 ) ;
	
	//Clean up
	free_grid( &grid ) ;
	free_approx_vars( &aVars, params ) ;
}

/*
 * TODO: Insert appropriate comment
 */
void test_grid_indices( AlgorithmParameters *params )
{
	int i ; //Loop counter
	const char **meshes ;
	GridParameters grid ;
	
	//Set up the coarsest grid with diagonal running from the bottom left to the
	//top right of the element
	meshes = meshesBL2TR ;
	init_grid( &grid, params, meshes[0] ) ;
	
	//Print out the indices stored in the mapping from the grid to the
	//approximation
	printf( "Map: Grid -> Approximation\n\t[ " ) ;
	for( i=0 ; i < grid.numNodes ; i++ )
		printf( "%d, ", grid.mapG2A[i] ) ;
	printf( "]\n" ) ;
	
	//Print the indices stored in the mapping from the approximation to the grid
	printf( "Map: Approximation -> Grid\n\t[ " ) ;
	for( i=0 ; i < grid.numNodes ; i++ ){
		printf( "%d, ", grid.mapA2G[i] ) ;
		if( i == grid.numUnknowns -1 )
			printf( "| " ) ;
	}
	printf( "]\n" ) ;
	
	//Print the mapping from the approximation into the grid and back. This
	//should give the numbers in ascending order, as the ordering of the nodes
	//on the grid is in numerical order.
	printf( "Map: Approximation -> Grid -> Approximation\n\t[ " ) ;
	for( i=0 ; i < grid.numNodes ; i++ )
		printf( "%d, ", grid.mapG2A[ grid.mapA2G[i] ] ) ;
	printf( "]\n" ) ;
}

/*
 * Prints out the staring indices of the elements on the coarsest grid in the
 * connectivity matrix
 *
 * INPUT:
 *	AlgorithmParameters *params: Parameters defining how the algorithm is to be
 *		executed
 *
 * OUTPUT:
 *	None. Any output from this function is printed to the console
 */
void print_row_start_inds( AlgorithmParameters *params )
{
	int i ; //Loop counter
	const char **meshes ; //The mesh structure to use
	GridParameters grid ;
	
	//Set up the coarsest grid
	meshes = meshesBL2TR ;
	init_grid( &grid, params, meshes[0] ) ;
	
	//Print out the row start indices
	printf( "Row start indices:\n\t[ " ) ;
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		printf( "%d, ", grid.rowStartInds[i] ) ;
	}
	printf( "]\n" ) ;
	
	//Clean up
	free_grid( &grid ) ;
}

/*
 * Prints out the neighbourhood on the coarsest grid so that we can see if the
 * connections are recorded correctly for each node
 */
void print_neighbours( AlgorithmParameters *params )
{
	int i, j ; //Loop counter
	const char **meshes ;
	GridParameters grid ;
	int nodeId ;
	const Node *node ;
	
	//Set up the coarsest grid
	meshes = meshesBL2TR ;
	init_grid( &grid, params, meshes[0] ) ;
	
	for( j=0 ; j < grid.numUnknowns ; j++ ){
		nodeId = grid.mapA2G[ j ]  ;
		node = &(grid.nodes[ nodeId ]) ;
	
		printf( "Connections for node: %d\n\t[ ", nodeId ) ;
		for( i=0 ; i < node->numNghbrs ; i++ )
			printf( "%d, ", node->nghbrs[i] ) ;
		printf( "]\n" ) ;
	}
}

/*
 * Tests that the reading of a function from file works as expected
 */
void test_read_func_from_file( AlgorithmParameters *params )
{
	const char **meshes ; //The grid hierarchy to work with
	GridParameters grid ;
	double *approx, *approxFromFile ;
	
	meshes = meshesBL2TR ;
	//Set up the grid on which we are working
	init_grid( &grid, params, meshes[ params->fGridLevel-1 ] ) ;
	
	//Initialise the approximation
	approx = (double*)malloc( grid.numUnknowns * sizeof( double ) ) ;
	approxFromFile = (double*)malloc( grid.numUnknowns * sizeof( double ) ) ;
	
	initialise_approx( params, &grid, approx ) ;
	
	print_grid_func_no_bnd_to_file( "/tmp/approxOut", &grid, approx, 0 ) ;
	
	//Now we read the approximation from the file
	read_grid_function_from_file( "/tmp/approxOut", &grid, approxFromFile ) ;
	
	//Now write the approximation to the file, so that we can see if we've got
	//the same function back in
	print_grid_func_no_bnd_to_file( "/tmp/approxReadOut", &grid,
		approxFromFile, 0 ) ;
	
	//Clean up
	free( approx ) ;
	free( approxFromFile ) ;
}

/*
 * TODO: Insert appropriate comment
 */
void test_jacobian_calculation( AlgorithmParameters *params )
{
	const char **meshes ;
	char outFName[100] ;
	FILE *outFile ;
	int i, j, rowInd ;
	const Node *nodeI, *nodeJ ;
	
	GridParameters grid ;
	ApproxVars aVars ;
	
	//Get the appropriate grid hierarchy
	meshes = get_grid_hierarchy( params ) ;
	
	//Set up the grid
	init_grid( &grid, params, meshes[params->fGridLevel-1] ) ;
	
	//Set up the approximation variables on the grid
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Initialise the approximation
	initialise_approx( params, &grid, aVars.approx ) ;
	
	//Now calculate the jacobian matrix
	calculate_jacobian_matrix( &grid, &aVars, params, aVars.iterMat, 0 ) ;
	
	
	//Print out the entries in the matrix to file
	sprintf( outFName, "/tmp/jacOutLvl%d.txt", params->fGridLevel ) ;
	outFile = fopen( outFName, "w" ) ;

	//Loop through all of the unknown nodes on the grid
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		//Get a placeholder to the current node on the grid
		nodeI = &(grid.nodes[ grid.mapA2G[ i ] ]) ;
		rowInd = grid.rowStartInds[i] ;
		//Loop through the neighbours of the current node (that are in the
		//interior of the grid)
		for( j = 0 ; j < nodeI->numNghbrs ; j++ ){
			//Get a placeholder for the neighbouring node on the grid
			nodeJ = &(grid.nodes[ nodeI->nghbrs[ j ] ]) ;
			fprintf( outFile, "Row: %d, Column: %d: %.18lf\n", nodeI->id,
				nodeJ->id, aVars.iterMat[ rowInd + j ] ) ;
		}
	}
	
	fclose( outFile ) ;
	
	//Clean up
	free_grid( &grid ) ;
	free_approx_vars( &aVars, params ) ;
}

/* Tests that the exact solution of a systems of equations is possible
 */
void test_exact_solve( AlgorithmParameters *params )
{
	GridParameters grid ; //The grid on which to operate
	ApproxVars aVars ; //Variables associated with the approximation
	const char **meshes ;
	char outFName[100] ;
	
	meshes = get_grid_hierarchy( params ) ;
	
	//Initialise the grid and the approximation variables
	init_grid( &grid, params, meshes[ params->fGridLevel-1 ] ) ;
	
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Set up the initial approximation
	initialise_approx( params, &grid, aVars.approx ) ;
	
	//Set up the right hand side
	initialise_rhs( &grid, params, &aVars, 1.0, aVars.rhs, aVars.bndTerm ) ;
	
	//If we need to recover the gradients, then do so
	if( params->recoverGrads )
		recover_gradients( params, &grid, aVars.approx, aVars.recGrads ) ;
	//Calculate the iteration matrix
	calculate_iteration_matrix( &grid, params, aVars.approx, &aVars ) ;
	
	
	sprintf( outFName, "/tmp/testOutLvl%d.txt", params->fGridLevel ) ;
	//Print the output to file
	print_grid_func_to_file( outFName, &grid, aVars.approx, 0 ) ;
	
	//Now solve the linear system
	direct_lin_solve( &grid, &aVars ) ;
	
	print_grid_func_to_file( outFName, &grid, aVars.approx, 1 ) ;
	
	//Clean up
	free_grid( &grid ) ;
	free_approx_vars( &aVars, params ) ;
}

/* Performs just FMG to see what the output of this method is. We expect this
 * to be 'close' to the exact solution if the solution can be resolved on a
 * given grid
 */
void test_fmg( AlgorithmParameters *params )
{
	GridParameters **grids ;
	ApproxVars **aVarsList ;
	ApproxVars *fVars ;
	const char **meshes ;
	Results results ;
	char outFName[100] ;
	
	//Set up the placeholder for the meshes to be used
	meshes = get_grid_hierarchy( params ) ;
	
	//Make sure that we are performing FMG
	params->isFMG = false ;
	
	//Inititalise the grids that we will need in this run to NULL pointers
	grids = (GridParameters**) malloc( params->fGridLevel *
		sizeof( GridParameters* ) ) ;
	initialise_grids_to_null( grids, params ) ;
	
	//Initialise the approximation variables that we will need in this run to
	//NULL pointers
	aVarsList = (ApproxVars**) malloc( params->fGridLevel *
		sizeof( ApproxVars* ) ) ;
	initialise_approx_vars_to_null( aVarsList, params ) ;
	
	//Set up the finest grid
	grids[params->fGridLevel-1] = (GridParameters*)malloc(
		sizeof( GridParameters ) ) ;
	init_grid( grids[params->fGridLevel -1], params,
		meshes[params->fGridLevel - 1] ) ;
		
	//This is to tell the multigrid functions not to set up an initial
	//approximation, as it will be given by the result of FMG
	params->setUpApprox = 0 ;
	fmg( grids, params, aVarsList, &results ) ;
	
	fVars = aVarsList[ params->fGridLevel-1 ] ;
	
	//Set the name of the file to output the result of FMG to
	sprintf( outFName, "./FMGApprox/" ) ;
	if( params->testCase == 1 )
		sprintf( outFName, "%sfmgtc1f%dc%d.txt", outFName, params->fGridLevel,
			params->cGridLevel ) ;
	else if( params->testCase != 6 && params->testCase != 7 )
		sprintf( outFName, "%sfmgtc%df%dc%da%.3lf.txt", outFName,
			params->testCase, params->fGridLevel, params->cGridLevel,
			params->alpha ) ;
	else
		sprintf( outFName, "%sfmgtc%df%dc%da%.3lfA%.3lfB%.3lfn%.3lfm%.3lf.txt",
			outFName, params->testCase, params->fGridLevel,
			params->cGridLevel, params->alpha, params->solSmoothAmp,
			params->solOscAmp, params->solXFreq, params->solYFreq ) ;
	//Print the approximation gained from FMG to file to see what we got
	print_grid_func_to_file( outFName, grids[params->fGridLevel-1],
		fVars->approx, 0 ) ;
		
	//Clean up
	free_grids( grids, params->fGridLevel ) ;
	free_approx_var_list( aVarsList, params->fGridLevel, params ) ;
	free( grids ) ;
	free( aVarsList ) ;
}

/* Tests the application of the operator to a known left hand side, which will
 * be read in. It is assumed that the user knows what output is expected, and
 * that they will check the result after having run this method. The output is
 * written to a file for the user to do with as they please
 */
void test_apply_operator( AlgorithmParameters *params )
{
	const char **meshes ;
	GridParameters grid ;
	ApproxVars aVars ;
	int i, j, rInd, aInd ;
	const Node *node ;
	double *result ;
	char outFName[80] ;
	
	//Check whether a file to read an approximation from has been specified
	if( strlen( params->initApproxFile ) == 0 ){
		printf( "Please specify a file from which to read an " ) ;
		printf( "approximation.\n" ) ;
		printf( "This can be done by setting INIT_APPROX_FILE in the " ) ;
		printf( "parameters file.\n" ) ;
		return ;
	}
	
	//Get the grids to operate on
	meshes = get_grid_hierarchy( params ) ;
	
	//Set up the grid and the variables associated with the approximation
	init_grid( &grid, params, meshes[ params->fGridLevel - 1] ) ;
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Read the approximation from file
	read_grid_function_from_file( params->initApproxFile, &grid,
		aVars.approx ) ;
	
	//Calculate the iteration matrix
	calculate_iteration_matrix( &grid, params, aVars.approx, &aVars ) ;
	
	result = (double*)malloc( grid.numUnknowns * sizeof( double ) ) ;
	
	//Apply the operator
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		//Get the current node on the grid
		node = &(grid.nodes[ grid.mapA2G[ i ] ]) ;
		//Get the index into the row of the operator for the current node
		rInd = grid.rowStartInds[ i ] ;
		result[i] = aVars.approx[i] * aVars.iterMat[ rInd ] ;
		//Loop through the neighbours of the current node
		for( j=1 ; j < node->numNghbrs ; j++ ){
			//Get the index into the approximation for the given neighbour
			aInd = grid.mapG2A[ node->nghbrs[ j ] ] ;
			//Apply the operator to the appropriate value in the approximation
			result[i] += aVars.approx[ aInd ] * aVars.iterMat[ rInd + j ] ;
		}
	}
	
	//Now that I have the result print it out to file
	sprintf( outFName, "/tmp/testRHSLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_no_bnd_to_file( outFName, &grid, result, 0 ) ;
	
	//Clean up
	free( result ) ;
}

/* Tests whether the transformation of a Hessenberg matrix into an upper
 * triangular matrix works or not. This method is required in the GMRES linear
 * solver
 */
void test_hess_to_upper( AlgorithmParameters *params )
{
	int i, j ; //Loop counters
	LeastSquaresContext ctxt ;
	
	//Set up the GMRES context
	initialise_least_squares_context( &ctxt, 5 ) ;
	
	//Seed the random number generator
	srand( time( NULL ) ) ;
	
	//Assume that we are on the 5th iteration
	ctxt.currSize = 5 ;
	//Now we put values into the Hessenberg matrix
	//Loop through the columns
	for( j=0 ; j < ctxt.currSize ; j++ ){
		//Loop through the rows
		for( i=0 ; i < j+2 ; i++ ){
			//Insert a random value into the matrix
			ctxt.hessMat[j][i] = (int)rand_in_range( 1, 101 ) / 100.0 ;
		}
	}
	ctxt.rhs[0] = (int)rand_in_range( 1, 101 ) / 100.0 ;
	
	//Display the matrix
	printf( "Original Matrix: (RHS)\n" ) ;
	//Display the first row in the matrix
	for( j=0 ; j < ctxt.currSize ; j++ )
		printf( "%.3lf, ", ctxt.hessMat[j][0] ) ;
	printf( "\t (%.3lf)\n", ctxt.rhs[0] ) ;
	//Loop through the remaining rows
	for( i=1 ; i < ctxt.currSize+1 ; i++ ){
		//Loop through the columns
		for( j=1 ; j < i ; j++ )
			printf( "0.000, " ) ;
		for( j=i-1 ; j < ctxt.currSize ; j++ ){
			printf( "%.3lf, ", ctxt.hessMat[j][i] ) ;
		}
		printf( "\t (%.3lf)\n", ctxt.rhs[i] ) ;
	}
	
	//Now transform the matrix into upper triangular form
	hess_to_upper( &ctxt ) ;
	
	//Now print out the matrix again
	//Display the first row in the matrix
	printf( "\nTransformed matrix: (RHS)\n" ) ;
	for( j=0 ; j < ctxt.currSize ; j++ )
		printf( "%.3lf, ", ctxt.upTMat[j][0] ) ;
	printf( "\t (%.3lf)\n", ctxt.rhs[0] ) ;
	//Loop through the remaining rows
	for( i=1 ; i < ctxt.currSize+1 ; i++ ){
		//Loop through the columns
		for( j=1 ; j < i ; j++ )
			printf( "0.000, " ) ;
		for( j=i-1 ; j < ctxt.currSize ; j++ ){
			printf( "%.3lf, ", ctxt.upTMat[j][i] ) ;
		}
		printf( "\t (%.3lf)\n", ctxt.rhs[i] ) ;
	}
}

/* Tests whether the back susbtitution solves an upper triangular system
 */
void test_back_sub( AlgorithmParameters *params )
{
	int i, j ; //Loop counters
	double **mat ;
	double *rhs ;
	int dim = 5 ;
	double tmp ;
	
	srand( time( NULL ) ) ;
	
	mat = (double**)calloc( dim, sizeof( double* ) ) ;
	rhs = (double*)calloc( dim, sizeof( double ) ) ;
	for( i=0; i < dim ; i++ )
		mat[i] = (double*)calloc( i+1, sizeof( double ) ) ;
			
	//Now set up the matrix and the right hand side
	for( i=0 ; i < dim ; i++ ){
		rhs[i] = (int)rand_in_range( 1, 101 ) / 100.0 ;
		for( j=i ; j < dim ; j++ )
			mat[j][i] = (int)rand_in_range(1,101) / 100.0 ;
	}
	
	//Print out the matrix
	//Loop through the rows
	printf( "Matrix: (RHS)\n" ) ;
	for( i=0 ; i < dim ; i++ ){
		for( j=0 ; j < i ; j++ )
			printf( "0.0000000, " ) ;
		//Loop through the columns
		for( j=i ; j < dim ; j++ )
			printf( "%.7lf, ", mat[j][i] ) ;
		printf( "\t (%.7lf)\n", rhs[i] ) ;
	}
	
	//Now solve the system
	back_sub( (double const**)mat, dim, rhs ) ;
	
	//Now print out the matrix and the right hand side
	printf( "\nSolution:\n" ) ;
	for( i=0 ; i < dim ; i++ )
		printf( "%.7lf, ", rhs[i] ) ;
	printf( "\n" ) ;
	
	//Now multiply the solution and matrix
	printf( "\nMatrix * solution:\n" ) ;
	for( i=0 ; i < dim ; i++ ){
		tmp = 0.0 ;
		for( j=i ; j < dim ; j++ )
			tmp += mat[j][i] * rhs[j] ;
		printf( "%.7lf, ", tmp ) ;
	}
	printf( "\n" ) ;
}

/* Tests whether a perturbation from the least squares problem gives something
 * larger or smaller in L2 norm from the solution that is returned
 */
void test_least_squares( AlgorithmParameters *params )
{
	int i, j, k ;
	double *rhs, *lsResult ;
	double **mat ;
	LeastSquaresContext ctxt ;
	int size = 30 ;
	double minNorm, newNorm ;
	
	srand( time( NULL ) ) ;
	
	//Set up the least squares context
	initialise_least_squares_context( &ctxt, size ) ;
	ctxt.currSize = size ;
	
	//set up a random matrix and right hand side
	rhs = (double*)calloc( size+1, sizeof( double ) ) ;
	lsResult = (double*)calloc( size+1, sizeof( double ) ) ;
	mat = (double**)calloc( size, sizeof( double*) ) ;
	for( i=0 ; i < size ; i++ )
		mat[i] = (double*)calloc( i+2, sizeof( double ) ) ;
	
	rhs[0] = (int)rand_in_range(1, 101) / 100.0 ;
	ctxt.rhs[0] = rhs[0] ;
	
	//Loop through the columns
	for( j=0 ; j < ctxt.currSize ; j++ ){
		//Loop through the rows
		for( i=0 ; i < j+2 ; i++ ){
			//Insert a random value into the matrix
			mat[j][i] = (int)rand_in_range( 1, 101 ) / 100.0 ;
			ctxt.hessMat[j][i] = mat[j][i] ;
		}
	}
	
	//Now perform the least squares
	least_squares( &ctxt ) ;
	
	//Now show that the norm for the least squares approximation is less than
	//that for the original approximation
	for( i=0 ; i < size+1 ; i++ )
		lsResult[i] = rhs[i] ;
	for( j=0 ; j < size ; j++ )
		for( i=0 ; i < j+2 ; i++ )
			lsResult[i] -= ctxt.rhs[j] * mat[j][i] ;
	minNorm = vect_discrete_l2_norm( size+1, lsResult ) ;
	//printf( "Norm for least squares approximation:\t%.18lf\n", minNorm ) ;
		
	//Now perturb the approximation. We perturb each position in the
	//approximation in turn by a small amount, and then perturb all positions by
	//a small amount and see if the resulting norm is smaller than the one
	//calculated using the least squares approximation. This is not an extensive
	//test to see if the resulting vector is the least squares approximation.
	//However, after having run a lot of test runs it has always been the case
	//that I do get the mimimum.
	for( k = 0 ; k < size ; k++ ){
		ctxt.rhs[k] += M_EPS ;
		for( i=0 ; i < size+1 ; i++ )
			lsResult[i] = rhs[i] ;
		for( j=0 ; j < size ; j++ )
			for( i=0 ; i < j + 2 ; i++ )
				lsResult[i] -= ctxt.rhs[j] * mat[j][i] ;
		newNorm = vect_discrete_l2_norm( size+1, lsResult ) ;
		//printf( "Norm for new approximation:\t\t%.18lf\n", newNorm ) ;
		ctxt.rhs[k] -= M_EPS ;
	
		if( minNorm <= newNorm ){
			printf( "New norm is larger than calculated least squares " ) ;
			printf( "solution\n" );
		}
		else
			printf( "Solution is not the least squares approximation!\n" ) ;
	}
	
	for( i=0 ; i < size ; i++ )
		ctxt.rhs[i] += M_EPS ;
	for( i=0 ; i < size+1 ; i++ )
		lsResult[i] = rhs[i] ;
	for( j=0 ; j < size ; j++ )
		for( i=0 ; i < j + 2 ; i++ )
			lsResult[i] -= ctxt.rhs[j] * mat[j][i] ;
	newNorm = vect_discrete_l2_norm( size+1, lsResult ) ;
	//printf( "Norm for new approximation:\t\t%.18lf\n", newNorm ) ;
	ctxt.rhs[k] -= M_EPS ;

	if( minNorm <= newNorm )
		printf( "New norm is larger than calculated least squares solution\n" );
	else
		printf( "Solution is not the least squares approximation!\n" ) ;
	
	
	//clean up
	free_least_squares_context( &ctxt ) ;
	free( rhs ) ;
	for( i=0 ; i < size ; i++ )
		free( mat[i] ) ;
	free( mat ) ;
}

/* Tests the factorial function to check that it returns the correct values
 */
void test_factorial( AlgorithmParameters *params )
{
	printf( "0!: %d\n", factorial( 0 ) ) ;
	printf( "1!: %d\n", factorial( 1 ) ) ;
	printf( "2!: %d\n", factorial( 2 ) ) ;
	printf( "3!: %d\n", factorial( 3 ) ) ;
	printf( "4!: %d\n", factorial( 4 ) ) ;
	printf( "5!: %d\n", factorial( 5 ) ) ;
	printf( "***\n" ) ;
	printf( "x^0: %.2lf\n", pow( rand_in_range(1,101), 0 ) ) ;
}

/* Prints out the type of boundary conditions that are found on each grid in 
 * a grid hierarchy specified in the parameters
 */
void test_grid_boundary_types( AlgorithmParameters *params )
{
	int gridCnt ; //Loop counters
	GridParameters grid ;
	int numGrids = 9 ; //The number of grids in the hierarchy
	const char **meshes ;
	int diff ;
	double minx, maxx, miny, maxy ;
	
	meshes = get_grid_hierarchy( params ) ;
	
	//Loop through all of the grids
	for( gridCnt = 0 ; gridCnt < numGrids ; gridCnt++ ){
		
		//Read in the grid
		init_grid( &grid, params, meshes[ gridCnt ] ) ;
		//We assume that the mesh we are working with is square
		minx = grid.nodes[0].x ;
		miny = grid.nodes[0].y ;
		maxx = grid.nodes[ grid.numNodes-1 ].x ;
		maxy = grid.nodes[ grid.numNodes-1 ].y ;
		diff = grid.numNodes - grid.numUnknowns ;
		
		printf( "Grid: %d\n\t", gridCnt ) ;
		printf( "Domain: (%.4lf, %.4lf) x (%.4lf, %.4lf)\n\t", minx, maxx, miny,
			maxy ) ;
		printf( "Number of Nodes: %d\n\t", grid.numNodes ) ;
		printf( "sqrt( numNodes ): %.1lf\n\t", sqrt( grid.numNodes ) ) ;
		printf( "Number of Unknowns: %d\n\t", grid.numUnknowns ) ;
		printf( "sqrt( numUnknowns ): %.1lf\n\t", sqrt( grid.numUnknowns ) ) ;
		//Now that I have the grid I want to see if we have any Dirichlet
		//boundary nodes
		printf( "Has Dirichlet Boundary: " ) ;
		if( diff == 0 )
			 printf( "No\n\t" ) ;
		else
			printf( "Yes\n\t" ) ;
			
		printf( "Has Neumann Boundary: " ) ;
		if( diff == 4 * ((int)sqrt(grid.numNodes) - 1) )
			printf( "No\n" ) ;
		else
			printf( "Yes\n" ) ;
		printf( "\n***\n\n" ) ;
		
		//Clean up the memory taken by the grid
		free_grid( &grid ) ;
	}
}

/* Calculates the exact and numerical derivatives for a given problem and
 * compares them
 */
void test_comp_exact_numeric_deriv( AlgorithmParameters *params )
{
	int i, j, k ; //Loop variables
	GridParameters grid ; //The grid to operate on
	ApproxVars aVars ; //Variables associated with the approximation on the
					   //current grid
	const char **meshes ; //The grid hierarchy to use
	double *nJac, *exJac ; //The numerical and the exact Jacobian
	const Node *node ; //A node on the grid
	
	//Get the hierarchy of grids to use
	meshes = get_grid_hierarchy( params ) ;
	
	//Set up the grid on the given mesh
	init_grid( &grid, params, meshes[ params->fGridLevel -1 ] ) ;
	
	//Assign the appropriate amount of memory for the numerical jacobian
	exJac = (double*)calloc( grid.numMatrixEntries, sizeof( double ) ) ;
	nJac = (double*)calloc( grid.numMatrixEntries, sizeof( double ) ) ;
	
	//Set up the approximation variables on the current grid
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Set up an initial approximation
	initialise_approx( params, &grid, aVars.approx ) ;
	
	//Now set up the iteration matrix
	calculate_iteration_matrix( &grid, params, aVars.approx, &aVars ) ;
	
	//Calculate the jacobian matrix using the discretization of the exact
	//derivative
	calculate_jacobian_matrix( &grid, &aVars, params, exJac, 0 ) ;
	
	//Calculate the jacobian matrix using numeric differentiation
	calculate_jacobian_matrix( &grid, &aVars, params, nJac, 1 ) ;
	
	//Loop through the rows, and print out each row of the matrices side by side
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		//Get the current node on the grid
		node = &(grid.nodes[ grid.mapA2G[ i ] ]) ;
		k = grid.rowStartInds[ i ] ;
		printf( "Row %d:\n\t", i ) ;
		//Print out the entries in the exact Jacobian
		for( j=0 ; j < node->numNghbrs-1 ; j++ ){
			printf( "%.18lf, ", exJac[k + j] ) ;
		}
		printf( "%.18lf\n\t", exJac[k+node->numNghbrs-1] ) ;
		//Print out the entries in the numerical Jacobian
		for( j=0 ; j < node->numNghbrs-1 ; j++ ){
			printf( "%.18lf, ", nJac[k + j] ) ;
		}
		printf( "%.18lf\n\n", nJac[k + j] ) ;
	}
	
	//Clean up
	free( nJac ) ;
	free( exJac ) ;
	free_approx_vars( &aVars, params ) ;
	free_grid( &grid ) ;
}


/* Prints out the progression of the exact solution for time dependent test case
 * 10
 */
void test_tc10_exact_sol_progression( AlgorithmParameters *params )
{
	const char **meshes ;
	GridParameters grid ;
	double *approx ;
	char outFName[100] ;
	int tCnt ;
	TDCtxt *tdCtxt ;
	
	params->testCase = 10 ;
	tdCtxt = &(params->tdCtxt) ;
	meshes = get_grid_hierarchy( params ) ;
	
	//set up the grid
	init_grid( &grid, params, meshes[ params->fGridLevel -1] ) ;
	
	approx = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	
	//Calculate the initial time
	tdCtxt->tStart = calculate_t_zero( params ) ;
	sprintf( outFName, "/tmp/tc10SolLvl%d.txt", params->fGridLevel ) ;
	
	//calculate the initial approximation
	initial_condition( params, &grid, approx ) ;
	//Print out the original function to file
	print_grid_func_no_bnd_to_file( outFName, &grid, approx, 0 ) ;
	
	for( tCnt=0 ; tCnt <= tdCtxt->numTSteps ; tCnt++ ){
		//Calculate the exact solution
		tdCtxt->currTime = tdCtxt->tStart + tCnt * tdCtxt->tStepSize ;
		printf( "(%d) Time = %.18lf\n", tCnt, tdCtxt->currTime ) ;
		calculate_exact_solution( params, &grid, approx ) ;
		
		//print out the function to file
		print_grid_func_no_bnd_to_file( outFName, &grid, approx, 1 ) ;
	}
	
	free( approx ) ;
	free_grid( &grid ) ;
}

/* Prints the exact solution for test case 10 at the final timestep to file
 */
void print_tc10_sol_at_timestep( AlgorithmParameters *params )
{
	const char **meshes ;
	GridParameters grid ;
	double *sol ;
	char outFName[100] ;
	TDCtxt *tdCtxt ;
	
	params->testCase = 10 ;
	tdCtxt = &(params->tdCtxt) ;
	meshes = get_grid_hierarchy( params ) ;
	
	//Set the filename to write output to
	sprintf( outFName, "/tmp/solTC10Lvl%d.txt", params->fGridLevel ) ;
	
	//Set up the grid
	init_grid( &grid, params, meshes[ params->fGridLevel -1] ) ;
	
	sol = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	tdCtxt->tStart = calculate_t_zero( params ) ;
	
	//Set the time to the end time
	tdCtxt->currTime = tdCtxt->tStart + tdCtxt->numTSteps * tdCtxt->tStepSize ;
	
	//Calculate the solution at the final time
	calculate_exact_solution( params, &grid, sol ) ;
	
	//Print the solution to file
	print_grid_func_no_bnd_to_file( outFName, &grid, sol, 0 ) ;
	free( sol ) ;
	free_grid( &grid ) ;
}

/* Compares the sizes of the nonsymmetric and symmetric parts of the derivative
 * of an operator, if the operator has a nonsymmetric part to the derivative
 */
void test_comp_symm_nonsymm_parts( AlgorithmParameters *params )
{
	const char** meshes ;
	int i, j, k ; //Loop counters
	GridParameters grid ;
	ApproxVars aVars ;
	double val ; //Stores the value of the approximation at a given point
	const Node *node ; //A node on the grid
	double *sPart ; //The symmetric part of the operator
	double *diff ; //The difference in the symmetric and full parts
	double *fullOp, *sOp, *nsOp ; //The result of applying the full, symmetric
								  //and non-symmetric parts of the jacobian,
								  //respectively
	char outFName[100] ;
	FILE *fout ;
	InfNorm max, min ;
	double avgRat ;
	
	//Check that we are using an appropriate test case
	if( params->testCase != 11 && params->testCase != 3 &&
		params->testCase != 7 ){
		printf( "Test function only set up for test cases 3, 7 and 11\n" ) ;
		printf( "Exiting test function\n" );
		return ;
	}
	
	//Find the appropriate grid hierarchy
	meshes = get_grid_hierarchy( params ) ;
	//Set up the grid
	init_grid( &grid, params, meshes[ params->fGridLevel-1] ) ;
	//Set up approximation variables on the given grid
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Initialise the approximation so that we can work out the iteration matrix,
	//and the symmetric part as well
	initialise_approx( params, &grid, aVars.approx ) ;
	
	//Calculate the Jacobian matrix
	calculate_jacobian_matrix( &grid, &aVars, params, aVars.iterMat, 0 ) ;
	
	//Calculate the symmetric part of the Jacobian matrix
	sPart = (double*)malloc( sizeof(double) * grid.numMatrixEntries ) ;
	calculate_jacobian_spart( &grid, &aVars, params, sPart, 0 ) ;
	
	//Calculate the difference in the two matrices
	diff = (double*)malloc( sizeof( double ) * grid.numMatrixEntries ) ;
	vect_diff( grid.numMatrixEntries, aVars.iterMat, sPart, diff ) ;
	
	//Apply the operator to the symmetric part, and to the full matrix and to
	//the difference (which should be the nonsymmetric part). First check that
	//the sum of the difference and the symmetric part add up to the full part,
	//and then see the size of each of the elements in the result of the vector
	//matrix multiplications
	fullOp = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	sOp = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	nsOp = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	//Apply the operator to the approximation
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		//Get the current node on the grid
		node = &(grid.nodes[ grid.mapA2G[ i ] ]) ;
		//Get the index into the sparse matrix representations of the operators
		//for the current row
		k = grid.rowStartInds[ i ] ;
		//Work out the values in the respective vectors on a row by row basis
		fullOp[i] = aVars.iterMat[ k ] * aVars.approx[i] ;
		sOp[i] = sPart[ k ] * aVars.approx[ i ] ;
		nsOp[i] = diff[ k ] * aVars.approx[ i ] ;
		for( j=1 ; j < node->numNghbrs ; j++ ){
			val = aVars.approx[ grid.mapG2A[ node->nghbrs[j] ] ] ;
			fullOp[i] += aVars.iterMat[ k + j ] * val ;
			sOp[i] += sPart[k + j] * val ;
			nsOp[i] += diff[k + j] * val ;
		}
	}
	
	//Now print everything out to file
	sprintf( outFName, "/tmp/compS-NS-TC%dLvl%d.txt", params->testCase,
		params->fGridLevel ) ;
	//Open the file for writing
	fout = fopen( outFName, "w" ) ;
	max.val = 0.0 ;
	min.val = atof( "Inf" ) ;
	max.ind = min.ind = 0 ;
	avgRat = 0.0 ;
	j = 0 ;
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		if( nsOp[i] != 0.0 ){
			val = fabs( sOp[i] / nsOp[i] ) ;
			avgRat += val ;
			j++ ;
			if( max.val < val ){
				max.val = val ;
				max.ind = i ;
			}
			if( min.val > val ){
				min.val = val ;
				min.ind = i ;
			}
		}
		fprintf( fout, "Symm: %.18lf, \tNon-Symm: %.18lf, \tRatio: %.18lf\n",
			sOp[i], nsOp[i], sOp[i]/nsOp[i] ) ;
	}
	avgRat /= j ;
	//Also print the function out to file so that we can see how the operator
	//differs for the different approximations that we use
	sprintf( outFName, "/tmp/testApproxLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_to_file( outFName, &grid, aVars.approx, 0 ) ;
	
	node = &(grid.nodes[ grid.mapA2G[ max.ind ] ]) ;
	printf( "Maximum Ratio: %.18lf at point (%.7lf, %.7lf)\n", max.val, node->x,
		node->y ) ;
	node = &(grid.nodes[ grid.mapA2G[ min.ind ] ]) ;
	printf( "Minimum Ratio: %.18lf at point (%.7lf, %.7lf)\n", min.val, node->x,
		node->y ) ;
	printf( "Ratio between maximum and minimum: %.18lf\n", max.val / min.val ) ;
	printf( "Average ratio: %.18lf\n", avgRat ) ;
	
	if( grid.numUnknowns < 5000 ){
		sprintf( outFName, "/tmp/testSymmOpLvl%d.txt", params->fGridLevel ) ;
		print_full_matrix_to_file( outFName, &grid, sPart ) ;
		sprintf( outFName, "/tmp/testNonSymmOpLvl%d.txt", params->fGridLevel ) ;
		print_full_matrix_to_file( outFName, &grid, diff ) ;
	}
	
	//Clean up
	fclose( fout ) ;
	free_grid( &grid ) ;
	free_approx_vars( &aVars, params ) ;
	free( sPart ) ;
	free( diff ) ;
	free( fullOp ) ;
	free( sOp ) ;
	free( nsOp ) ;
}

//Test that the hydraulic conducitivity is calculated correctly. We check this
//by plotting the calculated hydraulic conductivity vs pressure head
void test_hydr_cond_calc( AlgorithmParameters *params )
{
	int i ; //Loop counter
	int numPoints = 1000 ; //The number of points to use
	double minP = -20 ; //The minimum pressure to assign
	double stepSize ; //The step size between adjacent pressure points
	
	FILE *fout ; //File to write to
	
	double *pHead ;
	double *hydCond ;
	
	//Assign the appropriate amount of memory
	pHead = (double*) calloc( numPoints, sizeof( double ) ) ;
	hydCond = (double*) calloc( numPoints, sizeof( double ) ) ;
	
	//Calculate the step size
	stepSize = -minP / (numPoints -1) ;
	//Set the values for the pressure head
	for( i=0 ; i < numPoints ; i++ )
		pHead[i] = minP + i * stepSize ;
		
	//Print to console the values that are stored in the context for the
	//Richards equation
	for( i=0 ; i < params->reCtxt.numSoils ; i++ ){
		printf( "Richards Equation context:\n\t" ) ;
		printf( "Theta_{s}: %.5lf\n\t", params->reCtxt.thetaS[i] ) ;
		printf( "Theta_{r}: %.5lf\n\t", params->reCtxt.thetaR[i] ) ;
		printf( "Theta_{m}: %.5lf\n\t", params->reCtxt.thetaM[i] ) ;
		printf( "Saturated conductivity: %.5lf\n\t",
			params->reCtxt.satCond[i] ) ;
		printf( "n: %.5lf\n\t", params->reCtxt.n[i] ) ;
		printf( "m: %.5lf\n\t", params->reCtxt.m[i] ) ;
		printf( "Minimum pressure: %.5lf\n\t", params->reCtxt.hS[i] ) ;
		printf( "Alpha: %.5lf\n\t", params->reCtxt.alpha[i] ) ;
		printf( "Scaling factor for effective saturation: %.5lf\n\t",
			params->reCtxt.satScaleFact[i] ) ;
		printf( "Scaling factor for hydraulic conductivity: %.5lf\n",
			params->reCtxt.cHydrCond[i] ) ;
	}
		
	//Calculate the hydraulic conductivity
	for( i=0 ; i < numPoints ; i++ )
		hydCond[i] = calc_hydr_cond( &(params->reCtxt), 0, pHead[i] ) ;
	
	//Open the file for writing
	fout = fopen( "/tmp/test.txt", "w" ) ;
	
	//Write the values for the pressure head to file
	for( i=0 ; i < numPoints -1 ; i++ )
		fprintf( fout, "%.10lf, ", pHead[i] ) ;
	fprintf( fout, "%.10lf\n", pHead[numPoints-1] ) ;
	
	//Write the values for the hydraulic conductivity to file
	for( i=0 ; i < numPoints -1 ; i++ )
		fprintf( fout, "%.10lf, ", hydCond[i] ) ;
	fprintf( fout, "%.10lf\n", hydCond[numPoints-1] ) ;
	
	//Clean up
	fclose( fout ) ;
	free( pHead ) ;
	free( hydCond ) ;
}

//Tests whether the derivative of the hydraulic conductivity function with
//resepect to the pressure is calculated correctly or not. The different
//functions are printed out to file for further inspection
void test_hydr_cond_deriv( AlgorithmParameters *params )
{
	int i ; 
	int numPoints = 10000 ;
	double stepSize ;
	double minP = -10.0 ;
	
	FILE *fout ;
	
	double *pHead ; //The pressure head
	double *hCond ; //The hydraulic conductivity
	double *gradHCond ; //The derivative of the hydraulic conductivity with 
						//respect to the pressure
	
	//Set the appropriate memory
	pHead = (double*)calloc( numPoints, sizeof( double ) ) ;
	hCond = (double*)calloc( numPoints, sizeof( double ) ) ;
	gradHCond = (double*)calloc( numPoints, sizeof( double ) ) ;
	
	//Calculate the step size
	stepSize = -minP / (numPoints-1) ;
	//Set up the pressure
	for( i=0 ; i < numPoints ; i++ )
		pHead[i] = minP + i * stepSize ;
		
	//Print to console the values that are stored in the context for the
	//Richards equation
	for( i=0 ; i < params->reCtxt.numSoils ; i++ ){
		printf( "Richards Equation context:\n\t" ) ;
		printf( "Theta_{s}: %.5lf\n\t", params->reCtxt.thetaS[i] ) ;
		printf( "Theta_{r}: %.5lf\n\t", params->reCtxt.thetaR[i] ) ;
		printf( "Theta_{m}: %.5lf\n\t", params->reCtxt.thetaM[i] ) ;
		printf( "Saturated conductivity: %.5lf\n\t",
			params->reCtxt.satCond[i] ) ;
		printf( "n: %.5lf\n\t", params->reCtxt.n[i] ) ;
		printf( "m: %.5lf\n\t", params->reCtxt.m[i] ) ;
		printf( "Minimum pressure: %.5lf\n\t", params->reCtxt.hS[i] ) ;
		printf( "Alpha: %.5lf\n\t", params->reCtxt.alpha[i] ) ;
		printf( "Scaling factor for effective saturation: %.5lf\n\t",
			params->reCtxt.satScaleFact[i] ) ;
		printf( "Scaling factor for hydraulic conductivity: %.5lf\n",
			params->reCtxt.cHydrCond[i] ) ;
	}
		
	//Set the hydraulic conductivity
	for( i=0 ; i < numPoints ; i++ )
		hCond[i] = calc_hydr_cond( &(params->reCtxt), 0, pHead[i] ) /
			params->reCtxt.satCond[0] ;
	
	//Set the derivative of the hydraulic conductivity
	for( i=0 ; i < numPoints ; i++ )
		gradHCond[i] = calc_hydr_cond_deriv( &(params->reCtxt), 0, pHead[i] ) ;
	
	//Open the file for writing
	fout = fopen( "/tmp/testHCondD.txt", "w" ) ;
	
	for( i=0 ; i < numPoints ; i++ )
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i], hCond[i],
			gradHCond[i] ) ;
	
	//Clean up
	fclose( fout ) ;
	free( pHead ) ;
	free( hCond ) ;
	free( gradHCond ) ;
}

//Tests whether the theta in a Richards equation context is calculated properly
//as a function of the pressure head
void test_theta_calc( AlgorithmParameters *params )
{
	int i ; 
	int numPoints = 1000 ;
	double stepSize ;
	double minP = -20 ;
	
	FILE *fout ;
	
	double *pHead ; //The pressure head
	double *theta ; //The value of the volumetric water content
	
	//Assign the appropriate memory
	pHead = (double*)calloc( numPoints, sizeof( double ) ) ;
	theta = (double*)calloc( numPoints, sizeof( double ) ) ;
	
	//Calculate the step size
	stepSize = -minP / (numPoints-1) ;
	//set up the pressure head
	for( i=0 ; i < numPoints ; i++ )
		pHead[i] = minP + i * stepSize ;
		
	//Print to console the values that are stored in the context for the
	//Richards equation
	for( i=0 ; i < params->reCtxt.numSoils ; i++ ){
		printf( "Richards Equation context:\n\t" ) ;
		printf( "Theta_{s}: %.5lf\n\t", params->reCtxt.thetaS[i] ) ;
		printf( "Theta_{r}: %.5lf\n\t", params->reCtxt.thetaR[i] ) ;
		printf( "Theta_{m}: %.5lf\n\t", params->reCtxt.thetaM[i] ) ;
		printf( "Saturated conductivity: %.5lf\n\t",
			params->reCtxt.satCond[i] ) ;
		printf( "n: %.5lf\n\t", params->reCtxt.n[i] ) ;
		printf( "m: %.5lf\n\t", params->reCtxt.m[i] ) ;
		printf( "Minimum pressure: %.5lf\n\t", params->reCtxt.hS[i] ) ;
		printf( "Alpha: %.5lf\n\t", params->reCtxt.alpha[i] ) ;
		printf( "Scaling factor for effective saturation: %.5lf\n\t",
			params->reCtxt.satScaleFact[i] ) ;
		printf( "Scaling factor for hydraulic conductivity: %.5lf\n",
			params->reCtxt.cHydrCond[i] ) ;
	}
	
	//Calculate the volumetric water content
	for( i = 0 ; i < numPoints ; i++ )
		theta[i] = calc_theta( &(params->reCtxt), 0, pHead[i] ) ;
	
	//Open the file for writing
	fout = fopen( "/tmp/testTheta.txt", "w" ) ;
	
	//Print the pressure head to file
	for( i=0 ; i < numPoints -1 ; i++ )
		fprintf( fout, "%.10lf, ", pHead[i] ) ;
	fprintf( fout, "%.10lf\n", pHead[numPoints-1] ) ;
	
	//Print the volumetric water content to file
	for( i=0 ; i < numPoints -1 ; i++ )
		fprintf( fout, "%.10lf, ", theta[i] ) ;
	fprintf( fout, "%.10lf\n", theta[numPoints-1] ) ;
	
	//Clean up
	fclose( fout ) ;
	free( pHead ) ;
	free( theta ) ;
}

//Tests that the derivative of the volumetric wetness function with respect to
//the pressure head is calculated correctly. The different terms are printed
//out to file such that they can be examined further
void test_theta_deriv_calc( AlgorithmParameters *params )
{
	int i ;
	int numPoints = 10000 ;
	double stepSize ;
	double minP = -10 ;
//	int i ;
//	int numPoints ;
//	double stepSize = 1.01 ;
//	double minP = -1e-16 ;
//	double maxP = -1e3 ;
	
	FILE *fout ;
	
	double *pHead ; //The pressure in terms of the pressure head
	double *theta ; //The volumetric wetness
	double *gradTheta ; //The derivative of the volumetric wetness with respect
						//to the pressure
						
//	numPoints = (int)ceil( log10(maxP/minP) / log10( stepSize ) ) + 1 ;
//	printf( "Number of points: %d\n", numPoints ) ;
						
	//Assign the appropriate amount of memory
	pHead = (double*)calloc( numPoints, sizeof( double ) ) ;
	theta = (double*)calloc( numPoints, sizeof( double ) ) ;
	gradTheta = (double*)calloc( numPoints, sizeof( double ) ) ;
	
	//calculate the step size
	stepSize = -minP / (numPoints - 1) ;
//	pHead[0] = 0.0;
//	pHead[1] = minP ;
	//Set up the pressure
	for( i=0 ; i < numPoints ; i++ )
//		pHead[i] = pHead[i-1] * stepSize ;
		pHead[i] = minP + stepSize * i ;
		
	//Print to console the values that are stored in the context for the
	//Richards equation
	for( i=0 ; i < params->reCtxt.numSoils ; i++ ){
		printf( "Richards Equation context:\n\t" ) ;
		printf( "Theta_{s}: %.5lf\n\t", params->reCtxt.thetaS[i] ) ;
		printf( "Theta_{r}: %.5lf\n\t", params->reCtxt.thetaR[i] ) ;
		printf( "Theta_{m}: %.5lf\n\t", params->reCtxt.thetaM[i] ) ;
		printf( "Saturated conductivity: %.5lf\n\t",
			params->reCtxt.satCond[i] ) ;
		printf( "n: %.5lf\n\t", params->reCtxt.n[i] ) ;
		printf( "m: %.5lf\n\t", params->reCtxt.m[i] ) ;
		printf( "Minimum pressure: %.5lf\n\t", params->reCtxt.hS[i] ) ;
		printf( "Alpha: %.5lf\n\t", params->reCtxt.alpha[i] ) ;
		printf( "Scaling factor for effective saturation: %.5lf\n\t",
			params->reCtxt.satScaleFact[i] ) ;
		printf( "Scaling factor for hydraulic conductivity: %.5lf\n",
			params->reCtxt.cHydrCond[i] ) ;
	}
			
	//Calculate the volumetric water content
	for( i = 0 ; i < numPoints ; i++ )
		theta[i] = calc_theta( &(params->reCtxt), 0, pHead[i] ) ;
	
	//calculate the gradient of the volumetric water content
	for( i = 0 ; i < numPoints ; i++ )
		gradTheta[i] = calc_theta_deriv( &(params->reCtxt), 0, pHead[i] ) ;
	
	//Open the file for writing
	fout = fopen( "/tmp/testThetaD.txt", "w" ) ;
	
	//Write out the pressure head to file
	for( i=0 ; i < numPoints ; i++ ){
		fprintf( fout, "%.18lf, %.18lf, %.18lf\n", pHead[i],
			theta[i], gradTheta[i] ) ;
	}
	
	//Clean up
	fclose( fout ) ;
	free( pHead ) ;
	free( theta ) ;
	free( gradTheta ) ;
}

//Tests the smoothing of the nonlinear Richards equation. We only consider the
//case of nonlinear Jacobi smoothing for the time being
void test_smooth_richards_eq( AlgorithmParameters *params )
{	
	int smoothCnt ; //Loop counter
	int numSmooths = 2000 ; //The number of smoothing iterations to perform
	
	GridParameters grid ; //A grid on which to discretize
	const char **meshes ; //Points to the mesh hierarchy
	ApproxVars aVars ; //Variables associated with the approximation discretized
					   //on the current grid level
					   
	double residL2Norm ; //The L2 norm of the residual
	double prevResidL2Norm ; //The L2 norm of the residual at the previous
						 	 //approximation
	double origResidL2Norm ; //The norm in the original residual
					   
	double *eqRHS ; //Stores the value of the right hand side of the original
					//equation, without the contribution from the time
					//derivative
	Vector2D *flux ; //The flux field on the domain
					   
	char outFName[30] ; //File name to write output to
	
	//Make sure that we are performing the right test case
	if( !is_richards_eq( params->testCase ) ){
		printf( "Test case only works for Richards equation type problem\n" ) ;
		return ;
	}
	
	//Get the hierarchy of grid to work with
	meshes = get_grid_hierarchy( params ) ;
	
	//Set up the grid to be the fine grid level pointed to by the parameters
	init_grid( &grid, params, meshes[ params->fGridLevel - 1] ) ;
	
	//Check to see if the boundary conditions match
	if( !check_boundary_type( &grid, params ) ){
		free_grid( &grid ) ;
		free_re_ctxt_mem( &(params->reCtxt) ) ;
		printf( "Boundary type of grids read in do not match expected " ) ;
		printf( " boundary type\n" ) ;
		return ;
	}
	
	//Set the appropriate memory for the flux vector
	flux = (Vector2D*)calloc( grid.numNodes, sizeof( Vector2D ) ) ;
	
	//Now that the boundary conditions match I want to set up the approximation
	//variables on the grid level
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Set the file name to write to
	sprintf( outFName, "/tmp/testReSmoothLvl%d.txt", params->fGridLevel ) ;
	
	//Set the initial condition
	initial_condition( params, &grid, aVars.approx ) ;
	//Print the initial condition to file
	print_grid_func_to_file( outFName, &grid, aVars.approx, 0 ) ;
	
	//Set the appropriate right hand side. We have no right hand side data
	//except for the time derivative term. We still have to initialise the right
	//hand side, though, as this function is the only place at the moment where
	//I set the appropriate boundary conditions
	
	//Set the memory for the right hand side vector
	eqRHS = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	initialise_rhs( &grid, params, &aVars, params->tdCtxt.weightFact, eqRHS,
		aVars.bndTerm ) ;
		
	//Print out the right hand side and the boundary term to file
//	sprintf( outFName, "/tmp/testRHSOutLvl%d.txt", params->fGridLevel ) ;
//	print_grid_func_to_file( outFName, &grid, eqRHS, 0 ) ;
//	sprintf( outFName, "/tmp/testBndOutLvl%d.txt", params->fGridLevel ) ;
//	print_grid_func_to_file( outFName, &grid, aVars.bndTerm, 0 ) ;
//	sprintf( outFName, "/tmp/testReSmoothLvl%d.txt", params->fGridLevel ) ;
		
	//Make sure that for the moment we are using a backward Euler discretization
	//in time and that the smoothing method is a nonlinear Jacobi, as I have not
	//implemented any other methods for the time being
	params->tdCtxt.tDisc = TIME_DISC_BE ;
	params->smoothMethod = SMOOTH_JAC ;
	
	//Calculate the time dependent right hand side for the smoothing step
	calculate_td_rhs( params, eqRHS, &aVars, &grid, aVars.rhs ) ;
	
	calc_richards_resid( params, &grid, &aVars, aVars.approx, aVars.resid ) ;
	
	origResidL2Norm = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
	residL2Norm = prevResidL2Norm = origResidL2Norm ;
	
	//Print out the time dependent right hand side (i.e. the term that we are
	//using as the right hand side in this iteration) to file
//	sprintf( outFName, "/tmp/testTimeRHSOutLvl%d.txt", params->fGridLevel ) ;
//	print_grid_function_to_file( outFName, &grid, aVars.rhs, 0 ) ;
//	sprintf( outFName, "/tmp/testReSmoothLvl%d.txt", params->fGridLevel ) ;
	
	//Loop for the number of smooths that we want to perform
	for( smoothCnt = 0 ; smoothCnt < numSmooths &&
		residL2Norm / origResidL2Norm > 1e-6 ; smoothCnt++ ){
		prevResidL2Norm = residL2Norm ;
		
		if( !params->isSimplifiedNewt || smoothCnt == 0 ){
			//Update the approximation by performing a smoothing step
			re_smooth_nl_jacobi( &grid, params, 1, true, &aVars ) ;
		}
		else
			re_smooth_nl_jacobi( &grid, params, 1, false, &aVars ) ;
		
		//The residual is calculated in the smoothing method, and gives the
		//residual for the previous approximation. Taking the norm of the
		//residual here means that we avoid recalculation
		residL2Norm = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
		//Print out the residual norm to console
		printf( "Iteration: %d\tResidual norm: %.18lf\tRatio: %.18lf\n",
			smoothCnt, residL2Norm, residL2Norm / prevResidL2Norm ) ;
			
		//Write the approximation to file
		if( smoothCnt%10 == 0 )
			print_grid_func_to_file( outFName, &grid, aVars.approx, 1 ) ;
	}
	
	//Calculate the residual in approximation to get the residual for the most 
	//recent approximation
	calc_richards_resid( params, &grid, &aVars, aVars.approx, aVars.resid ) ;
	
	//Calculate the L2 norm of the residual and print it out to console
	residL2Norm = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
	printf( "Iteration: %d\tResidual norm: %.18lf\tRatio: %.18lf\n", smoothCnt,
		residL2Norm, residL2Norm / prevResidL2Norm ) ;
	
	//Calculate the flux
	set_flux( params, &grid, aVars.approx, aVars.reHCond, flux ) ;
	//Write out the vector field to file
	sprintf( outFName, "/tmp/testFluxOutLvl%d.txt", params->fGridLevel ) ;
	print_vector_field_to_file( outFName, &grid, flux, 0 ) ;
	
	//Print out the volumetric water content on the domain
	sprintf( outFName, "/tmp/testThetaOutLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_to_file( outFName, &grid, aVars.reTheta, 0 ) ;
	
	//Clean up
	free_approx_vars( &aVars, params ) ;
	free_grid( &grid ) ;
	free_re_ctxt_mem( &(params->reCtxt) ) ;
	free( eqRHS ) ;
	free( flux ) ;
}

//Tests that we can perform a time-stepping with the Richards equation, where
//we solve at each time step by smoothing only
void test_re_time_step_smooth( AlgorithmParameters *params )
{
	const char **meshes ; //Points to the filenames for the hierarchy of grids
	
	int numTSteps = 5 ; //The number of time steps to take
	int tCnt, smoothCnt ; //Loop counters
	
	char aFName[30], fFName[30], tFName[30] ; //Filenames of the files to write
											  //output to
	
	GridParameters grid ; //The grid on which to operate
	ApproxVars aVars ; //Variables associated with the approximation on the
					   //current grid level
	Vector2D *flux ; //Stores the flux for the current approximation
					   
	double *eqRHS ; //The right hand side of the equation, without any
					//contribution from the time derivative
	double currResid, prevResid, origResid ; //The current / previous / original
								 			 //residual in approximation,
								 			 //respectively
	double *prevApprox ; //The previous approximation
	
	//Make sure that we are performing the right test case
	if( params->testCase != 14 && params->testCase != 15 ){
		printf( "\tTest case only works for Richards equation type problem" ) ;
		printf( "\n\tPlease use test case 14 or 15\n" ) ;
		return ;
	}
	
	//Get the hierarchy of grid to work with
	meshes = get_grid_hierarchy( params ) ;
	
	//Set up the grid to be the fine grid level pointed to by the parameters
	init_grid( &grid, params, meshes[ params->fGridLevel - 1] ) ;
	
	//Check to see if the boundary conditions match
	if( !check_boundary_type( &grid, params ) ){
		free_grid( &grid ) ;
		printf( "Boundary type of grids read in do not match expected " ) ;
		printf( " boundary type\n" ) ;
		return ;
	}
	
	//Set the memory for the flux
	flux = (Vector2D*)calloc( grid.numUnknowns, sizeof( Vector2D ) ) ;
	
	//Now that the boundary conditions match I want to set up the approximation
	//variables on the grid level
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Set the file names to write to
	sprintf( aFName, "/tmp/testReTdPressureLvl%d.txt", params->fGridLevel ) ;
	sprintf( fFName, "/tmp/testReTdFluxLvl%d.txt", params->fGridLevel ) ;
	sprintf( tFName, "/tmp/testReTdThetaLvl%d.txt", params->fGridLevel ) ;
	
	//Set the initial condition
	initial_condition( params, &grid, aVars.approx ) ;
	//Print the initial condition to file
	print_grid_func_to_file( aFName, &grid, aVars.approx, 0 ) ;
	
	set_hydr_cond( params, &grid, aVars.approx, aVars.reHCond ) ;
	set_theta( params, &grid, aVars.approx, aVars.reTheta ) ;
	//Print the flux to file
	set_flux( params, &grid, aVars.approx, aVars.reHCond, flux ) ;
	print_vector_field_to_file( fFName, &grid, flux, 0 ) ;
	
	//Print the volumetric water content to file
	print_grid_func_to_file( tFName, &grid, aVars.reTheta, 0 ) ;

	//Set the appropriate right hand side. We have no right hand side data
	//except for the time derivative term. We still have to initialise the right
	//hand side, though, as this function is the only place at the moment where
	//I set the appropriate boundary conditions
	
	//Set the memory for the right hand side vector
	eqRHS = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	initialise_rhs( &grid, params, &aVars, params->tdCtxt.weightFact, eqRHS,
		aVars.bndTerm ) ;
		
	prevApprox = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
		
	//Make sure that for the moment we are using a backward Euler discretization
	//in time and that the smoothing method is a nonlinear Jacobi, as I have not
	//implemented any other methods for the time being
	params->smoothMethod = SMOOTH_JAC ;
	for( tCnt = 0 ; tCnt < numTSteps ; tCnt++ ){
//	while( tCnt == 0 || vect_diff_infty_norm( grid.numUnknowns, aVars.approx,
//		prevApprox ) > 1e-10 ){
		//Calculate the right hand side for the current time step
		calculate_td_rhs( params, eqRHS, &aVars, &grid, aVars.rhs ) ;
		
		//Calculate the residual
		calc_richards_resid( params, &grid, &aVars, aVars.approx,
			aVars.resid ) ;
		//Calculate the original residual norm
		origResid = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
		currResid = origResid ;
		
		//Copy the current approximation to the old approximation
		copy_vectors( grid.numUnknowns, prevApprox, aVars.approx ) ;
		
		//Now smooth
		smoothCnt = 0 ;
		while( currResid / origResid > 1e-7 && currResid > 1e-14 ){
			smoothCnt++ ;
			prevResid = currResid ;
			//Update the approximation by performing a smoothing step
			re_smooth_nl_jacobi( &grid, params, 1, true, &aVars ) ;
		
			//The residual is calculated in the smoothing method, and gives the
			//residual for the previous approximation. Taking the norm of the
			//residual here means that we avoid recalculation
			currResid = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
			
			printf( "Time step: %d\tIteration: %d\tResid Norm: %.18lf",
				tCnt, smoothCnt, currResid ) ;
			printf( "\tRatio: %.18lf\n", currResid / prevResid ) ;
		}
		
		//Print the approximation at the end of each time step
		print_grid_func_to_file( aFName, &grid, aVars.approx, 1 ) ;
		//Print the flux to file
		set_flux( params, &grid, aVars.approx, aVars.reHCond, flux ) ;
		print_vector_field_to_file( fFName, &grid, flux, 1 ) ;
		//Print the volumetric wetness to file
		print_grid_func_to_file( tFName, &grid, aVars.reTheta, 1 ) ;
	}
	
	//Clean up
	free( flux ) ;
	free( prevApprox ) ;
	free( eqRHS ) ;
	free_grid( &grid ) ;
	free_approx_vars( &aVars, params ) ;
}

//Test is solving a Newton step with a linear smoother works for the Richards
//equation
void test_re_newton_smooth( AlgorithmParameters *params )
{
	int i ; //Loop counter
	const char **meshes ; //The grid hiearchy
	GridParameters grid ; //The grid on which we are currently operating
	ApproxVars aVars ; //Variables associated with the approximation on the
					   //current grid
	double *eqRHS ; //The right hand side for the nonlinear system of equations
					//without contribution from the time derivative
	double *rhs ; //The right hand side of the nonlinear system of equations
				  //including a contribution from the time derivative
	double *approx ; //The approximation to the solution of the nonlinear system
					 //of equations

	double residL2Norm ; //The (discrete) L2 norm of the residual
	double origResidL2Norm ; //The original (discrete) L2 norm of the residual
	double prevResidL2Norm ; //The previous (discrete) L2 norm of the residual
	
	double lOrigResid ; //The original residual norm for the linear system of
						//equations
	double lResid ; //The residual norm for the linear system of equations
	double lPrevResid ; //The previous residual norm for the linear system of
						//equations
						
	int lItCnt ; //Iteration counter for the linear iterations
	int nItCnt ; //Iteration counter for the Newton iterations
	
	double *tmpRHS, *tmpApprox ; //Placeholders for the approximation and right
								 //hand side for the nonlinear system of
								 //equations such that the pointers can be
								 //swapped to point at something else
	Vector2D *flux ; //The flux given a pressure
								 
	char outFName[30] ; //Name of a file to write output to
	
	if( !is_richards_eq( params->testCase ) ){
		printf( "Test case only implemented for Richards equation type " ) ;
		printf( "problems\n" ) ;
		return ;
	}
	
	//Get the appropriate grid hiearchy
	meshes = get_grid_hierarchy( params ) ;
	
	//set up the current grid
	init_grid( &grid, params, meshes[ params->fGridLevel-1] ) ;
	
	//Check to see if the boundary conditions match
	if( !check_boundary_type( &grid, params ) ){
		free_grid( &grid ) ;
		free_re_ctxt_mem( &(params->reCtxt) ) ;
		printf( "Boundary type of grids read in do not match expected " ) ;
		printf( " boundary type\n" ) ;
		return ;
	}
	
	//Initialise the approximation variables
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Set up the right hand side
	eqRHS = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	rhs = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	approx = (double*)calloc( grid.numNodes, sizeof( double ) ) ;
	initialise_rhs( &grid, params, &aVars, params->tdCtxt.weightFact, eqRHS,
		aVars.bndTerm ) ;
		
	//Set up the initial approximation
	initial_condition( params, &grid, aVars.approx ) ;
	
	//Copy the values from the approximation variables object into the
	//approximation object
	copy_vectors( grid.numNodes, approx, aVars.approx ) ;
	
	//calculate the time dependent right hand side
	calculate_td_rhs( params, eqRHS, &aVars, &grid, rhs ) ;
	
	tmpRHS = aVars.rhs ;
	aVars.rhs = rhs ;
	
	//calculate the residual in approximation
	calc_richards_resid( params, &grid, &aVars, approx, aVars.resid ) ;
	aVars.rhs = tmpRHS ;
	//Calculate the discrete L2 norm of the residual in approximation
	origResidL2Norm = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
	residL2Norm = prevResidL2Norm = origResidL2Norm ;
	
	//Set the name of the file to print output to
	sprintf( outFName, "/tmp/testReNSmoothLvl%d.txt", params->fGridLevel ) ;
	//Print the original approximation to file
	print_grid_func_to_file( outFName, &grid, approx, 0 ) ;
	
	tmpApprox = aVars.approx ;
	
	//Set the approximation and the right hand side to point to the
	//approximation and right hand side of the nonlinear system of equations
	aVars.approx = approx ;
	aVars.rhs = rhs ;
	
	nItCnt = 0 ;
	while( residL2Norm / origResidL2Norm > 1e-6 && nItCnt < 20 ){
	
		//Calculate the Jacobian matrix
		calculate_jacobian_matrix( &grid, &aVars, params, aVars.iterMat, 0 ) ;
	
		aVars.rhs = tmpRHS ;
		aVars.approx = tmpApprox ;
		//Copy the residual into the right hand side of the approximation
		//variables
		copy_vectors( grid.numUnknowns, aVars.rhs, aVars.resid ) ;
		
		//Set the initial approximation to zero
		set_dvect_to_zero( grid.numNodes, aVars.approx ) ;
		
		//calculate the original residual in approximation for the linear
		//system of equations
		calculate_residual( params, &grid, &aVars ) ;
		//calculate the original norm in the residual for the linear system of
		//equations
		lOrigResid = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
		lResid = lPrevResid = lOrigResid ;
		
		lItCnt = 0 ;
		while( lResid / lOrigResid > 1e-6 ){
			lPrevResid = lResid ;
			//Smooth then calculate the residual
			smooth( &grid, params, 1, NULL, &aVars ) ;
			
			calculate_residual( params, &grid, &aVars ) ;
			lResid = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
			
			//Increase the count of the number of linear iterations performed
			lItCnt++ ;
			printf( "Iteration: %d\tResid Norm: %.18lf\tRatio: %.18lf\n",
				lItCnt, lResid, lResid / lPrevResid ) ;
		}
		
		//Update the approximation
		for( i=0 ; i < grid.numUnknowns ; i++ )
			approx[i] += params->newtonDamp * aVars.approx[i] ;
			
		//Calculate the residual in approximation
		aVars.rhs = rhs ;
		aVars.approx = approx ;
		calc_richards_resid( params, &grid, &aVars, aVars.approx,
			aVars.resid ) ;
		
		//Calculate the norm in the residual
		residL2Norm = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
		
		//Print out the approximation to file
		print_grid_func_to_file( outFName, &grid, approx, 1 ) ;
		
		//Increase the count for the number of Newton iterations performed
		nItCnt++ ;
	}
	
	//Now calculate the flux and print this to file
	flux = (Vector2D*)calloc( grid.numUnknowns, sizeof( Vector2D ) ) ;
	set_flux( params, &grid, approx, aVars.reHCond, flux ) ;
	
	sprintf( outFName, "/tmp/fluxOutLvl%d.txt", params->fGridLevel ) ;
	print_vector_field_to_file( outFName, &grid, flux, 0 ) ;
	
	//Clean up
	free( flux ) ;
	free( eqRHS ) ;
	free( tmpRHS ) ;
	free( tmpApprox ) ;
	free_approx_vars( &aVars, params ) ;
	free_grid( &grid ) ;
}

//Test if a Newton-Multigrid method works for the Richards equation
void test_re_newton_mg( AlgorithmParameters *params )
{
	int i ; //Loop counter
	
	const char **meshes ; //The mesh hierarchy to use
	
	GridParameters **grids ; //Stores the grids in the hierarchy
	ApproxVars **aVarsList ; //Stores variables associated with the
							 //approximation on the grids in the hierarchy
							 
	GridParameters *fGrid ; //A placeholder to the fine grid level
	ApproxVars *fVars ; //A placeholder to the approximation variables on the
						//fine grid level
						
	double *eqRHS ; //This is required to fit in with the code as I have it. It
					//is set to zero in the Richards equation, and contributes
					//to nothing
					
	TDCtxt *tdCtxt ; //Placeholder for the time dependent context stored in the
					 //algorithm parameters
					 
	Results results ; //Stores results from running the multigrid iteration
	Vector2D *flux ; //The flux for a given pressure
	
	char outFName[30] ; //Filename to write output to
	
	double residL2Norm ; //The L2 norm of the residual
							 
	//Check that we are performing the correct test case
	if( params->testCase != 14 && params->testCase != 15 ){
		printf( "Test case is only applicable to the Richards equation. " ) ;
		printf( "Please use test case 14 or 15 to perform this test case\n" ) ;
	}
	
	//At the moment we consider only performing linear multigrid, so set the
	//appropriate variable in the parameters object
	params->innerIt = IT_MULTIGRID ;
	
	//Inititalise the grids that we will need in this run to NULL pointers
	grids = (GridParameters**) malloc( params->fGridLevel *
		sizeof( GridParameters* ) ) ;
	initialise_grids_to_null( grids, params ) ;
	
	//Get the hierarchy of meshes for the current parameters
	meshes = get_grid_hierarchy( params ) ;
	
	//Initialise the approximation variables that we will need in this run to
	//NULL pointers
	aVarsList = (ApproxVars**) malloc( params->fGridLevel *
		sizeof( ApproxVars* ) ) ;
	initialise_approx_vars_to_null( aVarsList, params ) ;
	
	//Initialise the coarse grid level, and check that the boundary type matches
	//the expected boundary type
	grids[params->cGridLevel-2] = (GridParameters*)malloc(
		sizeof( GridParameters ) ) ;
	init_grid( grids[params->cGridLevel-2], params,
		meshes[params->cGridLevel-2] ) ;
	
	//Check that the boundary type of the grid that is read in matches the
	//expected boundary conditions for the test case
	if( !check_boundary_type( grids[params->cGridLevel-2], params ) ){
		printf( "Boundary of the mesh do not match expected boundary types " ) ;
		printf( "for the test case\n" ) ;
		//Clean up and exit if the boundary types do not match
		clean_up( grids, aVarsList, params ) ;
		return ;
	}
	
	//Initialise all of the grids
	for( i=params->cGridLevel-1 ; i < params->fGridLevel ; i++ ){
		if( grids[i] == NULL ){
			//Assign the appropriate memory
			grids[i] = (GridParameters*)malloc( sizeof( GridParameters ) ) ;
			//Set up the grid
			init_grid( grids[i], params, meshes[i] ) ;
		}
	}
	
	//Initialise the approximation variables on all of the grids
	for( i=params->cGridLevel-2 ; i < params->fGridLevel ; i++ ){
		if( aVarsList[i] == NULL ){
			//Assign the appropriate memory
			aVarsList[i] = (ApproxVars*) malloc( sizeof( ApproxVars ) ) ;
			init_approx_vars( aVarsList[i], grids[i], params ) ;
		}
	}
	
	//Set the placeholder to the fine grid level and to the approximation
	//variables on the fine grid level
	fGrid = grids[params->fGridLevel-1] ;
	fVars = aVarsList[params->fGridLevel-1] ;
	
	//Set up the initial condition
	initial_condition( params, fGrid, fVars->approx ) ;
	
	//Print the initial approximation to file
	sprintf( outFName, "/tmp/testRichNMGLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_to_file( outFName, fGrid, fVars->approx, 0 ) ;
	
	eqRHS = (double*)calloc( fGrid->numUnknowns, sizeof( double ) ) ;
	//Set up and store the original right hand side equation and the boundary
	//terms
	initialise_rhs( fGrid, params, fVars, 1.0, eqRHS, fVars->bndTerm ) ;
	
	//Print out the right hand side and the boundary terms to file
	sprintf( outFName, "/tmp/testRHSNMGLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_no_bnd_to_file( outFName, fGrid, eqRHS, 0 ) ;
	sprintf( outFName, "/tmp/bndTermNMGLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_no_bnd_to_file( outFName, fGrid, fVars->bndTerm, 0 ) ;
	sprintf( outFName, "/tmp/testRichNMGLvl%d.txt", params->fGridLevel ) ;
	
	//Make sure that we are performing the desired smoothing and time
	//discretization
	tdCtxt = &(params->tdCtxt) ;
	tdCtxt->tDisc = TIME_DISC_BE ;
	params->smoothMethod = SMOOTH_GS ;

	//We only test that the multigrid works for one time step, so we set up
	//the right hand side for the current approximation
	calculate_td_rhs( params, eqRHS, fVars, fGrid, fVars->rhs ) ;
	
	//Print the time dependent right hand side to file
	sprintf( outFName, "/tmp/tdRHSNMGLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_no_bnd_to_file( outFName, fGrid, fVars->rhs, 0 ) ;
	sprintf( outFName, "/tmp/testRichNMGLvl%d.txt", params->fGridLevel ) ;
	
	//calculate the residual in approximation
	calc_richards_resid( params, fGrid, fVars, fVars->approx, fVars->resid ) ;
	
	//Print the residual to file
	sprintf( outFName, "/tmp/residNMGLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_no_bnd_to_file( outFName, fGrid, fVars->resid, 0 ) ;
	sprintf( outFName, "/tmp/testRichNMGLvl%d.txt", params->fGridLevel ) ;
	
	//calculate the L2 norm of the residual
	residL2Norm = vect_discrete_l2_norm( fGrid->numUnknowns, fVars->resid ) ;
//	printf( "Residual norm before Newton-MG: %.18lf\n", residL2Norm ) ;
	
	//Perform Newton multigrid for the Richards equation
	richards_newton_mg( grids, aVarsList, params, &results ) ;
	
	//Calculate the residual in approximation
	calc_richards_resid( params, fGrid, fVars, fVars->approx, fVars->resid ) ;
	
	//Calculate the L2 norm of the residual
	residL2Norm = vect_discrete_l2_norm( fGrid->numUnknowns, fVars->resid ) ;
//	printf( "Residual norm after Newton-MG: %.18lf\n", residL2Norm ) ;
	
	//Print out the approximation to file
	print_grid_func_to_file( outFName, fGrid, fVars->approx, 1 ) ;
	
	//Now calculate the flux and print this to file
	flux = (Vector2D*)calloc( fGrid->numUnknowns, sizeof( Vector2D ) ) ;
	set_flux( params, fGrid, fVars->approx, fVars->reHCond, flux ) ;
	
	sprintf( outFName, "/tmp/fluxOutLvl%d.txt", params->fGridLevel ) ;
	print_vector_field_to_file( outFName, fGrid, flux, 0 ) ;
	
	//Clean up
	free( flux ) ;
	clean_up( grids, aVarsList, params ) ;
	free( eqRHS ) ;
}

//Tests that we get the right output from calculating the 'next' node on an
//element, in terms of the local element numbering
void test_next_node( AlgorithmParameters *params )
{
	int i ;
	//Don't actually need to use the parameters that are passed in
	
	for( i=0 ; i < 3 ; i++ )
		printf( "For node %d:\n\tNext node: %d\tPrevNode: %d\n", i,
			(i+1)%3, (i+2)%3 ) ;
}

//Tests that the type of soil for a test case that is read in is stored
//appropriately
void test_soil_type_allocation( AlgorithmParameters *params )
{
	int i, soilCnt ; //Loop counters
	const char **meshes ;
	GridParameters grid ; //Grid on which to operate
	RichardsEqCtxt *ctxt ; //Placeholder for the Richards equation context
	FILE *fout ; //File to print output to
	
	if( !is_richards_eq( params->testCase ) ){
		printf( "Test case only set up for Richards equation type problems\n") ;
		return ;
	}
	
	//Get the names of the filenames of the grids to read grid data from
	meshes = get_grid_hierarchy( params ) ;
	
	//Set up a grid
	init_grid( &grid, params, meshes[params->fGridLevel-1] ) ;
	
	ctxt = &(params->reCtxt) ;
	
	//Now we test if the correct number of soils have been set up by printing
	//to console information about each soil type
	for( soilCnt = 0 ; soilCnt < ctxt->numSoils ; soilCnt++ ){
		printf( "Soil type %d\n\t", soilCnt ) ;
		printf( "Residual water content: %.5lf\n\t", ctxt->thetaR[soilCnt] ) ;
		printf( "Saturated water content: %.5lf\n\t", ctxt->thetaS[soilCnt] ) ;
		printf( "Saturated conductivity: %.5lf\n\t", ctxt->satCond[soilCnt] ) ;
		printf( "n: %.5lf\n\t", ctxt->n[soilCnt] ) ;
		printf( "m: %.5lf\n\t", ctxt->m[soilCnt] ) ;
		printf( "Saturation pressure: %.5lf\n\t", ctxt->hS[soilCnt] ) ;
		printf( "Theta_m: %.5lf\n\t", ctxt->thetaM[soilCnt] ) ;
		printf( "Alpha: %.5lf\n\t", ctxt->alpha[soilCnt] ) ;
		printf( "Scaling factor for water content: %.5lf\n\t",
			ctxt->satScaleFact[soilCnt] ) ;
		printf( "Scaling factor for hydraulic conductivity: %.5lf\n\t",
			ctxt->cHydrCond[soilCnt] ) ;
		printf( "Flow in: %.5lf\n\t", ctxt->flowIn ) ;
		printf( "Pressure out: %.5lf\n", ctxt->hOut ) ;
	}
	
	//Now we want to print out the soil types to file. To do this we simply
	//print out for each grid node the value of the soil type
	fout = fopen( "/tmp/testSoilType.txt", "w" ) ;
	
	for( i=0 ; i < grid.numNodes ; i++ )
		fprintf( fout, "%.18lf, %.18lf, %d\n", grid.nodes[i].x, grid.nodes[i].y,
			grid.soilType[i] ) ;

	//Clean up
	fclose( fout ) ;
	free_grid( &grid ) ;
	free_re_ctxt_mem( ctxt ) ;
}

//Test that the volumetric wetness is set up appropriately in the presence of
//multiple soils
void test_theta_setup_multiple_soils( AlgorithmParameters *params )
{
	const char **meshes ; //Points to filenames of the grids in the hierarchy
	int i ; //Loop counter
	GridParameters grid ; //The grid on which to operate
	double *pHead ; //The pressure head
	double *theta ; //The volumetric water content
	char outFName[30] ; //Filename where to write output to
	int ind ;
	
	//Check that we are performing the correct test case
	if( params->testCase != 15 ){
		printf( "Test case is only set up for test case 15\n" ) ;
		return ;
	}
	
	meshes = get_grid_hierarchy( params ) ;
	
	//Initialise the grid
	init_grid( &grid, params, meshes[params->fGridLevel-1] ) ;
	
	if( !check_boundary_type( &grid, params ) ){
		//Clean up and exit if the boundary types do not match
		free_grid( &grid ) ;
		free_re_ctxt_mem( &(params->reCtxt) ) ;
		return ;
	}
	
	//Assign the appropriate amount of memory for the pressure and the
	//volumetric wetness
	pHead = (double*)calloc( grid.numNodes, sizeof( double ) ) ;
	theta = (double*)calloc( grid.numNodes, sizeof( double ) ) ;
	
	//Set the initial pressure
	initial_condition( params, &grid, pHead ) ;
	set_dirichlet_boundary( params, &grid, pHead ) ;
	
	//Calculate the value of theta on the Dirichlet boundary
	for( i=grid.numUnknowns ; i < grid.numNodes ; i++ ){
		ind = grid.mapA2G[ i ] ;
		theta[i] = calc_theta( &(params->reCtxt), grid.soilType[ind],
			pHead[i] ) ;
	}
	//Calculate the values of theta on the interior of the domain
	set_theta( params, &grid, pHead, theta ) ;
	
	//Print out the value of theta to file
	sprintf( outFName, "/tmp/testThetaMultSoilsLvl%d.txt",
		params->fGridLevel ) ;
	print_grid_func_to_file( outFName, &grid, theta, 0 ) ;
	
	//Print out the value of the approximation to file
	sprintf( outFName, "/tmp/testApproxLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_to_file( outFName, &grid, pHead, 0 ) ;
	
	//Clean up
	free_grid( &grid ) ;
	free_re_ctxt_mem( &(params->reCtxt) ) ;
	free( pHead ) ;
	free( theta ) ;
}

//Test that the hydraulic conductivity is set up correctly for a problem of
//Richards equation type with multiple soil types
void test_hydr_cond_setup_mult_soils( AlgorithmParameters *params )
{
	const char **meshes ; //Points to filenames of the grids in the hierarchy
	int i ; //Loop counter
	GridParameters grid ; //The grid on which to operate
	double *pHead ; //The pressure head
	double *hCond ; //The volumetric water content
	char outFName[30] ; //Filename where to write output to
	int ind ;
	
	//Check that we are performing the correct test case
	if( params->testCase != 15 ){
		printf( "Test case is only set up for test case 15\n" ) ;
		return ;
	}
	
	meshes = get_grid_hierarchy( params ) ;
	
	//Initialise the grid
	init_grid( &grid, params, meshes[params->fGridLevel-1] ) ;
	
	if( !check_boundary_type( &grid, params ) ){
		//Clean up and exit if the boundary types do not match
		free_grid( &grid ) ;
		free_re_ctxt_mem( &(params->reCtxt) ) ;
		return ;
	}
	
	//Assign the appropriate amount of memory for the pressure and the
	//volumetric wetness
	pHead = (double*)calloc( grid.numNodes, sizeof( double ) ) ;
	hCond = (double*)calloc( grid.numNodes, sizeof( double ) ) ;
	
	//Set the initial pressure
	initial_condition( params, &grid, pHead ) ;
	set_dirichlet_boundary( params, &grid, pHead ) ;
	
	//Calculate the value of theta on the Dirichlet boundary
	for( i=grid.numUnknowns ; i < grid.numNodes ; i++ ){
		ind = grid.mapA2G[ i ] ;
		hCond[i] = calc_hydr_cond( &(params->reCtxt), grid.soilType[ind],
			pHead[i] ) ;
	}
	//Calculate the values of theta on the interior of the domain
	set_hydr_cond( params, &grid, pHead, hCond ) ;
	
	//Print out the value of theta to file
	sprintf( outFName, "/tmp/testHCondMultSoilsLvl%d.txt",
		params->fGridLevel ) ;
	print_grid_func_to_file( outFName, &grid, hCond, 0 ) ;
	
	//Print out the value of the approximation to file
	sprintf( outFName, "/tmp/testApproxLvl%d.txt", params->fGridLevel ) ;
	print_grid_func_to_file( outFName, &grid, pHead, 0 ) ;
	
	//Clean up
	free_grid( &grid ) ;
	free_re_ctxt_mem( &(params->reCtxt) ) ;
	free( pHead ) ;
	free( hCond ) ;
}

//Tests the solution of the Richards equation on a 2D grid with only one column
//of unknowns. There is only flow due to gravity. This is included to test
//against what is contained in Vogel et al, 2001
void test_1d_richards_smooth( AlgorithmParameters *params )
{
	int i, tCnt, smoothCnt ; //Loop counters
	int ind ; //Index into the approximation for a node on the grid
	GridParameters grid ; //The grid on which we are operating
	const Node *node ; //A node on the grid
	ApproxVars aVars ; //Variables associated with the approximation on the
					   //current grid
	RichardsEqCtxt *ctxt ; //Context containing information about how the
						   //Richards equation should be solved
	TDCtxt *tdCtxt ; //Context containing information about how to execute
					 //the time dependent problem
						   
	double *eqRHS ; //The right hand side for the non time dependent right hand
					//side
	double *oldApprox ; //The old approximation to the time dependent problem
					
	double currResid, prevResid, origResid ; //The L2 norm of the residual
	double maxDiff ; //The difference in the new and old approximation
	Vector2D *flux ; //The flux in the approximation
	FILE *pOut, *fOut ; //Files to print out the pressure and flux to
	
	//The following variables are used for debugging purposes. They can be
	//removed if you are not sure what they do, as they have not impact on the
	//running of this test case
	FILE *debugOut ;
	int coordinates[5] = {1,4,7,10,0} ;

	//Get a placeholder to the Richards equation and time dependent contexts in
	//the parameters object
	ctxt = &(params->reCtxt) ;
	tdCtxt = &(params->tdCtxt) ;
	
	//Set the test case to point to one for testing purposes
	params->testCase = 101 ;
	//I am going to be solving using smoothing iterations, so make sure that
	//I am using a NLMG method with Jacobi smoothing
	params->nlMethod = METHOD_NLMG ;
	params->smoothMethod = SMOOTH_JAC ;
	
	//Set the appropriate variables in the Richards equation context
	free_re_ctxt_mem( ctxt ) ;
	ctxt->numSoils = 1 ;
	assign_re_ctxt_mem( ctxt ) ;
	ctxt->thetaR[0] = 0.068 ;
	ctxt->thetaS[0] = 0.380 ;
	ctxt->alpha[0] = 0.008 ;
	ctxt->n[0] = 1.09 ;
	ctxt->satCond[0] = 4.80 ;
	ctxt->hS[0] = -2.0 ;
	set_up_re_ctxt( ctxt ) ;
	
	//Set up the grid
	init_grid( &grid, params, "./Mesh/Fortran/SymmetricMesh/symm256by2.out" ) ;
	//Set up the approximation variables
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Set up the initial approximation. For this I am guessing that the pressure
	//is initially about -1200, just from looking at the paper by Vogel et al,
	//2001
	for( i=0 ; i < grid.numUnknowns ; i++ )
		aVars.approx[i] = -1200.0 ;
		
	//Set the dirichlet boundary to be zero
	for( i=grid.numUnknowns ; i < grid.numNodes ; i++ )
		aVars.approx[i] = 0.0 ;
		
	//Set the memory to store the old approximation
	oldApprox = (double*)malloc( grid.numUnknowns * sizeof( double ) ) ;
	//Set the memory to store the flux of the approximation
	flux = (Vector2D*)malloc( grid.numUnknowns * sizeof( Vector2D ) ) ;
		
	//Now set up the right hand side. As we are performing a Richards equation
	//I know that both the right hand side and the boundary values are zero
	eqRHS = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	set_dvect_to_zero( grid.numUnknowns, aVars.bndTerm ) ;
	
	pOut = fopen( "/tmp/pressure1d256_hs2.txt", "w" ) ;
	fOut = fopen( "/tmp/flux1d256_hs2.txt", "w" ) ;
	
	//Set the hydraulic conductivity and the flux for the current approximation
	set_hydr_cond( params, &grid, aVars.approx, aVars.reHCond ) ;
	set_flux( params, &grid, aVars.approx, aVars.reHCond, flux ) ;
	
	debugOut = fopen( "/tmp/debugOut.txt", "w" ) ;
	fprintf( debugOut, "Number of nodes on the grid: %d\n", grid.numNodes ) ;
	coordinates[4] = grid.numNodes-2 ;
	for( i=0 ; i < 5 ; i++ ){
		fprintf( debugOut, "Node with index %d:\n\tValue: %.18lf\n\t",
			coordinates[i],
			aVars.approx[ grid.mapG2A[ grid.nodes[ coordinates[i] ].id ] ] ) ;
		fprintf( debugOut, "X-Coordinate: %.18lf\n\tZ-Coordinate: %.18lf\n",
			grid.nodes[ coordinates[i] ].x, grid.nodes[ coordinates[i] ].y ) ;
	}
	fclose( debugOut ) ;
	
	//Print out the approximation at the Dirichlet boundary, where there is no
	//data for the flux
	fprintf( pOut, "%.18lf, %.18lf\n", grid.nodes[1].y,
		aVars.approx[ grid.mapG2A[ 1 ] ] ) ;
	//Print out the approximation and the flux to file
	for( i=4 ; i < grid.numNodes ; i+=3 ){
		//Get a placeholder to the current node on the grid
		node = &(grid.nodes[ i ]) ;
		//Get the index into the approximation for the current node
		ind = grid.mapG2A[ i ] ;
		//Print the value of the pressure to file, including the
		//z-coordinate of the node
		fprintf( pOut, "%.18lf, %.18lf\n", node->y, aVars.approx[ ind ] ) ;
		//If we are not at a Dirichlet boundary print out the z component
		//of the flux to file
		fprintf( fOut, "%.18lf, %.18lf\n", node->y, flux[ind].y ) ;
	}
	
	//Now I set up the time stepping
	tCnt = 0 ;
	maxDiff = 1.0 ;
	tdCtxt->currTime = tdCtxt->tStart ;
	while( maxDiff > 1e-10 && tCnt < tdCtxt->numTSteps ){
		//Copy the current approximation into the old approximation
		copy_vectors( grid.numUnknowns, oldApprox, aVars.approx ) ;
		//Set up the time dependent right hand side
		calculate_td_rhs( params, eqRHS, &aVars, &grid, aVars.rhs ) ;
		
		//Calculate the residual in the approximation for the system of
		//nonlinear equations
		calc_richards_resid( params, &grid, &aVars, aVars.approx,
			aVars.resid ) ;
			
		//Calculate the residual at the current time step
		origResid = vect_discrete_l2_norm( grid.numUnknowns,
			aVars.resid ) ;
		currResid = prevResid = origResid ;
		//Print out the residual at the current time step
		printf( "(%d) L2-norm of the residual at time %.5lf: %.18lf\n", tCnt,
			tdCtxt->currTime, currResid ) ;
		
		smoothCnt = 0 ;
		while( currResid / origResid > 1e-7 && currResid > 1e-14 ){
			smoothCnt++ ;
			prevResid = currResid ;
			//Update the approximation by performing a smoothing step
			re_smooth_nl_jacobi( &grid, params, 1, true, &aVars ) ;
		
			//The residual is calculated in the smoothing method, and gives the
			//residual for the previous approximation. Taking the norm of the
			//residual here means that we avoid recalculation
			currResid = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
			
			printf( "Time step: %d\tIteration: %d\tResid Norm: %.18lf",
				tCnt, smoothCnt, currResid ) ;
			printf( "\tRatio: %.18lf\n", currResid / prevResid ) ;
		}
		
		//Calculate the flux for the new approximation
		set_hydr_cond( params, &grid, aVars.approx, aVars.reHCond ) ;
		set_flux( params, &grid, aVars.approx, aVars.reHCond, flux ) ;
		
		//Now I want to print the pressure and the flux out to file, but only
		//on the centre line. I assume that the grid has only 3 nodes per row,
		//such that I only need to print out every third point
		fprintf( pOut, "-2.0, -2.0\n" ) ;
		fprintf( fOut, "-2.0, -2.0\n" ) ;
		fprintf( pOut, "%.18lf, %.18lf\n", grid.nodes[1].y,
			aVars.approx[ grid.mapG2A[ 1 ] ] ) ;
		for( i=4 ; i < grid.numNodes ; i+=3 ){
			//Get a placeholder to the current node on the grid
			node = &(grid.nodes[ i ]) ;
			//Get the index into the approximation for the current node
			ind = grid.mapG2A[ i ] ;
			//Print the value of the pressure to file, including the
			//z-coordinate of the node
			fprintf( pOut, "%.18lf, %.18lf\n", node->y, aVars.approx[ ind ] ) ;
			//If we are not at a Dirichlet boundary print out the z component
			//of the flux to file
			fprintf( fOut, "%.18lf, %.18lf\n", node->y, flux[ind].y ) ;
		}
		
		//Increase the counter of the current timestep
		tCnt++ ;
		tdCtxt->currTime = tdCtxt->tStart + tCnt * tdCtxt->tStepSize ;
		
		maxDiff = vect_diff_infty_norm( grid.numUnknowns, aVars.approx,
			oldApprox ) ;
	}
	
	//Clean up
	fclose( pOut ) ;
	fclose( fOut ) ;
	free_grid( &grid ) ;
	free_approx_vars( &aVars, params ) ;
	free_re_ctxt_mem( ctxt ) ;
	free( eqRHS ) ;
	free( oldApprox ) ;
	free( flux ) ;
}

//Tests the solution of the Richards equation on a 2D grid with only one column
//of unknowns. There is only flow due to gravity. This is included to test
//against what is contained in Vogel et al, 2001. We solve using a Newton
//iteration with repeated smooths to compare against the case of nonlinear
//smoothing
void test_1d_richards_newton_smooth( AlgorithmParameters *params )
{
	int i, tCnt, smoothCnt, newtCnt ; //Loop counters
	GridParameters grid ; //The grid on which to operate
	ApproxVars aVars ; //Variables associated with the approximation on the
					   //current grid
	TDCtxt *tdCtxt ; //Placeholder for the time dependent context in the
					 //parameters structure
	RichardsEqCtxt *ctxt ; //Placeholder for the richards equation context in
						   //the parameters structure
						   
	double *eqRHS ; //The right hand side of the nonlinear system of equations
	double *approx ; //The previous approximation to the nonlinear system
					 //of equations
	double *swapApprox ; //Used to swap pointers
	double *tdRHS ; //The nonlinear right hand side at a given time step
	double *tdApprox ; //The approximation at a given time step
	
	double nlResid, prevNLResid, origNLResid ; //The current and old residual
								  //and for the nonlinear system of equations
								  //at each time step
	double linResid, prevLinResid, origLinResid ; //The residual, previous
								//residual and original residual for the linear
								//system of equations per time step
								
	double maxDiff ; //Stores the value of the infinity norm for different
					 //vectors
	double tmp ; //Used to store intermediate values in calculations. When used
				 //in the code it should be directly obvious what this is doing
				 //else it should be specified in a comment
	InfNorm infNorm ; //Stores the infinity norm of a vector
	Vector2D *flux ; //The flux for a pressure profile
	
	int ind ; //An index of a node into the approximation
	
	FILE *pOut, *fOut ; //Files to print the pressure and flux out to
				 						   
	//Set a placeholder for the time dependent and Richards equation contexts
	tdCtxt = &(params->tdCtxt) ;
	ctxt = &(params->reCtxt) ;
	
	//Set the test case to point to one for testing purposes
	if( params->testCase != 101 && params->testCase != 102 ){
		printf( "Please use test cases 101 or 102 for this test case\n" ) ;
		return ;
	}
	//I am going to be using a Newton iteration, and want to use linear
	//Gauss-Seidel iterations, so make sure that these are set up properly
	params->nlMethod = METHOD_NEWTON_MG ;
	params->smoothMethod = SMOOTH_GS ;
	
	//Set the appropriate variables in the Richards equation context
	free_re_ctxt_mem( ctxt ) ;
	ctxt->numSoils = 1 ;
	assign_re_ctxt_mem( ctxt ) ;
	ctxt->thetaR[0] = 0.068 ;
	ctxt->thetaS[0] = 0.380 ;
	ctxt->alpha[0] = 0.008 ;
	ctxt->n[0] = 1.09 ;
	ctxt->satCond[0] = 4.80 ;
	ctxt->hS[0] = -2.0 ;
	set_up_re_ctxt( ctxt ) ;
	
	//Set up the grid
	if( params->testCase == 101 )
		init_grid( &grid, params,
			"./Mesh/Fortran/SymmetricMesh/NDNN/symm512by2.out" ) ;
	else if( params->testCase == 102 )
		init_grid( &grid, params,
			"./Mesh/Fortran/SymmetricMesh/NNND/symm512by2.out" ) ;
			
	//Set up the approximation variables
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Set the initial approximation at the unknown points
	for( i=0 ; i < grid.numUnknowns ; i++ ){
		aVars.approx[i] = -1000.0 ;
	}
	
	//Set the values at the Dirichlet boundary nodes
	set_dirichlet_boundary( params, &grid, aVars.approx ) ;
	
	//Set the memory for the previous nonlinear approximation
	approx = (double*)malloc( grid.numNodes * sizeof( double ) ) ;
	
	//Set up the right hand side, and the boundary contribution. Both of these
	//are zero
	eqRHS = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	set_dvect_to_zero( grid.numUnknowns, aVars.bndTerm ) ;
	
	//Set the memory to store the right hand side for each time step
	tdRHS = (double*)calloc( grid.numUnknowns, sizeof( double ) ) ;
	tdApprox = (double*)calloc( grid.numNodes,sizeof( double ) ) ;
	
	//Set the memory to store the flux
	flux = (Vector2D*)malloc( grid.numUnknowns * sizeof( Vector2D ) ) ;
	//Calculate the hydraulic conductivity so that we can calculate the flux
	set_hydr_cond( params, &grid, aVars.approx, aVars.reHCond ) ;
	set_flux( params, &grid, aVars.approx, aVars.reHCond, flux ) ;
	
	//Open the files to write the pressure and the flux to
	pOut = fopen( "/tmp/pressure5121dNewt_hs2_50_1.txt", "w" ) ;
	fOut = fopen( "/tmp/flux5121dNewt_hs2_50_1.txt", "w" ) ;
	
	//Print out the pressure and flux to file
	for( i=1 ; i < grid.numNodes ; i+=3 ){
		ind = grid.mapG2A[ i ] ;
		fprintf( pOut, "%.18lf, %.18lf\n", grid.nodes[i].y,
			aVars.approx[ ind ] ) ;
			
		if( ind <= grid.numUnknowns )
			fprintf( fOut, "%.18lf, %.18lf\n",
				grid.nodes[i].y, flux[ grid.mapG2A[ i ] ].y ) ;
	}
	
	//Initialise an ncurses display
	initscr() ;
	curs_set( 0 ) ;
	scrollok( stdscr, TRUE ) ;
	
	//Perform time stepping
	tdCtxt->currTime = tdCtxt->tStart ;
	tCnt = 0 ;
	maxDiff = 1.0 ;
	while( tCnt < tdCtxt->numTSteps && maxDiff > 1e-10 ){
		//Calculate the time dependent right hand side
		calculate_td_rhs( params, eqRHS, &aVars, &grid, aVars.rhs ) ;
		
		//Copy the time dependent right hand side into a temporary variable
		copy_vectors( grid.numUnknowns, tdRHS, aVars.rhs ) ;
		copy_vectors( grid.numNodes, tdApprox, aVars.approx ) ;
		
		//calculate the residual in approximation
		calc_richards_resid( params, &grid, &aVars, aVars.approx,
			aVars.resid ) ;

		//Calculate the residual norm
		origNLResid = vect_discrete_l2_norm( grid.numUnknowns, aVars.resid ) ;
		prevNLResid = nlResid = origNLResid ;
		
		//Loop for the Newton iteration
		newtCnt = 0 ;
		while( nlResid / origNLResid > 1e-3 && maxDiff > 1e-10 &&
			newtCnt < params->maxIts ){
			prevNLResid = nlResid ;
			
			//Copy the current approximation into the previous approximation
			copy_vectors( grid.numNodes, approx, aVars.approx ) ;
			
			//Calculate the jacobian matrix
			calculate_jacobian_matrix( &grid, &aVars, params, aVars.iterMat,
				0 ) ;
		
			//Now we want to set a zero approximation for the linear system of
			//equations
			set_dvect_to_zero( grid.numNodes, aVars.approx ) ;
		
			//Copy the residual into the right hand side
			copy_vectors( grid.numUnknowns, aVars.rhs, aVars.resid ) ;
		
			//Calculate the residual in the linear system of equations
			calculate_residual( params, &grid, &aVars ) ;
		
			//Calculate the norm of the residual
			origLinResid = vect_discrete_l2_norm( grid.numUnknowns,
				aVars.resid ) ;
			linResid = prevLinResid = origLinResid ;
		
			//Approximate the linear system using a Gauss-Seidel iteration
			maxDiff = 1.0 ;
			smoothCnt = 0 ;
			while( linResid / origLinResid > 1e-7 && maxDiff > 1e-10 ){
				maxDiff = 0.0 ;
				prevLinResid = linResid ;
			
				//Loop over all of the unknowns
				for( i=0 ; i < grid.numUnknowns ; i++ ){
					aVars.tmpApprox[i] = aVars.resid[i] /
						aVars.iterMat[ grid.rowStartInds[ i ] ] ;
					tmp = fabs( aVars.tmpApprox[i] ) ;
					maxDiff = (maxDiff > tmp) ? maxDiff : tmp ;
					aVars.approx[i] += params->smoothWeight *
						aVars.tmpApprox[i] ;
				}
				maxDiff *= params->smoothWeight ;
			
				//Calculate the residual in the linear system
				calculate_residual( params, &grid, &aVars ) ;
			
				//Calculate the residual norm
				linResid = vect_discrete_l2_norm( grid.numUnknowns,
					aVars.resid ) ;
			
				//Increase the smooth counter
				smoothCnt++ ;
				
//				printf( "\rTime Step: %d   Newton Iteration: %d   ", tCnt,
//					newtCnt ) ;
//				printf( "Iteration: %d   Resid Norm: %.18lf   ", smoothCnt,
//					linResid ) ;
//				printf( "Ratio: %.18lf", linResid / prevLinResid ) ;
			
				printw( "\rTime Step: %d   Newton Iteration: %d   ", tCnt,
					newtCnt ) ;
				printw( "Iteration: %d   Resid Norm: %.18lf   ", smoothCnt,
					linResid ) ;
				printw( "Ratio: %.18lf", linResid / prevLinResid ) ;
				refresh() ;
			}
		
			//Calculate the maximum correction
			vect_infty_norm( grid.numUnknowns, aVars.approx, &infNorm ) ;
			maxDiff = infNorm.val ;
		
			//Set the right hand side to point to the nonlinear right hand side
			copy_vectors( grid.numUnknowns, aVars.rhs, tdRHS ) ;
		
			if( params->adaptUpdate ){
				//Try and take a full Newton-Step
				params->newtonDamp = 1.0 ;
			
				//Update the approximation
				for( i=0 ; i < grid.numUnknowns ; i++ )
					approx[i] += params->newtonDamp * aVars.approx[i] ;
				
				//calculate the residual and the norm in the residual
				calc_richards_resid( params, &grid, &aVars, approx,
					aVars.resid ) ;
				nlResid = vect_discrete_l2_norm( grid.numUnknowns,
					aVars.resid ) ;
				
				//If we have not had a decrease in the residual norm decrease
				//the size of the step
				while( nlResid > prevNLResid &&
					params->newtonDamp > params->minNewtFact ){
					//Halve the Newton damping parameter
					params->newtonDamp /= 2.0 ;
					//Update the approximation
					for( i=0 ; i < grid.numUnknowns ; i++ )
						approx[i] -= params->newtonDamp * aVars.approx[i] ;
					//Calculate the residual and the norm in residual
					calc_richards_resid( params, &grid, &aVars, approx,
						aVars.resid ) ;
					nlResid = vect_discrete_l2_norm( grid.numUnknowns,
						aVars.resid ) ;
				}
			}
			else{
				//Update the approximation using the correction. We don't do
				//anything fancy here, just use the damping parameter to update
				//the approximation
				for( i=0 ; i < grid.numUnknowns ; i++ )
					approx[i] += params->newtonDamp * aVars.approx[i] ;
		
				//Calculate the residual for the nonlinear problem
				calc_richards_resid( params, &grid, &aVars, approx,
					aVars.resid ) ;
				//Calculate the current residual norm for the nonlinear problem
				nlResid = vect_discrete_l2_norm( grid.numUnknowns,
					aVars.resid ) ;
			}
		
			maxDiff *= params->newtonDamp ;
		
			//Swap the pointers for the nonlinear and linear approximations
			swapApprox = aVars.approx ;
			aVars.approx = approx ;
			approx = swapApprox ;
			
			//Increase the counter for the Newton step
			newtCnt++ ;
		}
		
		//Calculate the flux. We do not need to recalculate the hydraulic
		//conductivity, as this is done when calculating the residual in the
		//Richards equation
		set_flux( params, &grid, aVars.approx, aVars.reHCond, flux ) ;
		//Print out the pressure and flux to file
		fprintf( pOut, "-2.0, -2.0\n" ) ;
		fprintf( fOut, "-2.0, -2.0\n" ) ;
		for( i=1 ; i < grid.numNodes ; i+=3 ){
			ind = grid.mapG2A[ i ] ;
			fprintf( pOut, "%.18lf, %.18lf\n", grid.nodes[i].y,
				aVars.approx[ ind ] ) ;
			
			if( ind <= grid.numUnknowns )
				fprintf( fOut, "%.18lf, %.18lf\n",
					grid.nodes[i].y, flux[ grid.mapG2A[ i ] ].y ) ;
		}
		
		//Get the difference between the previous time step and this time step
		maxDiff = vect_diff_infty_norm( grid.numUnknowns, tdApprox,
			aVars.approx ) ;
			
		//Increment the time step
		tCnt++ ;
		
		//Print out appropriate data to console
//		printf( "\rTime step: %d   NL Residual Norm: %.18lf\n", tCnt,
//			nlResid ) ;

		printw( "\rTime step: %d   NL Residual Norm: %.18lf\n", tCnt,
			nlResid ) ;
		refresh() ;
	}
	
	//close the ncurses display
	curs_set(1) ;
	endwin() ;
	
	//Clean up
	free_grid( &grid ) ;
	free_approx_vars( &aVars, params ) ;
	free_re_ctxt_mem( ctxt ) ;
	free( tdRHS ) ;
	free( eqRHS ) ;
	free( approx ) ;
	free( flux ) ;
	free( tdApprox ) ;
}

//Tests that the pressure is correctly calculated given the volumetric wetness
void test_calc_pressure_from_theta( AlgorithmParameters *params )
{
	int i ;
	const char **meshes ;
	GridParameters grid ;
	double *theta, *pressure ; //The volumetric wetness and pressure,
							   //respectively
	double constTheta ;
							   
	//Make sure that the test case is one with multiple soil types
	if( params->testCase != 18 ){
		printf( "Test only valid for test case 18\n" ) ;
		return ;
	}
	
	//The Richard's equation context should have been set up appropriately. We
	//assume that the correct grids have been passed in, and simply read in
	//the grid on the finest level pointed to by the parameters
	meshes = get_grid_hierarchy( params ) ;
	
	init_grid( &grid, params, meshes[ params->fGridLevel -1] ) ;
	
	//Set the memory for the volumetric wetness and pressure functions
	theta = (double*)calloc( grid.numNodes, sizeof( double ) ) ;
	pressure = (double*)calloc( grid.numNodes, sizeof( double ) ) ;
	
	//Print out information stored in the Richards equation context to the
	//console
	for( i=0 ; i < params->reCtxt.numSoils ; i++ ){
		printf( "Richards Equation context:\n\t" ) ;
		printf( "Theta_{s}: %.5lf\n\t", params->reCtxt.thetaS[i] ) ;
		printf( "Theta_{r}: %.5lf\n\t", params->reCtxt.thetaR[i] ) ;
		printf( "Theta_{m}: %.5lf\n\t", params->reCtxt.thetaM[i] ) ;
		printf( "Saturated conductivity: %.5lf\n\t",
			params->reCtxt.satCond[i] ) ;
		printf( "n: %.5lf\n\t", params->reCtxt.n[i] ) ;
		printf( "m: %.5lf\n\t", params->reCtxt.m[i] ) ;
		printf( "Minimum pressure: %.5lf\n\t", params->reCtxt.hS[i] ) ;
		printf( "Alpha: %.5lf\n\t", params->reCtxt.alpha[i] ) ;
		printf( "Scaling factor for effective saturation: %.5lf\n\t",
			params->reCtxt.satScaleFact[i] ) ;
		printf( "Scaling factor for hydraulic conductivity: %.5lf\n",
			params->reCtxt.cHydrCond[i] ) ;
	}
	
	//Now that we have initialised the grid we can calculate the pressure,
	//once we have got a volumetric wetness
	constTheta = calc_theta( &(params->reCtxt), 0, -100.0 ) ;
	for( i=0 ; i < grid.numNodes ; i++ )
		theta[grid.mapG2A[i]] = constTheta ;
		
	//Print the volumetric wetness out to file
	print_grid_func_to_file( "/tmp/thetaOut.txt", &grid, theta, 0 ) ;
	
	//Calculate the pressure for the given volumetric wetness
	set_pressure_for_theta( params, &grid, theta, pressure ) ;
	
	//Print the pressure out to file
	print_grid_func_to_file( "/tmp/pressureOut.txt", &grid, pressure, 0 ) ;
	
	//Clean up
	free_grid( &grid ) ;
	free( theta ) ;
	free( pressure ) ;
}

//Tests that the coefficient for the p-Laplacian is set up properly
void test_pc_coeff_set_up( AlgorithmParameters *params )
{
	const char **meshes ; //The filenames for the hierarchy of grids to use
	GridParameters grid ; //The grid on which to calculate the coefficient
						  //function
	ApproxVars aVars ; //Variables associated with the approximation on the grid
	char outFName[32] ; //Filename to print output to
	
	if( !has_pc_coeff( params->testCase ) ){
		printf( "void test_pc_coeff_set_up:\n\t" ) ;
		printf( "Test case with a piecewise constant coefficient required\n" ) ;
		return ;
	}
	
	meshes = get_grid_hierarchy( params ) ;
	
	//Initialise the grid
	init_grid( &(grid), params, meshes[ params->fGridLevel -1] ) ;
	
	//Initialise the approximation variables
	init_approx_vars( &aVars, &grid, params ) ;
	
	//Now print the coefficient function to file. This is defined as a piecewise
	//constant function on the domain. This is printed out in a VTK format
	//such that it can be read in by Paraview
	sprintf( outFName, "/tmp/coeffOutLvl%d.vtk", params->fGridLevel ) ;
	print_cell_func_paraview_format( outFName, &grid, aVars.pcCoeff ) ;
		
	//Clean up
	free_grid( &grid ) ;
	free_approx_vars( &aVars, params ) ;
	free_re_ctxt_mem( &(params->reCtxt) ) ;
	free_coeff_regs( &(params->coeffReg) ) ;
}

//Test that the restriction of a piecewise constant function on a fine grid
//can be restricted to a coarse grid
void test_restrict_pc_coeff( AlgorithmParameters *params ){
	int i ; //Loop counter
	const char **meshes ; //The filenames for the hierarchy of grids to use
	GridParameters **grids ; //The grids in the multigrid hiearchy
	ApproxVars fVars ; //Variables associated with the approximation on the fine
					   //grid
	double **cCoeffs ; //The coefficient functions on the coarse grids
	char outFName[32] ; //The filename to print the output to
	
	if( !has_pc_coeff( params->testCase ) ){
		printf( "void test_restrict_pc_coeff:\n\t" ) ;
		printf( "Test case with a piecewise constant coefficient required\n" ) ;
		return ;
	}
	
	//Get the meshes that are pointed to by the algorithm parameters
	meshes = get_grid_hierarchy( params ) ;

	//Inititalise the grids that we will need in this run to NULL pointers
	grids = (GridParameters**) malloc( params->fGridLevel *
		sizeof( GridParameters* ) ) ;
	initialise_grids_to_null( grids, params ) ;
	
	cCoeffs = (double**)malloc( (params->fGridLevel-1) * sizeof( double* ) ) ;
	for( i=0 ; i < params->fGridLevel-1 ; i++ )
		cCoeffs[i] = NULL ;
	
	//Set up the finest grid
	grids[params->fGridLevel-1] = (GridParameters*)malloc(
		sizeof( GridParameters ) ) ;
	init_grid( grids[params->fGridLevel -1], params, 
		meshes[params->fGridLevel - 1] ) ;
		
	//Initialise the approximation variables on the finest grid only
	init_approx_vars( &fVars, grids[params->fGridLevel-1], params ) ;
	
	//Set up all of the grids and the approximation variables on those grids
	//Set up all of the grids on which we are operating
	for( i=params->fGridLevel-2 ; i >= params->cGridLevel-2 ; i-- ){
		params->currLevel = i + 1 ;
		//Assign the appropriate memory
		grids[i] = (GridParameters*)malloc( sizeof( GridParameters ) ) ;
		//Set up the grid
		init_grid( grids[i], params, meshes[i] ) ;
		//Assign the memory for the coefficients on the coarse grids
		cCoeffs[i] = (double*)calloc( grids[i]->numElements,
			sizeof( double ) ) ;
	}
	params->currLevel = params->fGridLevel ;
	
	//Print out the coefficient on the fine grid to file
	sprintf( outFName, "/tmp/coeffLvl%d.vtk", params->fGridLevel ) ;
		print_cell_func_paraview_format( outFName, grids[params->fGridLevel-1],
			fVars.pcCoeff ) ;
	
	//Restrict the approximation to the coarser grids one by one
	for( i = params->fGridLevel-1 ; i > params->cGridLevel-2 ; i-- ){
		if( i != params->fGridLevel-1 )
			restrict_pc_coeff( params, grids[i], grids[i-1], cCoeffs[i],
				cCoeffs[i-1] ) ;
		else
			restrict_pc_coeff( params, grids[i], grids[i-1], fVars.pcCoeff,
				cCoeffs[i-1] ) ;
	
		//Print out the coefficient functions to file
		sprintf( outFName, "/tmp/rCoeffLvl%d.vtk", i ) ;
		print_cell_func_paraview_format( outFName, grids[i-1], cCoeffs[i-1] ) ;
	}
	
	//Clean up
	for( i=0 ; i < params->fGridLevel-1 ; i++ ){
		if( cCoeffs[i] != NULL )
			free( cCoeffs[i] ) ;
	}
	free_grids( grids, params->fGridLevel ) ;
	free( cCoeffs ) ;
	free_approx_vars( &fVars, params ) ;
	free_re_ctxt_mem( &(params->reCtxt) ) ;
	free_coeff_regs( &(params->coeffReg) ) ;
}













