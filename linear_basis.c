#include "linear_basis.h"

/** Calculates the value of a basis function evaluated at point \f$( x, y)\f$
 *	on an element identified by \e e on the grid \e grid. It is assumed
 *	that the point \f$( x, y)\f$ is on the boundary or the interior of the
 *	element
 *	\param [in] grid	The grid on which to operate
 *	\param [in]	elNode	The identifier of a vertex on element \e e. This is in
 *						the range (0,1,2)
 *	\param [in] e	An element on the grid
 *	\param [in] x	The x-coordinate of a point for which to evaluate the
 *					function
 *	\param [in] y	The y-coordinate of a point for which to evaluate the
 *					function
 *	\return \p double value of the basis function evaluated at point
 *			\f$( x, y)\f$
 */
double linear_bf( const GridParameters *grid, int elNode,
	const Element *e, double x, double y )
{
	const Node *n1, *n2 ; //Nodes on the element that we are interested in
	double a, b, c ;
	
	n1 = &(grid->nodes[ e->nodeIDs[ (elNode+1)%3 ] ]) ;
	n2 = &(grid->nodes[ e->nodeIDs[ (elNode+2)%3 ] ]) ;
	
	//b is the difference in the y coordinates of the opposite nodes
	b = n1->y - n2->y ;
	
	//c is the difference in the x coordinates of the opposite nodes
	c = n2->x - n1->x ;
	
	//a is a product of the x and y coordinates of the opposite nodes
	a = n1->x * n2->y -
		n1->y * n2->x ;

	return (a + b * x + c * y) / (2.0 * e->area) ;
}

/** Calculates the gradient in the x-direction of a basis function on a given
 *	element of a grid
 *	\param [in] i	The index of a vertex on element \e e, where the basis
 *					function is centered
 *	\param [in] e	The element over which to calculate the gradient
 *	\param [in] nodes	A global list of nodes on the grid
 *	\return \p double value of the x-gradient of a basis function on element
 *			\e e
 */
double linear_bf_gradx( int i, const Element *e, const Node *nodes )
{
	int iN1, iN2 ; //Indices of the nodes on the triangle we are interested in
	
	iN1 = e->nodeIDs[ (i+1)%3 ] ;
	iN2 = e->nodeIDs[ (i+2)%3 ] ;
	
	return (nodes[iN1].y - nodes[iN2].y) / (2.0 * e->area);
}

/** Calculates the gradient in the y-direction of a basis function on a given
 *	element of a grid
 *	\param [in] i	The index of a vertex on element \e e, where the basis
 *					function is centered
 *	\param [in] e	The element over which to calculate the gradient
 *	\param [in] nodes	A global list of nodes on the grid
 *	\return \p double value of the y-gradient of a basis function on element
 *			\e e
 */
double linear_bf_grady( int i, const Element *e, const Node *nodes )
{
	int iN1, iN2 ; //Indices of the nodes on the triangle we are interested in
	
	iN1 = e->nodeIDs[ (i+1)%3 ] ;
	iN2 = e->nodeIDs[ (i+2)%3 ] ;
	
	return (nodes[iN2].x - nodes[iN1].x)/ (2.0*e->area) ;
}

/** Runs some basic tests for the basis functions. For each of the functions on
 *  the element this prints out the values of the functions evaluated at each
 *  vertex, the mid point of the edges and the centroid of the element. The
 *	values of the gradients in each direction, along with the coordinates of the
 *	vertices are also printed
 *	\param [in] grid	The grid on which to operate
 */
void test_bf( const GridParameters *grid )
{
	int elId ; //Element number
	Element *e ;
	//double area ;
	const Node *node1, *node2, *node3 ;
	
	elId = 21 ;
	e = &(grid->elements[elId]) ;
	//area = areas[e] ;
	
	node1 = &(grid->nodes[e->nodeIDs[0]]) ;
	node2 = &(grid->nodes[e->nodeIDs[1]]) ;
	node3 = &(grid->nodes[e->nodeIDs[2]]) ;
	
	//Check that the correct values are given for each point
	printf( "Basis function 1 evaluated at:\n\t" ) ;
	printf( "Point 1: %.2f\n\t", linear_bf( grid, 0, e, node1->x,
		node1->y ) ) ;
	printf( "Point 2: %.2f\n\t", linear_bf( grid, 0, e, node2->x,
		node2->y ) ) ;
	printf( "Point 3: %.2f\n\t", linear_bf( grid, 0, e, node3->x,
		node3->y ) ) ;
	printf( "Mid point of point 1 and 2: %.2f\n\t", linear_bf( grid, 0, e,
		(node1->x + node2->x)/2.0, (node1->y + node2->y)/2.0 ) ) ;
	printf( "Mid point of point 1 and 3: %.2f\n\t", linear_bf( grid, 0, e,
		(node1->x + node3->x)/2.0, (node1->y + node3->y)/2.0 ) ) ;
	printf( "Mid point of point 2 and 3: %.2f\n\t", linear_bf( grid, 0, e,
		(node2->x + node3->x)/2.0, (node2->y + node3->y)/2.0 ) ) ;
	printf( "Centroid: %.2f\n", linear_bf( grid, 0, e,
		(node1->x + node2->x + node3->x)/3.0,
		(node1->y + node2->y + node3->y)/3.0 ) ) ;
		
	//Second basis function
	printf( "\nBasis function 2 evaluated at:\n\t" ) ;
	printf( "Point 1: %.2f\n\t", linear_bf( grid, 1, e, node1->x,
		node1->y ) ) ;
	printf( "Point 2: %.2f\n\t", linear_bf( grid, 1, e, node2->x,
		node2->y ) ) ;
	printf( "Point 3: %.2f\n\t", linear_bf( grid, 1, e, node3->x,
		node3->y ) ) ;
	printf( "Mid point of point 1 and 2: %.2f\n\t", linear_bf( grid, 1, e,
		(node1->x + node2->x)/2.0, (node1->y + node2->y)/2.0 ) ) ;
	printf( "Mid point of point 1 and 3: %.2f\n\t", linear_bf( grid, 1, e,
		(node1->x + node3->x)/2.0, (node1->y + node3->y)/2.0 ) ) ;
	printf( "Mid point of point 2 and 3: %.2f\n\t", linear_bf( grid, 1, e,
		(node2->x + node3->x)/2.0, (node2->y + node3->y)/2.0 ) ) ;
	printf( "Centroid: %.2f\n", linear_bf( grid, 1, e,
		(node1->x + node2->x + node3->x)/3.0,
		(node1->y + node2->y + node3->y)/3.0 ) ) ;
		
	//Third basis function
	printf( "\nBasis function 3 evaluated at:\n\t" ) ;
	printf( "Point 1: %.2f\n\t", linear_bf( grid, 2, e, node1->x,
		node1->y ) ) ;
	printf( "Point 2: %.2f\n\t", linear_bf( grid, 2, e, node2->x,
		node2->y ) ) ;
	printf( "Point 3: %.2f\n\t", linear_bf( grid, 2, e, node3->x,
		node3->y ) ) ;
	printf( "Mid point of point 1 and 2: %.2f\n\t", linear_bf( grid, 2, e,
		(node1->x + node2->x)/2.0, (node1->y + node2->y)/2.0 ) ) ;
	printf( "Mid point of point 1 and 3: %.2f\n\t", linear_bf( grid, 2, e,
		(node1->x + node3->x)/2.0, (node1->y + node3->y)/2.0 ) ) ;
	printf( "Mid point of point 2 and 3: %.2f\n\t", linear_bf( grid, 2, e,
		(node2->x + node3->x)/2.0, (node2->y + node3->y)/2.0 ) ) ;
	printf( "Centroid: %.2f\n", linear_bf( grid, 2, e,
		(node1->x + node2->x + node3->x)/3.0,
		(node1->y + node2->y + node3->y)/3.0 ) ) ;
		
	//Print out the coordinates
	printf( "\nNode 1: (%.7f, %.7f)\n", node1->x, node1->y ) ;
	printf( "Node 2: (%.7f, %.7f)\n", node2->x, node2->y ) ;
	printf( "Node 3: (%.7f, %.7f)\n", node3->x, node3->y ) ;
	
	//Print out the gradients
	printf( "Gradient of basis function centred at node 1:\n\t" ) ;
	printf( "x-direction: %.7f\n\t", linear_bf_gradx( 0, e, grid->nodes ) ) ;
	printf( "y-direction: %.7f\n\n", linear_bf_grady( 0, e, grid->nodes ) ) ;
	printf( "Gradient of basis function centred at node 2:\n\t" ) ;
	printf( "x-direction: %.7f\n\t", linear_bf_gradx( 1, e, grid->nodes ) ) ;
	printf( "y-direction: %.7f\n\n", linear_bf_grady( 1, e, grid->nodes ) ) ;
	printf( "Gradient of basis function centred at node 3:\n\t" ) ;
	printf( "x-direction: %.7f\n\t", linear_bf_gradx( 2, e, grid->nodes ) ) ;
	printf( "y-direction: %.7f\n", linear_bf_grady( 2, e, grid->nodes ) ) ;
}























