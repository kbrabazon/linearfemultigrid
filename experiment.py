#!/usr/local/bin/python3

#Define a class which will hold the information about a test run
class Experiment:
	#Constructor that uses a dictionary to update as many values as possible
	def __init__( self, dictIn=None ):
		#As not all values will be assigned to I make sure that they are in the
		#object by calling the default constructor on the object
		self.default_init()
		#For each key that we find in the dictionary update the value in the
		#object
		if( dictIn is not None ):
			for key in dictIn.keys():
				self.update( key, dictIn[key] )
	#### End of constructor

	#Initialises the member variables to default values for the class
	def default_init(self):
		self.testCase = -1 #The test case to be run
		self.nlMethod = -1 #The type of nonlinear method to run. This is an
		                  #integer uniquely defining the method, and a list
		                  #can be found in 'useful_definitions.h'
		self.mgType = -1 #The type of multigrid iteration to run. This indicates
		                #the number of iterations of multigrid to run per grid
		                #level, i.e. 1 = V-Cylce, 2 = W-Cycle, etc.
		self.fineGrid = -1 #The finest grid to operate on
		self.coarseGrid = -1 #The coarsest grid on which we 'solve'
		self.smoothType = -1 #The type of smoothing iteration to run. This is an
		                    #integer uniquely defining the smoothing method. A
		                    #definition can be found in 'useful_definitions.h'
		self.preSmooth = -1 #The number of pre-smoothing iterations to run
		self.postSmooth = -1 #The number of post-smoothing iterations to run
		self.fmgPreSmooth = -1 #The number of pre-smoothing iterations to
                               #perform in FMG
		self.fmgPostSmooth = -1 #The number of post-smoothing iterations to
		                        #perform in FMG
		self.smoothWeight = -1 #The weighting factor used in the smoothing
								#iteration
		self.maxIts = -1 #The maximum number of iterations to perform
		self.alpha = -1 #The default value of 'alpha' in the formulation of the
		                 #problem
		self.sigma = -1 #The default value of 'sigma' in the NMLM
		self.nf = -1 #The damping factor in a Newton iteration
		self.innerIts = -1 #The number of linear multigrid iterations per Newton
						   #step
		self.innerItType = -1 #The type of linear iteration that is performed
                            #inside each newton step
		self.recover = False #Indicator as to whether smoothness has been
		                    #recovered for a function in the formulation
		self.initApprox = '' #String representation of the function used as the
							 #initial approximation
		self.tDisc = -1 #Indicates the discretization method used for the time
						 #derivative in time dependent problems
		self.tStart = -1 #Indicates the start time for time dependent problems
		self.tStepSize = -1 #The time step size for time dependent problems
		self.numTSteps = -1 #The number of time steps to perform for time
							  #dependent problems
		self.solSmoothAmp = -1 #The amplitude of the single sine component in
		                       #the solution
		self.solOscAmp = -1 #The amplitude of the oscillatory sine component in
		                    #the solution
		self.solXFreq = -1 #The frequency of the x-component of the oscillatory
		                   #sine component in the solution
		self.solYFreq = -1 #The frequency of the y-component of the oscillatory
		                   #sine component in the solution
							 
		#Now we define the variables that are related with the result of running
		#the experiment
		self.maxReached = False #Indicates whether or not the maximum number of
								#iterations has been exceeded or not
		self.diverged = False #Indicates whether or not the experiment diverged
		self.infNorm = -1 #The infinity norm of the approximation (if the
		                  #exact solution is known)
		self.l2Norm = -1 #The L2 norm of the approximation (if the exact
		                 #exact solution is known)
		self.h1Norm = -1 #The H1 norm of the approximation (if the exact
		                 #solution is known)
		self.execTime = -1 #The time taken to execute the experiment
		self.convFacs = [] #List containing the convergence factors in the L2
		                   #norm for the given experiment
		self.h1ConvFacs = [] #List containing the convergence factors in the H1
		                     #norm for the given experiment
		self.resids = [] #List containing the residuals at each iteration, and
		                 #before the iteration starts
		self.h1Resids = [] #List containing the residuals in H1 norm at each
		                   #iteration, and before the iteration starts
		self.fmg = False #Indicates whether or not Full Multigrid (FMG) was
		                 #performed to get an initial approximation
		self.numIts = -1 #The number of iterations required to reach convergence
	#### End of default constructor
		                    
	#Updates the object by assigning a value the the member variable identified
	#by the string identifier 'strId'
	def update( self, strId, val ):
		if( strId == "TEST_CASE" ):
			self.testCase = int( val )
		if( strId == "FMG" ):
			self.fmg = bool( int( val ) )
		elif( strId == "NL_METHOD" ):
			self.nlMethod = int( val )
		elif( strId == "NUM_MG_CYCLES" ):
			self.mgType = int( val )
		elif( strId == "FINEST_GRID" ):
			self.fineGrid = int( val )
		elif( strId == "COARSEST_GRID" ):
			self.coarseGrid = int( val )
		elif( strId == "SMOOTH_TYPE" ):
			self.smoothType = int( val )
		elif( strId == "PRE_SMOOTH" ):
			self.preSmooth = int( val )
		elif( strId == "POST_SMOOTH" ):
			self.postSmooth = int( val )
		elif( strId == "SMOOTH_WEIGHT" ):
			self.smoothWeight = float( val )
		elif( strId == "MAX_ITS" ):
			self.maxIts = int( val )
		elif( strId == "ALPHA" ):
			self.alpha = float( val )
		elif( strId == "SIGMA" ):
			self.sigma = float( val )
		elif( strId == "NEWTON_DAMP" ):
			self.nf = float( val )
		elif( strId == "RECOVER_GRAD" ):
			self.recover = bool( int( val ) )
		elif( strId == "INIT_APPROX" ):
			self.initApprox = val
		elif( strId == "MAX_REACHED" ):
			self.maxReached = bool( val )
		elif( strId == "DIVERGED" ):
			self.diverged = bool( val )
		elif( strId == "CFACS" ):
			self.convFacs = val
		elif( strId == "H1CFACS" ):
			self.h1ConvFacs = val
		elif( strId == "RESIDS" ):
			self.resids = val
		elif( strId == "H1RESIDS" ):
			self.h1Resids = val
		elif( strId == "INF_NORM" ):
			self.infNorm = float( val )
		elif( strId == "L2_NORM" ):
			self.l2Norm = float( val )
		elif( strId == "H1_NORM" ):
			self.h1Norm = float( val )
		elif( strId == "EXEC_TIME" ):
			self.execTime = float( val )
		elif( strId == "NUM_ITS" ):
			self.numIts = int( val )
		elif( strId == "TIME_DISC" ):
			self.tDisc = int( val )
		elif( strId == "T_START" ):
			self.tStart = float( val )
		elif( strId == "T_STEP_SIZE" ):
			self.tStepSize = float( val )
		elif( strId == "T_STEPS" ):
			self.numTSteps = int( val )
		elif( strId == "INNER_ITS" ):
			self.innerIts = int( val )
		elif( strId == "NEWTON_INNER_IT" ):
			self.innerItType = int( val )
		elif( strId == "FMG_PRE_SMOOTH" ):
			self.fmgPreSmooth = int( val )
		elif( strId == "FMG_POST_SMOOTH" ):
			self.fmgPostSmooth = int( val )
		elif( strId == "SOL_SMOOTH_AMP" ):
			self.solSmoothAmp = float( val )
		elif( strId == "SOL_OSC_AMP" ):
			self.solOscAmp = float( val )
		elif( strId == "SOL_X_FREQ" ):
			self.solXFreq = float( val )
		elif( strId == "SOL_Y_FREQ" ):
			self.solYFreq = float( val )
	#### End of method 'update'
	
	#Indicator to show if a test case is linear or not
	def isLinear(self):
		if( self.testCase == 1 ):
			return True
		return False
	#### End of function 'isLinear'
	
	#Indicator to show if gradients can be recovered or not
	def canRecover(self):
		if( self.testCase == 2 ):
			return True
		return False
	#### End of function 'canRecover'
	
	#Returns a string representation of the number of iterations that were
	#performed
	def it_string(self):
		#If we perform no pre- or post-smoothing this means that the iteration
		#was solved using the smoother only
		if( self.preSmooth == 0 and self.postSmooth == 0 ):
			if( self.diverged ):
				return "-"
			else:
				return str( self.numIts )
		
		#Check if have either diverged to infinity, or are diverging on average
		if( self.diverged or self.overall_conv() > 1.0 ):
			return "-"
		elif( self.maxReached ):
			return "$>$%d" % self.maxIts
		else:
			return str( self.numIts )
	#### End of function 'it_string'
	
	#Returns a string formatted for LaTeX that represents the function that was
	#used as the initial approximation
	def init_approx_latex_string(self):
		return self.initApprox.replace( '*', ' ' ).replace('sin','\\sin').replace( 'pi', '\\pi' )
	#### End of function 'init_approx_latex_string'
	
	#Converts the stored convergence factors to a comma separated string
	def conv_fac_string(self):
		retStr = ''
		if( len( self.convFacs ) == 0 ):#self.diverged or self.overall_conv() > 1.0 ):
			return 'inf\n'
		for i in range( 0, len(self.convFacs) ):
			if( i != len( self.convFacs)-1 ):
				retStr = retStr + "%.10f," % self.convFacs[i]
			else:
				retStr = retStr + "%.10f\n" % self.convFacs[i]
		return retStr
	#### End of function 'conv_fac_string'
	
	#Converts the stored convergence factors in the H1 norm to a comma separated
	#string
	def h1_conv_fac_string(self):
		retStr = ''
		#If there are no convergence factors return a string to indicate that
		#divergence occured to infinity
		if( len( self.h1ConvFacs ) == 0 ):
			return 'inf\n'
		#For each of the convergence factors stored
		for i in range( 0, len( self.h1ConvFacs ) ):
			#if we are at  the last element print a new line at the end of the
			#convergence factor, otherwise a comma
			if( i != len( self.h1ConvFacs)-1 ):
				retStr = retStr + "%.10f," % self.h1ConvFacs[i]
			else:
				retStr = retStr + "%.10f\n" % self.h1ConvFacs[i]
		#Return the string that was produced
		return retStr
	#### End of function 'h1_conv_fac_string'
	
	#Returns the mean convergence factor observed in the experiments
	def overall_conv(self):
		#If we have an infinite value in the list then return zero
		if( float( "inf" ) in self.convFacs or len( self.convFacs ) == 0 ):
			return float( "inf" )
		#Return the overall convergence factor by multiplying all convergence
		#factors together
		rVal = 1
		for convFac in self.convFacs:
			rVal *= convFac
		return rVal
	### End of function 'overall_conv'
	
	#Returns the mean convergence factor in H1 norm observed in the experiments
	def mean_h1_conv(self):
		#If we have an infinite value in the list, then return zero
		if( float( "inf" ) in self.h1ConvFacs or len( self.h1ConvFacs ) == 0 ):
			return float( "inf" )
		#Return the mean value of the list
		return sum( self.h1ConvFacs ) / float( len( self.h1ConvFacs ) )
	#### End of function 'mean_h1_conv'
	
	def isTimeDependent(self):
		if( self.testCase == 4 or self.testCase == 5 ):
			return True
		return False
	#### End of function 'isTimeDependent'
	
	#Gives a string representation of the object that is passed in
	def __str__(self):
		linTC = self.isLinear()
		#First print the test case
		strRep = "Test Case: {0}\n".format( self.testCase )
		#Print if we have performed full multigrid or not
		if( self.fmg ):
			strRep = strRep + "FMG: Yes\n"
			strRep = strRep + "Number of pre-smooths in FMG: %d\n" % self.fmgPreSmooth
			strRep = strRep + "Number of post-smooths in FMG: %d\n" % self.fmgPostSmooth
		else:
			strRep = strRep + "FMG: No\n"
		#Now append the multigrid method used
		if( self.mgType == 1 ):
			strRep = strRep + "Multigrid Type: V-Cycle\n"
		elif( self.mgType == 2 ):
			strRep = strRep + "Multigrid Type: W-Cycle\n"
		else:
			strRep = strRep + "Multigrid Type: {0}\n".format( self.mgType )
		#Now print the nonlinear method used
		if( not linTC ):
			if( self.nlMethod == 0 ):
				strRep = strRep + "Nonlinear Method: NMLM\n"
				#Since we are performing the NMLM it is important to know
				#which value of sigma we are using
				strRep = strRep + "Sigma: {0}\n".format( self.sigma )
			elif( self.nlMethod == 1 ):
				strRep = strRep + "Nonlinear Method: N-MG\n"
				strRep = strRep + "Linear iteration: "
				if( self.innerItType == 0 ):
					strRep = strRep + "Multigrid\n"
				elif( self.innerItType == 1 ):
					strRep = strRep + "Preconditioned Conjugate Gradient\n"
				elif( self.innerItType == 2 ):
					strRep = strRep + "Preconditioned GMRES\n"
				else:
					strRep = strRep + "Unknown iteration\n"
				#Since we are performing the Newton Multigrid it is important to
				#know which damping factor is being used in the Newton step
				strRep = strRep + "Newton Damping Factor: {0}\n".format(
				           self.nf )
				strRep = (strRep + "Number of linear iterations per Newton " +
				            " step: %d\n" % self.innerIts )
			else:
				strRep = strRep + "Nonlinear Method: Unknown\n"
			#Print the value of alpha that is used here as well
			strRep = strRep + "Alpha: {0}\n".format( self.alpha )
		#The type of smoothing iteration used
		if( self.smoothType == 0 ):
			strRep = strRep + "Smoothing Method: Gauss-Seidel\n"
		elif( self.smoothType == 1 ):
			strRep = strRep + "Smoothing Method: Jacobi\n"
		elif( self.smoothType == 2 ):
			strRep = strRep + "Smoothing Method: Block Jacobi (pointwise update)\n"
		elif( self.smoothType == 3 ):
			strRep = strRep + "Smoothing Method: Block Jacobi (equal average update)\n"
		elif( self.smoothType == 4 ):
			strRep = strRep + "Smoothing Method: Block Jacobi (centre weighted double)\n"
		elif( self.smoothType == 5 ):
			strRep = strRep + "Smoothing Method: Block Jacobi (centre weighted as sum of neighbours)\n"
		else:
			strRep = strRep + "Smoothing Method: Unknown\n"
		#The number of pre- and post-smoothing iterations
		strRep = strRep + "Number of Pre- (Post-)smoothing Iterations: {0} ({1})\n".format(
		           self.preSmooth, self.postSmooth )
		#Weighting factor in the smoothing iteration
		strRep = strRep + "Weighting factor for smoothing: {0}\n".format(
		           self.smoothWeight )
		#The finest grid level
		strRep = strRep + "Finest grid level: {0}\n".format( self.fineGrid )
		#The coarsest grid level
		strRep = strRep + "Coarsest grid level: {0}\n".format( self.coarseGrid )
		#The initial approximation
		strRep = strRep + "Initial approximation: " + self.initApprox + "\n"
		if( self.isTimeDependent() ):
			if( self.tDisc == 0 ):
				strRep = strRep + "Time discretization: Backward Euler\n"
			else:
				strRep = strRep + "Time discretization: Unknown\n"
			strRep = strRep + "Start time: {}\n".format( self.tStart )
			strRep = strRep + "Time step size: {}\n".format( self.tStepSize )
			strRep = strRep + "Number of time steps: {}\n".format( self.numTSteps )
		#Check if recovery was performed or not
		if( self.canRecover() ):
			if( self.recover ):
				strRep = strRep + "Recovery performed: Yes\n"
			else:
				strRep = strRep + "Recovery performed: No\n"
		#Now we want to be selective about what is printed depending on whether
		#the iteration converged or not
		#Check whether the iteration converged
		if( self.diverged ):
			if( self.numIts > 0 ):
				strRep = strRep + "Iteration divereged"
			else:
				strRep = strRep + "Initial approximation deemed poor"
			return strRep
		#Check whether the maximum number of iterations was exceeded
		if( self.maxReached ):
			strRep = strRep + "Number of Iterations: >{0}\n".format(
			           self.maxIts )
		else:
			strRep = strRep + "Number of Iterations: {0}\n".format(
			           self.numIts )
		#The convergence factors that were gained
		#strRep = strRep + "Convergence factors: {0}\n".format(
		#           str( self.convFacs ) )
		#The residuals that were gained
		#strRep = strRep + "Residuals: {}\n".format(
		#	str( self.resids ) )
		#Now print the error information, if it is available
		if( self.l2Norm > 0 ):
			strRep = strRep + "L2 Norm of Error: {0}".format( self.l2Norm )
			if( self.maxReached ):
				strRep = strRep + " (At {0} iterations)\n".format( self.maxIts )
			else:
				strRep = strRep + "\n"
			if( self.h1Norm != -1 ):
				strRep = strRep + "H1 Norm of Error: {}".format( self.h1Norm )
				if( self.maxReached ):
					strRep = strRep + " (At {} iterations)\n".format( self.maxIts )
				else:
					strRep = strRep + "\n"
			strRep = strRep + "Infinity Norm of Error: {0}".format(
			   self.infNorm )
			if( self.maxReached ):
				strRep = strRep + " (At {0} iterations)\n".format( self.maxIts )
			else:
				strRep = strRep + "\n"
		#The time taken for the iteration
		strRep = strRep + "Execution Time: {0}".format( self.execTime )
		return strRep
	#### End of function '__str__'
	
	#Returns true if experiments e1 and e2 are similar (i.e. they have been run with
	#the same parameters, excluding the fine grid levels). It is assumed that
	#experiment e1 and e2 are both for linear test cases.
	def is_lin_similar( self, other ):
		if( self.coarseGrid != other.coarseGrid ): return False
		if( self.mgType != other.mgType ): return False
		if( self.smoothType != other.smoothType ): return False
		if( self.preSmooth != other.preSmooth ): return False
		if( self.postSmooth != other.postSmooth ): return False
		if( self.smoothWeight != other.smoothWeight ): return False
		if( self.fmg != other.fmg ): return False
		if( self.initApprox != other.initApprox ): return False
		if( self.fmg ):
			if( self.fmgPreSmooth != other.fmgPreSmooth ): return False
			if( self.fmgPostSmooth != other.fmgPostSmooth ): return False
		#If we get to here everything that we are interested in is equal, so return
		#true
		return True
	#### End of function 'is_lin_similar'

	#Returns true if experiments e1 and e2 are similar (i.e. they have been run with
	#the same parameters, excluding the fine grid levels). It is assumed that
	#experiment e1 and e2 were both run using NMLM
	def is_nmlm_similar( self, other ):
		if( self.testCase != other.testCase ): return False
		if( self.coarseGrid != other.coarseGrid ): return False
		if( self.alpha != other.alpha ): return False
		if( self.mgType != other.mgType ): return False
		if( self.smoothType != other.smoothType ): return False
		if( self.preSmooth != other.preSmooth ): return False
		if( self.postSmooth != other.postSmooth ): return False
		if( self.smoothWeight != other.smoothWeight ): return False
		if( self.sigma != other.sigma ): return False
		if( self.recover != other.recover ): return False
		if( self.initApprox != other.initApprox ): return False
		if( self.fmg != other.fmg ): return False
		if( self.fmg ):
			if( self.fmgPreSmooth != other.fmgPreSmooth ): return False
			if( self.fmgPostSmooth != other.fmgPostSmooth ): return False
		if( self.isTimeDependent() ):
			if( self.tDisc != other.tDisc ): return False
			if( self.tStart != other.tStart ): return False
			#I am not sure what to do with the rest of the time dependent parameters
			#as we do want to compare the different discretizations and different
			#time steps. Come back to this if I need it, but I don't at the moment
		#If we get to here everything that we are interested in is equal, so return
		#True
		return True
	#### End of function 'is_nmlm_similar'

	#Returns true if experiments e1 and e2 are similar (i.e. they have been run with
	#the same parameters, excluding the fine grid levels). It is assumed that
	#experiment e1 and e2 were both run using NMG
	def is_nmg_similar( self, other ):
		if( self.testCase != other.testCase ): return False
		if( self.alpha != other.alpha ): return False
		if( self.mgType != other.mgType ): return False
		if( self.coarseGrid != other.coarseGrid ): return False
		if( self.smoothType != other.smoothType ): return False
		if( self.preSmooth != other.preSmooth ): return False
		if( self.postSmooth != other.postSmooth ): return False
		if( self.smoothWeight != other.smoothWeight ): return False
		if( self.nf != other.nf ): return False
		if( self.innerIts != other.innerIts ): return False
		if( self.innerItType != other.innerItType ): return False
		if( self.initApprox != other.initApprox ): return False
		if( self.fmg != other.fmg ): return False
		if( self.fmg ):
			if( self.fmgPreSmooth != other.fmgPreSmooth ): return False
			if( self.fmgPostSmooth != other.fmgPostSmooth ): return False
		if( self.isTimeDependent() ):
			if( self.tDisc != other.tDisc ): return False
			if( self.tStart != other.tStart ): return False
			#I am not sure what to do with the rest of the time dependent parameters
			#as we do want to compare the different discretizations and different
			#time steps. Come back to this if I need it, but I don't at the moment
		#If we get to here everything that we are interested in is equal, so return
		#True
		return True
	#### End of function 'is_nmg_similar'

	#Returns true if e1 and e2 are the same experiment, if we ignore the fine grid
	#on which they are operating
	def is_similar( self, other ):
		#Check if the test case is linear
		if( self.testCase == 1 and other.testCase == 1 ):
			return self.is_lin_similar( other )
		elif( self.nlMethod == 0 and other.nlMethod == 0 ):
			return self.is_nmlm_similar( other )
		elif( self.nlMethod == 1 and other.nlMethod == 1 ):
			return self.is_nmg_similar( other )
		#If we are at this point we know that the solution method is not the same
		#for the two experiments, so we return False
		return False
	#### End of function 'is_similar'
	
	#Defines the less than operator. Comparison is done only using the fine grid
	def __lt__(self, other):
		if( self.fineGrid < other.fineGrid ): return True
		return False
	#### End of function __lt__
	
	#Defines equality operator. Comparison is done only using the fine grid
	def __eq__(self,other):
		if( self.fineGrid == other.fineGrid ): return True
		return False
	#### End of function __eq__
	
	#Defines the greater than operator. Comparison is done only using the fine grid
	def __gt__(self,other):
		if( self.fineGrid > other.fineGrid ): return True
		return False
	#### End of function '__gt__'
		
#### End of class definition 'Experiment'

















