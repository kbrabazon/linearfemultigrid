#ifndef _LINEAR_BASIS_H
#define _LINEAR_BASIS_H

#include "useful_definitions.h"
/** \file linear_basis.h
 *	\brief Functions related to a piecewise linear nodal basis
 */

//Calculate the values of the linear basis function
double linear_bf( const GridParameters *grid, int elNode,
	const Element *e, double x, double y ) ;
	
//Calculate the values of the derivatives of the linear basis function
double linear_bf_gradx( int i, const Element *e, const Node *nodes ) ;

double linear_bf_grady( int i, const Element *e, const Node *nodes ) ;
	
//Runs a basic test on the values of the basis functions
void test_bf( const GridParameters *grid ) ;

#endif
