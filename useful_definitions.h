#ifndef _USEFUL_DEFINITIONS_H
#define _USEFUL_DEFINITIONS_H
/*! \file useful_definitions.h
	\brief Definitions of useful constants, structs and functions.
	
	Contains structures and functions that are not specific to a particular
	area of multigrid and that can be reused. This file is included in all other
	files in the project
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>

/**
 * \defgroup GlobConstDefs Global Constant Definitions
 * @{
 */
/*! The default file to read parameters in from */
#define PARAMS_IN_FILE ("./parameters.txt")
/*! Pi */
#define PI (3.141592653589793)
/*! Pi squared */
#define PI2 (9.869604401089358)

/*! The maximum number of neighbours of a node on a grid */
#define SPARSE (7)
/*! Default file to print approximation data to */
#define APPROX_OUT_FILE ("/tmp/approxOut")
/*! Default file to print RHS data to */
#define RHS_OUT_FILE ("/tmp/rhsOut")
/*! Default file to print debug data to */
#define DEBUG_OUT_FILE ("/tmp/debugOut")
/*! The relative reduction in residual for a stopping criterion for the
	multigrid iteration */
#define TOL1 (1e-7)
/*! Maximum absolute difference between approximations before an approximation
	is considered converged in an iterative solve */
#define TOL2 (1e-10)
/*! Maximum absolute difference between approximations at different time steps
	before an approximation is considered to be at the steady state */
#define STEADY_STATE_DIFF (1e-10)
/*! Relative reduction in residual per time step */
#define TD_TOL (1e-3)
/*! Size of perturbation to use when calculating the numerical derivative */
#define M_EPS (1e-7)
/*! Maximum number of smoothing iterations to perform for nonlinear test cases*/
#define MAX_NL_SMOOTHS (10)
/*! Maximum number of smoothing iterations to perform for linear test cases */
#define MAX_LIN_SMOOTHS (1000)
/*! Maximum number of inner iterations to perform per Newton outer iteration */
#define MAX_INNER_ITS (5)
/*! The accuracy to which the linear system of equations should be solved */
#define LIN_TOL (1e-3)
/*! Maximum size of residual before execution is stopped */
#define MAX_RESID (1e14)
/*! Maximum relative increase in residual before execution is stopped*/
#define MAX_RESID_FACT (1e6)
/*! The minimum sigma to be used in a Nonlinear Multigrid iteration */
#define MIN_SIGMA (1e-3)
/*! The density of water to be used in problems of Richards equation type */
#define WAT_DENS (1.0)
/*! Acceleration due to gravity (given in m/s^2) */
#define G_ACC (-9.80665)
//#define G_ACC (-1.0)
/**@}*/

//#define CLEAR_STR ("\33[2K\r")
//#define CURS_INVIS_STR ("\33[?25l")
//#define CURS_VIS_STR ("\33[?25h")


/**
 * \defgroup GridVariables Global pointers to grid files
 * @{
 */
/*! Points to a file list defining structured grids where the diagonal runs from
	the bottom left to the top right of an element. The grids are defined on the
	domain (0,1) x (0,1) */
extern const char* meshesBL2TR[] ;
/*! Points to a file list defining structured grids where the diagonal runs from
	the bottom left to the top right of an element. The grids are defined on the
	domain (-1,1) x (-1,1) */
extern const char* exMeshesBL2TR[] ;
/*! Points to a file list defining structured grids where the diagonal runs from
	the bottom right to the top left of an element. The grids are defined on the
	domain (0,1) x (0,1) */
extern const char* meshesBR2TL[] ;
/*! Points to a file list defining structured grids where the diagonal runs from
	the bottom left to the top right of an element. The grids are defined on the
	domain (0, 0.25) x (0, 0.25) */
extern const char* meshesBL2TR_Q[] ;
/*! Points to a file list defining structured grids where the diagonal runs from
	the bottom right to the top left of an element. The grids are defined on the
	domain (0, 0.25) x (0, 0.25) */
extern const char* meshesBR2TL_Q[] ;
/**@}*/

/*! \cond */
extern void dgesv_(int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b,
	int *ldb, int *info ) ;
/*! \endcond */


/**
 * \defgroup UsefulEnums Enums used to give names to numeric data
 * @{
 */
/** The different types of nonlinear method that can be performed. */
enum NL_ITERATION_METHOD{
	METHOD_NLMG, /*!< The nonlinear multigrid method */
	METHOD_NEWTON_MG /*!< The Newton-Multigrid method */
} ;

/** The different types of restriction that can be performed.
	The different types of restriction that can be performed when  moving from
	fine to coarse grids.
*/
enum RESTRICT_TYPE{
	R_TYPE_FW, /*!< Full weighting restriction */
	R_TYPE_INJECTION /*!< Injection restriction */
} ;

/** The different types of node we can have on the grid. This enumeration starts
	from -2 */
enum NODE_TYPE{
	NODE_NEUM_BNDRY = -2, /*!< A Neumann boundary node */
	NODE_DIR_BNDRY, /*!< A Dirichlet boundary node */
	NODE_INTERIOR /*!< An node in the interior of the grid */
} ;

/** The types of smoothing it is possible to perform */
enum SMOOTH_TYPE{
	SMOOTH_GS,				 /*!< Pointwise Gauss-Seidel */
	SMOOTH_JAC,				 /*!< Pointwise Jacobi */
	SMOOTH_BLOCK_JAC_SINGLE, /*!< Update a node only with the value calculated
							 at the node */
	SMOOTH_BLOCK_JAC_AVG,    /*!< Update a node with the value of the average
						     caluclated at the neighbouring nodes */
	SMOOTH_BLOCK_JAC_DWEIGHT,/*!< Update a node with a weighted average from the
							 neighbouring nodes. The node at the centre gets
							 double the value of the other nodes */
	SMOOTH_BLOCK_JAC_HWEIGHT, /*!< Update a node with a weighted average from the
							 neighbouring nodes. The node at the centre gets
							 half weight, and the others are distributed 
							 equally */
	SMOOTH_UNKNOWN /*!< Indicates that the smoothing method is not known */
} ;

/*! The discretization methods available to use for the time discretization */
enum TIME_DISC_TYPE{
	TIME_DISC_BE, /*!< Backward Euler time discretization */
	TIME_DISC_TRAP, /*!< Second order Trapezoidal time discretization */
	TIME_DISC_UNKNOWN /*!< Indicates that the time discretization is not
					  known */
} ;

/** The different types of mesh available to use */
enum MESH_TYPE{
	MESH_DIAG_BL2TR, /*!< Domain (0,1) x (0,1) with diagonal running from bottom
					 left to top right */
	MESH_DIAG_BL2TR_Q,/*!< Domain (0,0.25) x (0,0.25) with diagonal running from
					  bottom left to top right */
	MESH_DIAG_BR2TL,/*!< Domain (0,1) x (0,1) with diagonal running from bottom
					right to top left */
	MESH_DIAG_BR2TL_Q/*!< Domain (0,0.25) x (0,0.25) with diagonal running from
					 bottom right to top left */
} ;

/*! The different methods available to iterate over the linear system resulting
	from performing a Newton step */
enum INNER_IT_TYPE{
	IT_MULTIGRID, /*!< Linear multigrid */
	IT_PCG,		  /*!< Preconditioned conjugate gradients */
	IT_PCGMRES	  /*!< Preconditioned GMRES */
} ;

/*! Indicates the direction in which a shift was made (for example during the
	calculation of the update parameter in the Newton step we may shift a
	sliding window of values either up or down */
enum SHIFT_DIRECTION{
	SHIFT_DOWN,
	SHIFT_UP,
	SHIFT_UNKNOWN
} ;
/**@}*/

/*! Stores the value and the index at which the infinity norm occurs in a
	vector */
typedef struct{
	double val ; /*!< The value of the infinity norm */
	int ind ; /*!< The index into a vector where the infinity norm occurs */
}InfNorm ;

/**
 * \defgroup UsefulTypdefs Definitions of structures
 * @{
 */
/** \brief A vector with an x- and a y- component. */
typedef struct{
	double x ; /*!< x-component of a 2-D vector */
	double y ; /*!< y-component of a 2-D vector */
}Vector2D ;

/** \brief A node on a grid.

	Defines a node on a grid
*/
typedef struct{
	int id ; /*!< Identifier and index into the grid for the node */
	enum NODE_TYPE type ; /*!< Indicates the type of node */
	
	double x ; /*!< x-coordinate of the node */
	double y ; /*!< y-coordinate of the node */
	
	/**
	 * List of non-Dirichlet boundary neighbours on a given grid. These are
	 * limited in number by the sparse structure of the matrix
	 */
	int nghbrs[SPARSE] ;
	/** The number of non-Dirichlet boundary neighbours of a node */
	int numNghbrs ;
	/** The number of Dirichlet boundary neighbours of a node */
	int numDirNghbrs ;
	/** The weighting factor to use at this node when restricting an integral */
	double weightFact ;
}Node ;

/** \brief A countable vector with x- and y- component */
typedef struct{
	double x ; /*!< \f$x\f$ component of a two dimensional vector */
	double y ; /*!< \f$y\f$ component of a two dimensional vector */
	int cnt ; /*!< Used as a counter for the node. For example can be used to
			  count the number of times that a node is visited */
}CVector2D ;

/** \brief The bounds in \f$x\f$ and \f$y\f$ for an object in the \f$xy\f$
	plane */
typedef struct{
 	double minX ;
 	double maxX ;
 	double minY ;
 	double maxY ;
}XYBounds ;

/** \brief An triangular element on a grid */
typedef struct{
	int id ;
	int nodeIDs[3] ; /*!< The IDs of the nodes on the vertices of the element */
	double area ; /*!< The area of the element */
}Element ;

/** \brief Structure used to interface with LAPACK

	Defines a structure to store information about the block to be used in a
	block smoothing step. All we need is to have the approximation, the rhs and
	then the length of the approximation and the rhs. We know that we can fit a
	maximum of 7 nodes into one block, but it can be less
*/
typedef struct{
	int len ; /*!< The number of elements in the block */
	int pivot[7] ; /*!< The rows that are exchanged in the solve of the linear
				   system using LAPACK */
	int ok ; /*!< Return flag to show the exit status of the LAPACK function to
			 solve the linear system */
	
	double rhs[7] ; /*!< The rhs to solve for. This has at most 7 values */
				  
	double deriv[49] ; /*!< The appropriate values from the Jacobian matrix.
					   There are at most 7x7 = 49 values in this matrix */
}SmoothBlock ;

/**	\brief Structure used to store information about regions with varying
	coefficient values on the domain for test cases with a piecewise constant
	coefficient function
*/
typedef struct{
	int numRegions ; /*!< The number of regions to specify a coefficient value
					for */
	Vector2D *cornerPoints ; /*!< The bottom left hand corner of each
				coefficient region */
	double *width ; /*!< The width of each of the coefficient regions */
	double *height ; /*!< The height of each of the coefficient regions */
	double *val ; /*!< The value to set in each of the coefficient regions */
}CoeffRegions ;

/** \brief Stores variables to be used in the calculation of the solution to
 *	a problem formulated using Richards equation
 */
typedef struct{
	int numSoils ; /*!< The number of different soils to store information
				   for */
	double *thetaR ; /*!< Residual volumetric wetness of a soil */
	double *thetaS ; /*!< Saturated volumetric wetness of a soil */
	double *satCond ; /*!< The hydraulic conductivity of a saturated soil */
	double *n ; /*!< Exponent in the van Genuchten-Mualem model for soil
			    hydraulic conductivity */
	double *m ; /*!< \f$ 1 - 1/n \f$ */
	double *hS ; /*!< Artifical non-positive pressure at which we assume soil
				 satruation is achieved. This comes from the Vogel, et. al
				 modification to the van Genuchten-Mualem model */
	double *thetaM ; /*!< Volumetric wetness for the value which would be gained
					 at zero hydraulic head */
	double *alpha ; /*!< Material property of soil. Not quite sure what this
				   represents */
	double *satScaleFact ; /*!< A scaling factor between the effective
						   saturation and the scaled effective saturation given
						   a nonzero \f$h_{s}\f$ */
	double *cHydrCond ; /*!< A constant scaling factor used in the calculation
						of the unsaturated hydraulic conductivity. This is
						stored to save repeated calculation. This corresponds to
						\f$1 - F(1)\f$ from Vogel et al, 2001 */
	double flowIn ; /*!< The value of the constant flux at the inflow boundary
					for a Richards equation type problem */
	double flowOut ; /*!< The value of the constant flux at the outflow boundary
					 for a Richards equation type problem */
	double hOut ; /*!< The constant pressure on the outflow boundary for a
				  Richards equation type problem */
				  
	bool useCelia ; /*!< Indicates whether the model by Celia et al. should be
					used */
}RichardsEqCtxt ;

/** \brief Groups variables to define a grid

	Acts as a wrapper for all of the different variables that are required to
	define one specific grid
*/
typedef struct{
	int numNodes ;/*!< The number of nodes, including Dirichlet boundary nodes */
	int numElements ;/*!< The number of elements on the grid */
	int numUnknowns ;/*!< The number of non-Dirichlet boundary  nodes */
	int numMatrixEntries ;/*!< The number of nonzero entries in the mass matrix */
							 
	Node *nodes ; /*!< List of nodes on the grid */
	Element *elements ; /*!< List of elements on the grid */
	
	int numCols ; /*!< The number of columns in the grid. This is under the
	              assumption that the grid is regular and rectangular */
	int numRows ; /*!< The number of rows in the grid. This is under the
	              assumption that the grid is regular and rectangular */
	
	int *mapG2A ; /*!< Maps from the position in the grid to the position in the
				  approximation */
	int *mapA2G ; /*!< Maps from the position in the approximation to the
				  position in the grid */
	int *rowStartInds ; /*!< Indices of the start of the rows in the sparse
						representation of the matrix for each of the
						non-Dirichlet boundary nodes */
	int *soilType ; /*!< List of soil types for each node on the grid */
							   
	double gridSpacing ; /*!< The grid spacing on the current grid. This is
	                     non-zero if we are working on a regular grid */
	                     
	enum NODE_TYPE bndType[4] ; /*!< Array of length 4 containing the type of
	                            nodes we get at the boundary of the domain,
	                            assuming that we have a rectangular domain. This
	                            is in the order [L(eft), B(ottom), R(ight),
	                            T(op)] */
}GridParameters ;

/** \brief Stores variables to be used in iterations for time dependent problems
 */
typedef struct{
	enum TIME_DISC_TYPE tDisc ; /*!< The type of discretization that we use for
							    the time derivative, if applicable */
	int numTSteps ; /*!< The number of time steps to take, if applicable */
	double tStart ;/*!< Start time for the time dependent problems,
				   if applicable */
	double tEnd ; /*!< The end time for time dependent problems, if
				  applicable */
	double tStepSize ;/*!< The size of the time step to take, if applicable */
	double currTime ; /*!< The value of the time for the current iteration */
	
	bool adaptTStep ; /*!< Indicator as to whether to adaptively update the time
					  step during execution or not */
	double maxTStep ; /*!< The maximum time step that is allowed when performing
					  adaptive time stepping */
	double minTStep ; /*!< The minimum time step that is allowed when performing
					  adaptive time stepping */
	
	double weightFact ; /*!< A weighting factor (dependent on the time step) to
						be used to multiply components in the discretization.
						This is different for different time dependent
						schemes */
}TDCtxt ;

/** \brief Parameters defining how the algorithm is to be executed */
typedef struct{
	bool lumpMassMat ; /*!< Indicator as to whether to lump the mass matrix
					   entries in the time derivative or not */
	bool isFMG ; /*!< Indicator to show if FMG is to be performed or not */
	bool isSimplifiedNewt ; /*!< Indicator as to whether to perform a simplified
							Newton method */
	bool exactCoarseSolve ; /*!< Indicator as to whether to perform an exact
							coarse grid solve */

	int testCase ;/*!< Identifier of the test case to run */
	int maxIts ; /*!< The maximum number of iterations of Nonlinear multigrid
				 or the maximum number of Newton steps to perform before
				 stopping execution of the algorithm */
				  
	enum MESH_TYPE meshType ; /*!< The type of mesh to use */
	
	enum NL_ITERATION_METHOD nlMethod ; /*!< The type of nonlinear iteration to
										perform, if applicable */
	
	int fGridLevel ; /*!< The finest grid level to approximate on */
	int cGridLevel ; /*!< The coarsest grid level to solve on */
	int currLevel ; /*!< The grid level currenty being operated on */
	
	enum SMOOTH_TYPE smoothMethod ;/*!< The type of smoothing to perform */
	int numPreSmooths ; /*!< The number of pre-smooths to perform */
	int numPostSmooths ;/*!< The number of post-smooths to perform */
	int fmgPreSmooths ; /*!< The number of pre-smooths to perfurm during FMG */
	int fmgPostSmooths ; /*!< The number of post-smooths to perform during
						 FMG */
						
	double smoothWeight ;/*!< The relaxation weight to use */
						  
	int numMgCycles ;/*!< The number of multigrid cycles to perform */
	
	double alpha ; /*!< Defines the size of the nonlinearity */
	
	double sigma ; /*!< Parameter used to define the iteration the nonlinear
				   multilevel method of Hackbusch (see Multi-Grid Methods and
				   Applications, Hackbusch, 1985) */
	int adaptUpdate ; /*!< Indicates whether or not to calculate the update
					  parameters in a nonlinear iteration adaptively or not.
					  Zero indicates false, otherwise true. The parameter that
					  is updated for nonlinear multigrid is
					  AlgorithmParameters::sigma and for the Newton-Multigrid
					  is AlgorithmParameters::newtonDamp */
				   
	enum INNER_IT_TYPE innerIt ;/*!< The type of iteration to perform as the
								inner iteration of the Newton method */
	double newtonDamp ;/*!< Damping parameter used to weight a Newton step */
	double minNewtFact ; /*!< The minimum update per Newton step that is
						permitted */
	double maxNewtFact ; /*!< The maximum update per Newton step that is
						permitted */
	int innerIts ; /*!< The number of iterations of linear multigri to perform
				   per Newton step in the Newton-MG algorithm */
	int gmresReset ; /*!< The number of iterations of preconditioned conjugate
				    gradients to perform before resetting the algorithm. NB:
				    Functionality to make use of this does not yet exist */
	int gmresPrecSPart ; /*!< Indicator as to whether the GMRES preconditioner
						 should precondition the symmetric part of an operator
						 only */
	
	char initApproxFile[80] ;/*!< Filename for a file to read in the initial
							 approximation in from */
	
	double solXFreq ; /*!< Defines the freqeuncy of a sine component in the
					  x-direction for the solution, if applicable. This is
					  available for test cases 6 and 7*/
	double solYFreq ;/*!< Defines the frequency of a sine component in the
					 y-direction for the solution, if applicable. This is
					 available for test cases 6 and 7*/
	double solSmoothAmp ;/*!< Defines the amplitude of the smooth component of
						 the solution, if it can be specified. This is available
						 for test cases 6 and 7*/
	double solOscAmp ;/*!< Defines the amplitude of the oscillatory component of
					  the solution, if it can be specified. This is available
					  for test cases 6 and 7*/
					  
	TDCtxt tdCtxt ; /*!< Context used for iterations of time dependent
					   problems */
					  
	int powU ; /*!< For test case 10 (of type porous medium equation) gives the
			   power of $u$ to use in the diffusion coefficient. As a reminder
			   this test case is given by \f$u_{t} = \nabla \cdot \left\{u^{m}
			   \nabla u\right\}\f$, and this parameter refers to the $m$ in the
			   equation */
	double initRad ; /*!< For test case 10 gives the initial radius of the
					 solution. Test case 10 is \f$u_{t} = \nabla \cdot \left\{
					 u^{m}\nabla u\right\}\f$ */
	
	int recoverGrads ; /*!< Indicator whether or not to 'recover' some
					   continuity from the gradients. This available for
					   test cases 2, 4 and 6 */
	
	int testing ; /*!< Integer value indicating whether testing is to be
				  performed. A value zero indicates that it should not be
				  performed, else it will */
				  
	int setUpApprox ; /*!< Indicates whether an approximation should be set
					  up or not. This should not be set explicitly by the user
					  and depends on how other parts of the algorithm are run.
					  For example, if FMG was performed to get an approximation
					  it is not desired to set up another approximation */
				   
	int debug ; /*!< Indicates whether to perform debugging */
	int readFMGSol ; /*!< Indicates whether or not to attempt to read the
					 solution to the system of equations on a given level during
					 the nested iteration. It is assumed that all files are
					 written in the folder './ConvergedSols', and that they are
					 in a format that can be read by
					 read_grid_function_from_file() */
					 
	RichardsEqCtxt reCtxt ; /*!< Used if we are using a test case in the form
							 of Richards equation. This is set to NULL if not
							 required */

	CoeffRegions coeffReg ; /*!< For test cases with a piecewise constant
				coefficient function stores the information about regions with
				set coefficient values */
							 
	double plExp ; /*!< The exponent to use in a problem of \f$p\f$-Laplace
				   type */
}AlgorithmParameters ;


/** \brief Groups variables related to (the calculation of) the approximation


	The approximation parameters structure stores information that is updated
	(or that may be updated) in the iteration, and that is related to the
	calculation of the approximation
*/
typedef struct{
	double *approx ; /*!< Stores the current value of the approximation */
	double *rhs ; /*!< Stores the values of the right hand side of the weak
				  formulation of a problem */
	double *bndTerm ; /*!< Stores the value of the contribution from boundary
						term. This is stored in a separate array because the
						boundary terms are not the same on each grid level */
	double *resid ; /*!< Stores the value of the residual in approximation */
	double *iterMat ; /*!< The iteration matrix used to calculate the
					  appropriate approximations */
	double *massMat ; /*!< The mass matrix. This is required for the time
					  dependent problems only, at the moment */
	double *tmpApprox ;/*!< Used when required to take a copy of the
					   approximation, such as during Jacobi smoothing */
						
	double *jDiags ; /*!< Used to store the diagonals of the Jacobian matrix.
					 This is only required in the nonlinear multigrid
					 case for the smoothing. Having this defined here reduces
					 the number of malloc and free's in the code */
						
	CVector2D* recGrads ; /*!< Stores the value of the recovered gradient, if
						  applicable. This is applicable for test cases 2, 4
						  and 6 */
						  
	void *usrCtxt ; /*!< A variable that may or may not be used. This can
						store context variables for a subsolver. For example
						this can be variables required for conjugate gradients
						or for GMRES */
	double *reTheta ; /*!< Stores the pointwise values of the volumetric wetness
					for a Richards equation type problem. This is stored to
					avoid repeated recalculation in the numeric integration */
	double *reHCond ; /*!< Stores the pointwise values of the hydraulic
					  conductivity for a Richards equation type problem. This is
					  stored to avoid repeated recalculation in the numeric
					  integration */
	double *reHCondDeriv ; /*!< Stores the pointwise values of the derivative
						   of the hydraulic conductivity for a Richards equation
						   type problem */
						   
	double *pcCoeff ; /*!< Stores the pointwise values of the coefficient
					  function for a \f$p\f$-Laplacian type problem */
}ApproxVars ;

/** \brief Stores results of a Multigrid run

	A stucture to store some information about the results of running the
	multigrid iteration, so that this can be passed around the different
	methods easily
*/
typedef struct{
	double origResidL2Norm ; /*!< The L2 norm of the residual at the start of
							 an experiment */
	double oldResidL2Norm ; /*!< The L2 norm of the residual at the previous
							iteration */
	double residL2Norm ; /*!< The current L2 norm of the residual */
	double residRatio ; /*!< The ratio between successive residual norms */
	
	double origResidH1Norm ; /*!< The H1 norm of the residual at the start of
							 an experiment */
	double oldResidH1Norm ; /*!< The H1 norm of the residual at the previous
							iteration */
	double residH1Norm ; /*!< The current H1 norm of the residual */
	
	double maxCorrection ; /*!< The maximum absolute difference between two
						   approximations. This is used to test for convergence
						   in time dependent problems */
	
	int itCnt ; /*!< Counts the number of multigrid iterations performed */
}Results ;

/** \brief Stores variables to be used in a least squares solve.
 *
 *	Note that the least squares that is implemented in this library is not a
 *	general algorithm, and simply deals with the case that the matrix for which
 *	to solve the least squares problem is a Hessenberg matrix of order
 *	\f$(m+1)\times m\f$, for small \f$m\f$.
 */
typedef struct{
	int maxSize ;/*!< The maximum size of the least squares problem that will be
				 solved using this context */
	int currSize ;/*!< The size of the current least squares problem being
				  solved using this context */
	double **hessMat ; /*!< The Hessenberg matrix for which to get a least
					   squares approximation */
	double **upTMat ; /*!< The upper triangular matrix to transform the
					  Hessenberg matrix into */
	double *rhs ; /*!< The right hand side of the least squares system to solve
				  for */
} LeastSquaresContext ;

/** \brief Stores variables to be used in a GMRES iteration
 *
 *	Note that the name FGMRES stands for Flexible GMRES, and is required to
 *	be able to use an iterative process (i.e. Multigrid) as a preconditioner
 */
typedef struct{
	int reset ; /*!< The number of iterations to perform before resetting the
				GMRES algorithm */
	int currIt ; /*!< The iteration number of the iteration currently being
				 performed */
	double origResidNorm ; /*!< The original residual norm before starting
						   preconditioned GMRES iterations */
	double **subSpaceMat ; /*!< Matrix containing the orthonormal basis
						   produced by the GMRES algorithm */
	double **precSpaceMat ; /*!< Matrix containing the preconditioned basis
							vectors */
	double *precMat ; /*!< The preconditioning matrix. This is used only for
					 test cases in which the preconditioning matrix is different
					 from the iteration matrix in the linear system */
	LeastSquaresContext *lsCtxt ; /*!< A context for solving least squares
								  problems */
} FGMResContext ;

/** \brief Stores variables to be used in a preconditioned conjugate gradients
 *	iteration
 */
typedef struct{
	double *resid ; /*!< The residual of the linear system */
	double *currApprox ; /*!< The current approximation for the linear system */
	double *p ; /*!< The preconditioned residual, used in the iteration */
	double *aP ; /*!< The preconditioned residual multiplied by the linear
				 operator */
} CGContext ;
/**@}*/

/**
 * \defgroup UsefulFunctions Problem non-specific functions
 *	Definitions of functions that are useful in general, and that aren't
 *	specific to one of the other files that are used.
 *	@{
 */
void set_dvect_to_zero( int numPoints, double* vect ) ;
void set_ivect_to_zero( int numPoints, int* vect ) ;

void vect_diff( int numPoints, const double* vect1, const double* vect2,
	double* diff ) ;
	
void copy_vectors( int numPoints, double* vec1, const double* vec2 ) ;
void add_to_vect( int numPoints, double *vec1, const double *vec2 ) ;

//Random number generator with a uniform distribution of the values in the given
//range
unsigned int rand_in_range( unsigned int min, unsigned int max ) ;

//Vector norms
double vect_diff_infty_norm( int numPoints, const double* vect1,
	const double* vect2 ) ;
double vect_discrete_l2_norm( int numPoints, const double* vect ) ;
void vect_infty_norm( int numPoints, const double* vect, InfNorm *norm ) ;
void vect_max_min_val( int numPoints, const double* vect,
	Vector2D *maxMin ) ;
double vect_discrete_h1_seminorm( const GridParameters *grid,
	const double *vect ) ;
double vect_discrete_h1_norm( const GridParameters *grid, const double *vect ) ;
double vect_inner_product( int numPoints, const double *vect1,
	const double *vect2 ) ;
double vect_weighted_inner_product( int numPoints, const double *vec1,
	const double *vec2 ) ;

//Calculates the approximation to the gradient of a given function assuming that
//we are using a regular mesh
//void func_grad_regular_mesh( int numNodes, int numRows, int rNodes, double h,
void func_grad_regular_mesh( const GridParameters *grid, const double *func,
	Vector2D *grad ) ;
	
//Some useful functions
unsigned int factorial( unsigned int n ) ;

//Moves the cursor one line up in a LINUX style terminal
void move_cursor_pos_up( int numLines ) ;

//Prints a dense matrix representation of the sparse matrix to the specified
//file
void print_full_matrix_to_file( const char *fName, const GridParameters *grid,
	const double *sparseMat ) ;
	
//Calculates the magnitudes of the vectors in a two dimensional vector field
void vect_field_magnitude( int numPoints, const Vector2D *vectField,
	double *magnitude ) ;
	
//Finds the minimum x-coordinate of a vertex on a given element
double element_min_x( const Element *e, const Node *nodes ) ;

//Finds the maximum x-coordinate of a vertex on a given element
double element_max_x( const Element *e, const Node *nodes ) ;

//Finds the minimum y-coordinate of a vertex on a given element
double element_min_y( const Element *e, const Node *nodes ) ;

//Finds the maximum y-coordinate of a vertex on a given element
double element_max_y( const Element *e, const Node *nodes ) ;

//Sets the upper and lower bounds of a box containing the given element
void set_element_bounds( const Element *e, const Node *nodes,
	XYBounds *bounds ) ;
	
	
//Initialises the coefficient regions variables to NULL
void initialise_coeff_regs_to_null( CoeffRegions *regs ) ;
//Frees the memory associated with the coefficient regions variable passed in
void free_coeff_regs( CoeffRegions *regs ) ;
/**@}*/	

//We now define a new page on which graphs for data flow diagrams of the main
//functions that are used
/** \page graphPage Data Flow Diagrams for Important Functions
	\section secMGGraph Data Flow Diagram - Multigrid
	The following gives a graphical representation of the multigrid iteration
	(both linear and nonlinear versions), as is executed in the function
	multigrid().
	\dot
		digraph mg {
			center=true ;
			ranksep=0.25 ;
			edge [color=blue4,fontname=FreeSans,fontsize=10] ;
			node [height=0.2,width=0.2,shape=box,fontname=FreeSans,
				fontsize=10] ;
			start [label="Start", style=filled,color=dimgray,rank=source] ;
			end [label="End", style=filled,color=dimgray,rank=sink] ;
			a [label="Initialise Approximation: u^{0}",
				URL="\ref initialise_approx()", tooltip="initialise_approx()"] ;
			b [label="Initialise RHS: f", URL="\ref initialise_rhs()",
				tooltip="initialise_rhs()"] ;
			c [label="Calculate Iteration Matrix: A^{0}",
				URL="\ref calculate_iteration_matrix()",
				tooltip="calculate_iteration_matrix()"] ;
			d [label="Calculate Residual: r^{0} = f - A^{0}u^{0}",
				URL="\ref calculate_residual()",
				tooltip="calculate_residual()"] ;
			e [shape=diamond, label="Is Linear?"] ;
			f [label="Linear Multigrid\n(for System A^{i}u^{i+1} = f",
				URL="\ref linear_multigrid()", tooltip="linear_multigrid()"] ;
			g [label="Nonlinear Multigrid\n(for System A^{i}u^{i+1} = f",
				URL="\ref nl_multigrid()", tooltip="nl_multigrid()"] ;
			h [label="Calculate Iteration Matrix: A^{i+1}",
				URL="\ref calculate_iteration_matrix()",
				tooltip="calculate_iteration_matrix()"] ;
			i [label="Calculate Residual: r^{i+1} = f - A^{i+1}u^{i+1}",
				URL="\ref calculate_residual()",
				tooltip="calcualte_residual()"] ;
			j [shape=diamond, label="Is r^{0}/r^{i+1} < tol?"] ;
			start -> a ;
			a -> b ;
			b -> c ;
			c -> d ;
			d -> e [label=" Set i=0"] ;
			e:w -> f:n [label=" Yes"] ;
			e:s -> g:n [label=" No"] ;
			g -> h ;
			f:s -> i:n [label=" A^{i+1} = A^{i}"] ;
			h:s -> i:n ;
			i -> j:n ;
			j:e -> e:e [label=" No\n Set i=i+1"] ;
			j:s -> end:n [label=" Yes"] ;
		}
	\enddot
	<HR>
	\section secNewtGraph Data Flow Diagram - Newton-Multigrid
	The following gives a graphical representation of the entire
	Newton-Multigrid iteration, as is executed in the function newton_mg().
	\dot
 		digraph newton_mg {
 			center=true ;
 			ranksep=0.25 ;
			edge [color=blue4,fontname=FreeSans,fontsize=10] ;
			node [height=0.2,width=0.2,shape=box,fontname=FreeSans,
				fontsize=10] ;
			start [label="Start", style=filled,color=dimgray,rank=source] ;
			end [label="End", style=filled,color=dimgray,rank=sink] ;
			a [label="Initialise Approximation: u^{0}",
				URL="\ref initialise_approx()",tooltip="initialise_approx()"] ;
			b [label="Initialise RHS: f", URL="\ref initialise_rhs()",
				tooltip="initialise_rhs()"] ;
			c [label="Calc. Iteration Matrix: A^{0}",
				URL="\ref calculate_iteration_matrix()",
				tooltip="calculate_iteration_matrix()"] ;
			d [label="Calc. Residual: r^{0} = f - A^{0}u^{0}",
				URL="\href calculate_residual()",
				tooltip="calculate_residual()"] ;
			e [label="Calc. Jacobian Matrix: J",
				URL="\ref calculate_jacobian_matrix()",
				tooltip="calculate_jacobian_matrix()"] ;
			f [label="Linear Multigrid\n(for System Je = r^{i})",
				URL="\ref linear_multigrid()",tooltip="linear_multigrid()"] ;
			g [label="Update approximation: u^{i+1} = u^{i}+e",
				URL="\ref add_to_vect()", tooltip="add_to_vect()"] ;
			h [label="Calc. Iteration Matrix: A^{i+1}",
				URL="\ref calculate_iteration_matrix()",
				tooltip="calculate_iteration_matrix()"] ;
			i [label="Calc. Residual: r^{i+1} = f - A^{i+1}u^{i+1}",
				URL="\ref calculate_residual()",
				tooltip="calculate_residual()"] ;
			j [shape=diamond,label="r^{0} / r^{i+1} < tol?"] ;
			start -> a ;
			a -> b ;
			b -> c ;
			c -> d ;
			d -> e [label=" i=0"] ;
			e -> f [label="Set e=0 as initial estimate"] ;
			f -> f [label=" Until convergence criterion met"] ;
			f -> g ;
			g -> h ;
			h -> i ;
			i:s -> j:n ;
			j:e ->e:e [label=" No\n Set i = i+1"] ;
			j -> end [label=" Yes"] ;
		}
 	\enddot
 	<HR>
	\section secLinGraph Data Flow Diagram - Linear Multigrid
	The following gives a graphical representation of a single linear multigrid
	iteration, as is executed in function linear_multigrid().
	\dot
		digraph linear_multigrid {
			center=true ;
 			ranksep=0.25 ;
			edge [color=blue4,fontname=FreeSans,fontsize=10,samehead=true] ;
			node [height=0.2,width=0.2,shape=box,fontname=FreeSans,fontsize=10] ;
			start [label="Start\nwith approximation u_{h},\nright hand side f_{h} and\noperator A_{h} given",
				style=filled,color=dimgray,rank=source] ;
			end [label="End",color=dimgray,rank=sink] ;
			a [label="Pre-Smooth: u_{h} = S_{h}u_{h}", URL="\ref smooth()",
				tooltip="smooth()"] ;
			b [label="Calculate Residual: r_{h} = f_{h} - A_{h}u_{h}",
				URL="\ref calculate_residual()", tooltip="calculate_residual"] ;
			c [label="Restrict Residual: r_{2h} = I_{h}^{2h}r_{h}",
				URL="\ref restriction()", tooltip="restriction()"] ;
			d [label="Calculate Iteration Matrix: A_{2h}",
				URL="\ref calculate_iteration_matrix()",
				tooltip="calculate_iteration_matrix()"] ;
			e [shape=diamond, label="On Coarsest Grid?"] ;
			f [label="Solve\n(for System A_{2h}e_{2h} = r_{2h})",
				URL="\ref gs_to_converge()", tooltip="gs_to_converge()"] ;
			g [label="Linear Multigrid\n(for System A_{2h}e_{2h} = r_{2h})",
				URL="\ref linear_multigrid()", tooltip="linear_multigrid()"] ;
			h [label="Prolongate Error: e_{h} = I_{2h}^{h}e_{h}",
				URL="\ref prolongate()", tooltip="prolongate()"] ;
			i [label="Update Approximation: u_{h} = u_{h} + e_{h}"] ;
			j [label="Post-Smooth: u_{h} = S_{h}u_{h}", URL="\ref smooth()",
				tooltip="smooth()"] ;
			
			start -> a ;
			a -> b ;
			b -> c ;
			c -> d ;
			d -> e:n ;
			e:w -> f:n [label=" Yes"] ;
			e:e -> g:n [label=" No\n Set e_{2h} = 0 initially"] ;
			f:s -> h:n ;
			g:s -> h:n ;
			h -> i ;
			i -> j ;
			j -> end ;
		}
	\enddot
	<HR>
	\section secNLGraph Data Flow Diagram - Nonlinear Multigrid
	The following gives a graphical representation of a single nonlinear
	multigrid iteration, as is executed in function nl_multigrid(). The
	nonlinear multigrid calculates an updated approximation to \f$u_{h}\f$ for
	the system \f$A_{h}u_{h} = f_{h}\f$. The iteration matrix \f$A_{h}\f$ is
	not given, as this depends on the approximation, and so is updated when the
	approximation is changed
	\dot
 		digraph nl_multigrid {
 			center=true ;
 			ranksep=0.25 ;
			edge [color=blue4,fontname=FreeSans,fontsize=10,samehead=true] ;
			node [height=0.2,width=0.2,shape=box,fontname=FreeSans,
				fontsize=10] ;
			start [label="Start\nwith approximation u_{h} and\nright hand side f_{h} given",
				style=filled,color=dimgray,rank=source] ;
			end [label="End"style=filled,color=dimgray,rank=sink] ;
			a [label="Pre-Smooth: u_{h} = S_{h}u_{h}", URL="\ref smooth()",
				tooltip="smooth()"] ;
			b [label="Calculate Iteration Matrix: A_{h}",
				URL="\ref calculate_iteration_matrix()",
				tooltip="calculate_iteration_matrix()"] ;
			c [label="Calculate Residual: r_{h} = f_{h} - A_{h}u_{h}",
				URL="\ref calculate_residual()",
				tooltip="calculate_residual()"] ;
			d [label="Restrict Approximation: u_{2h} = I_{h}^{2h}u_{h}",
				URL="\ref restriction()", tooltip="restriction()"] ;
			e [label="Restrict Residual: r_{2h} = I_{h}^{2h}r_{h}",
				URL="\ref restriction()", tooltip="restriction()"] ;
			f [label="Calculate Iteration Matrix: A_{2h}",
				URL="\ref calculate_iteration_matrix()",
				tooltip="calculate_iteration_matrix()"] ;
			g [label="Calculate RHS: f_{2h} = r_{2h} + A_{2h}u_{2h}"] ;
			h [label="On Coarsest Grid?",shape=diamond] ;
			j [label="Solve\n(for System A_{2h}v_{2h} = f_{2h})",
				URL="\ref nl_gauss_seidel_to_converge()",
				tooltip="nl_gauss_seidel_to_converge()"] ;
			k [label="Nonlinear Multigrid\n(for System A_{2h}v_{2h} = f_{2h})",
				URL="\ref nl_multigrid()", tooltip="nl_multigrid()"] ;
			l [label="Calculate Error: e_{2h} = v_{2h} - u_{2h}"] ;
			m [label="Prolongate Error: e_{h} = I_{2h}^{h}e_{2h}",
				URL="\ref prolongate()", tooltip="prolongate()"] ;
			n [label="Update Approximation: u_{h} = u_{h} + e_{h}"] ;
			o [label="Post Smooth: u_{h} = S_{h}u_{h}", URL="\ref smooth()",
				tooltip="smooth()"] ;
			{rank="same, j, k"} ;
			start -> a ;
			a -> b ;
			b -> c ;
			c -> d ;
			d -> e ;
			e -> f ;
			f -> g ;
			g:s -> h:n ;
			h:w -> j:n [label=" Yes"] ;
			h:e -> k:n [label=" No\n Set v_{2h} = u_{2h} initially"] ;
			j:s -> l:n ;
			k:s -> l:n ;
			l -> m ;
			m -> n ;
			n -> o ;
			o -> end ;
 		}
 	\enddot
*/

#endif
