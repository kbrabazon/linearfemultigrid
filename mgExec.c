#define INLINE
#include "test_cases.h"
#include "nl_multigrid.h"
#include "richards_eq.h"
#include "useful_definitions.h"

/** Entry point for the program
 */
int main( int argc, char** argv )
{
	int i ; //Loop counter
	AlgorithmParameters params ;// Parameters that define how the algorithm is
								// to be executed
	GridParameters **grids ; //Array of pointers to the grids in the hierarchy
	ApproxVars **aVarsList ; //Array of pointers to the approximation variables
							 //in the grid hierarchy
							 
	GridParameters *fineGrid ; //Pointer to the finest grid in the hierarchy
	ApproxVars *fVars ; //Variables related to the approximation on the finest
					   //grid level
	Results results ; //Stores information about the results of the multigrid
					  //iteration
	InfNorm infNorm ; //Stores information about the infinity norm of a vector
	const Node *node ; //A node on the grid
					  
	int rNodes ;//The number of nodes in a row on the interior of the grid we
				//are currently working on. This is calculated by assuming that
				//the grid is structured, regular and square
	double h ;//The grid spacing on the grid that we are currently operating on
						
	char *strSol ; //String representation of the exact solution
	double *sol ; //Double containting the value of the exact solution (if it is
				  //known) on the finest grid level
	double *err ; //The error in approximation, if the exact solution is known
						
	const char* paramsFile ;//The file name of the parameter file to read in
							//from
	
	clock_t start, end ; //Used to calculate the run time of the algorithm
	
	const char **meshes ; //Placholder to the mesh in the current hierarchy
	char outFName[100] ; //Filename to print output to
	Vector2D *deriv ; //Approximation to the derivative of the final approximation
					
	//Here I am assuming that the parameters are given in a correct format, and
	//that the second argument is the name of a file which contains parameters			
	if( argc >= 2 ) paramsFile = argv[1] ;
	else paramsFile = PARAMS_IN_FILE ;
	
	//Read in the grid parameters from a parameters file. The filename is
	//defined by PARAMETERS_IN_FILE in useful_definitions.h
	if( !read_alg_params( &params, paramsFile ) ){
		printf( "Error reading parameters from file\n" ) ;
		return 0 ;
	}
	
	//If we are to perform testing we do this without checking the validity of 
	//the parameters, as these may not affect the test case. Return after
	//having run the test
	if( params.testing ){
		test( &params ) ;
		return 0 ;
	}
	
	//Validate the parameters read in from file. If they are not valid return
	//from the program
	if( !validate_alg_params( &params ) )
		return 0 ;
		
	//Set up the placeholder for the meshes to be used
	meshes = get_grid_hierarchy( &params ) ;
	
	//Seed the random number generator
	srand( time( NULL ) ) ;
	//srand( 1366553037 ) ;
	
	//Start the clock
	start = clock() ;
	
	//Inititalise the grids that we will need in this run to NULL pointers
	grids = (GridParameters**) malloc( params.fGridLevel *
		sizeof( GridParameters* ) ) ;
	initialise_grids_to_null( grids, &params ) ;
	
	//Initialise the approximation variables that we will need in this run to
	//NULL pointers
	aVarsList = (ApproxVars**) malloc( params.fGridLevel *
		sizeof( ApproxVars* ) ) ;
	initialise_approx_vars_to_null( aVarsList, &params ) ;
	
	//Set up the finest grid
	grids[params.fGridLevel-1] = (GridParameters*)malloc(
		sizeof( GridParameters ) ) ;
	init_grid( grids[params.fGridLevel -1], &params, 
		meshes[params.fGridLevel - 1] ) ;
		
	//Check that the boundary type of the grid that is read in matches the
	//expected boundary conditions for the test case
	if( !check_boundary_type( grids[params.fGridLevel-1], &params ) ){
		//Clean up and exit if the boundary types do not match
		clean_up( grids, aVarsList, &params ) ;
		return 0 ;
	}
	
	//Set up all of the grids and the approximation variables on those grids
	//Set up all of the grids on which we are operating
	for( i=params.fGridLevel-1 ; i >= params.cGridLevel-2 ; i-- ){
		params.currLevel = i + 1 ;
		if( grids[i] == NULL ){
			//Assign the appropriate memory
			grids[i] = (GridParameters*)malloc( sizeof( GridParameters ) ) ;
			//Set up the grid
			init_grid( grids[i], &params, meshes[i] ) ;
		}
		if( aVarsList[i] == NULL ){
			//Assign the appropriate memory
			aVarsList[i] = (ApproxVars*) malloc( sizeof( ApproxVars ) ) ;
			init_approx_vars( aVarsList[i], grids[i], &params ) ;
		}
	}
	params.currLevel = params.fGridLevel ;
	
	//If the test case uses a piecewise constant coefficient on the domain
	//calculate the appropriate values by restricting the function from fine
	//to coarser grids
	if( has_pc_coeff( params.testCase ) ){
		//Loop through the grid hierarchy and restrict the coefficient function
		//from fine to coarse grids
		for( i = params.fGridLevel-1 ; i >= params.cGridLevel-1 ; i-- )
			restrict_pc_coeff( &params, grids[i], grids[i-1],
				aVarsList[i]->pcCoeff, aVarsList[i-1]->pcCoeff ) ;
	}
	
		
	//If the test case is time dependent, then perform a time dependent solve,
	//and return
	if( is_time_dependent( params.testCase ) ){
		//Make sure that we are not performing FMG
		params.isFMG = false ;
		
		//Solve the time dependent problem
		if( is_richards_eq( params.testCase ) )
			solve_richards_eq( &params, grids, aVarsList ) ;
		else
			time_dependent_solve( &params, grids, aVarsList ) ;
		
		end = clock() ;
		printf( "Total running time: %.7lf\n",
			((float)(end - start))/CLOCKS_PER_SEC ) ;
		
		//Free the memory allocated and exit
		clean_up( grids, aVarsList, &params ) ;
		return 0 ;
	}
		
	//Set a placeholder for the finest grid to make the code look neater
	fineGrid = grids[params.fGridLevel-1] ;
	
	//set a placholder for the ApproxVars structure on the current grid level
	fVars = aVarsList[params.fGridLevel-1] ;
	
	//Check if we are to perform FMG to get an initial approximation
	if( params.isFMG ){
		//This is to tell the multigrid functions not to set up an initial
		//approximation, as it will be given by the result of FMG
		params.setUpApprox = 0 ;
		fmg( grids, &params, aVarsList, &results ) ;
		//We now no longer want to perform FMG
		params.isFMG = false ;
		
		//Print the approximation gained from FMG to file to see what we got
		if( params.debug ){
			print_grid_func_to_file( "/tmp/fmgApprox",
				grids[params.fGridLevel-1], fVars->approx, 0 ) ;
		}
		
		//The initial approximation is stored in 'fVars', which will be used in
		//the multigrid iterations below
	}
			
	//Perform the appropriate multigrid method
	if( is_non_linear(params.testCase) && params.nlMethod == METHOD_NEWTON_MG ){
		//Newton multigrid
		newton_mg( grids, &params, aVarsList, &results ) ;
	}
	else{
		//Nonlinear or linear multigrid
		multigrid( grids, &params, aVarsList, &results ) ;
	}
		
	//We have finished MG, so now we want to print some information about what
	//happened
	
	//End the clock
	end = clock() ;
	
	//If we know the solution then print information about the error
	if( is_solution_known( params.testCase ) ){
		strSol = (char*) calloc( 200, sizeof( char ) ) ;
		sol = (double*) calloc( fineGrid->numUnknowns, sizeof( double ) ) ;
		err = (double*) calloc( fineGrid->numUnknowns, sizeof( double ) ) ;
		
		//Get the relevant information about the exact solution
		solution_as_string( &params, strSol ) ;
		calculate_exact_solution( &params, fineGrid, sol ) ;
		
		//Calculate the error
		vect_diff( fineGrid->numUnknowns, sol, fVars->approx, err ) ;
		
		if( params.debug ){
			sprintf( outFName, "/tmp/exactSolLvl%d.txt", params.fGridLevel ) ;
			print_grid_func_no_bnd_to_file( outFName,
				grids[params.fGridLevel-1], sol, 0 ) ;
		}
		
		//Don't need the exact solution any more
		free( sol ) ;
		
		printf( "Exact solution: %s\n  ", strSol ) ;
		vect_infty_norm( fineGrid->numUnknowns, err, &infNorm ) ;
		node = &(fineGrid->nodes[ fineGrid->mapA2G[ infNorm.ind ] ]) ;
		printf( "Infinity norm of the error: %.18lf at point (%.5lf, %.5lf)\n",
			infNorm.val, node->x, node->y ) ;
		printf( "  L2 norm of the error: %.18lf\n",
			vect_discrete_l2_norm( fineGrid->numUnknowns, err ) ) ;
			
		//Find the number of nodes in a row on the interior of the grid, and the
		//regular grid spacing on the current grid so that we can calculate the
		//H1 norm
		rNodes = sqrt( fineGrid->numUnknowns ) ;
		h = fineGrid->nodes[1].x - fineGrid->nodes[0].x ;
		printf( "  H1 norm of the error: %.18lf\n",
			vect_discrete_h1_norm( fineGrid, err ) ) ;
		
		//Clean up
		free( strSol ) ;
		free( err ) ;
	}
	
	//Print out information that can be known about every experiment, whether
	//the analytic solution is known or not
	printf( "Number of iterations performed: %d\n", results.itCnt );
	
	if( results.residL2Norm / results.origResidL2Norm > TOL1 &&
		results.itCnt == params.maxIts )
		printf( "Maximum number of iterations reached without convergence\n" ) ;
	else if( results.residL2Norm >= MAX_RESID_FACT * results.origResidL2Norm ){
		printf( "Approximation is too far from solution. This means either " ) ;
		printf( "the initial approximation is deemed to be too poor, or we " ) ;
		printf( "are diverging\n" ) ;
	}
	
	//Print the execution time to the console
	printf( "Execution time: %.7lf\n", ((float)(end - start))/CLOCKS_PER_SEC ) ;
	
	//Print the final approximation to file
	sprintf( outFName, "/tmp/finalApproxLvl%d.txt", params.fGridLevel ) ;
	print_grid_func_to_file( outFName, grids[params.fGridLevel-1],
		fVars->approx, 0 ) ;
		
	if( params.testCase == 13 ){
		//Approximate the derivative of the approximation, and print out to
		//file
		deriv = (Vector2D*)malloc( fineGrid->numUnknowns *
			sizeof( Vector2D ) ) ;
		//Calculate the derivative
		func_grad_regular_mesh( fineGrid, fVars->approx, deriv ) ;
		sprintf( outFName, "/tmp/finalGradLvl%d.txt", params.fGridLevel ) ;
		print_vector_field_to_file( outFName, fineGrid, deriv, 0 ) ;
		
		free( deriv ) ;
	}
	
		
	//Free the memory
	clean_up( grids, aVarsList, &params ) ;
	
	return 0 ;
}


