#!/usr/local/bin/python3

import argparse
import subprocess
from os import path, fsync

#This file is used to run a certain amount of runs of the multigrid algorithm.
#This is going to change each time, and this should be used as an easy way in
#which many iterations can be run. I first want a function that can change the
#value of a variable in the parameters file, which is given as a command line
#argument

#Define colors to be used to change the output font
class bcolors:
	BOLD = '\033[01m'
	BOLD_U = '\033[01;04m'
	BLACK_BACK_B = '\033[01;07m'
	RED_B = '\033[01;31m'
	ENDC = '\033[0m'
#### End definition of class 'bcolors'

#This updates a parameters file with filename 'fname', with the lines stored
#in 'lines', and values of parameters stored in 'params'
def update_parameters_file( fname, lines, params ):
	#Open the parameters file for writing
	fout = open( fname, 'w' )
	#For every line
	for line in lines:
		#If the line is a new line or a comment then print it as it is
		if( line[0] == '#' or line[0] == '\n' ):
			fout.write( line )
		else:
			#If we have a parameter value then output the updated value stored
			#in the 'params' dictionary
			key = line.strip().split(':')[0]
			fout.write( '{0}:{1}\n'.format( key, params[key] ) )
		
	#Close the file
	fout.close()
	
#Indicator whether recovery is being performed
def perform_recover( params ):
	testCase = int( params["TEST_CASE"] )
	if( int( params["RECOVER_GRAD"] ) != 0 and (testCase == 2 or
	    testCase == 4 or testCase == 6 ) ):
		return True
	return False
#### End function 'perform_recover'

#This generates an appropriate output filename for the given parameters
def generate_out_fname( params, folder, ignore_repeat ):
	cnt = 1
	#Replace decimal points in the alpha parameter with a 'p'
	alpha = str( params["ALPHA"] ).replace( '.', 'p' )
	#Find out which test case we are working with
	test_case = str( params["TEST_CASE"] )
	#Find out the smooth weight that we are using
	smooth_weight = str( params["SMOOTH_WEIGHT"] ).replace( '.', 'p' )
	#Get the number of pre-smooths
	pre = str( params["PRE_SMOOTH"] )
	#Get the number of post-smooths
	post = str( params["POST_SMOOTH"] )
	#Get the number of inner iterations for a Newton-MG experiment
	inner_its = str( params["INNER_ITS"] )
	if( folder == '' ):
		folder = 'results'
	else:
		folder = 'results/{}'.format( folder )
	#Find out which nonlinear method is being used to solve the problem
	if( params["NL_METHOD"] == 0 ):
		method = 'nmlm'
		#Replace decimal points in the sigma parameter with a 'p'
		sigma = str( params["SIGMA"] ).replace( '.', 'p' )
		#Form the file name
		if( not perform_recover( params ) ):
			fname = '{}/{}_tc{}_a{}_s{}_sw{}_pre{}_post{}.txt'.format( folder, method,
			          test_case, alpha, sigma, smooth_weight, pre, post )
		else:
			fname = '{}/{}_tc{}_a{}_s{}_sw{}_pre{}_post{}_rec.txt'.format( folder, method,
			          test_case, alpha, sigma, smooth_weight, pre, post )
	else:
		method = 'nmg'
		#Replace decimal poinst in the newton damping parameter with 'p'
		nfact = str( params["NEWTON_DAMP"] ).replace( '.', 'p' )
		#Form the file name
		fname = '{}/{}_inner{}_tc{}_a{}_n{}_sw{}_pre{}_post{}.txt'.format(
		        folder, method, inner_its, test_case, alpha, nfact,
		        smooth_weight, pre, post )
	#If the file exists then return an empty value, if we are not ignoring repeats.
	#If we are ignoring repeated files then return the value that was calculated
	#before
	if( not ignore_repeat ):
		if( path.isfile( fname ) ):
			return ''
	return fname

def print_params_to_file( outfile, params ):
	outfile.write( 'Algorithm parameters used: (Note that the grid level is not' +
		' listed as this value changes here)\n' )
	for key in iter( params ):
		if( key != "FINEST_GRID" ):
			outfile.write( "{0}:{1}\n".format( key, params[key] ) )
	outfile.write( '\n' )
	outfile.write( '************************************************\n' )
	outfile.write( ( 'Initial approximation: \n\n' + 
	    'sin(pi x)sin( pi y )\n\n' ) )
#		'0.7 * sin( pi * x ) * sin( pi * y ) + h\\vect{r}\n\n' ) )
	outfile.write( '************************************************\n\n' )
#### End definition of function 'print_params_to_file'

def run_mg_on_grids( params, args, lines ):
	#Generate an appropriate filename to print the output to
	fname = generate_out_fname( params, args.resfolder, args.append )
	
	#Formulate an appropriate string to print to the console to demonstrate
	#which test case is being run
	if( params["NL_METHOD"] == 0 ): #NMLM
		printStr = ( bcolors.BOLD + "  Alpha:" + bcolors.RED_B + 
		    " {},  ".format( params["ALPHA"] ) + bcolors.ENDC )
		printStr = ( printStr + bcolors.BOLD + "Sigma:" + bcolors.RED_B +
		    " {},".format( params["SIGMA"] ) + bcolors.ENDC )
	else:
		printStr = ( bcolors.BOLD + "  Alpha:" + bcolors.RED_B +
			" {},  ".format( params["ALPHA"] ) + bcolors.ENDC )
		printStr = ( printStr + bcolors.BOLD + "Newton Factor:" + bcolors.RED_B +
			" {},".format( params["NEWTON_DAMP"] ) + bcolors.ENDC )
	printStr = (printStr + "  " + bcolors.BOLD + "SW:" + bcolors.RED_B +
	    " {}".format( params["SMOOTH_WEIGHT"] ) + bcolors.ENDC )
	printStr = (printStr + bcolors.BOLD + "  Pre(Post):" + bcolors.RED_B +
	    " {}({})".format( params["PRE_SMOOTH"], params["POST_SMOOTH"] ) +
	    bcolors.ENDC )
	
	#If an empty string is returned it means that the file already exists
	#and that we don't want to overwrite it. Notify the user by printing
	#output to the console
	if( fname == '' ):
		print( printStr + bcolors.RED_B + " - File exists for test case" +
			bcolors.ENDC )
		return
	lvlcnt = 0
	
	#Formulate an appropriate string to print to the console to demonstrate
	#which test case is being run
	printstr = printStr + ',  ' + bcolors.BOLD + 'Level: ' + bcolors.RED_B
	
	#Loop through the coarse grid levels
	for clvl in args.clvls:
		#Set the coarsest grid level that we are working on
		params["COARSEST_GRID"] = clvl
	
		#If this is the first iteration then print the parameters to the
		#output file
		if( lvlcnt == 0 ):
			if( args.append ):
				wmode = 'a'
			else:
				wmode = 'w'
			outfile = open( fname, wmode )
			print_params_to_file( outfile, params )
			outfile.close()
	
		#Write the coarse grid level to file
		outfile = open( fname, 'a' )
		outfile.write( "Coarse Grid Level: {0}".format( clvl ) )
		if( params["NL_METHOD"] == 1 ):
		    outfile.write( "  Inner Iterations: {}\n".format( params["INNER_ITS"] ) )
		else:
			outfile.write( "\n" )
		outfile.close()
	
		#Print the coarse grid level to the screen
		if( params["NL_METHOD"] == 0 ):
			print( bcolors.BOLD_U + "Coarse Grid Level:" + bcolors.ENDC + " " +
				bcolors.RED_B + "{0}".format( clvl ) + bcolors.ENDC )
		else:
			print( bcolors.BOLD_U + "Coarse Grid Level:" + bcolors.ENDC + " " +
				bcolors.RED_B + "{0}  ".format( clvl ) + bcolors.ENDC +
				bcolors.BOLD_U + "Inner Iterations:" + bcolors.ENDC + bcolors.RED_B +
				" {}".format( params["INNER_ITS"] ) + bcolors.ENDC )
		
		for lvl in args.lvls:
			#Check that the coarsest grid level is not larger than the
			#finest grid level
			if( clvl > lvl ):
				continue
			print( printstr + str( lvl ) + bcolors.ENDC )
			#Set the finest level that we are working on in the parameters
			#object
			params["FINEST_GRID"] = lvl
			#Update the parameters file with the current parameters
			update_parameters_file( args.fname, lines, params )

			#Open the file for appending
			outfile = open( fname, 'a' )
			#Print the grid level, and flush the output to make sure that
			#the output to the file is printed in the correct order
			outfile.write( 'Grid Level: {0}\n'.format( lvl ) )
			outfile.flush()
			#Sync the file so that we are reading the most up to date version
			fsync( outfile )
			#Call the multigrid method, and divert the output to the output
			#file
			subprocess.check_call( ["./runMe", args.fname], stdout=outfile )
			outfile.write( '\n' )
			#Flush the outptu to make sure that it has been written before
			#we continue
			outfile.flush()
			#Sync the file so  that wer are reading the most up to date version
			fsync( outfile )
			#Close the file
			outfile.close()
			#Increment the level count
			lvlcnt = lvlcnt+1
		#Print a blank line to console so that we can see where a run has
		#been completed for one set of parameters for several grid levels
		print( '' )
#### End definition of function 'run_mg_on_grids'

def run_mg_for_post_smooth( params, args, lines ):
	#We now loop through the sigmas / newton factors
	if( params["NL_METHOD"] == 0 ):
		#Loop through the values of sigma, and update the parameters
		for sigma in args.sigmas:
			params["SIGMA"] = sigma
			#Now run multigrid on the different grids
			run_mg_on_grids( params, args, lines )
	else:
		#Loop through the values of the newton factors, and update the
		#parameters
		for nfact in args.nfacts:
			params["NEWTON_DAMP"] = nfact
			#Now run multigrid on the different grids
			run_mg_on_grids( params, args, lines )
#### End defintion of function 'run_mg_for_post_smooth'

def run_mg_for_pre_smooth( params, args, lines ):
	#All we need to do is set the number of post smooths, and run for that value
	for post in args.post:
		params["POST_SMOOTH"] = post
		if( args.fmg and args.fmgpost == 0 ):
			params["FMG_PRE_SMOOTH"] = post
		run_mg_for_post_smooth( params, args, lines )
#### End definition of function 'run_mg_for_pre_smooth'
	
#Runs the multigrid iteration for the smoothing factor currently stored in
#params
def run_mg_for_smooth_factor( params, args, lines ):
	#All we need to do is set the number of pre smooths and run for that value
	for pre in args.pre:
		params["PRE_SMOOTH"] = pre
		if( args.fmg and args.fmgpre == 0 ):
			params["FMG_PRE_SMOOTH"] = pre
		if( args.preposteq ):
			params["POST_SMOOTH"] = pre
			if( args.fmg and args.fmgpre == 0 ):
				params["FMG_POST_SMOOTH"] = pre
			run_mg_for_post_smooth( params, args, lines )
		else:
			run_mg_for_pre_smooth( params, args, lines )
#### End definition of function 'run_mg_for_smooth_factor'

#Runs multigrid iterations for the given number of inner iterations per Newton
#step. This only changes the running of the program for the Newton-MG algorithm
def run_mg_for_inner_its( params, args, lines ):
	#Check what method we are using
	if( params["NL_METHOD"] == 0 ): #NMLM
		#Give a default value to the number of inner iterations so that we can
		#print them out
		params["INNER_ITS"] = 1
		run_mg_for_smooth_factor( params, args, lines )
	else: #Newton-MG
		for its in args.inner_its:
			params["INNER_ITS"] = its
			run_mg_for_smooth_factor( params, args, lines )
#### End definition of function 'run_mg_for_inner_its'

#Runs the multigrid iteration for the alpha currently stored in params
def run_mg_for_alpha( params, args, lines ):
	for factor in args.sfacts:
		params["SMOOTH_WEIGHT"] = factor
		run_mg_for_inner_its( params, args, lines )
#### End definition of function 'run_mg_for_alpha'

#Define how to parse the input arguments, and read appropriate data from the command line
parser = argparse.ArgumentParser(description='Process parameters with which to run multigrid.')

#The file to read parameters from
parser.add_argument( 'fname', metavar='parameter_file', action='store',
                   help='The filename of the file to read in parameters from' )
#The test case to run the method for
parser.add_argument( 'tc', metavar='test_case', action='store', type=int,
                   help='The test case to run multigrid for' )
#The value of alpha to use. This changes the size of the nonlinearity
parser.add_argument( '--alphas', action='store', type=float, nargs='+',
                   default=[0.0], help='List of alpha values to use' )
#The nonlinear method to use when running the algorithm
parser.add_argument( '--method', action='store', type=int, default=0, choices=range(0,2),
                   help='Nonlinear method to run' )
#The various levels on which to run the multigrid algorithm. The default behaviour
#is to run on all the possible grid levels
parser.add_argument( '--lvls', action='store', type=int, nargs='+', default=list(range(2,11)),
                   choices=range(2,11), help='List of finest levels to run the algorithm for' )
#The various coarsest levels on which the multigrid algorithm will solve the
#problem directly. The default is to only solve on grid level 2
parser.add_argument( '--clvls', action='store', type=int, nargs='+', default=[2],
                   choices=range(2,10), help='List of coarsest levels to run the algorithm for' )
#The number of multigrid iterations to perform on each grid level. E.g. 1 is a
#V-cycle
parser.add_argument( '--itype', action='store', type=int, default=1,
                   help='Number of multigrid iterations to perform on each grid level,' +
                   ' i.e. 1 is a V-Cycle, 2 a W-Cycle, etc.' )
#The smoothing method to perform
parser.add_argument( '--smooth', action='store', type=int, default=0, choices=range(0,6),
                   help='Type of smoothing iteration to use. For a definition of ' +
                   'what integer is identified with which method see ''useful_definitions.h''' )
#The number of pre-smoothing iterations to perform
parser.add_argument( '--pre', action='store', nargs='+', type=int, default=[2],
                   help='Number of pre-smoothing iterations to perform' )
#The number of post-smoothing iterations to perform
parser.add_argument( '--post', action='store', nargs='+', type=int, default=[2],
                   help='Number of post-smoothing iterations to perform' )
#Indicator as to whether to set the post-smoothing equal to the pre-smoothing.
#The default is to have this off
parser.add_argument( '--preposteq', action='store_true',
                   help='Indicates whether to set the number of post-' +
                   ' smoothing iterations equal to the number of pre-' +
                   ' smoothing iterations' )
#The number of pre-smoothing iterations to perform in FMG
parser.add_argument( '--fmgpre', action='store', type=int, default=0,
                   help='Number of pre-smoothing iteratinos to perform in a ' +
                   'FMG iteration. If this is set to zero the number of ' +
                   'pre-smooths to perform is taken to be the number of ' +
                   'pre-smooths performed in the multigrid iteration' )
#The number of post-smoothing iterations to perform in FMG
parser.add_argument( '--fmgpost', action='store', type=int, default=0,
                   help='Number of post-smoothing iterations to perform in a' +
                   ' FMG iteration. If this is set to zero the number of ' +
                   'post-smooths to perform is taken to be the number of ' +
                   'post-smooths performed in the multigrid iteration' )
#The smoothing factor
parser.add_argument( '--sfacts', action='store', type=float, nargs='+', default=[0.8],
                   help='The smoothing weights to use' )
#The values of sigma to use in the NMLM
parser.add_argument( '--sigmas', action='store', type=float, nargs='+',
                   default=[1.0], help='The values of sigma to use in the NMLM' )
#The weighting factors to use in the Newton method
parser.add_argument( '--nfacts', action='store', type=float, nargs='+',
                   default=[1.0], help='The weighting factors to use in the N-MG' )
#The amplitude of the smooth component of the solution for test cases 6 and 7
parser.add_argument( '--samp', action='store', type=float, default=1.0,
                   help='Amplitude of the smooth component of the exact ' +
                   'solution. This is only used for test cases 6 and 7' )
#The amplitude of the oscillatory component of the solution for test cases 6
#and 7
parser.add_argument( '--oamp', action='store', type=float, default=0.0,
                   help='Amplitude of the oscillatory component of the exact ' +
                   'solution. This is only used for test cases 6 and 7' )
#The frequency of the x-component of the oscillatory component of the solution
#for test cases 6 and 7
parser.add_argument( '--xfreq', action='store', type=float, default=5.0,
                    help='Frequency of the x-component of the oscillatory ' +
                    'mode of the exact solution. This is only used for test ' +
                    'cases 6 and 7' )
#The frequency of the y-componnet of the oscillatory component of the solution
#for test cases 6 and 7
parser.add_argument( '--yfreq', action='store', type=float, default=5.0,
                   help='Frequency of the y-component of the oscillatory ' +
                   'mode of the exact solution. This is only used for test ' +
                   'cases 6 and 7' )
#The maximum number of iterations of multigrid to perform before terminating
#execution
parser.add_argument( '--max_its', type=int, action='store', default=20,
                   help='The maximum number of iterations to perform before ' +
                   'terminating execution' )
#Indicates whether or not to perform recover of smoothness of functions (if it
#is possible for the given test case
parser.add_argument( '--pr', action='store_true',
                   help='Indicates whether or not to recover smoothness of ' +
                   'functions, if it is possible for the given test case. ' +
                   'The default is false' )
#Indicator whether or not to append to the end of output files or to overwrite
#them
parser.add_argument( '--append', action='store_true',
                   help='Indicates whether or not to append output to output ' +
                   'files. The default is false' )

#Name of a folder where to put the results files. This is a folder which will
#be searched for / added in the path '~/multigridCode/results/'
parser.add_argument( '--resfolder', action='store', default='',
                   help='Name of a folder in which to put the results files ' +
                   'for the current test run' )
                   
#Indicator as to whether an FMG iteration should be run to gain an initial
#approximation
parser.add_argument( '--fmg', action='store_true',
                   help='Indicates whether or not to run an FMG iteration to ' +
                   'gain an initial approximation on the finest grid' )
                   
#Number of inner iterations to run per Newton step for the Newton-MG algorithm
parser.add_argument( '--inner_its', action='store', type=int, nargs='+',
                   default=[1], help='The number of iterations of linear ' +
                   'multigrid to run per Newton step in Newton-MG' )
#Type of linear iterative method to use for the inner iteration
parser.add_argument( '--lin_iteration', action='store', type=int, default=0,
                    choices=range(0,3),
                    help='The type of iteration to perform for the inner loop' +
                    ' of a Newton method. See useful_definitions.h for a ' +
                    'definition of which integers represent which method' )
parser.add_argument( '--adaptive', action='store_true',
                   help='Indicates whether or not the update step for the ' +
                   'nonlinear methods should be adaptive or not. This means ' +
                   'that the Newton factor, in the case of Newton multigrid, ' +
                   'and the value of sigma are calculated to try and improve ' +
                   'the convergence behaviour' )
                   
args = parser.parse_args()
print( "\n" + bcolors.BLACK_BACK_B + str(args) + bcolors.ENDC + "\n" )

#The arguments have now been read in, and we want to perform the multigrid iteration
#several times for the different values of the methods

#First thing to do is to read in values from the file given
infile = open( args.fname, 'r' )

#Read the entire file into an object
lines = infile.readlines()

#Close the file
infile.close()

#Declare a dictionary to store the values of the different parameters
params = {}

#For each line in the file
for line in lines:
	#If the line is not a comment or a new line, then assume that it has the
	#appropriate format and store the value of the object in the dictionary
	if( line[0] != '\n' and line[0] != '#' ):
		kv_pair = line.strip().split(':')
		params.update( {kv_pair[0]:kv_pair[1]} )

#Set the values of the parameters that are not going to change (i.e. the
#input variables that are not lists)
params["TEST_CASE"] = args.tc
params["NL_METHOD"] = args.method
params["SMOOTH_TYPE"] = args.smooth
params["NUM_MG_CYCLES"] = args.itype
params["MAX_ITS"] = args.max_its
params["SOL_SMOOTH_AMP"] = args.samp
params["SOL_OSC_AMP"] = args.oamp
params["SOL_X_FREQ"] = args.xfreq
params["SOL_Y_FREQ"] = args.yfreq
params["FMG_PRE_SMOOTH"] = args.fmgpre
params["FMG_POST_SMOOTH"] = args.fmgpost
params["NEWTON_INNER_IT"] = args.lin_iteration
params["ADAPT_UPDATE"] = int( args.adaptive )
if( args.fmg ):
	params["FMG"] = 1
else:
	params["FMG"] = 0
if( args.pr ):
	params["RECOVER_GRAD"] = 1
else:
	params["RECOVER_GRAD"] = 0

#This is the writing mode for the given file. We either overwrite or append
#depending on the value of the 'args.append' variable
wmode = 'w'
if( args.append ):
	wmode = 'a'
	
#Right, this is the order in which I am going to run the loops:
#1.) Loop through Alphas
#2.) Loop through the smoothing weights
#3.) Loop through the pre-smooths
#4.) Loop through the post-smooths
#5.) Loop through Sigmas / Newton Facts
#6.) Loop through the coarse grids
#7.) Loop through the fine grids. Run the program here

for alpha in args.alphas:
	params["ALPHA"] = alpha
	run_mg_for_alpha( params, args, lines )










