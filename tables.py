#!/usr/local/bin/python3

#This file will contain all of the functions that output formatted results in
#LaTeX tables. The input format for the files is very specific

#Import the different modules that are required
import experiment

#This is the function that should be called to output the data that we want.
#There will be different functions defined in this file with different outputs,
#but from outside this file only this function should be called, where it will
#then be decided which output to produce
def write_output( experiments ):
	#Call whichever function we want to print out the appropriate information
	#that we have gathered into the experiments
	
	#Prints out latex tables directly comparing the NMLM for recovered and
	#non-recovered formulations to the NMG for different values of alpha,
	#different coarse grids, but the same initial guess, and values of sigma and
	#a Newton weighting factor of 1.0
	#comp_rec_nmlm_nmg( experiments )
	
	#Prints out latex tables for the NMLM for the recovered and non-recovered
	#formulations and the NMG for different values of sigma and Newton weighting
	#factor on different grid levels for varying values of alpha
	#nmlm_nmg_sig_nf( experiments )
	
	#Prints out the convergence factors for iterations in a format that can be
	#read by matlab to put it into figures
	#l2conv_facs_for_matlab( experiments )
	
	#Prints out the convergence factors in h1 norm for iterations in a format
	#that can be read by MATLAB to put into figures
	#h1conv_facs_for_matlab( experiments )
	
	#Prints out the convergence factors in the l2 and h1 norm in a format that
	#can be read by MATLAB for plotting on graphs
	#l2h1conv_facs_for_matlab( experiments )
	
	#Prints out latex tables for the NMLM for recovered and non-recovered 
	#formulations with the number of iterations required for convergence when
	#the smoothing factor is changed.
	#comp_smooth_facs( experiments )
	
	#Prints out latex tables for different values of the pre- and post- smoothing
	#factor where the same number of pre- and post-smooths were performed
	#table_equal_pre_post( experiments )
	
	#Prints out latex tabls for different values of the inner iterations of MG
	#that were used per Newton step
	#comp_inner_its( experiments )
	
	#Prints out latex tables for the number of iterations required for
	#convergence for different values of alpha
	#table_diff_alpha( experiments )
	
	#Prints the running times of the experiments to file in a specified format
	print_running_times( experiments )
	
	#For a single grid level print the convergence with varying alpha for the
	#block and point smoothers
	#print_convergence_with_alpha_for_matlab( experiments )
#### End of function defintion 'write_tables'

#Returns a string containing the table head with the given alignment string
def table_head( alignStr ):
	lStr = '\\begin{center}\n\\begin{tabular}{' + alignStr + '}\\hline\n'
	return lStr
#### End of function 'table_head'

#Returns a string containing the table head for a tabularx environment, with the
#given alignemnt string
def table_x_head( alignStr ):
	lStr = '\\begin{center}\n\\begin{tabularx}{0.8\\textwidth}{' + alignStr + '}\\hline\n'
	return lStr
#### End of function 'table_x_head'

#Closes a table
def table_foot():
	return '\\hline\n\\end{tabular}\n\\end{center}\n'
#### End of function 'table_foot'

#Closes a tabularx table
def table_x_foot():
	return '\\hline\n\\end{tabularx}\n\\end{center}\n'
#### End of function 'table_x_foot'

def get_experiments_in_series( experiments, e ):
	#First thing I want to do is to get the experiments that are equal up to
	#the fine grid level
	series = []
	toRemove = []
	for i in range( 0, len(experiments) ):
		if( e.is_similar( experiments[i] ) ):
			series.append( experiments[i] )
			toRemove.append( i )
	#Now remove the appropriate experiments that have been found
	cnt = 0
	for i in sorted( toRemove ):
		experiments.pop( i - cnt )
		cnt = cnt+1
	return series
#### End of function 'get_experiments_in_series'

#Given a variable set of parameters this function finds the experiments with
#matching parameters
def select_experiments( experiments, **params ):
	selection = experiments
	#If no selection criteria have been given then just return the experiments
	#as they are
	if( len(params) == 0 ):
		return experiments
	#Trim the selection of experiments according to the parameter values given
	for name, value in params.items():
		selection = select_experiments_by_param( name, value, selection )
	return selection
#### End of function 'select_experiments'

#Finds experiments with the appropriate parameter value
def select_experiments_by_param( name, value, experiments ):
	#Initialise the new selection to be the empty list
	newSelection = []
	#If there are no experiments to search then return from the function
	if( len( experiments ) == 0 ):
		return newSelection
	if( name.lower() == "alpha" ):
		#Add all the experiments with the given alpha value to the new selection
		for experiment in experiments:
			if( experiment.alpha == float( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "coarsegrid" ):
		#Add all experiments with the given coarse grid to the new selection
		for experiment in experiments:
			if( experiment.coarseGrid == int( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "testcase" ):
		#Add all experiments with the given test case to the new selection
		for experiment in experiments:
			if( experiment.testCase == int( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "nlmethod" ):
		#Add all experiments with the given nonlinear method to the new
		#selection
		for experiment in experiments:
			if( experiment.nlMethod == int( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "innerittype" ):
		#Add all experiments with the given linear iteration for the Newton
		#step
		for experiment in experiments:
			if( experiment.innerItType == int( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "mgtype" ):
		#Add all experiments with the given Multigrid iteration type to the new
		#selection
		for experiment in experiments:
			if( experiment.mgType == int( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "finegrid" ):
		#Add all experiments with the given fine grid to the new selection
		for experiment in experiments:
			if( experiment.fineGrid == int( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "smoothtype" ):
		#Add all experiments with the given smoothing type to the new selection
		for experiment in experiments:
			if( experiment.smoothType == int( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "presmooth" ):
		#Add all experiments with the given number of pre-smoothing iterations
		#to the new selection
		for experiment in experiments:
			if( experiment.preSmooth == int( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "postsmooth" ):
		#Add all experiments with the given number of post-smoothing iterations
		#to the new selection
		for experiment in experiments:
			if( experiment.postSmooth == int( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "smoothweight" ):
		#Add all the experiments with the given smooth weight to the new
		#selection
		for experiment in experiments:
			if( experiment.smoothWeight == float( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "sigma" ):
		#Add all the experiments with the given sigma to the new selection
		for experiment in experiments:
			if( experiment.sigma == float( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "nf" ):
		#Add all the experiments with the given Newton damping factor to the
		#new selection
		for experiment in experiments:
			if( experiment.nf == float( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "recover" ):
		#Add all the experiments with the given recovery value to the new
		#selection
		for experiment in experiments:
			if( experiment.recover == bool( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "initapprox" ):
		#Add all the experiments with the given initial approximation to the new
		#selection
		for experiment in experiments:
			if( experiment.initApprox == str( value ) ):
				newSelection.append( experiment )
	elif( name.lower() == "innerits" ):
		#Add all the experiments with the given number of inner iterations to
		#the new selection
		for experiment in experiments:
			if( experiment.innerIts == int( value ) ):
				newSelection.append( experiment )
	else:
		print( "Unknown argument %s" % name )
		exit( 0 )
	#Return the selection
	return newSelection
#### End of function 'select_experiments_by_param'

#Returns the unique pre-smooths that are in the experiments, and sorts them in
#ascending order
def get_experiment_pre_smooth( experiments ):
	pres = []
	for experiment in experiments:
		if( experiment.preSmooth not in pres ):
			pres.append( experiment.preSmooth )
	pres.sort()
	return pres
#### End of function 'get_experiment_pre_smooth'

#Returns the unique post-smooths that are used in the experiments, and sorts
#them in ascending order
def get_experiment_post_smooth( experiments ):
	posts = []
	for experiment in experiments:
		if( experiment.postSmooth not in posts ):
			posts.append( experiment.postSmooth )
	posts.sort()
	return posts
#### End of function 'get_experiment_post_smooth'

#Returns the unique alphas that are in the experiments, and sorts them in
#ascending order
def get_experiment_alphas( experiments ):
	alphas = []
	for experiment in experiments:
		if( experiment.alpha not in alphas ):
			alphas.append( experiment.alpha )
	alphas.sort()
	return alphas
#### End of function 'get_experiment_alphas'

#Returns the unique Newton weighting factors that are used in the experiments
#and sorts them in ascending order
def get_experiment_nfs( experiments ):
	nfs = []
	for experiment in experiments:
		if( experiment.nf not in nfs ):
			nfs.append( experiment.nf )
	nfs.sort()
	return nfs
#### End of function 'get_experiment_nfs'

#Return the unique sigmas that are in the experiments and sorts them in
#ascending order
def get_experiment_sigmas( experiments ):
	sigmas = []
	for experiment in experiments:
		if( experiment.sigma not in sigmas ):
			sigmas.append( experiment.sigma )
	sigmas.sort()
	return sigmas
#### End of function 'get_experiment_sigmas'

#Returns a sorted list of unique coarse grids that are in the experiments
def get_experiment_coarse_grids( experiments ):
	cGrids = []
	for experiment in experiments:
		if( experiment.coarseGrid not in cGrids ):
			cGrids.append( experiment.coarseGrid )
	cGrids.sort()
	return cGrids
#### End of function 'get_experiment_coarse_grids'

#Returns a sorted list of unique fine grids that are in the experiments
def get_experiment_fine_grids( experiments ):
	fGrids = []
	for experiment in experiments:
		if( experiment.fineGrid not in fGrids ):
			fGrids.append( experiment.fineGrid )
	fGrids.sort()
	return fGrids
#### End of function 'get_experiment_fine_grids'

#Returns a list of unique initial approximations that are in the experiments
def get_experiment_init_approxs( experiments ):
	approxs = []
	for experiment in experiments:
		if( experiment.initApprox not in approxs ):
			approxs.append( experiment.initApprox )
	return approxs
#### End of function 'get_experiment_init_approxs'

#Returns a list of unique smoothing factors used in the given experiments
def get_experiment_smooth_facs( experiments ):
	sfacts = []
	for experiment in experiments:
		if( experiment.smoothWeight not in sfacts ):
			sfacts.append( experiment.smoothWeight )
	return sfacts
#### End of function 'get_experiment_smooth_facs'

#Returns a list of unique numbers of MG iterations to perform per Newton iteration
def get_experiment_inner_its( experiments ):
	iIts = []
	for experiment in experiments:
		if( experiment.innerIts not in iIts ):
			iIts.append( experiment.innerIts )
	iIts.sort()
	return iIts
#### End of function 'get_experiment_inner_its'

#Writes the appropriate string for the number of iterations taken by an iteration
#to converge
def write_num_its_entry( outFile, experiments, writeAmp ):
	amp = ""
	if( writeAmp ):
		amp = "&"
	if( len( experiments ) == 0 ):
		outFile.write( "None %s " % amp )
	else:
		if( len( experiments ) > 1 ):
			print( "Multiple experiments exist for:\n" +
			       "%s\n\n********\n" % experiments[0] )
		outFile.write( "%s %s " % (experiments[0].it_string(), amp) )
#### End of function 'write_num_its_entry'

#Compares the number of iterations required for convergence for the NMLM for
#the recovery, non recovery to the number of iterations required for the N-MG to
#converge
def comp_rec_nmlm_nmg( experiments ):
	selection = experiments
	#Since I am performing the recovery I am only using test case 2, so make
	#sure that I have only got experiments run for test case two. Also perform
	#some selection on other criteria
	selection = select_experiments( selection, testCase=2, smoothWeight=0.8,
	               smoothType=0, preSmooth=2, postSmooth=2,
	               initApprox="0.7*sin(pi*x)*sin(pi*y)" )
	
	#Open a file for writing
	fout = open( "/tmp/fout", "w" )
	fout.write( "\\section{Results}\n\n" )
	
	#Get the unique alphas that we have
	alphas = get_experiment_alphas( selection )
	#Loop through each of the alphas
	for alpha in alphas:
		#Get the experiments with the given alpha value
		aSelection = select_experiments_by_param( "alpha", alpha, selection )
		
		#Now get the unique coarse grids and also the fine grids
		cGrids = get_experiment_coarse_grids( aSelection )
		fGrids = get_experiment_fine_grids( aSelection )
		
		#Loop through each of the coarse grids
		fout.write( "\\subsection{$\\alpha = %.3f$" % alpha )
		fout.write( ",\ Initial Approximation = $%s$}\n" %
		              aSelection[0].init_approx_latex_string() )
		for cGrid in cGrids:
			#Get the experiments related to the current coarse grid
			cSelection = select_experiments_by_param( "coarsegrid", cGrid,
			               aSelection )
			               
			#Write the table head to file
			fout.write( table_head( '|c|ccc|' ) )
			#Now write the specific head that we have for this table
			fout.write( '\t\\multicolumn{4}{|c|}{Coarse Grid = %d}\\\\ \\hline' % cGrid +
						'\\multirow{2}{*}{Fine Grid} & ' +
						'\\multicolumn{2}{c}{NMLM} & ' +
						'\\multirow{2}{*}{N-MG} \\\\\n\t & No Rec. & Rec. & \\\\\n\t' )
						
			#Loop through the fine grids. For each fine grid we have a row in
			#the table
			for fGrid in fGrids:
				if( fGrid <= cGrid ):
					continue
				#Get the experiments to do with the fine grid level
				fSelection = select_experiments_by_param( "finegrid", fGrid,
					cSelection )
					
				#Write the grid level that we are currently on
				fout.write( '%d & ' % fGrid )
				#Get the selection for the recovered NMLM
				nlSelection = select_experiments( fSelection, recover=False,
				            nlMethod=0, sigma=1.0 )
				            
				#Write the appropriate entry to the table
				write_num_its_entry( fout, nlSelection, True )
					
				#Get the selection for the non recovered NMLM
				nlSelection = select_experiments( fSelection, recover=True,
					nlMethod=0, sigma=1.0 )
					
				#Write the appropriate entry to the table
				write_num_its_entry( fout, nlSelection, True )
				
				#Get the selection for the Newton-Multigrid
				nlSelection = select_experiments( fSelection, nlMethod=1,
					nf=1.0 )
					
				#Write the appropriate entry to the table
				write_num_its_entry( fout, nlSelection, False )
				
				fout.write( "\\\\\n\t" )
			
			#Write the table foot to file
			fout.write( table_foot() + '\n\n' )
		
	#Close the file
	fout.close()
#### End of function 'comp_rec_nmlm_nmg'

#Prints out the number of iterations required for the convergence of the nmlm
#and nmg methods for differing values of the sigma and nf variables
	selection = experiments
	#First trim down the selection of experiments
	selection = select_experiments( selection, testCase=6, smoothWeight=0.8,
	               smoothType=0, preSmooth=3, postSmooth=3 )
	
	#Now get the experiments that used the nmlm without recovery
	nmlmSelection = select_experiments( selection, nlmethod=0, recover=False )
	
	#Find all of the sigmas that are involved with the nmlm test runs
	sigmas = get_experiment_sigmas( nmlmSelection )
	#Get the string heading required for the different sigmas
	sigStr = '& '
	for sigma in sigmas:
		sigStr = sigStr + str( sigma )
		if( sigma != sigmas[ len(sigmas) -1 ] ):
			sigStr = sigStr + ' & '
		else:
			sigStr = sigStr + '\\\\\\hline\n'
	#Find all of the alpha values that are used with the nmlm test runs
	alphas = get_experiment_alphas( nmlmSelection )
	
	#Open a file for writing
	fout = open( "/tmp/fout", "w" )
	
	#Write the headings to file
	fout.write( "\\section{Results}\\subsection{NMLM (Without Recovery)}\n" )
	
	#Loop through all of the alpha values
	for alpha in alphas:
		#Find the experiments with the given alpha
		aSelection = select_experiments_by_param( "alpha", alpha, nmlmSelection )
		
		#Write a subsection with the alpha value and the initial approximation
		fout.write( "\\subsubsection{$\\alpha = %.3f$" % alpha )
		fout.write( ",\ Initial Approximation = $%s$}\n" %
		              aSelection[0].init_approx_latex_string() )
		              
		#Get the coarse grids in the selection that we are given
		cGrids = get_experiment_coarse_grids( aSelection )
		#Get the fine grids in the selection that we are given
		fGrids = get_experiment_fine_grids( aSelection )
		
		#Loop through the coarse grids
		for cGrid in cGrids:
			#Write a table head to file
			fout.write( table_head( '|c|' + 'c'*len(sigmas) + '|' ) )
			#Write which coarse grid we are operating on
			fout.write( '\\multicolumn{%d}{|c|}{Coarse Grid = %d}\\\\\\hline' % (
			             len(sigmas)+1, cGrid ) )
			             
			#Get the experiments to do with the current coarse grid level
			cSelection = select_experiments_by_param( "coarsegrid", cGrid,
			               aSelection )

			#Write the fine grid level in the left hand column, and the sigmas
			#in the rest
			fout.write( '\\multirow{2}{*}{Fine Grid} & ' )
			fout.write( '\\multicolumn{%d}{|c|}{$\\sigma$}\\\\\n\t' % len(sigmas) )
			fout.write( sigStr )
			
			#Now loop through the fine grids
			for fGrid in fGrids:
				#Check that the coarse grid is not finer than the fine grid
				if fGrid <= cGrid:
					continue
					
				#Write the grid level that we are currently on
				fout.write( '%d & ' % fGrid )
				
				#Get the experiments to do with the fine grid level
				fSelection = select_experiments_by_param( "finegrid", fGrid,
					cSelection )
					
				#Get the experiments to do with the given sigma value
				for sigma in sigmas:
					sSelection = select_experiments_by_param( "sigma", sigma,
						fSelection )
						
					write_num_its_entry( fout, sSelection, False )
					if( sigma != sigmas[ len(sigmas)-1] ):
						fout.write( ' & ' )
				fout.write( ' \\\\\n' )
			
			fout.write( table_foot() + '\n' )
			
	#Now we write the results for the next section
	fout.write( "\\subsection{NMLM (With Recovery)}\n" )
	#Get the experiments for the recovered nmlm
	nmlmSelection = select_experiments( selection, nlmethod=0, recover=True )
	
	#Find all of the sigmas that are involved with the nmlm test runs
	sigmas = get_experiment_sigmas( nmlmSelection )
	#Get the string heading required for the different sigmas
	sigStr = '& '
	for sigma in sigmas:
		sigStr = sigStr + str( sigma )
		if( sigma != sigmas[ len(sigmas) -1 ] ):
			sigStr = sigStr + ' & '
		else:
			sigStr = sigStr + '\\\\\\hline\n'
	#Find all of the alpha values that are used with the nmlm test runs
	alphas = get_experiment_alphas( nmlmSelection )
	
	#Loop through all of the alpha values
	for alpha in alphas:
		#Find the experiments with the given alpha
		aSelection = select_experiments_by_param( "alpha", alpha, nmlmSelection )
		
		#Write a subsection with the alpha value and the initial approximation
		fout.write( "\\subsubsection{$\\alpha = %.3f$" % alpha )
		fout.write( ",\ Initial Approximation = $%s$}\n" %
		              aSelection[0].init_approx_latex_string() )
		              
		#Get the coarse grids in the selection that we are given
		cGrids = get_experiment_coarse_grids( aSelection )
		#Get the fine grids in the selection that we are given
		fGrids = get_experiment_fine_grids( aSelection )
		
		#Loop through the coarse grids
		for cGrid in cGrids:
			#Write a table head to file
			fout.write( table_head( '|c|' + 'c'*len(sigmas) + '|' ) )
			#Write which coarse grid we are operating on
			fout.write( '\\multicolumn{%d}{|c|}{Coarse Grid = %d}\\\\\\hline' % (
			             len(sigmas)+1, cGrid ) )
			             
			#Get the experiments to do with the current coarse grid level
			cSelection = select_experiments_by_param( "coarsegrid", cGrid,
			               aSelection )

			#Write the fine grid level in the left hand column, and the sigmas
			#in the rest
			fout.write( '\\multirow{2}{*}{Fine Grid} & ' )
			fout.write( '\\multicolumn{%d}{|c|}{$\\sigma$}\\\\\n\t' % len(sigmas) )
			fout.write( sigStr )
			
			#Now loop through the fine grids
			for fGrid in fGrids:
				#Check that the coarse grid is not finer than the fine grid
				if fGrid <= cGrid:
					continue
					
				#Write the grid level that we are currently on
				fout.write( '%d & ' % fGrid )
				
				#Get the experiments to do with the fine grid level
				fSelection = select_experiments_by_param( "finegrid", fGrid,
					cSelection )
					
				#Get the experiments to do with the given sigma value
				for sigma in sigmas:
					sSelection = select_experiments_by_param( "sigma", sigma,
						fSelection )
						
					write_num_its_entry( fout, sSelection, False )
					if( sigma != sigmas[ len(sigmas)-1] ):
						fout.write( ' & ' )
				fout.write( ' \\\\\n' )
			
			fout.write( table_foot() + '\n' )
	
	#Now we want to print the results for the Newton-Multigrid iteration
	fout.write( "\\subsection{NMG}" )
	
	#Get the experiments related to the Newton Multigrid test run
	nmlmSelection = select_experiments_by_param( "nlmethod", 1, selection )
	
	#Find the Newton weighting factors that are used in the experiments
	nfs = get_experiment_nfs( nmlmSelection )
	#Work out what the string is to print at the heading of the table for the
	#given Newton weighting factors
	nfStr = '& '
	for nf in nfs:
		nfStr = nfStr + str( nf )
		if( nf != nfs[ len(nfs) -1] ):
			nfStr = nfStr + ' & '
		else:
			nfStr = nfStr + ' \\\\\\hline\n'
	#Get the alphas that were used
	alphas = get_experiment_alphas( nmlmSelection )
	
	#Now loop through the alphas
	for alpha in alphas:
		#Get the experiments related to the current value of alpha
		aSelection = select_experiments_by_param( "alpha", alpha, nmlmSelection )
		
		#Print out the alpha value and the initial approximation
		fout.write( "\\subsubsection{$\\alpha = %.3f$" % alpha )
		fout.write( ",\ Initial Approximation = $%s$}\n" %
		              aSelection[0].init_approx_latex_string() )
		              
		#Get the coarse and fine grids that were used
		cGrids = get_experiment_coarse_grids( aSelection )
		fGrids = get_experiment_fine_grids( aSelection )
		
		#Loop through the coarse grids
		for cGrid in cGrids:
			#Write a table head to file
			fout.write( table_head( '|c|' + 'c'*len(nfs) + '|' ) )
			#Write which coarse grid we are operating on
			fout.write( '\\multicolumn{%d}{|c|}{Coarse Grid = %d}\\\\\\hline' % (
			             len(nfs)+1, cGrid ) )
			             
			#Get the experiments to do with the current coarse grid level
			cSelection = select_experiments_by_param( "coarsegrid", cGrid,
			               aSelection )
			               
			#Write the fine grid level in the left hand column, and the sigmas
			#in the rest
			fout.write( '\\multirow{2}{*}{Fine Grid} & ' )
			fout.write( '\\multicolumn{%d}{|c|}{Newton Weighting Factor}\\\\\n\t' % len(nfs) )
			fout.write( nfStr )
			
			#Now loop through the fine grids
			for fGrid in fGrids:
				#Check that the coarse grid is not finer than the fine grid
				if fGrid <= cGrid:
					continue
					
				#Write the grid level that we are currently on
				fout.write( '%d & ' % fGrid )
				
				#Get the experiments to do with the fine grid level
				fSelection = select_experiments_by_param( "finegrid", fGrid,
					cSelection )
					
				#Get the experiments to do with the given sigma value
				for nf in nfs:
					sSelection = select_experiments_by_param( "nf", nf,
						fSelection )
						
					write_num_its_entry( fout, sSelection, False )
					if( nf != nfs[ len(nfs)-1] ):
						fout.write( ' & ' )
				fout.write( ' \\\\\n' )
			
			fout.write( table_foot() + '\n' )
	
	#Close the file
	fout.close()
#### End of function 'nmlm_nmg_sig_nf_tc2'

def comp_smooth_facs( experiments ):
	selection = experiments
	#I want to compare the different smoothing factors to each other in a latex
	#table
	
	#First we trim down the selection
	selection = select_experiments( selection, testCase=2, smoothType=0,
				   preSmooth=2, postSmooth=2,
	               initApprox="0.7*sin(pi*x)*sin(pi*y)+h\\vect{r}" )
	
	#The NMLM results are to be selected first, so find the appropriate ones
	#Select the experiments for the nonlinear method without recovery
	nmlmSelection = select_experiments( selection, sigma=1.0, nlMethod=0,
	                  recover=False )
	
	#Find all of the smoothing factors that were used
	sfacts = sorted( get_experiment_smooth_facs( selection ) )
	
	#Make a string heading for the latex tables that includes the smoothing
	#factors
	hStr = " & "
	for sfact in sfacts:
		hStr = hStr + str( sfact )
		if( sfact != sfacts[ len(sfacts) -1 ] ):
			hStr = hStr + ' & '
		else:
			hStr = hStr + '\\\\\\hline\n'
			
	#Open a file for writing to
	fout = open( "/tmp/fout", "w" )
	
	#Write some headings to file
	fout.write( "\\section{Results}\n\\subsection{NMLM without Recovery}\n" )
	
	#Get the values of alpha that are in the experiments
	alphas = sorted( get_experiment_alphas( selection ) )
	
	#Loop through all of the alphas
	for alpha in alphas:
		#Select all of the alphas that are in the nmlm selection
		aSelection = select_experiments_by_param( "alpha", alpha,
		        nmlmSelection )
		
		#Print the value of alpha to file
		fout.write( "\\subsubsection{$\\alpha = %.3f$, Initial Approximation: $%s$}\n" % (
		         alpha, aSelection[0].init_approx_latex_string() ) )
		         
		#Get the coarse grids that are in the current experiments
		cGrids = get_experiment_coarse_grids( aSelection )
	
		#Loop through all of the coarse grids
		for cGrid in cGrids:
			#We want to print a table per coarse grid that we encounter
			#Write a table head to file
			fout.write( table_head( '|c|' + 'c'*len(sfacts) + '|' ) )
		
			#Write which coarse grid we are operating on
			fout.write( '\\multicolumn{%d}{|c|}{Coarse Grid = %d}\\\\\\hline\n' % (
				         len(sfacts)+1, cGrid ) )
				      
			#Write the column headings to file   
			fout.write( '\\multirow{2}{*}{Fine Grid} & ' )
			fout.write( '\\multicolumn{%d}{|c|}{Smoothing Weighting Factor}\\\\\n' % len( sfacts ) )
			fout.write( hStr )
				         
			#Get the experiments to do with the current coarse grid level
			cSelection = select_experiments_by_param( "coarsegrid", cGrid,
				           aSelection )
				           
			#Get the fine grids for the given coarse grids
			fGrids = get_experiment_fine_grids( aSelection )
		
			#Loop through the different fine grids
			for fGrid in fGrids:
				if fGrid <= cGrid:
					continue
				#Get the experiments for the given fine grid level
				fSelection = select_experiments_by_param( "fineGrid", fGrid,
					           cSelection )
					           
				#Write the fine grid to file
				fout.write( "%d & " % fGrid )
					           
				#Select the smoothing weights that we have got
				for sfact in sfacts:
					sfSelection = select_experiments_by_param( "smoothWeight",
						           sfact, fSelection )
				
					#Write the number of iterations required for convergence to file
					write_num_its_entry( fout, sfSelection, False )
					if( sfact != sfacts[ len(sfacts)-1] ):
						fout.write( ' & ' )
				#Write a new line to file
				fout.write( ' \\\\\n' )
			
			#Write the foot of the table
			fout.write( table_foot() + '\n' )
				
	
	#Now select the elements for the nonlinear method with recovery
	nmlmSelection = select_experiments( selection, sigma=1.0, nlMethod=0,
	                  recover=True )
	                  
	#Write the heading of the section to file
	fout.write( "\\subsection{NMLM with Recovery}" )
	
	for alpha in alphas:
		#Select all of the alphas that are in the nmlm selection
		aSelection = select_experiments_by_param( "alpha", alpha,
		        nmlmSelection )
		
		#Print the value of alpha to file
		fout.write( "\\subsubsection{$\\alpha = %.3f$, Initial Approximation: $%s$}\n" % (
		         alpha, aSelection[0].init_approx_latex_string() ) )
			
		#For all of the coarse grids in the selection
		cGrids = get_experiment_coarse_grids( aSelection )
		for cGrid in cGrids:
			#We want to print a table per coarse grid that we encounter
			#Write a table head to file
			fout.write( table_head( '|c|' + 'c'*len(sfacts) + '|' ) )
		
			#Write which coarse grid we are operating on
			fout.write( '\\multicolumn{%d}{|c|}{Coarse Grid = %d}\\\\\\hline\n' % (
				         len(sfacts)+1, cGrid ) )
				      
			#Write the column headings to file   
			fout.write( '\\multirow{2}{*}{Fine Grid} & ' )
			fout.write( '\\multicolumn{%d}{|c|}' % len(sfacts) +
			            '{Smoothing Weighting Factor}\\\\\n' )
			fout.write( hStr )
				         
			#Get the experiments to do with the current coarse grid level
			cSelection = select_experiments_by_param( "coarsegrid", cGrid,
				           aSelection )
				           
			#Get the fine grids for the given coarse grids
			fGrids = get_experiment_fine_grids( aSelection )
		
			#Loop through the different fine grids
			for fGrid in fGrids:
				if fGrid <= cGrid:
					continue
				#Get the experiments for the given fine grid level
				fSelection = select_experiments_by_param( "fineGrid", fGrid,
					           cSelection )
					           
				#Write the fine grid to file
				fout.write( "%d & " % fGrid )
					           
				#Select the smoothing weights that we have got
				for sfact in sfacts:
					sfSelection = select_experiments_by_param( "smoothWeight",
						           sfact, fSelection )
				
					#Write the number of iterations required for convergence to file
					write_num_its_entry( fout, sfSelection, False )
					if( sfact != sfacts[ len(sfacts)-1] ):
						fout.write( ' & ' )
				#Write a new line to file
				fout.write( ' \\\\\n' )
			
			#Write the foot of the table
			fout.write( table_foot() + '\n' )
	
	#Close the file
	fout.close()
#### End of function 'comp_smooth_facs'

#Prints out the convergence factors in the L2 norm for the different experiments
#This is in the order:
#	Non-recovered
#	Recovered
#	Newton-MG
#and the output is used to plot graphs in MATLAB (see the file
#'~/My Matlab/TestResultGraphFunctions/plotConvFacs.m')
def l2conv_facs_for_matlab( experiments ):
	conv_facs_for_matlab( experiments, 0 ) ;
#### End of function 'l2conv_facs_for_matlab'

def h1conv_facs_for_matlab( experiments ):
	conv_facs_for_matlab( experiments, 1 ) ;
#### End of function 'h1conv_facs_for_matlab'

#Prints out the convergence factors for the iterations in the order
#	Non-Recovered:
#	Recoverd
#	NMG
#and the output is used to plot graphs in MATLAB (see the functions in folder
#'/My Matlab/TestResultGraphFunctions/'
def l2h1conv_facs_for_matlab( experiments ):
	conv_facs_for_matlab( experiments, 2 )
#### End of function 'l2h1conv_facs_for_matlab'

#Prints out the convergence factor in the l2 norm or the h1 norm, or both,
#depending on the value of mode. The order in which this is done is for the
#Non-recovered version, then the recovered version, then for Newton-MG. The
#output is read by a MATLAB function and plotted on graphs. For definitions
#of these functions see the folder '~/My Matlab/TestResultGraphFunctions'
def conv_facs_for_matlab( experiments, mode ):
	selection = experiments
	#Here I want to output the convergence factors in a useful way so that
	#they can be read in by MATLAB and plotted easily. I want to be able
	#to identify the convergence factors with the experiments as well
	
	#Trim down the experiments that we are looking at
	selection = select_experiments( selection, smoothWeight=0.8, 
				  testCase=41, preSmooth=3, postSmooth=3 )
#	              initApprox="0.7*sin(pi*x)*sin(pi*y)+h\\vect{r}", testCase=2, preSmooth=2,
#	              postSmooth=2, smoothType=2 )
	              
	#Now I want to have a look at the different alphas, so get the different
	#alphas that are in the experiments
	alphas = get_experiment_alphas( selection )
	
	#Open a file for writing
	fout = open( "/tmp/fout", "w" )
	
	#Loop through the alphas
	for alpha in alphas:
		#Trim down the selection to get the current alpha
		aSelection = select_experiments_by_param( "alpha", alpha, selection )
		
		#Get the coarse grids associated with the current alpha
		cGrids = get_experiment_coarse_grids( aSelection )
		#Loop through the coarse grids
		for cGrid in cGrids:
			#Get the experiments for the current coarse grid
			cSelection = select_experiments_by_param( "coarseGrid", cGrid,
			               aSelection )
			#Get the fine grids for the current coarse grid.
			fGrids = get_experiment_fine_grids( cSelection )
			#Loop through the fine grids
			for fGrid in fGrids:
				#Check that the fine grid is not coarser than the coarse grid
				if( fGrid <= cGrid ):
					continue
				#Get the information for the non recovered NMLM
				fSelection = select_experiments( cSelection, recover=False,
				               nlmethod=0, sigma=1.0, fineGrid=fGrid )
				#Check to see if we have more than one experiment. If we do then
				#there are duplicates, and inform the user that we have a
				#duplicate
				if( len( fSelection ) > 1 ):
					print( "Multiple experiments exist for:\n" +
					       "%s\n\n********\n" % fSelection[0] )
				#If we have at least one experiment, then we can print out some
				#information
				if( len( fSelection ) != 0 ):
					#Print out the convergence factors in the l2 and h1 norm
					if( mode == 0 or mode == 2 ):
						#Print out information so that the iteration can be
						#identified
						fout.write( "%.3f,%d,%d," % (alpha, cGrid, fGrid ) )
						fout.write( fSelection[0].conv_fac_string() )
					if( mode == 1 or mode == 2 ):
						#Print out information so that the iteration can be
						#identified
						fout.write( "%.3f,%d,%d," % (alpha, cGrid, fGrid ) )
						fout.write( fSelection[0].h1_conv_fac_string() )
				else:
					fout.write( "%.3f,%d,%d,nan\n" % ( alpha, cGrid, fGrid ) )
					if( mode == 2 ):
						fout.write( "%.3f,%d,%d,nan\n" % ( alpha, cGrid,
						   fGrid ) )
				
				#Get the information for the recoverd NMLM
				fSelection = select_experiments( cSelection, recover=True,
				               nlMethod=0, sigma=1.0, fineGrid=fGrid )
				
				#Check to see if we have more than one experiment. If we do then
				#there are duplicates, and inform the user that we have a
				#duplicate
#				if( len( fSelection ) > 1 ):
#					print( "Multiple experiments exist for:\n" +
#					    "%s\n\n********\n" % fSelection[0] )
#				if( len( fSelection ) != 0 ):
#					#Print out the convergence factors
#					if( mode == 0 or mode == 2 ):
#						#Print out information so that the iteration can be
#						#identified
#						fout.write( "%.3f,%d,%d," % ( alpha, cGrid, fGrid ) )
#						fout.write( fSelection[0].conv_fac_string() )
#					if( mode == 1 or mode == 2 ):
#						#Print out information so that the iteration can be
#						#identified
#						fout.write( "%.3f,%d,%d," % ( alpha, cGrid, fGrid ) )
#						fout.write( fSelection[0].h1_conv_fac_string() )
#				else:
#					fout.write( "%.3f,%d,%d,nan\n" % ( alpha, cGrid, fGrid ) )
#					if( mode == 2 ):
#						fout.write( "%.3f,%d,%d,nan\n" % ( alpha, cGrid,
#						   fGrid ) )
				
				#Get the information for the NMG
				fSelection = select_experiments( cSelection, nlMethod=1,
				               nf=1.0, fineGrid=fGrid )
				
				#Check to see if we have more than one experiment. If we do then
				#there are duplicates, and inform the user that we have a
				#duplicate
				if( len( fSelection ) > 1 ):
					print( "Multiple experiments exist for:\n" +
					    "%s\n\n********\n" % fSelection[0] )
				if( len( fSelection ) != 0 ):
					#Print out the convergence factors
					if( mode == 0 or mode == 2 ):
						#Print out information so that the iteration can be
						#identified
						fout.write( "%.3f,%d,%d," % ( alpha, cGrid, fGrid ) )
						fout.write( fSelection[0].conv_fac_string() )
					if( mode == 1 or mode == 2 ):
						#Print out information so that the iteration can be
						#identified
						fout.write( "%.3f,%d,%d," % ( alpha, cGrid, fGrid ) )
						fout.write( fSelection[0].h1_conv_fac_string() )
				else:
					fout.write( "%.3f,%d,%d,nan\n" % ( alpha, cGrid, fGrid ) )
					if( mode == 2 ):
						fout.write( "%.3f,%d,%d,nan\n" % ( alpha, cGrid,
						   fGrid ) )
	#Close the file
	fout.close()
#### End of function 'conv_facs_for_matlab'

#Prints out latex tables for different values of the pre- and post- smoothing
#factor where the same number of pre- and post-smooths were performed
def table_equal_pre_post( experiments ):
	#First I want to trim down the experiments that I have
	selection = select_experiments( experiments, testCase=7 )
			
	#Open a file for writing to
	fout = open( "/tmp/fout", "w" )
	
	#First thing to write to the file is if we are using recovery or not. We
	#will first write the results for the non-recovered formulation, and then
	#for the recovered formulation
	initApproxStr = selection[0].init_approx_latex_string()
	
	fout.write( "\\subsection{NMLM (No Recovery)}\n" )
	
	#Get the experiments only concerning the non-recovered formulation
	nmlmSelection = select_experiments( selection, recover=False,
	      nlMethod=0, sigma=1.0 )
	
	#Now get the pre-smooths used in the selected experiments
	pres = get_experiment_pre_smooth( nmlmSelection )
	
	#Make a string heading for the latex tables that includes the number of
	#pre and post smooths
	hStr = " & "
	head = "|c|"
	for pre in pres:
		if( pre == 0 ):
			continue
		head = head + ">{\\centering\\arraybackslash}X"
		hStr = hStr + str( pre )
		if( pre != pres[ len(pres) -1 ] ):
			hStr = hStr + ' & '
		else:
			hStr = hStr + '\\\\\\hline\n'
	head = head + "|"
		    	    
	#Get the alphas that were used
	alphas = get_experiment_alphas( selection )
	
	#For each of the alphas
	for alpha in alphas:
		aSelection = select_experiments_by_param( "alpha", alpha, nmlmSelection )
		
		fout.write( "\\subsubsection{$\\alpha = %.3f$}\n" % alpha ) ;
	
		#Get the coarse grids from the selection
		cGrids = get_experiment_coarse_grids( aSelection )
	
		#For each of the coarse grids
		for cGrid in cGrids:
			#Get the experiments related to the coarse grid
			cSelection = select_experiments_by_param( "coarseGrid", cGrid,
				aSelection )
				
			single_table_equal_pre_post( fout, cSelection, table_x_head( head ),
				table_x_foot(), hStr, cGrid, pres )
				
	fout.write( "\\subsection{NMLM (Recovery)}\n" )
	
	#Get the experiments only concerning the recovered formulation
	nmlmSelection = select_experiments( selection, recover=True,
	      nlMethod=0, sigma=1.0 )
	
	#Now get the pre-smooths used in the selected experiments
	pres = get_experiment_pre_smooth( nmlmSelection )
	
	#Make a string heading for the latex tables that includes the number of
	#pre and post smooths
	hStr = " & "
	head = "|c|"
	for pre in pres:
		if( pre == 0 ):
			continue
		head = head + ">{\\centering\\arraybackslash}X"
		hStr = hStr + str( pre )
		if( pre != pres[ len(pres) -1 ] ):
			hStr = hStr + ' & '
		else:
			hStr = hStr + '\\\\\\hline\n'
	head = head + "|"
		    	    
	#Get the alphas that were used
	alphas = get_experiment_alphas( selection )
	
	#For each of the alphas
	for alpha in alphas:
		aSelection = select_experiments_by_param( "alpha", alpha, nmlmSelection )
		
		fout.write( "\\subsubsection{$\\alpha = %.3f$}\n" % alpha ) ;
	
		#Get the coarse grids from the selection
		cGrids = get_experiment_coarse_grids( aSelection )
	
		#For each of the coarse grids
		for cGrid in cGrids:
			#Get the experiments related to the coarse grid
			cSelection = select_experiments_by_param( "coarseGrid", cGrid,
				aSelection )
				
			single_table_equal_pre_post( fout, cSelection, table_x_head( head ),
				table_x_foot(), hStr, cGrid, pres )
				
	#Get the experiments relating to Newton Multigrid
	fout.write( "\\subsection{NMG}\n" )
				
	#Get the experiments only concerning the recovered formulation
	nmlmSelection = select_experiments( selection, nlMethod=1, innerIts=1 )
	
	#Now get the pre-smooths used in the selected experiments
	pres = get_experiment_pre_smooth( nmlmSelection )
	
	#Make a string heading for the latex tables that includes the number of
	#pre and post smooths
	hStr = " & "
	head = "|c|"
	for pre in pres:
		if( pre == 0 ):
			continue
		head = head + ">{\\centering\\arraybackslash}X"
		hStr = hStr + str( pre )
		if( pre != pres[ len(pres) -1 ] ):
			hStr = hStr + ' & '
		else:
			hStr = hStr + '\\\\\\hline\n'
	head = head + "|"
		    	    
	#Get the alphas that were used
	alphas = get_experiment_alphas( selection )
	
	#For each of the alphas
	for alpha in alphas:
		aSelection = select_experiments_by_param( "alpha", alpha, nmlmSelection )
		
		fout.write( "\\subsubsection{$\\alpha = %.3f$}\n" % alpha ) ;
	
		#Get the coarse grids from the selection
		cGrids = get_experiment_coarse_grids( aSelection )
	
		#For each of the coarse grids
		for cGrid in cGrids:
			#Get the experiments related to the coarse grid
			cSelection = select_experiments_by_param( "coarseGrid", cGrid,
				aSelection )
				
			single_table_equal_pre_post( fout, cSelection, table_x_head( head ),
				table_x_foot(), hStr, cGrid, pres )
	
	#Close the file
	fout.close()
#### End of function 'table_equal_pre_post'

# Writes a single latex table for the given experiments using an equal number
# of pre- and post-smooths
#
# INPUT:
#	fout: The file to which the output is written
#	experiments: The experiments to select appropriate data from
#	thead: String representing the head of a table
#	tfoot: String representing the foot of a table
#	heading: Represents the column headings in the table
#	cGrid: The coarse grid for which the table is to be output
#	pres: The pre-smoothing numbers to show the results for
def single_table_equal_pre_post( fout, experiments, thead, tfoot, heading, cGrid,
    pres ):
	#Print out the head of a latex table to file
	fout.write( thead )

	#Write which coarse grid we are operating on
	fout.write( '\\multicolumn{%d}{|c|}{Coarse Grid = %d}\\\\\\hline\n' % (
		         len(pres)+1, cGrid ) )
		      
	#Write the column headings to file   
	fout.write( '\\multirow{2}{*}{Fine Grid} & ' )
	fout.write( '\\multicolumn{%d}{c|}{\# of Pre- and Post-Smooths}\\\\\n' % (len(pres) ) )
	fout.write( heading )
	
	#Now get the fine grids in the experiments
	fGrids = get_experiment_fine_grids( experiments )
	
	#For each of the fine grids
	for fGrid in fGrids:
		if( fGrid <= cGrid ):
			continue
		#Get the experiments with the current fine grid
		fSelection = select_experiments_by_param( "fineGrid", fGrid,
		     experiments )
		
		#Print the fine grid to file
		fout.write( " %d & " % fGrid )
		
		#Now loop throug the pre-smooths that we have run for
		for pre in pres:
			if( pre == 0 ):
				continue
			#Get the experiments with the current number of pre- and post-
			#smooths
			sSelection = select_experiments( fSelection, preSmooth=pre,
			    postSmooth=pre )
			    
			#Now print out the number of iterations for convergence
			write_num_its_entry( fout, sSelection, False )
			#If we are not at the last element in pres print out a field
			#separator
			if( pre != pres[ len( pres ) - 1 ] ):
				fout.write( " & " )
		#Print out a new line, and print the result for the next fine grid
		fout.write( "\\\\\n" )
	#Print the table foot, and some new lines to make the output more
	#presentable
	fout.write( tfoot+ "\n" )
#### End of function 'single_table_equal_pre_post'


#Writes latex tables to display the number of iterations required for
#convergence of the Newton-MG method with different numbers of linear MG
#iterations performed per Newton step
def comp_inner_its( experiments ):
	selection = experiments
	#First I want to trim down the experiments that I have
	selection = select_experiments( selection, nlMethod=1, smoothWeight=0.8,
	    nf=1.0, testCase=6, smoothType=1, preSmooth=3, postSmooth=3 )
	 
			
	#Open a file for writing to
	fout = open( "/tmp/fout", "w" )
	
	#First thing to write to the file is if we are using recovery or not. We
	#will first write the results for the non-recovered formulation, and then
	#for the recovered formulation
	#initApproxStr = selection[0].init_approx_latex_string()
	
	#Get the different numbers of inner iterations of MG that are in the
	#selection
	innerIts = get_experiment_inner_its( selection )
	
	#Make a string heading for the latex tables that includes the number of
	#multigrid cycles performed per Newton step
	hStr = " & "
	head = "|c|"
	for innerIt in innerIts:
		head = head + ">{\\centering\\arraybackslash}X"
		hStr = hStr + str( innerIt )
		if( innerIt != innerIts[ len(innerIts) -1 ] ):
			hStr = hStr + ' & '
		else:
			hStr = hStr + '\\\\\\hline\n'
	head = head + "|"
		    	    
	#Get the alphas that were used
	alphas = get_experiment_alphas( selection )
	
	#For each of the alphas
	for alpha in alphas:
		#Select the experiments with the current alpha value
		aSelection = select_experiments_by_param( "alpha", alpha, selection )
		
		fout.write( "\\subsubsection{$\\alpha = %.3f$}" % alpha ) #, Initial Approximation = $%s$}" % (
		    #alpha, initApproxStr ) ) ;
	
		#Get the coarse grids from the selection
		cGrids = get_experiment_coarse_grids( aSelection )
	
		#For each of the coarse grids
		for cGrid in cGrids:
			#Get the experiments related to the coarse grid
			cSelection = select_experiments_by_param( "coarseGrid", cGrid,
				aSelection )
				
			fout.write( table_x_head( head ) )

			#Write which coarse grid we are operating on
			fout.write( '\\multicolumn{%d}{|c|}{Coarse Grid = %d}\\\\\\hline\n' % (
						 len(innerIts)+1, cGrid ) )
					  
			#Write the column headings to file   
			fout.write( '\\multirow{2}{*}{Fine Grid} & ' )
			fout.write( '\\multicolumn{%d}{c|}{\# of inner' % (len(innerIts) ) +
			            ' Multigrid iterations}\\\\\n' )
			fout.write( hStr )
	
			#Now get the fine grids in the experiments
			fGrids = get_experiment_fine_grids( cSelection )
	
			#For each of the fine grids
			for fGrid in fGrids:
				if( fGrid <= cGrid ):
					continue
				#Get the experiments with the current fine grid
				fSelection = select_experiments_by_param( "fineGrid", fGrid,
					 cSelection )
		
				#Print the fine grid to file
				fout.write( " %d & " % fGrid )
		
				#Now loop through the different number of iterations of multigrid
				#per Newton step that we have run for
				for innerIt in innerIts:
					#Get the experiments with the current number of inner
					#iterations
					iSelection = select_experiments_by_param( "innerIts", innerIt,
					    fSelection )
					
					#Now print out the number of iterations for convergence
					write_num_its_entry( fout, iSelection, False )
					#If we are not at the last element in the list of iterations
					#print out a field separator
					if( innerIt != innerIts[ len( innerIts ) - 1 ] ):
						fout.write( " & " )
				#Print out a new line, and print the result for the next fine grid
				fout.write( "\\\\\n" )
			#Print the table foot, and some new lines to make the output more
			#presentable
			fout.write( table_x_foot() + "\n" )
	#Close the file
	fout.close()
#### End of function 'table_equal_pre_post'

#Writes a table of results where the different alpha values are listed as the
#columns
def table_diff_alpha( experiments ):
	selection = experiments
	#Trim down the selection
	selection = select_experiments( selection, nlMethod=0, preSmooth=2,
		postSmooth=2, smoothWeight=0.8, initApprox="0.7*sin(pi*x)*sin(pi*y)+h\\vect{r}",
		testCase=2, smoothType=0, sigma=1.0 )
		
	#Now we select the experiments that have got a recovered gradient
	nlSelection = select_experiments_by_param( "recover", True, selection )
	
	#Get the alphas in this selection
	alphas = get_experiment_alphas( nlSelection )
	
	#Create a string that will be the heading for the table
	head = "|c|"
	hStr = "& "
	for alpha in alphas:
		head = head + ">{\\centering\\arraybackslash}X"
		hStr = hStr + str( alpha )
		if( alpha != alphas[ len( alphas ) -1 ] ):
			hStr = hStr + " & "
		else:
			head = head + "|"
			hStr = hStr + " \\\\ \\hline\n"
			
	#Open a file for writing
	fout = open( "/tmp/fout", "w" )
	
	
	#Get the coarse grids used in the experiments
	cGrids = get_experiment_coarse_grids( nlSelection )
	#Get the fine grids used in the experiments
	fGrids = get_experiment_fine_grids( nlSelection )
	
	#Write a section heading to file
	fout.write( "\\subsection{NMLM (With Recovery)}" )
	
	#Loop through the coarse grids
	for cGrid in cGrids:
		#Get the experiments related to the current coarse grid
		cSelection = select_experiments_by_param( "coarseGrid", cGrid, nlSelection )
		#Start writing the table
		fout.write( table_x_head( head ) )
		fout.write( "\\multicolumn{%d}{|c|}{Coarse Grid Level %d}\\\\\\hline\n" % (
		    len( alphas ) + 1, cGrid ) )
		fout.write( "\\multirow{2}{*}{Fine Grid} & " )
		fout.write( "\\multicolumn{%d}{c|}{$\\alpha$}\\\\\n" % len( alphas ) )
		fout.write( hStr )
		
		#Loop through the fine grids
		for fGrid in fGrids:
			if( fGrid <= cGrid ):
				continue
			#Get the experiments for the current fine grid
			fSelection = select_experiments_by_param( "fineGrid", fGrid, cSelection )
			#Write the fine grid we are currently working on
			fout.write( "%d & " % fGrid )
			#Loop through the alphas
			for alpha in alphas:
				#Select the experiments for the current alpha
				aSelection = select_experiments_by_param( "alpha", alpha, fSelection )
				#Print out the number of iterations for convergence
				write_num_its_entry( fout, aSelection, False )
				
				if( alpha != alphas[ len(alphas) -1] ):
					fout.write( " & " )
			#Write a new line
			fout.write( "\\\\\n" )
		#Finish writing the table for the given coarse grid
		fout.write( table_x_foot() + "\n" )
	#Close the file
	fout.close()
#### End of function 'table_diff_alpha'

#Prints the running times of the experiments that are passed in. Experiments are
#grouped together to include all experiments run with the same parameters,
#except for the fine grid level
def print_running_times( experiments ):
	#First thing to do is to copy the experiments into a local variable
	selection = experiments
	#selection = select_experiments_by_param( "preSmooth", 3, selection )
	fout = open( "/tmp/fout", "w" )
	#For the first experiment get all the experiments that are 'similar'
	while( len( selection ) != 0 ):
		similarExperiments = get_experiments_in_series( selection,
			selection[0] )
		similarExperiments = sorted( similarExperiments )
		e = similarExperiments[0]
		#Now we print out the nonlinear method that is used
		fout.write( "%d, %d, %d, %.3lf, %d, %d, " % ( e.testCase, e.nlMethod,
		    e.smoothType, e.alpha, e.coarseGrid, e.innerItType ) )
		for e in similarExperiments:
			fout.write( "%.3lf" % (e.execTime) )
			if( e != similarExperiments[ len(similarExperiments)-1 ] ):
				fout.write( ", " )
		fout.write( "\n" )
	fout.close()
#### End of function 'print_running_times'

#Prints the number of iterations required for convergence of the experiments
#passed in, for varying alpha, in a format that is to be read by Matlab
def print_convergence_with_alpha_for_matlab( experiments ):
	selection = experiments
	tc=38
	#Trim down the selection of experiments
	
	#Select Newton multigrid iterations only
	nlmgSelection = select_experiments( selection, nlMethod=0, preSmooth=3,
	    postSmooth=3, smoothWeight=0.8, sigma=1.0, coarseGrid=5,
	    fineGrid=7 )
	nmgSelection = select_experiments( selection, nlMethod=1,
	    preSmooth=3, postSmooth=3, smoothWeight=0.8, nf=1.0,
	    coarseGrid=5, innerIts=3, fineGrid=7, smoothType=0 )
	    
	#Get the different alphas in the experiments
	alphas = get_experiment_alphas( nlmgSelection )
	if( len( get_experiment_alphas( nmgSelection ) ) > len(alphas) ):
		alphas = get_experiment_alphas( nmgSelection )
	
	#Print the alphas to file
	#Open a file for writing
	fout = open( "/tmp/fout", "w" ) ;
	
	i = 0 ;
	#Write 3 comma separated values to file, so that the line lengths match with
	#successive entries. These values are required so that the file is easier
	#to deal with in MATLAB, and are ignored
	fout.write( "0, 0, 0, " ) 
	while( i < len(alphas)-1 ):
		fout.write( "%.3f, " % alphas[i] )
		i = i+1
	fout.write( "%.3f\n" % alphas[i] )
	
	#Select the experiments using block smoothing
	blockSelection = select_experiments_by_param( "smoothType", 5,
	    nlmgSelection )
	#For each alpha
	if( len( blockSelection ) > 0 ):
		#Write out the nonlinear method and smoothing type, and also a zero
		#where the linear iteration will be defined for the newton multigrid
		fout.write( "0, 5, 0, " )
		i=0
		while( i < len(alphas)-1 ):
			#Get the experiments only dealing with the current alpha
			aSelection = select_experiments_by_param( "alpha", alphas[i],
				blockSelection )
			#Should have only one alpha at the moment. Write the number of
			#iterations required for convergence to file
			if( len( aSelection ) == 0 ):
				numIts = -1
			else:
				numIts = aSelection[0].it_string()
				if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
					numIts = -1
				else:
					numIts = int( numIts )
			i = i+1		
			fout.write( "%d, " % numIts )
		##End while
	
		aSelection = select_experiments_by_param( "alpha", alphas[i],
			blockSelection )
		if( len( aSelection ) == 0 ):
			numIts = -1
		else:
			numIts = aSelection[0].it_string()
			if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
				numIts = -1
			else:
				numIts = int( numIts )
		fout.write( "%d\n" % numIts )
	##End if statement for NLMG with block smoothing
	
	#Get the experiments for the pointwise smoothing
	pwSelection = select_experiments_by_param( "smoothType", 0,
	    nlmgSelection )
	if( len( pwSelection ) > 0 ):
		#Write the nonlinear method and smooth type to file, and also a zero
		#where the linear iteration will be defined for the newton multigrid
		fout.write( "0, 0, 0, " )
		i = 0 
		while( i < len(alphas)-1 ):
			aSelection = select_experiments_by_param( "alpha", alphas[i],
				pwSelection )
			if( len( aSelection ) == 0 ):
				numIts = -1
			else:
				numIts = aSelection[0].it_string()
				if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
					numIts = -1
				else:
					numIts = int( numIts )
			fout.write( "%d, " % numIts )
			i = i+1
		##End while
	
		aSelection = select_experiments_by_param( "alpha", alphas[i],
		    pwSelection )
		if( len( aSelection ) == 0 ):
			numIts = -1
		else:
			numIts = aSelection[0].it_string()
			if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
				numIts = -1
			else:
				numIts = int( numIts )
		fout.write( "%d\n" % numIts )
	
	#Select the experiments performed using Newton multigrid
	mgSelection = select_experiments( nmgSelection, innerItType=0 )
	    
	if( len( mgSelection ) > 0 ):
		#Write the nonlinear method, linear iteration and smooth type to file
		fout.write( "1, 0, 0, " )
		i = 0 
		while( i < len(alphas)-1 ):
			aSelection = select_experiments_by_param( "alpha", alphas[i],
				mgSelection )
			if( len( aSelection ) == 0 ):
				numIts = -1
			else:
				numIts = aSelection[0].it_string()
				if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
					numIts = -1
				else:
					numIts = int( numIts )
			fout.write( "%d, " % numIts )
			i = i+1
		##End while
	
		aSelection = select_experiments_by_param( "alpha", alphas[i],
		    nmgSelection )
		if( len( aSelection ) == 0 ):
			numIts = -1
		else:
			numIts = aSelection[0].it_string()
			if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
				numIts = -1
			else:
				numIts = int( numIts )
		fout.write( "%d\n" % numIts )
		
	#Select the experiments performed using Newton-PCG
	npcgSelection = select_experiments( nmgSelection, innerItType=1 )
	    
	if( len( npcgSelection ) > 0 ):
		#Write the nonlinear method, linear iteration and smooth type to file
		fout.write( "1, 1, 1, " )
		i = 0 
		while( i < len(alphas)-1 ):
			aSelection = select_experiments_by_param( "alpha", alphas[i],
				npcgSelection )
			if( len( aSelection ) == 0 ):
				numIts = -1
			else:
				numIts = aSelection[0].it_string()
				if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
					numIts = -1
				else:
					numIts = int( numIts )
			fout.write( "%d, " % numIts )
			i = i+1
		##End while
	
		aSelection = select_experiments_by_param( "alpha", alphas[i],
		    npcgSelection )
		if( len( aSelection ) == 0 ):
			numIts = -1
		else:
			numIts = aSelection[0].it_string()
			if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
				numIts = -1
			else:
				numIts = int( numIts )
		fout.write( "%d\n" % numIts )
		
	#Select the experiments performed using Newton-PGMRES
	npgmresSelection = select_experiments( nmgSelection, innerItType=2 )
	    
	if( len( npgmresSelection ) > 0 ):
		#Write the nonlinear method, linear iteration and smooth type to file
		fout.write( "1, 2, 0, " )
		i = 0 
		while( i < len(alphas)-1 ):
			aSelection = select_experiments_by_param( "alpha", alphas[i],
				npgmresSelection )
			if( len( aSelection ) == 0 ):
				numIts = -1
			else:
				numIts = aSelection[0].it_string()
				if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
					numIts = -1
				else:
					numIts = int( numIts )
			fout.write( "%d, " % numIts )
			i = i+1
		##End while
	
		aSelection = select_experiments_by_param( "alpha", alphas[i],
		    npgmresSelection )
		if( len( aSelection ) == 0 ):
			numIts = -1
		else:
			numIts = aSelection[0].it_string()
			if( numIts.startswith( "-" ) or numIts.startswith( "$" ) ):
				numIts = -1
			else:
				numIts = int( numIts )
		fout.write( "%d\n" % numIts )
	
	#Close the file
	fout.close()
#### End of fucntion 'print_convergence_with_alpha_for_matlab'
























