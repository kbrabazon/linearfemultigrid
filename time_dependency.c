#include "time_dependency.h"
#include "test_cases.h"
#include "richards_eq.h"

/** Performs timestepping for the problems defined by the given parameters
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grids	A hierarchy of grids available to use
 *	\param [in,out] aVarsList	A hierarchy of approximation variables available
 *								to use. The approximation on the finest grid
 *								level is the desired output from this function
 */
void time_dependent_solve( AlgorithmParameters *params, GridParameters **grids,
	ApproxVars **aVarsList )
{
	int tCnt ; //Loop counter
				 
	const Node *node ; //A node on the grid
	InfNorm infNorm ; //Stores information about the infinity norm of a vector
	
	double *prevApprox ; //Stores the value of the approximation from the
						 //previous time step, so that we can check if we have
						 //reached a steady state
	double *sol, *error ; //Used if we know the exact solution to the time
						  //dependent problem
		
	GridParameters *fGrid ; //The fine grid on which we are solving the
							//equation. For a list of member variables see
							//'useful_definitions.h'
	ApproxVars *fVars ; //The approximation variables on the grid on which we
						//are solving. For a list of member variables see file
					    //'useful_definitions.h'
	double *eqRHS ; //The rhs of the continuous equation. This is modified by
					//the timestepping, and so cannot be stored as part of the
					//ApproxVars structure
	Results results ; //Stores information about the results of the experiments
					  //run on the current grid
					  
	char outFName[100] ; //Stores the value of the filename to which to output
						 //data
						 
	const char **meshes ; //Points to the files we are using in the current
						  //grid hierarchy
	TDCtxt *tdCtxt ; //Context that stores information about the time dependent
					 //iteration						  
						  
	//Set the meshes that we want to set up
	meshes = get_grid_hierarchy( params ) ;
	
	tdCtxt = &(params->tdCtxt) ;
	
//	printf( "Time step: %.7lf\tWeighting Factor: %.8lf\n", tdCtxt->tStepSize,
//		tdCtxt->weightFact ) ;

	//Set a placeholder for the current grid level to make the code look neater.
	//We assume that the memory has been assigned and the grid set up
	//appropriately elsewhere
	fGrid = grids[params->fGridLevel-1] ;
	
	//Set a placeholder to the variables associated with the approximation on
	//the fine grid. We assume that the memory has been assigned and the
	//variables set up appropriately elsewhere
	fVars = aVarsList[params->fGridLevel-1] ;
	
	//Set up the initial approximation for the current grid level
	initial_condition( params, fGrid, fVars->approx ) ;
	if( params->debug ){
		sprintf( outFName, "/tmp/td_approxLvl%d_0.vtk", params->fGridLevel ) ;
		print_grid_func_paraview_format( outFName, fGrid, fVars->approx ) ;
	}
	
	//Get the RHS for the weak formulation of the non-time dependent equation.
	//This will not change at all, and we simply need to add on a term related
	//to the current approximation
	eqRHS = (double*) calloc( fGrid->numUnknowns, sizeof( double ) ) ;
	if( params->testCase != 10 && params->testCase != 12 &&
		params->testCase != 52 )
		initialise_rhs( fGrid, params, fVars, tdCtxt->weightFact, eqRHS,
			fVars->bndTerm ) ;
		
	//Set the memory to store the previous approximation
	prevApprox = (double*)malloc( fGrid->numUnknowns * sizeof( double ) ) ;
	
	//Initialise some variables
	results.residL2Norm = -1.0 ;
	results.itCnt = 0 ;
	
	//Now I have calculated everything that I need to start the time stepping.
	//For each time step
	tdCtxt->currTime = tdCtxt->tStart ;
	for( tCnt = 0 ; tCnt < tdCtxt->numTSteps ; tCnt++ ){
		//Calculate the residual in approximation here, and then in the
		//multigrid method we set up the variables only if we are not solving a
		//time dependent problem
		if( params->recoverGrads )
			recover_gradients( params, fGrid, fVars->approx, fVars->recGrads ) ;
		calculate_iteration_matrix( fGrid, params, fVars->approx, fVars ) ;
		
		//Now set up the right hand side of the equation to solve. For this I
		//need to get the current approximation, multiply it by the mass matrix
		//and add it to 'eqRHS'
		calculate_td_rhs( params, eqRHS, fVars, fGrid, fVars->rhs ) ;
		
		if( params->debug ){
			sprintf( outFName, "/tmp/td_rhsLvl%d.txt", params->fGridLevel ) ;
			print_grid_func_no_bnd_to_file( outFName, fGrid,
				fVars->rhs, tCnt ) ;
		}
		
		calculate_residual( params, fGrid, fVars ) ;
				
		//If we are at the first time step we calculate the residual in the
		//approximation here. This will already be done for the subsequent
		//iterations
		if( results.residL2Norm < 0.0 ){
			results.residL2Norm = vect_discrete_l2_norm( fGrid->numUnknowns,
				fVars->resid ) ;
		}
		//Print out the residual at the current time step
		printf( "(%d) L2-norm of residual at time %.5f:  %.18lf", tCnt,
			tdCtxt->currTime, results.residL2Norm ) ;
		printf( "  with %d iterations performed\n", results.itCnt ) ;
		
		//Set the previous approximation with the values of the current
		//approximation
		copy_vectors( fGrid->numUnknowns, prevApprox, fVars->approx ) ;
		
		//Now that I have the right hand side, the approximation and the mass
		//matrix I think I can perform the appropriate multigrid iteration
		//to solve the system of equations
		if( params->nlMethod == METHOD_NLMG ) //Nonlinear multigrid
			multigrid( grids, params, aVarsList, &results ) ;
		else //Newton-Multigrid
			newton_mg( grids, params, aVarsList, &results ) ;
			
		if( params->debug ){
			sprintf( outFName, "/tmp/td_approxLvl%d_%d.vtk",
				params->fGridLevel, tCnt + 1 ) ;
			print_grid_func_paraview_format( outFName, fGrid, fVars->approx ) ;
		}
			
		//Get the current time
		tdCtxt->currTime += tdCtxt->tStepSize ;
		//Get the maximum difference between the approximations at subsequent
		//timesteps
		if( vect_diff_infty_norm( fGrid->numUnknowns, fVars->approx,
			prevApprox ) < 1e-10 ){
			printf( "Steady state reached at time %.5f\n", tdCtxt->currTime ) ;
			break ;
		}
	}
	
	printf( "L2-norm of the residual at final time %.5f: %.18lf",
		tdCtxt->currTime, results.residL2Norm ) ;
	printf( "  with %d iterations performed\n", results.itCnt ) ;
	//Print the function out to file if we are debugging
	//if( params->debug ){
		sprintf( outFName, "/tmp/finalApproxLvl%d.vtk", params->fGridLevel ) ;
		print_grid_func_paraview_format( outFName, fGrid, fVars->approx ) ;
	//}
	
	//If we know the solution, then we print out some statistics about how
	//accurate our approximation is
	if( is_solution_known( params->testCase ) ){
		//Allocate the appropriate memory
		sol = (double*)malloc( fGrid->numUnknowns * sizeof( double ) ) ;
		error = (double*) malloc( fGrid->numUnknowns * sizeof( double ) ) ;
		
		//Calculate the exact solution
		calculate_exact_solution( params, fGrid, sol ) ;
		//Calculate the error
		vect_diff( fGrid->numUnknowns, sol, fVars->approx, error ) ;
		
		//Print the error out to file for debugging
		sprintf( outFName, "/tmp/td_errLvl%d.txt", params->fGridLevel ) ;
		print_grid_func_no_bnd_to_file( outFName, fGrid, error, 0 ) ;
		
		//Get the infinity norm of the vector
		printf( "Error Statistics:\n\t" ) ;
		vect_infty_norm( fGrid->numUnknowns, error, &infNorm ) ;
		node = &(fGrid->nodes[ fGrid->mapA2G[ infNorm.ind ] ]) ;
		
		printf( "Infinity norm of the error: %.18lf at point ", infNorm.val ) ;
		printf( "(%.7lf, %.7lf)\n\t", node->x, node->y ) ;
		printf( "Radius of the point with maximum error: %.18lf\n\t",
			sqrt( node->x*node->x + node->y*node->y ) ) ;
		//Print out the error at the centre point
		node = &(fGrid->nodes[ (fGrid->numNodes-1) / 2 ] ) ;
		printf( "Node at mid point: (%.18lf, %.18lf)\n\t", node->x, node->y ) ;
		printf( "Error of the approximation at point (0,0): %.18lf\n\t",
			error[ fGrid->mapG2A[ (fGrid->numNodes-1) / 2 ] ] ) ;
		//Now the L2 norm of the error
		printf( "L2 norm of the error: %.18lf\n\t",
			vect_discrete_l2_norm( fGrid->numUnknowns, error ) ) ;
		printf( "H1 norm of the error: %.18lf\n",
			vect_discrete_h1_norm( fGrid, error ) ) ;
		
		sprintf( outFName, "/tmp/td_solLvl%d.txt", params->fGridLevel ) ;
		print_grid_func_no_bnd_to_file( outFName, fGrid, sol, 0 ) ;
		//Clean up
		free( sol ) ;
		free( error ) ; 
	}
	
	//Free the memory that was set in this method
	free( eqRHS ) ;
	free( prevApprox ) ;
}

/** Sets up an initial condition for the given parameters
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to operate
 *	\param [out] initApprox	The function to populate with the initial condition
 */
void initial_condition( const AlgorithmParameters *params,
	const GridParameters *grid, double *initApprox )
{
	int i ; //Loop counter
	const Node *node ; //A node on the grid
	//double tmp ; //It should be explained in a comment whenever this value is
				 //used. It is used as a placeholder or intermediate value in
				 //calculations
	
	//If a file has been specified from which to read an approximation try and 
	//read from the file. No checks are done to make sure that the file is in
	//the right format, or that enough points have been specified, or that
	//enough memory has been allocated. It is trusted that the user has passed
	//in an appropriate file
	if( strlen( params->initApproxFile ) > 0 ){
		read_grid_function_from_file( params->initApproxFile, grid,
			initApprox ) ;
		return ;
	}
	
	//If we are performing test case 18 we want to calculate the pressure
	//assuming that the initial saturation has been given
//	if( params->testCase == 18 ){
//		tmp = calc_theta( &(params->reCtxt), grid->soilType[0],
//			params->reCtxt.hOut ) ;
//		for( i=0 ; i < grid->numNodes ; i++ )
//			initApprox[i] = tmp ;
//		set_pressure_for_theta( params, grid, initApprox, initApprox ) ;
//		return ;
//	}
	
	//Loop through every grid point, and assign the appropriate pointwise value
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		node = &(grid->nodes[ grid->mapA2G[i] ]) ;
		initApprox[i] = initial_func( params, grid, node ) ;
	}
}

/** Evaluates the function given as the initial condition at point \f$(x, y)\f$
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] x	\f$x\f$-coordinate of the point at which to evaluate the
 *					function
 *	\param [in] y	\f$y\f$-coordinate of the point at which to evaluate the
 *					function
 *	\return \p double value containing the initial function evaluated at the
 *			given node
 */
double initial_func( const AlgorithmParameters *params,
	const GridParameters *grid, const Node *node )
{
	double retVal = 0.0 ; //Initialise the return value to zero
	
	if( params->testCase == 10 || params->testCase == 12 ||
		params->testCase == 52 ){
		//See the file test_cases.pdf to see how the calculation of the initial
		//condition works. Note that at time t = t_{0} we have that the
		//parameter mu = 1
		//Get the distance from the origin squared divided by the initial radius
		retVal = ( pow( node->x, 2 ) + pow( node->y, 2 ) ) /
			pow( params->initRad, 2 )  ;
		retVal = pow( fmax( 1 - retVal, 0 ), 1.0 / params->powU ) ;
	}
	else if( params->testCase == 15 ||
		params->testCase == 16 || params->testCase == 17 ||
		params->testCase == 18 || params->testCase == 19 ||
		params->testCase == 21 || params->testCase == 22 ||
		params->testCase == 23 || params->testCase == 24 ||
		params->testCase == 25 || params->testCase == 26 ||
		params->testCase == 27 ){
		//We use either the pressure on the outflow boundary, if the problem
		//has an outflow, or we simply take the value that would be assigned
		//to an outflow boundary, just so that we have a starting point
		retVal = params->reCtxt.hOut ;
	}
	else if( params->testCase == 14 ){
		retVal = params->reCtxt.hOut - 50 + node->x / 2 ;
	}
	else if( params->testCase == 20 ){
		//We want a linear profile running from zero at the top bounary to
		//params->reCtxt.hOut at the bottom boundary
		if( node->y >= -25 )
			retVal = 0.0 ;
		else{
			retVal = -params->reCtxt.hOut * ( 25 + node->y ) / 75 ;
		}
	}
		
	//Return from the function
	return retVal ;
}

/** Calculates the value of the initial time for test case 10, which solves the
 *	problem \f$u_{t} = \nabla\cdot\left\{u^{m}\nabla u\right\}\f$. A particular
 *	value of the initial time is required so that we have access to the analytic
 *	solution
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\return \p double value containing the initial time for the problem
 */
double calculate_t_zero( const AlgorithmParameters *params )
{
	//See the file test_cases.pdf for the definition of how to calculate the
	//initial time
	return ( pow( params->initRad, 2 ) * params->powU ) /
		(4.0 * (1 + params->powU) ) ;
}

/** Calculates the value of \f$\mu\f$ for test case 10 (see file
 *	test_cases.pdf for a definition of \f$\mu\f$). This is required so that we
 *	have access to an analytic solution for test case 10
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\return \p double value of \f$\mu\f$
 */
double calculate_mu( const AlgorithmParameters *params )
{
	return pow( params->tdCtxt.currTime / params->tdCtxt.tStart,
		0.5 / (1 + params->powU) ) ;
}

/** Calculates the appropriate right hand side for a time dependent problem
 *	for a given time. This calculation changes depending on which time
 *	discretization is used
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] eqRHS	The integral of the RHS of the equation to solve before
 *						the time dependent term is added
 *	\param [in] aVars	Variables associated with the approximation, which are
 *						used in the calculation of the contribution to the
 *						right hand side at the current time
 *	\param [in] grid	The grid on which we have discretized
 *	\param [out] tdRHS	The right hand side for the current time
 */
void calculate_td_rhs( const AlgorithmParameters *params, const double *eqRHS,
	const ApproxVars *aVars, const GridParameters *grid, double *tdRHS )
{
	const TDCtxt *tdCtxt ;
	
	tdCtxt = &(params->tdCtxt) ;
	
	//Check which discretization is used in time
	if( tdCtxt->tDisc == TIME_DISC_BE ){
		//Backward Euler time discretization
		rhs_backward_euler( params, grid, aVars, eqRHS, tdRHS ) ;
	}
	else if( tdCtxt->tDisc == TIME_DISC_TRAP ){
		//Trapezoidal rule time discretization
		rhs_trap( params, grid, aVars, eqRHS, tdRHS ) ;
	}
}

/** Calculates the right hand side for a time dependent problem where the time
 *	discretization is performed using a backward Euler method
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] aVars	Variables associated with the approximation on the
 *						current grid level
 *	\param [in] eqRHS	The rhs of the original equation, without the extra term
 *						due to the discretization in time
 *	\param [out] tdRHS	The rhs including the term due to the discretization in
 *						time. This is populated in this method
 */
void rhs_backward_euler( const AlgorithmParameters *params,
	const GridParameters *grid, const ApproxVars *aVars, const double *eqRHS,
	double *tdRHS )
{
	int i, j ; //Loop counters
	double tmp ; //Placeholder to store intermediate values in calculations
	const Node *node ; //Placeholder for a node on the grid
	int rowInd ; //The index into the row of the connection matrix for a given
				 //node

	if( is_richards_eq( params->testCase ) ){
		//Calculate the pointwise values of volumetric wetness using the current
		//approximation to the pressure (given in terms of hydraulic head)
		set_theta( params, grid, aVars->approx, aVars->reTheta ) ;
		
		//Set the value of the right hand side to initially contain only the
		//terms from a boundary contribution
		copy_vectors( grid->numUnknowns, tdRHS, aVars->bndTerm ) ;
		
		//If the mass matrix has been lumped we simply multiply theta by the
		//diagonal of the boundary term
		if( params->lumpMassMat ){
			for( i = 0 ; i < grid->numUnknowns ; i++ ){
				rowInd = grid->rowStartInds[i] ;
				tdRHS[i] += aVars->reTheta[i] * aVars->massMat[rowInd] ;
			}
		}
		else{
			//Loop over the nodes on the grid
			for( i = 0 ; i < grid->numUnknowns ; i++ ){
				//For each node get the starting index into the mass matrix
				//row
				node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
				rowInd = grid->rowStartInds[ i ] ;
				//Calculate the matrix-vector product between the mass matrix
				//and the current value of theta
				tmp = aVars->massMat[ rowInd ] * aVars->reTheta[ i ] ;
				//Loop through the neigbourhood of the current node
				for( j=1 ; j < node->numNghbrs ; j++ ){
					tmp += aVars->massMat[rowInd+j] *
						aVars->reTheta[ grid->mapG2A[ node->nghbrs[j] ] ] ;
				}
				tdRHS[i] += tmp ;
			}
		}
	}
	else{
		for( i=0 ; i<grid->numUnknowns ; i++ ){
			//Calculate the value of the mass matrix for the current row times
			//the current approximation
			tmp = 0.0 ;
			node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
			rowInd = grid->rowStartInds[ i ] ;
			for( j=0 ; j < node->numNghbrs ; j++ )
				tmp += aVars->massMat[rowInd+j] *
					aVars->approx[ grid->mapG2A[ node->nghbrs[j] ] ] ;
			//Set the correct value for the right hand side for the current row
			tdRHS[i] = eqRHS[i] + tmp ;
		}
	}
}

/** Performs the calculation of the right hand side for a time dependent problem
 *	where the time discretization has been taken using a trapezoidal rule
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are currently operating
 *	\param [in] aVars	Variables associated with the approximation on the
 *						current grid level
 *	\param [in] eqRHS	The rhs of the original equation, without the extra term
 *						due to the discretization in time
 *	\param [out] tdRHS	The rhs including the term due to the discretization in
 *						time. This is populated in this method
 */
void rhs_trap( const AlgorithmParameters *params, const GridParameters *grid,
	const ApproxVars *aVars, const double *eqRHS, double *tdRHS )
{
	int i, j, elCnt ; //Loop counters
	const Node *node ; //Placeholder for a node on the grid
	int rowInd ; //The index into the connection matrix for the current node on
				 //the grid
	double tmp ; //Stores an intermediate value in the calculation
	const Element *e ; //Placeholder for an element on the grid
	int ind ; //The index of a node in the ordering of the approximation
	
	if( is_richards_eq( params->testCase ) ){
		//Calculate the value of the volumetric wetness and the hydraulic
		//conductivity at the current approximation to the pressure
		set_theta( params, grid, aVars->approx, aVars->reTheta ) ;
		set_hydr_cond( params, grid, aVars->approx, aVars->reHCond ) ;
			
		//Set the time dependent right hand side to zero. We know that this is
		//the base value for the Richards equation, as the initial right hand
		//side is always zero. The term on the right hand side of the system
		//of equations to solve is fully determined by the time discretization
		copy_vectors( grid->numUnknowns, tdRHS, aVars->bndTerm ) ;
		
		//If the mass matrix has been lumped we simply multiply theta by the
		//diagonal of the boundary term
		if( params->lumpMassMat ){
			for( i = 0 ; i < grid->numUnknowns ; i++ ){
				rowInd = grid->rowStartInds[i] ;
				tdRHS[i] += aVars->reTheta[i] * aVars->massMat[rowInd] ;
			}
		}
		else{
			//Loop over the nodes on the grid
			for( i = 0 ; i < grid->numUnknowns ; i++ ){
				//For each node get the starting index into the mass matrix
				//row
				node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
				rowInd = grid->rowStartInds[ i ] ;
				//Calculate the matrix-vector product between the mass matrix
				//and the current value of theta
				tmp = aVars->massMat[ rowInd ] * aVars->reTheta[ i ] ;
				//Loop through the neigbourhood of the current node
				for( j=1 ; j < node->numNghbrs ; j++ ){
					tmp += aVars->massMat[rowInd+j] *
						aVars->reTheta[ grid->mapG2A[ node->nghbrs[j] ] ] ;
				}
				tdRHS[i] += tmp ;
			}
		}
			
		//Loop over all the elements on the grid
		for( elCnt = 0 ; elCnt < grid->numElements ; elCnt++ ){
			//Get a placeholder to the current element on the grid
			e = &(grid->elements[elCnt]) ;
			//Loop over the vertices on the current element
			for( i=0 ; i < 3 ; i++ ){
				node = &(grid->nodes[ e->nodeIDs[i] ]) ;
				//Check that we are not at a Dirichlet boundary
				if( node->type == NODE_DIR_BNDRY ) continue ;
				//Get the index into the approximation for the current node
				ind = grid->mapG2A[ e->nodeIDs[i] ] ;
				//Add to the right hand side term for the current point
				tdRHS[ind] -= params->tdCtxt.weightFact *
					int_richards_h_cond( params, grid, aVars->reHCond, e, i,
					aVars->approx ) ;
			}
		}
	}
	else{
		//For the trapezoidal rule we take off a factor of td/2.0 times the
		//nonlinear diffusion term (assuming that we have no forcing or
		//advection terms)
		//Loop through all of the nodes
		for( i=0 ; i < grid->numUnknowns ; i++ ){
			tmp = 0.0 ;
			//Get the current node on the grid
			node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
			rowInd = grid->rowStartInds[ i ] ;
			//Loop throug the neighbours of the current node, and count a
			//contribution
			for( j=0 ; j < node->numNghbrs ; j++ ){
				//Take a contribution from the mass matrix and from the
				//nonlinear diffusion term
				tmp += ( 2.0 * aVars->massMat[rowInd+j] -
					aVars->iterMat[rowInd+j] ) *
					aVars->approx[ grid->mapG2A[ node->nghbrs[j] ] ] ;
			}
			//Update the appropriate right hand side value
			tdRHS[i] = eqRHS[i] + tmp ;
		}
	}
}

/** Performs updating of the time step. At the moment this is just a basic
 *	implementation which uses very little information about the problem being
 *	solved to update the time step, just to show that updating the time step
 *	is a useful thing to do
 */
void update_time_step( TDCtxt *tdCtxt, const AlgorithmParameters *params,
	const Results *results )
{
	if( results->itCnt >= 3 * params->maxIts / 4 &&
		tdCtxt->tStepSize > tdCtxt->minTStep ){
		//Halve the time step
		tdCtxt->tStepSize /= 2 ;
		tdCtxt->tStepSize = (tdCtxt->tStepSize < tdCtxt->minTStep) ?
			tdCtxt->minTStep : tdCtxt->tStepSize ;
	}
	else if( results->itCnt < 5 ){
		//Double the step size
		tdCtxt->tStepSize *= 2 ;
		tdCtxt->tStepSize = (tdCtxt->tStepSize > tdCtxt->maxTStep) ?
			tdCtxt->maxTStep : tdCtxt->tStepSize ;
	}
	
	//If the time step takes us past the end time, limit the time step to take
	//us to the end time
	if( tdCtxt->currTime + tdCtxt->tStepSize > tdCtxt->tEnd )
		tdCtxt->tStepSize = tdCtxt->tEnd - tdCtxt->currTime ;

	//Make sure we have the correct weighting factor
	switch( tdCtxt->tDisc ){
		case TIME_DISC_BE:
			tdCtxt->weightFact = tdCtxt->tStepSize ;
			break ;
		case TIME_DISC_TRAP:
			tdCtxt->weightFact = tdCtxt->tStepSize / 2.0 ;
			break ;
		default:
			tdCtxt->weightFact = tdCtxt->tStepSize ;
			break ;
	}
}






