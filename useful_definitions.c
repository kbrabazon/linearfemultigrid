#include "useful_definitions.h"
#include <math.h>

/** Set all values of a given vector of doubles to zero
 * 	\param [in] numPoints The number of points in the vector
 *	\param [out] vect  The vector to set the values to zero for
 */
void set_dvect_to_zero( int numPoints, double* vect )
{
	int i ; //Counter
	
	for( i=0 ; i<numPoints ; i++ )
		vect[i] = 0.0 ;
}

/** Set all values of a given vector of ints to zero
 *	\param [in] numPoints The number of points in the vector
 *	\param [out] vect  The vector to set the values to zero for
 */
void set_ivect_to_zero( int numPoints, int* vect )
{
	int i ; //Counter
	
	for( i=0 ; i < numPoints ; i++ )
		vect[i] = 0 ;
}

/** Calculates the infinity norm of the difference of two vectors
 *	\param [in] numPoints The number of points in the vectors
 *	\param [in] vect1	  One vector to take the difference with
 *	\param [in] vect2	  A second vector to take the difference with
 *	\return A double containing the value of the infinity norm of the difference
 			between the vectors passed in
 */
double vect_diff_infty_norm( int numPoints, const double* vect1,
	const double* vect2 )
{
	int i ; //Loop counter
	double max, comp ;
	
	//Loop through all the points and return the maximum value
	max = fabs( vect1[0] - vect2[0] ) ;
	for( i=1 ; i < numPoints ; i++ ){
		comp = fabs( vect1[i] - vect2[i] ) ;
		if( max < comp ) max = comp ;
	}
	
	return max ;
}


/** Calculates the difference between two given vectors and stores it in a third
 *	\param [in] numPoints The number of points in the vectors passed in
 *	\param [in] vect1	  First vector used to take the difference with
 *	\param [in] vect2	  The values in this vector are subtracted from that of
 						  vect1
 *	\param [out] diff	  Vector containing the pointwise differences between
 						  vect1 and vect2
 */
void vect_diff( int numPoints, const double* vect1, const double* vect2,
	double* diff )
{
	int i ;
	for( i=0 ; i < numPoints ; i++ )
		diff[i] = vect1[i] - vect2[i] ;
}

/** Calculates the infinity norm of the vector passed in
 *	\param [in] numPoints The number of points in the vector passed in
 *	\param [in] vect	  The vector to calculate the infinity norm for
 *	\return double value containing the infinity norm of the vector over the
 			given number of points
 */
void vect_infty_norm( int numPoints, const double* vect, InfNorm *norm )
{
	int i ;
	double comp ;
	norm->val = fabs( vect[0] ) ;
	norm->ind = 0 ;
	for( i=1 ; i < numPoints ; i++ ){
		comp = fabs( vect[i] ) ;
		if( norm->val < comp ){
			norm->val = comp ;
			norm->ind = i ;
		}
	}
}

/** Returns the maximum and minimum values of a vector
 * 	\param [in] numPoints	The number of points in the vector passed in
 *	\param [in] vect	The vector for which to find the maximum absolute value
 *	\param [out] maxMin	Two dimensional vector containing the values of the 
 *						maximum and minimum elements in the vector \e vect
 */
void vect_max_min_val( int numPoints, const double* vect,
	Vector2D *maxMin )
{
	int i ;
	double comp ;
	
	maxMin->y = maxMin->x = vect[0] ;
	for( i=1 ; i < numPoints ; i++ ){
		comp = vect[i] ;
		if( comp > maxMin->x ) maxMin->x = comp ;
		else if( comp < maxMin->y ) maxMin->y = comp ;
	}
}

/** Moves the cursor up the specified number of lines in a LINUX terminal only.
 *  This code may be a problem when used on other systems. On windows this will
 *  certainly not work.
 *  params [in] numLines	The number of lines to move the cursor up in a LINUX
 *							terminal
 */
void move_cursor_pos_up( int numLines )
{
	printf( "\033[%dA", numLines ) ;
	fflush( stdout ) ;
}

/** Calculates the (discrete) L2 norm of a function
 *	\param [in] numPoints The number of points in the vector passed in
 *	\param [in] vect	  The vector to calculate the discrete L2 norm for
 *	\return double value containing the discrete L2 norm of the vector over the
 			given number of points
 */
double vect_discrete_l2_norm( int numPoints, const double* vect )
{
	int i ;//Loop counter
	double sum ;//Stores the sum of the squares of the components of 'vect'
	
	//Initialise the sum
	sum = 0.0 ;
	
	//Add the squares of the individiual components to the sum
	for( i=0 ; i < numPoints ; i++ )
		sum += pow(vect[i],2) ;
	
	//Use the number of points as a weighting factor
	sum /= numPoints ;
	
	//Return the square root of the mean of the squares
	return sqrt( sum ) ;
}

/** Calculates the (discrete) H1 seminorm of a function discretized on a regular
 * 	triangular mesh
 *	\param [in] grid	The grid on which the vector represents a discretized
 *						function. This is required to calculate the derivative
 *						of the vector
 *	\param [in] vect	  The vector to calculate the discrete H1 seminorm for
 *	\return double value containing the discrete H1 seminorm of the vector over
 			the	given number of points
 */
double vect_discrete_h1_seminorm( const GridParameters *grid,
	const double *vect )
{
	int i ; //Loop counter
	Vector2D *grads ; //Array to store the value of the approximation to the
					//pointwise gradients
	double sum ;//Stores the running total of the sum of the squares of the
				//elementsin the gradient
	
	//Set up the required memory for the gradients
	grads = (Vector2D*)malloc( grid->numUnknowns * sizeof( Vector2D ) ) ;
	
	//Approximate the grad of the function
	func_grad_regular_mesh( grid, vect, grads ) ;
	
	//Initialise the sum to zero
	sum = 0.0 ;
	
	//Loop through all of the points and update the sum
	for( i=0 ; i < grid->numUnknowns ; i++ )
		sum += grads[i].x * grads[i].x + grads[i].y * grads[i].y ;
		
	//Weight the sum according to the number of points that are on the grid
	sum /= grid->numUnknowns ;
	
	//Clean up
	free( grads ) ;
	
	//return the square root of the sum
	return sqrt( sum ) ;
}

/** Calculates the (discrete) H1 norm of a vector
 *	\param [in] grid	The grid on which the vector represents a discretized
 *						function. This is required to calculate the appropriate
 *						derivative of the vector
 *	\param [in] vect	  The vector to calculate the discrete H1 norm for
 *	\return double value containing the discrete H1 norm of the vector over
 			the	given number of points
 */
double vect_discrete_h1_norm( const GridParameters *grid, const double *vect )
{
	//We calclulate the L2 norm, the H1 semi-norm, and then return the square
	//root of the sum of the squares
	double l2Norm ;
	double h1SNorm ;
	
	//Get the value of the L2 norm
	l2Norm = vect_discrete_l2_norm( grid->numUnknowns, vect ) ;
	//Get the value of the H1 seminorm
	h1SNorm = vect_discrete_h1_seminorm( grid, vect ) ;
	
	//Return the value of the H1 norm
	return sqrt( pow( l2Norm, 2 ) + pow( h1SNorm, 2 ) ) ;
}

/** Calculates the inner product of two different vectors. It is assumed that
 *	all vectors passed in have the correct amount of memory assigned to them.
 *	\param [in] numPoints	The number of elements in the vectors
 *	\param [in] vect1		The first vector to take the inner product with
 *	\param [in] vect2		The second vector to take the inner product with
 *	\return double value containing the result of taking the inner product of
 *			the vectors passed in
 */
double vect_inner_product( int numPoints, const double *vect1,
	const double *vect2 )
{
	int i ; //Loop counter
	double sum = 0 ; //Set the sum to be zero to start with
	
	for( i=0 ; i < numPoints ; i++ )
		sum += vect1[i] * vect2[i] ;
		
	return sum ;
}

/** Calculates a weighted inner product between two different vectors. It is
 *	assumed that all vectors passed have the correct amount of memory assigned
 *	to them
 *	\param [in] numPoints	The number of elements in the vectors. This is also
 *							the weighting factor to be used
 *	\param [in] vec1		The first vector to take the inner product with
 *	\param [in] vec2		The second vector to take the inner product with
 *	\return	double value containing the result of taking the inner product of
 *			the vectors passed in, weighted by the inverse of the number of
 *			points in the vectors.
 */
double vect_weighted_inner_product( int numPoints, const double *vec1,
	const double *vec2 )
{
	return vect_inner_product( numPoints, vec1, vec2 ) / numPoints ;
}

/** Approximates the gradient of a function discretized on a regular mesh,
 *	assuming that a piecewise linear basis has been used
 *	\param [in] grid	The grid on which to calculate the derivative of the
 *						function. This assumes that the function passed in is
 *						discretized on this grid
 *	\param [in] func		The function to approximate the derivative for
 *	\param [out] grad		The approximation to the gradient of the discretized
 							function
 */
void func_grad_regular_mesh( const GridParameters *grid, const double *func,
	Vector2D *grad )
{
	int l, r, t, b ; //Indicators as to whether we are at the left (l), right
					 //(r), top (t), or bottom (b) of the grid
	int row ; //Counter for which row we are on
	int index ; //Index into the variables 'func' and 'grad'
	
	//Stores the value of twice the grid spacing to save on multiplications in
	//the code
    double h, h2 ;
    int numCols, numRows ; //The number of columns / rows in the grid function,
    					   //excluding the boundaries
    
    numCols = grid->numCols ;
    numRows = grid->numRows ;
    //If there is a Dirichlet boundary on the left or the right reduce the
    //number of columns in the approximation by one
    if( grid->bndType[0] == NODE_DIR_BNDRY ) numCols-- ;
    if( grid->bndType[1] == NODE_DIR_BNDRY ) numRows-- ;
    if( grid->bndType[2] == NODE_DIR_BNDRY ) numCols-- ;
    if( grid->bndType[3] == NODE_DIR_BNDRY ) numRows-- ;
	
	//Set placeholders for the grid spacing and double the grid spacing
	h = grid->gridSpacing ;
	h2 = 2.0 * h ;
	
	//Set the row to initially be zero
	row = 0 ;
	
	//We know that we start off at the bottom-left corner
	b=l=1 ;
	r=t=0 ;
	
	//Loop through every unknown point
	for( index = 0 ; index < grid->numUnknowns ; index++ ){
		//Figure out if we are at the right boundary or not
		//If we are at the last node in a row we are at the right hand boundary
		if( (index % (numCols)) == numCols-1 ) r=1 ;
		
		//If I am at the left or right boundary I want to take a one sided
		//approximation of the x-gradient
		if( l ){
			//Take the value to the right of the current index and calculate the
			//one sided approximation to the x-gradient
			grad[index].x = (func[index+1] - func[index]) / h ;
			//Make sure we don't think that we are on the left hand boundary
			//any more
			l=0 ;
		}
		else if( r ){
			//Take the value to the left of the current index, and calculate the
			//one sided approximation to the x-gradient
			grad[index].x = (func[index] - func[index-1]) / h ;
		}
		else{
			//We are in the interior of the row, so we can take a central
			//difference approximation for the x-gradient
			grad[index].x = (func[index+1] - func[index-1]) / h2 ;
		}
		
		//If I am at the bottom or top boundary I want to take a one sided
		//approximation of the y-gradient
		if( b ){
			//Take the value to the top of the current index, and calculate the
			//one sided approximation to the y-gradient
			grad[index].y = (func[index + numCols] - func[index]) / h ;
		}
		else if( t ){
			//Take the value to the bottom of the current index, and caluclate
			//the one sided approximation to the y-gradient
			grad[index].y = (func[index] - func[index - numCols]) / h ;
		}
		else{
			//We are in the interior of the column, so we can take a central
			//difference approximation
			grad[index].y = (func[index + numCols] - func[index - numCols])
				/ h2 ;
		}
		
		//If we were previously at the right hand boundary we are now at the
		//left hand boundary
		if( r ){
			r=0 ;
			l=1 ;
			//increment the row counter, and make sure that we are not at the
			//bottom anymore
			row++ ;
			b=0 ;
		}
		
		//We are at the top boundary if we are on the last row
		if( row == numRows-1 ) t=1 ;
	}
}

/** Copies the contents of one vector into another
 *	\param [in] numPoints	The number of points in the vectors
 *	\param [out] vec1		The vector to copy the values into
 *	\param [in] vec2		The vector to copy the values from
 */
void copy_vectors( int numPoints, double* vec1, const double* vec2 )
{
	int i ; //Used as a counter in for loops
	
	for( i=0 ; i < numPoints ; i++ )
		vec1[i] = vec2[i] ;
}

/** Adds one vector to another, and stores the result in the first vector
 *	\param [in] numPoints	The number of points in the vectors
 *	\param [in,out] vec1	The vector to which to add a contribution
 *	\param [in] vec2		The vector of contribution values
 */
void add_to_vect( int numPoints, double *vec1, const double *vec2 )
{
	int i ; 
	for( i = 0 ; i < numPoints ; i++ )
		vec1[i] += vec2[i] ;
}

/** Calculates a pseudo-random integer in the given range [min,max). This uses a
 *	suggestion that was posted on 'http://stackoverflow.com/questions/
 *	2509679/how-to-generate-a-random-number-from-within-a-range-c'
 *	so proper credit needs to be given. It is also assumed that the random
 *	function has been seeded appropriately. The necessity for this extended
 *	function is to ensure a uniform distribution of random numbers
 *	\param [in] min		The lower limit of the range of numbers to return
 *	\param [in] max		One integer larger than the maximum value to return from
 						from this function
 *	\return pseudo-random integer in the range [min, max)
 */
unsigned int rand_in_range( unsigned int min, unsigned int max )
{
	int rNum ; //The initial random number
	int range ; //The size of the interval which to return
	int remainder ; //The remainder term
	int bucket ; //The width of a 'bucket' into which to heap numbers
	
	rNum = rand() ; //Returns a number in [0, RAND_MAX]
	//If the random number returned is the maximum then re-run the method to get
	//some other number. This is necessary, as otherwise there will be the
	//possibility of division by zero later on
	if( rNum == RAND_MAX ) return rand_in_range( min, max ) ;
	
	//Find the range of the values to find
	range = max - min ;
	//Get the remainder of the division of RAND_MAX by the range. This gives
	//an interval at the end which may be smaller than the other intervals,
	//and so gives an invalid term to return
	remainder = RAND_MAX % range ;
	//Find the width of a 'bucket' from which a number is assigned to a
	//particular number in the given range
	bucket = RAND_MAX / range ;
	
	//Check that the random number is within the acceptable range of values
	if( rNum < RAND_MAX - remainder ){
		//Return the appropriate pseudo-random integer
		return rNum / bucket + min ;
	}
	else{
		return rand_in_range( min, max ) ;
	}
}

/** Calculates the factorial of an integer value. This assumes that the integer
 *	passed in is non-negative, and will return 1 for values of zero or negative
 *	numbers
 * \param [in] n	The integer number which we are interested in knowing the
 *					factorial of
 * \return	\p unsigned \p int value containing the value of the factorial of
 *			the number that is passed in
 */
unsigned int factorial( unsigned int n )
{
	unsigned int retVal = 1 ;
	int i ;
	for( i = 2 ; i < n + 1 ; i++ )
		retVal *= i ;
		
	return retVal ;
}

/** Converts the sparse matrix representation of a matrix to a full matrix
 *  and prints it out to a file
 *	\param [in] fName	The filename of the file to write output to
 *	\param [in] grid	The grid on which the function is defined
 *	\param [in] sparseMat	The sparse representation of the matrix to print
 *							to file
 */
void print_full_matrix_to_file( const char *fName, const GridParameters *grid,
	const double *sparseMat )
{
	int i, j, k ;
	int fRowInd ; //The index of the starting position in the row of the full
				  //matrix
	const Node *node ;
	FILE *fout ;
	double *fullMat ;
	fout = fopen( fName, "w" ) ;
	
	//Initialise a matrix to zero with the correct number of entries in
	fullMat = (double*)calloc( grid->numUnknowns * grid->numUnknowns, 
		sizeof( double ) ) ;
	
	//For each row in the matrix, or equivalently each point in the
	//approximation
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		//Get the current node on the grid
		node = &(grid->nodes[ grid->mapA2G[ i ] ]) ;
		//Get the starting index of the row in the sparse matrix representation
		k = grid->rowStartInds[i] ;
		fRowInd = i * grid->numUnknowns ;
		for( j=0 ; j < node->numNghbrs ; j++ ){
			fullMat[ fRowInd + grid->mapG2A[ node->nghbrs[j] ] ] =
				sparseMat[ k + j ] ;
		}
	}
	
	//Now write out the matrix to file
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		fRowInd = i * grid->numUnknowns ;
		for( j=0 ; j < grid->numUnknowns-1 ; j++ )
			fprintf( fout, "%.18lf, ", fullMat[ fRowInd + j ] ) ;
		fprintf( fout, "%.18lf\n", fullMat[ fRowInd + grid->numUnknowns -1 ] ) ;
	}
	
	//Clean up
	free( fullMat ) ;
	fclose( fout ) ;
}

/** Calculates the magnitudes of the vectors in a two dimensional vector field
 *	\param [in] numPoints	The number of points in the vector field
 *	\param [in] vectField	The vector field for which to calculate the
 *							magnitudes
 *	\param [out] magnitude	The magnitudes of the vectors at the points in the
 *							vector field
 */
void vect_field_magnitude( int numPoints, const Vector2D *vectField,
	double *magnitude )
{
	int i ;
	
	//Loop through the number of points in the vector field, and calculate the
	//magnitude of each vector component
	for( i = 0 ; i < numPoints ; i++ ){
		magnitude[i] = sqrt( pow( vectField[i].x, 2 ) +
			pow( vectField[i].y, 2 ) ) ;
	}
}

/**	Finds the minimum \f$x\f$-coordinate of a vertex on a given element
 *	\param [in] e	The element on which to find the minimum \f$x\f$-coordinate
 *					of a vertex
 *	\param [in] nodes	A list of nodes on the grid
 *	\return \p double value containing the minimum \f$x\f$-coordinate of a node
 *			on the current element
 */
double element_min_x( const Element *e, const Node *nodes )
{
	int i ; //Loop counter
	double minX, compVal ;
	
	//Set the x-coordinate of the first node to be the minimum
	minX = nodes[ e->nodeIDs[0] ].x ;
	
	//If the x-coordinate of another node on the element is less than the first
	//node then update the value of the minimum x-coordinate
	for( i=1 ; i<3 ; i++ ){
		compVal = nodes[ e->nodeIDs[i] ].x ;
		minX = (minX < compVal ) ? minX : compVal ;
	}
	
	//Return the minimum x-coordinate of a vertex on the current element
	return minX ;
}

/** Finds the maximum \f$x\f$-coordinate of a vertex on a given element
 *	\param [in] e	The element on which to find the maximum \f$x\f$-coordinate
 *					of a vertex
 *	\param [in] nodes	A list of nodes on the grid
 *	\return \p double value containing the maximum \f$x\f$-coordinate of a node
 *			on the current element
 */
double element_max_x( const Element *e, const Node *nodes )
{
	int i ; //Loop counter
	double maxX, compVal ; //The maximum x-value and the value to compare to
	
	//Set the x-coordinate of the first node to be the maximum
	maxX = nodes[ e->nodeIDs[0] ].x ;
	
	//If the x-coordinate of another node on the element is less than the first
	//node then update the value of the maximum x-coordinate
	for( i=1 ; i < 3 ; i++ ){
		compVal = nodes[ e->nodeIDs[i] ].x ;
		maxX = (maxX > compVal) ? maxX : compVal ;
	}
	
	//Return the maximum x-coordinate of a vertex on the current element
	return maxX ;
}

/** Finds the minimum \f$y\f$-coordinate of a vertex on a given element
 *	\param [in] e	The element on which to find the minimum \f$y\f$-coordinate
 *					of a vertex
 *	\param [in] nodes	A list of nodes on the grid
 *	\return \p double value containing the minimum \f$y\f$-coordinate of a node
 *			on the current element
 */
double element_min_y( const Element *e, const Node *nodes )
{
	int i ; //Loop counter
	double minY, compVal ;
	
	//Set the y-coordinate of the first node to be the minimum
	minY = nodes[ e->nodeIDs[0] ].y ;
	
	//If the y-coordinate of another node on the element is less than the first
	//node then update the value of the minimum y-coordinate
	for( i=1 ; i<3 ; i++ ){
		compVal = nodes[ e->nodeIDs[i] ].y ;
		minY = (minY < compVal ) ? minY: compVal ;
	}
	
	//Return the minimum y-coordinate of a vertex on the current element
	return minY ;
}

/** Finds the maximum \f$y\f$-coordinate of a vertex on a given element
 *	\param [in] e	The element on which to find the maximum \f$y\f$-coordinate
 *					of a vertex
 *	\param [in] nodes	A list of nodes on the grid
 *	\return \p double value containing the maximum \f$y\f$-coordinate of a node
 *			on the current element
 */
double element_max_y( const Element *e, const Node *nodes )
{
	int i ; //Loop counter
	double maxY, compVal ; //The maximum y-value and the value to compare to
	
	//Set the y-coordinate of the first node to be the maximum
	maxY = nodes[ e->nodeIDs[0] ].y ;
	
	//If the y-coordinate of another node on the element is less than the first
	//node then update the value of the maximum y-coordinate
	for( i=1 ; i < 3 ; i++ ){
		compVal = nodes[ e->nodeIDs[i] ].y ;
		maxY = (maxY > compVal) ? maxY : compVal ;
	}
	
	//Return the maximum y-coordinate of a vertex on the current element
	return maxY ;
}

/** Finds and sets the minimum and maximum \f$x\f$- and \f$y\f$-coordinates for
 *	a given element
 *	\param [in] e	The element to calculate the bounds for
 * 	\param [in] nodes	A list of nodes on the grid
 *	\param [out] bounds	The upper and lower limits of a bounding box containing
 *						the given element
 */
void set_element_bounds( const Element *e, const Node *nodes, XYBounds *bounds )
{
	int i ; //Loop counter
	double compVal ; //Value used in comparisons
	
	//Set the minimum and maximum x and y values to be the x and y values of the
	//first node on the element
	bounds->minX = bounds->maxX = nodes[ e->nodeIDs[0] ].x ;
	bounds->minY = bounds->maxY = nodes[ e->nodeIDs[0] ].y ;
	
	//Check if the maximum or minimum occurs at a different node on the element
	//from the first one. If it does then update the appropriate value in the
	//bounds
	for( i=1 ; i < 3 ; i++ ){
		//Check the case of the x-coordinate
		compVal = nodes[ e->nodeIDs[i] ].x ;
		if( compVal < bounds->minX ) bounds->minX = compVal ;
		else if( compVal > bounds->maxX ) bounds->maxX = compVal ;
		
		//Check the case of the y-coordinate
		compVal = nodes[ e->nodeIDs[i] ].y ;
		if( compVal < bounds->minY ) bounds->minY = compVal ;
		else if( compVal > bounds->maxY ) bounds->maxY = compVal ;
	}
}

/** Initialises the variables storing information on the conant coefficient
 *	function regions on the domain to NULL
 *	\param [in,out] regs	The coefficient regions variable to initialise
 */
void initialise_coeff_regs_to_null( CoeffRegions *regs )
{
	regs->numRegions = 0 ;
	regs->cornerPoints = NULL ;
	regs->width = NULL ;
	regs->height = NULL ;
	regs->val = NULL ;
}

/** Frees the memory for the variables storying information on the constant
 *	coefficient function on the domain
 *	\param [in,out] regs	The coefficient regions variable for which to free
 *							the memory
 */
void free_coeff_regs( CoeffRegions *regs )
{
	if( regs->cornerPoints != NULL ){
		free( regs->cornerPoints ) ;
		regs->cornerPoints = NULL ;
	}
	if( regs->width != NULL ){
		free( regs->width ) ;
		regs->width = NULL ;
	}
	if( regs->height != NULL ){
		free( regs->height ) ;
		regs->height = NULL ;
	}
	if( regs->val != NULL ){
		free( regs->val ) ;
		regs->val = NULL ;
	}
}
















