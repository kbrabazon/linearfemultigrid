#include "test_cases.h"
#include "useful_definitions.h"
#include "multigrid_functions.h"
#include "richards_eq.h"
 
 /** Initialises a given function with values depending on the parameters passed
 *	in
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to operate
 *	\param [out] approx	The function to populate with an initial value
 */
void initialise_approx( const AlgorithmParameters *params,
	const GridParameters *grid,	double* approx )
{
	int cnt, index ;
	
	//If we specified a file to read from then update the approximation with
	//what is in the file. It is expected that the user knows what file is
	//being passed in, and that the values in there are appropriate
	if( strlen( params->initApproxFile ) > 0 ){
		read_grid_function_from_file( params->initApproxFile, grid, approx ) ;
		return ;
	}
	
	for( cnt=0 ; cnt < grid->numUnknowns ; cnt++ ){
		index = grid->mapA2G[cnt] ;
		//index = grid->interior_node_index[cnt] ;
		//We use the difference in the x coordinates of the first two vertices
		//as the grid spacing used. This is because I know that I am using a
		//regular mesh, and that the nodes are ordered in rows. Therefore the
		//difference in x-coordinate between the first and second nodes will
		//give the correct value
		approx[cnt] = approx_function( params, grid->nodes[index].x,
			grid->nodes[index].y, grid->nodes[1].x - grid->nodes[0].x ) ;
	}
}

/** Returns the value of the function used to initialise the approximation
 *	evaluated at point \f$(x, y)\f$
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] x	The x coordinate of the point at which to evaluate the
 *					function
 *	\param [in] y	The y coordinate of the point at which to evaluate the
 *					function
 *	\param [in] gridSpacing	The uniform grid spacing in the x- and y-directions.
 *							This can be used to add a scaled perturbation to an
 *							approximation
 *	\return	\p double value of the initialising function evaluated at the given
 *			coordinate
 */
double approx_function( const AlgorithmParameters *params,
	double x, double y, double gridSpacing )
{
	//IMPORTANT: ALWAYS READ THIS WHEN CHANGING THIS VALUE!!!!!
	//Remember that if this is changed to change the value that is printed to
	//the output files in file 'multigrid_runs.py' so that the correct output is
	//given
	//
	//IMPORTANT: REMEMBER TO READ THE ABOVE PARAGRAPH!
	double fac ;
	//If we are using test case 6 or 7 we want to use a different function
	if( params->testCase == 6 || params->testCase == 7 ||
		params->testCase == 13 || is_p_laplacian( params->testCase ) ||
		(params->testCase >= 40 && params->testCase <= 51) ||
		(params->testCase >= 56 && params->testCase <= 58) )
		return 0.7 * solution_function( params, x, y ) ;
		//return 0.7 * (params->solSmoothAmp * sin( PI * x ) * sin( PI * y ) ) ;// +
			//params->solOscAmp * sin( params->solXFreq * PI * x ) *
			//sin( params->solYFreq * PI * y )) ;
	else if( params->testCase == 8 || params->testCase == 9 )
		return 0.0 ;
	else if( params->testCase == 11 )
		return 0.9 * cos( PI * x / 2 ) * cos( PI * y / 2 ) ;
			
	fac = 1.0 ;// / (3.0 * gridSpacing) ;
	return 0.7 * sin( fac * PI * x ) * sin( fac * PI * y ) ;
		//+ gridSpacing * rand_in_range( 1, 101 ) / 100.0 ; //READ THE ABOVE!
	//READ THE COMMENTS ABOVE!
}

/** Returns the value of the right hand side function evaluated at the given
 *	coordinate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] x		The x coordinate of the point for which to calculate the
 *						function
 *	\param [in] y		The y coordinate of the point for which to calculate the
 *						function
 *	\return \p double with the value fo the right hand side function evaluated
 *			at the given point
 */
double rhs_function( const AlgorithmParameters* params, const ApproxVars *aVars,
	double x, double y,	int elId )
{
	double retVal ;
	double m ; //Placholder required for the calculation of the rhs for test
			   //case 11
	double sx, sy ; //Placeholders for the value of sin(pi x[y]) if they are
					 //required often
	double cx, cy, cxcy ; //Placeholders for the value of cos(pi x[y]) if they
						  //are required often
	double s2x, s2y, c2x, c2y ; //Placeholders for the values of cos^2 and sin^2
								//of x and y if they are required
								
	double oSx, oSy ; //Value of sin( n[m] pi x[y] )
	double oCx, oCy ; //Value of cos( n[m] pi x[y] )
	double oS2x, oS2y, oC2x, oC2y ; //The squares of the preceding values
								
	double dx, dy, d2x, d2y ; //The derivative of the solution wrt x and y,
							  //and the second partial derivatives wrt x and y
	double xDerivDx2, xDerivDy2 ; //The partial derivatives wrt x of the
								  //partial derivative wrt x and y squared
	double yDerivDx2, yDerivDy2 ; //The partial derivatives wrt y of the
								  //partial derivative wrt x and y squared
	double u ; //The solution function. This is used for some test cases to
			   //calculate the rhs (see test case 7)
	
	//Set a default value for the return value
	retVal = 0.0 ;
	//For Richards equation test cases the right hand side is zero
	if( is_richards_eq( params->testCase ) ) return retVal ;
	
	if( params->testCase == 1 )
		retVal = 2 * PI * PI * sin( PI * x ) * sin( PI * y );
	else if( params->testCase == 2 || params->testCase == 4 ){
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		cx = cos( PI * x ) ;
		cy = cos( PI * y ) ;
		retVal = 2 * PI2 * sx * sy *( 1 - params->alpha * 2.0 *
			PI2 * ( pow(cx,2) * pow(cy,2) - pow(sx,2) * pow(cy,2) -
				pow(cx,2) * pow(sy,2) ) ) ;
	}
	else if( params->testCase == 3 || params->testCase == 5 ){
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		s2x = pow( sx, 2 ) ;
		s2y = pow( sy, 2 ) ;
		c2x = pow( cos( PI * x ), 2 ) ;
		c2y = pow( cos( PI * y ), 2 ) ;
		
		retVal = PI2 * sx * sy * ( 2.0 - params->alpha * (
			s2x * ( 2.0 * c2y - s2y ) + s2y * ( 2.0 * c2x - s2x) ) ) ;
	}
	else if( params->testCase == 6 ){
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		cx = cos( PI * x ) ;
		cy = cos( PI * y ) ;
		c2x = pow( cx, 2 ) ;
		c2y = pow( cy, 2 ) ;
		s2x = pow( sx, 2 ) ;
		s2y = pow( sy, 2 ) ;
		
		//Calculate and store the values of the sines for the oscillatory
		//components of the error
		oSx = sin( params->solXFreq * PI * x ) ;
		oSy = sin( params->solYFreq * PI * y ) ;
		oCx = cos( params->solXFreq * PI * x ) ;
		oCy = cos( params->solYFreq * PI * y ) ;
		oS2x = pow( oSx, 2 ) ;
		oS2y = pow( oSy, 2 ) ;
		oC2x = pow( oCx, 2 ) ;
		oC2y = pow( oCy, 2 ) ;
		//Calculate the partial derivative of the solution wrt x, as we know
		//the exact solution for this function, and use this to calculate the
		//rhs
		dx = PI * ( params->solSmoothAmp * cx * sy + params->solOscAmp *
			params->solXFreq * oCx * oSy ) ;
		//Calculate the partial derivative wrt y
		dy = PI * (params->solSmoothAmp * sx * cy +
			params->solOscAmp * params->solYFreq * oSx * oCy ) ;
		//Calculate the second partial derivative wrt x
		d2x = - PI2 * (params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solXFreq, 2 ) * oSx * oSy ) ;
		//Calculate the second partial derivative wrt y
		d2y = - PI2 * (params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solYFreq, 2 ) *
			oSx * oSy ) ;
		//Calculate the partial derivative wrt x of the square of the first
		//partial derivative wrt x
		xDerivDx2 = - 2 * pow( PI, 3 ) * ( pow( params->solSmoothAmp, 2 ) * cx *
			sx * s2y + pow( params->solXFreq, 3 ) *
			pow( params->solOscAmp, 2 ) * oCx * oSx * oS2y +
			params->solSmoothAmp * params->solOscAmp *
			params->solXFreq * sy * oSy * ( sx * oCx +
			params->solXFreq * cx * oSx ) ) ;
		//Calculate the partial derivative wrt x of the square of the first
		//partial derivative wrt y
		xDerivDy2 = 2 * pow( PI, 3 ) * ( pow( params->solSmoothAmp, 2 ) * sx *
			cx * c2y + pow( params->solOscAmp, 2 ) *
			pow( params->solYFreq, 2 ) * params->solXFreq * oSx * oCx * oC2y +
			params->solSmoothAmp * params->solOscAmp * params->solYFreq * cy *
			oCy * ( cx * oSx + params->solXFreq * sx * oCx ) ) ;
		//Calculate the partial derivative wrt y of the square of the first
		//partial derivative wrt x
		yDerivDx2 = 2 * pow( PI ,3 ) * ( pow( params->solSmoothAmp, 2 ) * c2x *
			sy * cy + pow( params->solOscAmp, 2 ) * pow( params->solXFreq, 2 ) *
			params->solYFreq * oC2x * oSy * oCy + params->solSmoothAmp *
			params->solOscAmp * params->solXFreq * cx * oCx * ( cy * oSy +
			params->solYFreq * sy * oCy ) ) ;
		//Calculate the partial derivative wrt y of the square of the first
		//partial derivative wrt y
		yDerivDy2 = - 2 * pow( PI, 3 ) * ( pow( params->solSmoothAmp, 2 ) *
			s2x * cy * sy + pow( params->solOscAmp, 2 ) *
			pow( params->solYFreq, 3 ) * oS2x * oCy * oSy +
			params->solSmoothAmp * params->solOscAmp * params->solYFreq * sx *
			oSx * ( sy * oCy + params->solYFreq * cy * oSy ) ) ;
			
		//Calculate the x partial derivative part of the divergence operator
		retVal = -d2x * ( 1 + params->alpha * ( pow(dx,2) + pow(dy,2) ) ) -
			params->alpha * dx * (xDerivDx2 + xDerivDy2) ;
		//Now add on the y partial derivative part of the divergence operator
		retVal -= d2y * ( 1 + params->alpha * ( pow(dx,2) + pow(dy,2) ) ) +
			params->alpha * dy * ( yDerivDx2 + yDerivDy2 ) ;
	}
	else if( params->testCase == 7 ){
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		cx = cos( PI * x ) ;
		cy = cos( PI * y ) ;
		s2x = pow( sx,2 ) ;
		s2y = pow( sy, 2 ) ;
		c2x = pow( cx, 2 ) ;
		c2y = pow( cy, 2 ) ;
		
		oSx = sin( params->solXFreq * PI * x ) ;
		oSy = sin( params->solYFreq * PI * y ) ;
		oCx = cos( params->solXFreq * PI * x ) ;
		oCy = cos( params->solYFreq * PI * y ) ;
		oS2x = pow( oSx, 2 ) ;
		oS2y = pow( oSy, 2 ) ;
		oC2x = pow( oCx, 2 ) ;
		oC2y = pow( oCy, 2 ) ;
		//Calculate the partial derivative of the solution wrt x, as we know
		//the exact solution for this function, and use this to calculate the
		//rhs
		dx = PI * ( params->solSmoothAmp * cx * sy + params->solOscAmp *
			params->solXFreq * oCx * oSy ) ;
		//Calculate the partial derivative wrt y
		dy = PI * (params->solSmoothAmp * sx * cy +
			params->solOscAmp * params->solYFreq * oSx * oCy ) ;
		//Calculate the second partial derivative wrt x
		d2x = - PI2 * (params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solXFreq, 2 ) * oSx * oSy ) ;
		//Calculate the second partial derivative wrt y
		d2y = - PI2 * (params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solYFreq, 2 ) *
			oSx * oSy ) ;
		//Calculate the exact solution
		u = params->solSmoothAmp * sx * sy + params->solOscAmp * oSx * oSy ;
		//Set the return value
		retVal = - (1 + params->alpha * pow(u,2)) * ( d2x + d2y ) -
			2 * params->alpha * u * ( pow(dx,2) + pow(dy,2) ) ;
	}
	else if( params->testCase == 8 || params->testCase == 9 ){
		//For test case 8 we have a constant right hand side. This means that we
		//will not know the exact solution, but it will also mean that there
		//will be several frequencies present
		retVal = ( params->solSmoothAmp * (int)rand_in_range(1, 101) / 100.0 ) *
			( 1 - 2*rand_in_range(0, 2) ) ;
	}
	else if( params->testCase == 11 ){
		m = params->powU ;
		cx = cos( PI * x / 2 ) ;
		cy = cos( PI * y / 2 ) ;
		sx = sin( PI * x / 2 ) ;
		sy = sin( PI * y / 2 ) ;
		cxcy = cx * cy ;
		retVal = cxcy - params->alpha * (PI2/2.0) *( m * pow( cxcy, m-1 ) *
			(pow(sx,2) * pow(cy,2) + pow(cx,2) * pow(sy,2)) /2.0 -
			pow( cxcy, m+1 ) ) ;
	}
	else if( params->testCase == 13 ){
		sx = sin( PI * x ) ;
		cx = cos( PI * x ) ;
		s2y = pow( sin( PI * y ), 2 ) ;
		retVal = (2  * sx - 2 * PI * ( 1- 2*x) * cx + PI2*x*(1-x) * sx ) * s2y +
			2 * PI2 * x * (1-x) * sx * ( 2 * s2y -1 ) - 2 ;
	}
	else if( params->testCase == 28 || params->testCase == 31 ||
		params->testCase == 34 || params->testCase == 37 ||
		params->testCase == 53 ){
		double p = params->plExp ;
		double gradU ; //The absolute value of the gradient of the solution
					   //squared
		double ux, uy, uxy, uxx, uyy ; //The partial derivative of the exact
						//solution wrt x, y, xy, xx and yy.
					   
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		
		if( p == 2.0 )
			return 2 * PI2 * aVars->pcCoeff[ elId ] * params->solSmoothAmp *
				sx * sy ;

		cx = cos( PI * x ) ;
		cy = cos( PI * y ) ;
		
		oSx = sin( params->solXFreq * PI * x ) ;
		oSy = sin( params->solYFreq * PI * y ) ;
		oCx = cos( params->solXFreq * PI * x ) ;
		oCy = cos( params->solYFreq * PI * y ) ;
		
		ux = PI * ( params->solSmoothAmp * cx * sy +
			params->solOscAmp * params->solXFreq * oCx * oSy ) ;
		uy = PI * ( params->solSmoothAmp * sx * cy +
			params->solOscAmp * params->solYFreq * oSx * oCy ) ;
		uxy = PI2 * ( params->solSmoothAmp * cx * cy +
			params->solOscAmp * params->solXFreq * params->solYFreq *
			oCx * oCy ) ;
		uxx = - PI2 * ( params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solXFreq, 2 ) * oSx * oSy ) ;
		uyy = - PI2 * ( params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solYFreq, 2 ) * oSx * oSy ) ;
		
		gradU = sqrt( pow( ux, 2 ) + pow( uy, 2 ) ) ;
			
		retVal = - aVars->pcCoeff[ elId ] * ( (p-2) * pow( gradU, p-4.0 ) * (
			pow( ux, 2 ) * uxx + 2 * ux * uy * uxy + pow( uy, 2 ) * uyy ) +
			pow( gradU, p-2.0 ) * ( uxx + uyy ) ) ;
	}
	else if( params->testCase == 29 || params->testCase == 32 ||
		params->testCase == 35 || params->testCase == 38 ||
		params->testCase == 54 ){
		double p = params->plExp ;
		double gradU ; //The absolute value of the gradient of the solution
					   //squared
		double ux, uy, uxy, uxx, uyy ; //The partial derivative of the exact
						//solution wrt x, y, xy, xx and yy.
					   
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		
		if( p == 2.0 )
			return 2 * PI2 * ( 1 + aVars->pcCoeff[ elId ] ) *
				params->solSmoothAmp * sx * sy ;

		cx = cos( PI * x ) ;
		cy = cos( PI * y ) ;
		
		oSx = sin( params->solXFreq * PI * x ) ;
		oSy = sin( params->solYFreq * PI * y ) ;
		oCx = cos( params->solXFreq * PI * x ) ;
		oCy = cos( params->solYFreq * PI * y ) ;
		
		ux = PI * ( params->solSmoothAmp * cx * sy +
			params->solOscAmp * params->solXFreq * oCx * oSy ) ;
		uy = PI * ( params->solSmoothAmp * sx * cy +
			params->solOscAmp * params->solYFreq * oSx * oCy ) ;
		uxy = PI2 * ( params->solSmoothAmp * cx * cy +
			params->solOscAmp * params->solXFreq * params->solYFreq *
			oCx * oCy ) ;
		uxx = - PI2 * ( params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solXFreq, 2 ) * oSx * oSy ) ;
		uyy = - PI2 * ( params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solYFreq, 2 ) * oSx * oSy ) ;
		
		gradU = sqrt( pow( ux, 2 ) + pow( uy, 2 ) ) ;
		
		retVal = - aVars->pcCoeff[ elId ] * ( (p-2) * pow( gradU, p-4.0 ) * (
			pow( ux, 2 ) * uxx + 2 * ux * uy * uxy + pow( uy, 2 ) * uyy ) +
			pow( gradU, p-2.0 ) * ( uxx + uyy ) ) - (uxx + uyy) ;
	}
	else if( params->testCase == 30 || params->testCase == 33 ||
		params->testCase == 36 || params->testCase == 39 ||
		params->testCase == 55 ){
		double p = params->plExp ;
		double gradU ; //The absolute value of the gradient of the solution
					   //squared
		double ux, uy, uxy, uxx, uyy ; //The partial derivative of the exact
						//solution wrt x, y, xy, xx and yy.
					   
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		
		if( p == 2.0 )
			return 4 * PI2 * aVars->pcCoeff[ elId ] * params->solSmoothAmp *
				sx * sy ;

		cx = cos( PI * x ) ;
		cy = cos( PI * y ) ;
		
		oSx = sin( params->solXFreq * PI * x ) ;
		oSy = sin( params->solYFreq * PI * y ) ;
		oCx = cos( params->solXFreq * PI * x ) ;
		oCy = cos( params->solYFreq * PI * y ) ;
		
		ux = PI * ( params->solSmoothAmp * cx * sy +
			params->solOscAmp * params->solXFreq * oCx * oSy ) ;
		uy = PI * ( params->solSmoothAmp * sx * cy +
			params->solOscAmp * params->solYFreq * oSx * oCy ) ;
		uxy = PI2 * ( params->solSmoothAmp * cx * cy +
			params->solOscAmp * params->solXFreq * params->solYFreq *
			oCx * oCy ) ;
		uxx = - PI2 * ( params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solXFreq, 2 ) * oSx * oSy ) ;
		uyy = - PI2 * ( params->solSmoothAmp * sx * sy +
			params->solOscAmp * pow( params->solYFreq, 2 ) * oSx * oSy ) ;
		
		gradU = sqrt( pow( ux, 2 ) + pow( uy, 2 ) ) ;
		
		retVal = - aVars->pcCoeff[ elId ] * ( (p-2) * pow( gradU, p-4.0 ) * (
			pow( ux, 2 ) * uxx + 2 * ux * uy * uxy + pow( uy, 2 ) * uyy ) +
			( pow( gradU, p-2.0 ) + 1 ) * ( uxx + uyy ) ) ;
	}
	else if( params->testCase == 40 || params->testCase == 43 ||
		params->testCase == 46 || params->testCase == 49 ||
		params->testCase == 56){
		double u, ux, uy, uxx, uyy ; //The value of the exact solution and the
				//derivatives wrt x, y, xx and yy
		double m = params->powU ;
		
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		
		if( m == 0.0 )
			return 2 * PI2 * aVars->pcCoeff[ elId ] * params->solSmoothAmp *
				sx * sy ;
			
		cx = cos( PI * x ) ;
		cy = cos( PI * y ) ;
		
		oSx = sin( PI * params->solXFreq * x ) ;
		oSy = sin( PI * params->solYFreq * y ) ;
		oCx = cos( PI * params->solXFreq * x ) ;
		oCy = cos( PI * params->solYFreq * y ) ;
		
		u = params->solSmoothAmp * sx * sy + params->solOscAmp * oSx * oSy ;
		ux = PI * ( params->solSmoothAmp * cx * sy + params->solOscAmp *
			params->solXFreq * oCx * oSy ) ;
		uy = PI * ( params->solSmoothAmp * sx * cy + params->solOscAmp *
			params->solYFreq * oSx * oCy ) ;
		uxx = - PI2 * ( params->solSmoothAmp * sx * sy + params->solOscAmp *
			pow( params->solXFreq, 2 ) * oSx * oSy ) ;
		uyy = - PI2 * ( params->solSmoothAmp * sx * sy + params->solOscAmp *
			pow( params->solYFreq, 2 ) * oSx * oSy ) ;
			
		retVal = - aVars->pcCoeff[ elId ] * ( m * pow( u, m-1 ) *
			(pow( ux, 2 ) + pow( uy, 2 )) + pow( u, m ) * (uxx + uyy) ) ;
	}
	else if( params->testCase == 41 || params->testCase == 44 ||
		params->testCase == 47 || params->testCase == 50 ||
		params->testCase == 57 ){
		double u, ux, uy, uxx, uyy ; //The value of the exact solution and the
				//derivatives wrt x, y, xx and yy
		double m = params->powU ;
		
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		
		if( m == 0.0 )
			return 2 * PI2 * ( 1 + aVars->pcCoeff[ elId ] ) *
				params->solSmoothAmp * sx * sy ;
			
		cx = cos( PI * x ) ;
		cy = cos( PI * y ) ;
		
		oSx = sin( PI * params->solXFreq * x ) ;
		oSy = sin( PI * params->solYFreq * y ) ;
		oCx = cos( PI * params->solXFreq * x ) ;
		oCy = cos( PI * params->solYFreq * y ) ;
		
		u = params->solSmoothAmp * sx * sy + params->solOscAmp * oSx * oSy ;
		ux = PI * ( params->solSmoothAmp * cx * sy + params->solOscAmp *
			params->solXFreq * oCx * oSy ) ;
		uy = PI * ( params->solSmoothAmp * sx * cy + params->solOscAmp *
			params->solYFreq * oSx * oCy ) ;
		uxx = - PI2 * ( params->solSmoothAmp * sx * sy + params->solOscAmp *
			pow( params->solXFreq, 2 ) * oSx * oSy ) ;
		uyy = - PI2 * ( params->solSmoothAmp * sx * sy + params->solOscAmp *
			pow( params->solYFreq, 2 ) * oSx * oSy ) ;
			
		retVal = - aVars->pcCoeff[ elId ] * ( m * pow( u, m-1 ) *
			(pow( ux, 2 ) + pow( uy, 2 )) + pow( u, m ) * (uxx + uyy) ) -
			(uxx + uyy) ;
	}
	else if( params->testCase == 42 || params->testCase == 45 ||
		params->testCase == 48 || params->testCase == 51 ||
		params->testCase == 58 ){
		double u, ux, uy, uxx, uyy ; //The value of the exact solution and the
				//derivatives wrt x, y, xx and yy
		double m = params->powU ;
		
		sx = sin( PI * x ) ;
		sy = sin( PI * y ) ;
		
		if( m == 0.0 )
			return 4 * PI2 * aVars->pcCoeff[ elId ] * params->solSmoothAmp *
				sx * sy ;
			
		cx = cos( PI * x ) ;
		cy = cos( PI * y ) ;
		
		oSx = sin( PI * params->solXFreq * x ) ;
		oSy = sin( PI * params->solYFreq * y ) ;
		oCx = cos( PI * params->solXFreq * x ) ;
		oCy = cos( PI * params->solYFreq * y ) ;
		
		u = params->solSmoothAmp * sx * sy + params->solOscAmp * oSx * oSy ;
		ux = PI * ( params->solSmoothAmp * cx * sy + params->solOscAmp *
			params->solXFreq * oCx * oSy ) ;
		uy = PI * ( params->solSmoothAmp * sx * cy + params->solOscAmp *
			params->solYFreq * oSx * oCy ) ;
		uxx = - PI2 * ( params->solSmoothAmp * sx * sy + params->solOscAmp *
			pow( params->solXFreq, 2 ) * oSx * oSy ) ;
		uyy = - PI2 * ( params->solSmoothAmp * sx * sy + params->solOscAmp *
			pow( params->solYFreq, 2 ) * oSx * oSy ) ;
			
		retVal = - aVars->pcCoeff[ elId ] * ( m * pow( u, m-1 ) *
			(pow( ux, 2 ) + pow( uy, 2 )) + (pow( u, m ) + 1) * (uxx + uyy) ) ;
	}
	return retVal ;
}

/** Indicator function to show if a given test case represents a nonlinear test
 *	case
 *	\param [in] testCase	The identifier of the test case to check
 *	\return \p boolean value indicating whether the given identifier represents
 *			a nonlinear test case.
 */  
bool is_non_linear( int testCase )
{
	//We only have one linear test case, so it would be easier to check if we
	//are performing a linear solve or not
	if( testCase == 1 || testCase == 13 )
		return false ;
	
	return true ;
}


/** Indicator function to indicate whether or not we can recover continuity for
 *  the gradient for the given test case
 *	\param [in] testCase	The identifier for the test case to check
 *	\return \p boolean indicating whether or not recovery can be performed.
 */
bool can_recover_grads( int testCase )
{
	if( testCase == 2 || testCase == 4 || testCase == 6 || testCase == 8 )
		return true ;
	
	return false ;
}

/** Function to indicate whether or not the analytic derivative is known for a
 *	given test case
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\return \p boolean indicating whether an analytic derivative is known
 */
bool exists_analytic_deriv( const AlgorithmParameters* params )
{
	int testCase ;
	
	testCase = params->testCase ;
	
	//If recovery is being performed then the derivative is fiddly to work out,
	//so instead we use a numerical derivative
	if( params->recoverGrads ){
		if( testCase == 2 || testCase == 4 || testCase == 6 || testCase == 8 )
			return false ;
	}
	else if( testCase == 1 || testCase == 2 || testCase == 3 || testCase == 4 ||
			 testCase == 5 || testCase == 6 || testCase == 7 || testCase == 8 ||
			 testCase == 9 || testCase == 10 || testCase == 11 ||
			 testCase == 12 || is_p_laplacian( testCase ) ||
			 (params->testCase >= 40 && params->testCase <= 58) ){
		return true ;
	}
		
	//return false by default
	return false ;
}

/** Indicator to show whether a given problem is time dependent or not
 *	\param [in] testCase	The identifier of the test case to check
 *	\return \p boolean indicating whether the given test case is time dependent
 *			or not
 */
bool is_time_dependent( int testCase )
{
	if( testCase == 4 || testCase == 5 || testCase == 10 || testCase == 12 ||
		testCase == 14 || testCase == 15 || testCase == 16 ||
		testCase == 17 || testCase == 18 || testCase == 19 ||
		testCase == 20 || testCase == 21 || testCase == 22 || testCase == 23 ||
		testCase == 24 || testCase == 25 || testCase == 26 || testCase == 27 ||
		testCase == 52 || testCase == 101 || testCase == 102 )
		return true ;
	return false ;
}

/** Indicator to show whether or not the smoothing procedure used in the given
 *	parameters is a block Jacobi smoother or not
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\return \p boolean indicating whether block smoothing is to be performed.
 */
bool is_block_jacobi_smooth( const AlgorithmParameters *params )
{
	if( params->smoothMethod == SMOOTH_BLOCK_JAC_SINGLE ||
		params->smoothMethod == SMOOTH_BLOCK_JAC_AVG ||
		params->smoothMethod == SMOOTH_BLOCK_JAC_DWEIGHT ||
		params->smoothMethod == SMOOTH_BLOCK_JAC_HWEIGHT )
		return true ;
	
	//Return zero by default
	return false ;
}

/** Indicator to show whether or not the exact solution is known for a given
 *	test case
 *	\param [in] testCase	The identifier of the test case to check
 *	\return \p boolean value indicating whether the exact solution is known for
 *			the	given test case
 */
bool is_solution_known( int testCase )
{
	if( testCase == 1 || testCase == 2 || testCase == 3 || testCase == 6 ||
		testCase == 7 || testCase == 10 || testCase == 11 || testCase == 12 ||
		testCase == 13 || testCase == 28 ||  testCase == 29 || testCase == 30 ||
		testCase == 40 || testCase == 41 || testCase == 42 )
		return true ;
		
	return false ;
}

/** Calculates the discretization of the exact mathematical solution on a given
 *	grid
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which to operate
 *	\param [out] sol	The grid function to be populated with the value of the
 *						solution
 */
void calculate_exact_solution( const AlgorithmParameters *params,
	const GridParameters* grid,	double* sol )
{
	int i ; //Loop counter
	const Node *node ; //A Node on the grid
	//For every point on the interior of the grid
	for( i=0 ; i < grid->numUnknowns ; i++ ){
		//Get the coordinate for the current point
		node = &(grid->nodes[ grid->mapA2G[i] ]) ;
		sol[i] = solution_function( params, node->x, node->y ) ;
	}
}

/** Calculates the value of the exact solution for a given test case at the
 *  point with coordinate \f$( x, y)\f$
 *	\param [in] params	Parmeters defining how the algorithm is to be executed
 *	\param [in] x	The x-coordinate of the point at which to evaluate the
 *					function
 *	\param [in] y	The y-coordinate of the point at which to evaluate the
 *					function
 *	\return \p double value of the function evaluated at the given point
 */
double solution_function( const AlgorithmParameters *params, const double x,
	const double y )
{
	double retVal ;
	double mu ; //Needed in the calculation of the solution for test case 10
	retVal = 0.0 ;
	if( params->testCase == 1 || params->testCase == 2 ||
		params->testCase == 3 ){
		//The exact solution is given by sin( pi x ) * sin( pi y )
		retVal = sin( PI * x ) * sin( PI * y ) ;
	}
	else if( params->testCase == 6 || params->testCase == 7 ||
		is_p_laplacian( params->testCase ) ||
		(params->testCase >= 40 && params->testCase <= 51) ||
		(params->testCase >= 56 && params->testCase <= 58) ){
		retVal = params->solSmoothAmp * sin( PI * x ) * sin( PI * y ) +
			params->solOscAmp * sin( params->solXFreq * PI * x ) *
			sin( params->solYFreq * PI * y ) ;
	}
	else if( params->testCase == 10 || params->testCase == 12 ){
		mu = calculate_mu( params ) ;
		retVal = (pow(x,2) + pow(y,2)) / pow(params->initRad * mu, 2) ;
		retVal = pow( fmax( 1 - retVal, 0.0 ), 1.0 / params->powU ) /
			pow( mu, 2 ) ;
	}
	else if( params->testCase == 11 ){
		retVal = cos( PI * x / 2 ) * cos( PI * y / 2 ) ;
	}
	else if( params->testCase == 13 ){
		retVal = x * ( 1 - x ) * sin( PI * x) * pow( sin( PI * y ), 2 ) + y*y ;
	}
	
	return retVal ;
}

/** Inserts the string representation of the solution function into a string
 *  parameter passed in
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [out] string	The variable to populate with the string representation
 *						of the exact solution
 */
void solution_as_string( const AlgorithmParameters *params, char* string )
{
	if( params->testCase == 1 || params->testCase == 2 ||
		params->testCase == 3 )
		sprintf( string, "sin( pi * x ) sin( pi * y )" ) ;
	else if( params->testCase == 6 || params->testCase == 7 ||
		is_p_laplacian( params->testCase ) || params->testCase == 40 ||
		params->testCase == 41 || params->testCase == 42 ){
		sprintf( string, "%.2lf * sin( pi * x ) sin( pi * y )",
			params->solSmoothAmp ) ;
		sprintf( string, "%s + %.2lf * sin( %.2lf * pi * x ) ",
			string, params->solOscAmp, params->solXFreq ) ;
		sprintf( string, "%ssin( %.2lf * pi * y )", string, params->solYFreq ) ;
	}
	else if( params->testCase == 10 || params->testCase == 12 ){
		sprintf( string, "See file test_cases.pdf (test case 10)" ) ;
	}
	else if( params->testCase == 13 ){
		sprintf( string, "x * (1-x) * sin(pi * x) * sin^2(pi * y) + y^2" ) ;
	}
}

/** Returns the contribution to the 'stiffness' matrix on element \a e for nodes
 *	\a node1 and \a node2. 'Stiffness' is in inverted commas, as for a nonlinear
 *	problem there is no stiffness matrix. Instead for nonlinear problems a
 *	matrix is assembled, which is dependent on the current approximation, such
 *	that for one iteration the matrix can be treated the same as the stiffness
 *	matrix for a linear problem
 * 	\param [in] node1	Index of a node on element \a e. This is in the range
 *						(0,1,2), and gives the row of the matrix to add a
 *						contribution to
 *	\param [in] node2	Index of a node on element \a e. This is in the range
 *						(0,1,2), and gives the column of the matrix to add a
 *						contribution to
 *	\param [in] e		The element from which to get the contribution
 *	\param [in] approx	The approximation to the solution on the grid. This is
 *						required for nonlinear problems
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how to execute the algorithm
 *	\return \p double value containing the contribution from element \a e in the
 *			row for \a node1 and column for \a node2
 */
double exact_stiff_term( int node1, int node2, const Element *e,
	const double* approx, const ApproxVars *aVars, const GridParameters* grid,
	const AlgorithmParameters* params )
{
	int i ; //Loop counters
	double retVal ; //The value to return from the method
	double tmp[3] ;//Used as a temporary storage. A description of how it is
				   //used should be given when it is used
	double s, r ; //Variables used in the calculation of the contribution
				     //for test case 10
	int m ; //Ditto
	double fVal ; //Placeholder for the value of the approximation function at a
				  //given point
	const Node *node ; //Placeholder for a node on the grid
	
	//Initialise 'retVal' to zero as this will be the default return value
	retVal = 0.0 ;
	
	if( params->testCase == 1 || params->testCase == 13 ){
		//Linear Laplacian. We simply return the x-
		//and y- derivatives multiplied by each other. This is assuming a linear
		//basis
		retVal = ( ( b( node1, e, grid->nodes ) *
			b( node2, e, grid->nodes ) ) + 
			( c( node1, e, grid->nodes ) * 
			c( node2, e, grid->nodes ) ) ) / (4.0 * e->area) ;
	}
	else if( params->testCase == 2 || params->testCase == 4 ||
		params->testCase == 6 || params->testCase == 8 ){
		//For this test case we need to have an extra term to calculate in the
		//coefficient. The variable 'tmp' will be used to strore the values of
		//the sum of the x- and y-deriviatives of basis functions on an element
		tmp[0] = tmp[1] = 0.0 ;
		for( i=0; i<3; i++ ){
			node = &(grid->nodes[ e->nodeIDs[i] ]) ;
			//We don't mind if a node is on the boundary, but need to treat it
			//a little differently, as the value is not in the approximation
			fVal = approx[ grid->mapG2A[ node->id ] ] ;
			
			tmp[0] += fVal * b( i, e, grid->nodes ) ;
			tmp[1] += fVal * c( i, e, grid->nodes ) ;
		}
		retVal = (1.0 + (params->alpha / (4.0 * e->area * e->area )) *
			( tmp[0] * tmp[0] + tmp[1] * tmp[1] ) ) *
			( ( b( node1, e, grid->nodes ) * b( node2, e, grid->nodes ) ) + (
			c( node1, e, grid->nodes ) * c( node2, e, grid->nodes ) ) ) /
			(4.0 * e->area ) ;
	}
	else if( params->testCase == 3 || params->testCase == 5 ||
		params->testCase == 7 || params->testCase == 9 ){
		//The variable 'tmp' will hold the values of the approximation at each
		//of the nodes on the triangle. A boundary node will be assigned the
		//appropriate boundary value
		for( i=0 ; i < 3 ; i++ ){
			node = &(grid->nodes[ e->nodeIDs[i] ]) ;
			tmp[i] = approx[ grid->mapG2A[ node->id ] ] ;
		}
		retVal = ( 1.0 + (params->alpha / 6.0) *
			( tmp[0] * ( tmp[0] + tmp[1] + tmp[2] ) + 
			tmp[1] * ( tmp[1] + tmp[2] ) + tmp[2] * tmp[2] ) ) * 
			( ( b( node1, e, grid->nodes ) * b( node2, e, grid->nodes ) ) +
			( c( node1, e, grid->nodes ) * c( node2, e, grid->nodes ) ) ) /
			(4.0 * e->area) ;
	}
	else if( params->testCase == 11 || params->testCase == 10 ||
		params->testCase == 12 ){
		//Get the values of the nodes and save them in the variable 'tmp'
		m = params->powU ;
		s = 0.0 ;
		for( i=0 ; i < 3 ; i++ ){
			node = &(grid->nodes[ e->nodeIDs[i] ]) ;
			tmp[i] = approx[ grid->mapG2A[ node->id ] ] ;
			s += tmp[i] ;
		}
		r = s - tmp[0] ;
		
		//The following implements the case of general m, which is the power to
		//which u is raised in the coefficient of the diffusion term in the 
		//porous medium equation
		for( i=2 ; i <= m ; i++ ){
			r = r * tmp[1] + pow( tmp[2], i ) ;
			s = s * tmp[0] + r ;
		}
		retVal = s * ( b( node1, e, grid->nodes ) * b( node2, e, grid->nodes ) +
			c( node1, e, grid->nodes ) * c( node2, e, grid->nodes ) ) /
			(2 * e->area * (m+1) * (m+2)) ;
		if( params->testCase == 11 )
			retVal *= params->alpha ;
	}
	else if( params->testCase == 52 || params->testCase == 40 ||
		params->testCase == 43 || params->testCase == 46 ||
		params->testCase == 49 || params->testCase == 56 ){
		//Get the values of the nodes and save them in the variable 'tmp'
		m = params->powU ;
		s = 0.0 ;
		for( i=0 ; i < 3 ; i++ ){
			node = &(grid->nodes[ e->nodeIDs[i] ]) ;
			tmp[i] = approx[ grid->mapG2A[ node->id ] ] ;
			s += tmp[i] ;
		}
		r = s - tmp[0] ;
		
		//The following implements the case of general m, which is the power to
		//which u is raised in the coefficient of the diffusion term in the 
		//porous medium equation
		for( i=2 ; i <= m ; i++ ){
			r = r * tmp[1] + pow( tmp[2], i ) ;
			s = s * tmp[0] + r ;
		}
		retVal = s * ( b( node1, e, grid->nodes ) * b( node2, e, grid->nodes ) +
			c( node1, e, grid->nodes ) * c( node2, e, grid->nodes ) ) *
			aVars->pcCoeff[ e->id ] / (2 * e->area * (m+1) * (m+2)) ;
	}
	else if( params->testCase == 41 || params->testCase == 44 ||
		params->testCase == 47 || params->testCase == 50 ||
		params->testCase == 57 ){
		//Get the values of the nodes and save them in the variable 'tmp'
		m = params->powU ;
		s = 0.0 ;
		for( i=0 ; i < 3 ; i++ ){
			node = &(grid->nodes[ e->nodeIDs[i] ]) ;
			tmp[i] = approx[ grid->mapG2A[ node->id ] ] ;
			s += tmp[i] ;
		}
		r = s - tmp[0] ;
		
		//The following implements the case of general m, which is the power to
		//which u is raised in the coefficient of the diffusion term in the 
		//porous medium equation
		for( i=2 ; i <= m ; i++ ){
			r = r * tmp[1] + pow( tmp[2], i ) ;
			s = s * tmp[0] + r ;
		}
		retVal = ( b( node1, e, grid->nodes ) * b( node2, e, grid->nodes ) +
			c( node1, e, grid->nodes ) * c( node2, e, grid->nodes ) ) * ( s *
			aVars->pcCoeff[ e->id ] / (2 * e->area * (m+1) * (m+2)) +
			1 / (4 * e->area) ) ;
	}
	else if( params->testCase == 42 || params->testCase == 45 ||
		params->testCase == 48 || params->testCase == 51 ||
		params->testCase == 58 ){
		//Get the values of the nodes and save them in the variable 'tmp'
		m = params->powU ;
		s = 0.0 ;
		for( i=0 ; i < 3 ; i++ ){
			node = &(grid->nodes[ e->nodeIDs[i] ]) ;
			tmp[i] = approx[ grid->mapG2A[ node->id ] ] ;
			s += tmp[i] ;
		}
		r = s - tmp[0] ;
		
		//The following implements the case of general m, which is the power to
		//which u is raised in the coefficient of the diffusion term in the 
		//porous medium equation
		for( i=2 ; i <= m ; i++ ){
			r = r * tmp[1] + pow( tmp[2], i ) ;
			s = s * tmp[0] + r ;
		}
		retVal = ( b( node1, e, grid->nodes ) * b( node2, e, grid->nodes ) +
			c( node1, e, grid->nodes ) * c( node2, e, grid->nodes ) ) * ( s /
			(2 * e->area * (m+1) * (m+2)) +
			1 /	(4 * e->area) ) * aVars->pcCoeff[ e->id ] ;
	}
	else if( params->testCase == 28 || params->testCase == 31 ||
		params->testCase == 34 || params->testCase == 37 ||
		params->testCase == 53 ){
		double gradSum[2] = { 0.0, 0.0 } ;
		double gradAbs = 0.0 ; //The absolute value of the gradient of the
							   //approximation function over the element
		int ind ; //The index into the approximation for a node on the grid
		
		for( i = 0 ; i < 3 ; i++ ){
			ind = grid->mapG2A[ e->nodeIDs[i] ] ;
			gradSum[0] += b( i, e, grid->nodes ) * approx[ ind ] ;
			gradSum[1] += c( i, e, grid->nodes ) * approx[ ind ] ;
		}
		
		gradAbs = (gradSum[0] * gradSum[0] + gradSum[1] * gradSum[1]) / 
			(4.0 * pow( e->area, 2 ) ) ;
			
		if( params->plExp > 2 ){
			retVal = pow( gradAbs, (params->plExp - 2.0)/2.0 ) *
				aVars->pcCoeff[ e->id ] * ( b(node1, e, grid->nodes ) *
				b( node2, e, grid->nodes ) + c( node1, e, grid->nodes ) *
				c( node2, e, grid->nodes ) ) / (4 * e->area) ;
		}
		else{
			retVal = aVars->pcCoeff[ e->id ] * (b(node1, e, grid->nodes ) *
				b( node2, e, grid->nodes ) + c( node1, e, grid->nodes ) *
				c( node2, e, grid->nodes ) ) / (4 * e->area) ;
		}
	}
	else if( params->testCase == 29 || params->testCase == 32 ||
		params->testCase == 35 || params->testCase == 38 ||
		params->testCase == 54 ){
		double gradSum[2] = { 0.0, 0.0 } ;
		double gradAbs = 0.0 ; //The absolute value of the gradient of the
							   //approximation function over the element
		int ind ; //The index into the approximation for a node on the grid
		
		for( i = 0 ; i < 3 ; i++ ){
			ind = grid->mapG2A[ e->nodeIDs[i] ] ;
			gradSum[0] += b( i, e, grid->nodes ) * approx[ ind ] ;
			gradSum[1] += c( i, e, grid->nodes ) * approx[ ind ] ;
		}
		
		gradAbs = (gradSum[0] * gradSum[0] + gradSum[1] * gradSum[1]) / 
			(4.0 * pow( e->area, 2 ) ) ;
			
		retVal = pow( gradAbs, (params->plExp - 2.0)/2.0 ) *
			aVars->pcCoeff[ e->id ] * ( b(node1, e, grid->nodes ) *
			b( node2, e, grid->nodes ) + c( node1, e, grid->nodes ) *
			c( node2, e, grid->nodes ) ) / (4 * e->area) +
			(b(node1, e, grid->nodes) * b(node2, e, grid->nodes ) +
			c(node1, e, grid->nodes) * c(node2, e, grid->nodes) )
			/ (4 * e->area );
	}
	else if( params->testCase == 30 || params->testCase == 33 ||
		params->testCase == 36 || params->testCase == 39 ||
		params->testCase == 55 ){
		double gradSum[2] = { 0.0, 0.0 } ;
		double gradAbs = 0.0 ; //The absolute value of the gradient of the
							   //approximation function over the element
		int ind ; //The index into the approximation for a node on the grid
		
		for( i = 0 ; i < 3 ; i++ ){
			ind = grid->mapG2A[ e->nodeIDs[i] ] ;
			gradSum[0] += b( i, e, grid->nodes ) * approx[ ind ] ;
			gradSum[1] += c( i, e, grid->nodes ) * approx[ ind ] ;
		}
		
		gradAbs = (gradSum[0] * gradSum[0] + gradSum[1] * gradSum[1]) / 
			(4.0 * pow( e->area, 2 ) ) ;
			
		retVal = aVars->pcCoeff[ e->id ] * (
			pow( gradAbs, (params->plExp - 2.0)/2.0 ) * ( b(node1, e,
			grid->nodes ) * b( node2, e, grid->nodes ) + c( node1, e,
			grid->nodes ) * c( node2, e, grid->nodes ) ) +
			(b(node1, e, grid->nodes) * b(node2, e, grid->nodes ) + c(node1, e,
			grid->nodes) * c(node2, e, grid->nodes) ) ) / (4 * e->area );
	}
	
	//If the problem is time dependent then we multiply by a factor of the time
	//step size
	if( is_time_dependent( params->testCase ) )
		retVal *= params->tdCtxt.weightFact ;
	
	//Return zero by default
	return retVal ;
}


/** Returns the contribution to the 'stiffness' matrix on element \a e for nodes
 *	\a node1 and \a node2 for formulations where recovery of continuity has been
 *	performed on the gradient function. 'Stiffness' is in inverted commas, as
 *	for a nonlinear problem there is no stiffness matrix. Instead for nonlinear
 *	problems a matrix is assembled, which is dependent on the current
 *	approximation, such that for one iteration the matrix can be treated the
 *	same as the stiffness matrix for a linear problem
 * 	\param [in] node1	Index of a node on element \a e. This is in the range
 *						(0,1,2), and gives the row of the matrix to add a
 *						contribution to
 *	\param [in] node2	Index of a node on element \a e. This is in the range
 *						(0,1,2), and gives the column of the matrix to add a
 *						contribution to
 *	\param [in] e		The element from which to get the contribution
 *	\param [in] approx	The approximation to the solution on the grid. This is
 *						required for nonlinear problems
 *	\param [in] recGrads	The recovered gradients on the grid, if applicable.
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how to execute the algorithm
 *	\return \p double value containing the contribution from element \a e in the
 *			row for \a node1 and column for \a node2
 */
double recover_stiff_term( int node1, int node2, const Element *e,
	const double* approx, const CVector2D* recGrads, const GridParameters* grid,
	const AlgorithmParameters* params )
{
	int i ;
	double sumX, sumY ;
	double currX, currY ;
	double retVal ;
	
	sumX = sumY = retVal = 0.0 ;
	
	if( params->testCase == 2 || params->testCase == 4 ||
		params->testCase == 6 || params->testCase == 8 ){
		//Get the contribution to the total from the recovered terms
		for( i=2; i >= 0 ; i-- ){
			currX = recGrads[ e->nodeIDs[i] ].x ;
			currY = recGrads[ e->nodeIDs[i] ].y ;
			sumX += currX ;
			sumY += currY ;
			
			retVal += currX * sumX + currY * sumY ;
		}
		
		//Now return the correct value
		retVal = ( ( b( node1, e, grid->nodes ) *
			b( node2, e, grid->nodes ) +
			c( node1, e, grid->nodes ) * c( node2, e, grid->nodes ) ) *
			( 1.0 + ( params->alpha / 6.0 ) * retVal ) ) / (4.0 * e->area) ;
	}
	else
		return retVal ;
	
	//If the problem is time dependent then we multiply by a factor of the time
	//step size
	if( is_time_dependent( params->testCase ) )
		retVal *= params->tdCtxt.weightFact ;
	
	//Return the value calculated. If no value is calculated then zero is
	//returned by default, as this is what 'retVal' is initialised to
	return retVal ;
}

/** Returns the contribution to the 'stiffness' matrix on element \a e for nodes
 *	\a node1 and \a node2 for formulations where recovery of continuity has been
 *	performed on the gradient function. 'Stiffness' is in inverted commas, as
 *	for a nonlinear problem there is no stiffness matrix. Instead, for nonlinear
 *	problems a matrix is assembled which is dependent on the current
 *	approximation, such that (for one iteration) the matrix can be treated the
 *	same as the stiffness matrix for a linear problem. The variables
 *	\a approx and \e recGrads are only defined on element \a e for this version
 *	of the function
 *	\param [in] node1	Index of a node on element \a e. This is in the range
 *						(0,1,2), and gives the row of the matrix to add a
 *						contribution to
 *	\param [in] node2	Index of a node on element \a e. This is in the range
 *						(0,1,2), and gives the column of the matrix to add a
 *						contribution to
 *	\param [in] e		The element from which to get the contribution
 *	\param [in] approx	The approximation to the solution on element \a e. This
 *						is required for nonlinear problems
 *	\param [in] recGrads	The recovered gradients on the element \a e
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\return \p double value containing the contribution from element \a e in the
 *			row for \a node1 and column for \a node2
 */
double recover_stiff_term_ln( int node1, int node2, const Element *e,
	const double* approx, const CVector2D* recGrads, const GridParameters* grid,
	const AlgorithmParameters* params )
{	
	double retVal ; //The value to be returned from the function
	
	int i ; //Loop counter
	
	Vector2D sumGrads ; //Used to store the values of the sum of the recovered
						 //gradients, if they are required
	
	//Set the return value to zero, which will be the default value to return
	retVal = 0.0 ;
	
	//Set the sum of the recovered gradients to be zero. This is so that we can
	//perform updates to these variables if needed later on, rather than needing
	//to assign a value
	sumGrads.x = sumGrads.y = 0.0 ;
	
	//Check which test case we are to calculate the term for
	if( params->recoverGrads ){
		for( i=0 ; i < 3 ; i++ ){
			sumGrads.x += recGrads[i].x ;
			sumGrads.y += recGrads[i].y ;
			
			//Update the return value
			retVal += sumGrads.x * recGrads[i].x + sumGrads.y * recGrads[i].y ;
		}
		
		retVal = ( b( node1, e, grid->nodes ) * b(node2, e, grid->nodes ) +
			c( node1, e, grid->nodes ) * c( node2, e, grid->nodes ) ) *
			( 1.0 + (params->alpha / 6.0) * retVal ) / (4.0 * e->area) ;
	}
	
	//Return the calculated value, or the default value of zero if no
	//calculations have been performed
	return retVal ;
}

/** If the analytic derivative is known then this method returns the discrete
 *	analytic derivative of the operator on element \e e where the derivative
 *	of equation corresponding to node identified by \e ni is taken with respect
 *	to the node identified by \e nj
 * 	\param [in] ni	Identifier of a vertex on element \e e. This identifies
 *					the row in the Jacobian to update
 *	\param [in] nj	Identifier of a vertex on element \e e. This identifies
 *					the column in the Jacobian to update
 *	\param [in] e	The element over which to get the conribution to the
 *					derivative
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation, which may
 *						be used in the calucluation of the element contribution
 *						to the Jacobian
 *	\return \p double value of the contribution to an element of the Jacobian
 *			matrix from element \e e
 */
double exact_deriv_term( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars )
{
	int i ; //Loop counter
	double s, r, x, t ; //Variables needed for the calculation of the derivative
						//for test case 11
	int inds[3] ; //Ditto
	
	const Node *node ; //Placeholder for a node on the grid
	
	double retVal ; //The value to return from the function
	
	double sumRX, sumRY ; //Used to store the values of the x- and
						  //y- components of the recovered gradients
						  
	double sumVals ; //Used to store the sum of the nodal values on the element,
					 //if required
									  
	double tmp[2] ;//This can be used to store anything in this method. It 
				   //should always be commented what it is used for when it is
				   //used
	
	double bfGrads[3][2] ;//Stores the values of the gradients of the basis
						  //basis functions on the current element
	double aVals[3] ;//Stores the values of the approximation at the vertices
					 //of the current element
	double rVals[3][2] ;//Stores the values of the recovered gradients at the
					    //vertices of the current element. Only used if required
	
	//Put the correct values in the temporary variables 'aVals' and 'rVals'
	for( i=0 ; i < 3 ; i++ ){
		node = &(grid->nodes[ e->nodeIDs[i] ]) ;
		//If the current node is a boundary node then set the value of the
		//approximation to be zero. This is assuming homogeneous Dirichlet
		//boundary conditions
//		if( node->type == NODE_DIR_BNDRY )
//			aVals[i] = bfGrads[i][0] = bfGrads[i][1] = 0.0 ;
//		else{
			aVals[i] = aVars->approx[grid->mapG2A[node->id]] ;
			//The gradients that are assigned to the basis functions are scaled
			//by a factor of 2*e->Area, which is taken into account later on in
			//this function
			bfGrads[i][0] = b( i, e, grid->nodes ) ;
			bfGrads[i][1] = c( i, e, grid->nodes ) ;
//		}
		
		//Store the nodal values of the recovered gradient
		if( params->recoverGrads ){
			rVals[i][0] = aVars->recGrads[ e->nodeIDs[i] ].x ;
			rVals[i][1] = aVars->recGrads[ e->nodeIDs[i] ].y ;
		}
	}
	
	//Initialise the return value to be equal to zero. This is the value that
	//will be returned by default
	retVal = 0.0 ;
	
	//Find out which test case and which formulation we need to calculate the
	//value for
	if( params->recoverGrads ){
		if( params->testCase == 2 || params->testCase == 4 ||
			params->testCase == 6 || params->testCase == 8 ){
			//Now we calculate the appropriate value for the derivative of the
			//operator on the current element
			tmp[0] = 0.0 ; //Here 'tmp[0]' stores the value of the sum of the
						   //multiplication between the x and y gradients of the
						   //recovered gradient
			sumRX = sumRY = 0.0 ;
			for( i = 0 ; i < 3 ; i++ ){
				sumRX += rVals[i][0] ;
				sumRY += rVals[i][1] ;
				
				tmp[0] += rVals[i][0] * sumRX + rVals[i][1] * sumRY ;
			}
			
			//Update the return value with what we have so far
			retVal = (bfGrads[ni][0] * bfGrads[nj][0] + bfGrads[ni][1] *
				bfGrads[nj][1]) * ( 1.0 + (params->alpha / 6.0) * tmp[0] ) /
				(4.0 * e->area) ;
			
			tmp[0] = tmp[1] = 0.0 ;
			//In the following 'tmp' will store the value of the sum of the
			//gradients of the basis functions multiplied by the nodal values on
			//the element
			for( i=0 ; i < 3 ; i++ ){
				tmp[0] += aVals[i] * bfGrads[i][0] ;
				tmp[1] += aVals[i] * bfGrads[i][1] ;
			}
			
			//Update the return value with the rest of the values required. This
			//gives us all the information that we need
			retVal += params->alpha * ( tmp[0] * bfGrads[ni][0] +
				tmp[1] * bfGrads[ni][1] ) *
				( sumRX * bfGrads[nj][0] + sumRY * bfGrads[nj][1] ) / (24.0 *
				pow(e->area, 2) ) ;
		}
	}
	else if( params->testCase == 2 || params->testCase == 4 ||
		params->testCase == 6 || params->testCase == 8 ){
		//tmp[0] will contain the sum of the x-gradients of the basis
		//functions on the current element, scaled by a factor of 2*tArea
		//tmp[1] will contain the sum of the y-gradients
		tmp[0] = tmp[1] = 0.0 ;
		for( i=0 ; i < 3 ; i++ ){
			tmp[0] += aVals[i] * bfGrads[i][0] ;
			tmp[1] += aVals[i] * bfGrads[i][1] ;
		}
		
		//Calculate the return value (i.e. the integral of the given basis
		//functions on the given element)
		retVal = ( bfGrads[ni][0] * bfGrads[nj][0] +
			bfGrads[ni][1] * bfGrads[nj][1] ) * ( 1.0 + params->alpha *
			( (tmp[0] * tmp[0] + tmp[1] * tmp[1]) / (4.0 * pow(e->area,2)) ) ) /
			(4.0 * e->area) + ( params->alpha *
			(tmp[0] * bfGrads[ni][0] + tmp[1] * bfGrads[ni][1] ) *
			(tmp[0] * bfGrads[nj][0] + tmp[1] * bfGrads[nj][1] ) ) / (8.0 *
			pow(e->area,3)) ;
	}
	else if( params->testCase == 3 || params->testCase == 5 ||
		params->testCase == 7 || params->testCase == 9 ){
		//tmp[0] will contain the sum of the x-gradients of the basis functions
		//on the current element, scaled by a factor of 2*tArea
		//tmp[1] will contain the sum of the y-gradients
		tmp[0] = tmp[1] = sumVals = retVal = 0.0 ;
		for( i=0 ; i < 3 ; i++ ){
			tmp[0] += aVals[i] * bfGrads[i][0] ;
			tmp[1] += aVals[i] * bfGrads[i][1] ;
			
			//Sum the nodal values on the current element, and set an
			//intermediate value for retVal
			sumVals += aVals[i] ;
			retVal += sumVals * aVals[i] ;
		}
		
		//Now update the value of retVal to be the one that we will return
		retVal = ( (bfGrads[nj][0] * bfGrads[ni][0] +
			bfGrads[nj][1] * bfGrads[ni][1]) *
			(1.0 + (params->alpha / 6.0) * retVal) +
			(params->alpha / 6.0) *
			(tmp[0] * bfGrads[ni][0] + tmp[1] * bfGrads[ni][1]) *
			(sumVals + aVals[nj]) ) / (4.0 * e->area) ;
	}
	else if( params->testCase == 10 || params->testCase == 11 ||
		params->testCase == 12 ){
		r = aVals[1] + aVals[2] ;
		s = aVals[0] + r ;
		
		for( i=2 ; i <= params->powU ; i++ ){
			r = aVals[1] * r + pow( aVals[2], i ) ;
			s = aVals[0] * s + r ;
		}
		retVal = (bfGrads[ni][0] * bfGrads[nj][0] +
			bfGrads[ni][1] * bfGrads[nj][1]) *
			s / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ;
		
		tmp[0] = tmp[1] = 0.0 ;	
		for( i=0 ; i < 3 ; i++ ){
			inds[i] = (nj + i) %3 ;
			tmp[0] += aVals[i] * bfGrads[i][0] ;
			tmp[1] += aVals[i] * bfGrads[i][1] ;
		}
		t = x = 1.0 ;
		for( i=1 ; i <= params->powU - 1 ; i++ ){
			x = aVals[inds[1]] * x + pow( aVals[inds[0]], i ) * (i+1) ;
			t = aVals[inds[2]] * t + x ;
		}
		
		retVal = retVal + (tmp[0] * bfGrads[ni][0] + tmp[1] * bfGrads[ni][1]) *
			t / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ;
		if( params->testCase == 11 )
			retVal *= params->alpha ;
	}
	else if( params->testCase == 52 || params->testCase == 40 ||
		params->testCase == 43 || params->testCase == 46 ||
		params->testCase == 49 || params->testCase == 56 ){
		r = aVals[1] + aVals[2] ;
		s = aVals[0] + r ;
		
		for( i=2 ; i <= params->powU ; i++ ){
			r = aVals[1] * r + pow( aVals[2], i ) ;
			s = aVals[0] * s + r ;
		}
		retVal = (bfGrads[ni][0] * bfGrads[nj][0] +
			bfGrads[ni][1] * bfGrads[nj][1]) *
			s / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ;
		
		tmp[0] = tmp[1] = 0.0 ;	
		for( i=0 ; i < 3 ; i++ ){
			inds[i] = (nj + i) %3 ;
			tmp[0] += aVals[i] * bfGrads[i][0] ;
			tmp[1] += aVals[i] * bfGrads[i][1] ;
		}
		t = x = 1.0 ;
		for( i=1 ; i <= params->powU - 1 ; i++ ){
			x = aVals[inds[1]] * x + pow( aVals[inds[0]], i ) * (i+1) ;
			t = aVals[inds[2]] * t + x ;
		}
		
		retVal = (retVal + (tmp[0] * bfGrads[ni][0] + tmp[1] * bfGrads[ni][1]) *
			t / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ) *
			aVars->pcCoeff[ e->id ] ;
	}
	else if( params->testCase == 41 || params->testCase == 44 ||
		params->testCase == 47 || params->testCase == 50 ||
		params->testCase == 57 ){
		r = aVals[1] + aVals[2] ;
		s = aVals[0] + r ;
		
		for( i=2 ; i <= params->powU ; i++ ){
			r = aVals[1] * r + pow( aVals[2], i ) ;
			s = aVals[0] * s + r ;
		}
		retVal = (bfGrads[ni][0] * bfGrads[nj][0] +
			bfGrads[ni][1] * bfGrads[nj][1]) *
			s / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ;
		
		tmp[0] = tmp[1] = 0.0 ;	
		for( i=0 ; i < 3 ; i++ ){
			inds[i] = (nj + i) %3 ;
			tmp[0] += aVals[i] * bfGrads[i][0] ;
			tmp[1] += aVals[i] * bfGrads[i][1] ;
		}
		t = x = 1.0 ;
		for( i=1 ; i <= params->powU - 1 ; i++ ){
			x = aVals[inds[1]] * x + pow( aVals[inds[0]], i ) * (i+1) ;
			t = aVals[inds[2]] * t + x ;
		}
		
		retVal = (retVal + (tmp[0] * bfGrads[ni][0] + tmp[1] * bfGrads[ni][1]) *
			t / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ) *
			aVars->pcCoeff[ e->id ] + (bfGrads[ni][0] * bfGrads[nj][0] +
			bfGrads[ni][1] * bfGrads[nj][1]) / ( 4 * e->area ) ;
	}
	else if( params->testCase == 42 || params->testCase == 45 ||
		params->testCase == 48 || params->testCase == 51 ||
		params->testCase == 58 ){
		r = aVals[1] + aVals[2] ;
		s = aVals[0] + r ;
		
		for( i=2 ; i <= params->powU ; i++ ){
			r = aVals[1] * r + pow( aVals[2], i ) ;
			s = aVals[0] * s + r ;
		}
		retVal = (bfGrads[ni][0] * bfGrads[nj][0] +
			bfGrads[ni][1] * bfGrads[nj][1]) * (
			s / (2 * e->area * (params->powU + 1) * (params->powU + 2)) +
			1 / (4 * e->area ) ) ;
		
		tmp[0] = tmp[1] = 0.0 ;	
		for( i=0 ; i < 3 ; i++ ){
			inds[i] = (nj + i) %3 ;
			tmp[0] += aVals[i] * bfGrads[i][0] ;
			tmp[1] += aVals[i] * bfGrads[i][1] ;
		}
		t = x = 1.0 ;
		for( i=1 ; i <= params->powU - 1 ; i++ ){
			x = aVals[inds[1]] * x + pow( aVals[inds[0]], i ) * (i+1) ;
			t = aVals[inds[2]] * t + x ;
		}
		
		retVal = (retVal + (tmp[0] * bfGrads[ni][0] + tmp[1] * bfGrads[ni][1]) *
			t / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ) *
			aVars->pcCoeff[ e->id ] ;
	}
	else if( is_richards_eq( params->testCase ) ){
		//Not sure what to do with this test case. I need to integrate
		//numerically, but I do know the analytic form of the derivative, so
		//this doesn't quite fit into my method or the numeric one. I will have
		//the same solution method for both of these
		retVal = numeric_deriv_term( ni, nj, e, grid, params, aVars ) ;
	}
	else if( params->testCase == 28 || params->testCase == 31 ||
		params->testCase == 34 || params->testCase == 37 ||
		params->testCase == 53 ){
		double p = params->plExp ; //The exponent in the p-Laplacian
		double gradSum[2] = {0.0, 0.0} ; //The sum of the gradients of the
							//approximation on the current element
		double gradAbs ; //The absolute value of the gradient of the
						 //approximation on the current grid level
						 
		for( i=0 ; i < 3 ; i++ ){
			gradSum[0] += bfGrads[i][0] * aVals[i] ;
			gradSum[1] += bfGrads[i][1] * aVals[i] ;
		}
		
		//Calculate the square of the absolute value of the gradient of the
		//approximation on the current grid level
		gradAbs = (gradSum[0] * gradSum[0] + gradSum[1]*gradSum[1]) / 
			(4 * pow( e->area, 2 )) ;
			
		if( p > 2 ){
			retVal = (p-2) * pow( gradAbs, (p-4.0)/2.0 ) * ( ( gradSum[0] *
				bfGrads[nj][0] + gradSum[1] * bfGrads[nj][1] ) / e->area ) *
				( ( gradSum[0] * bfGrads[ni][0] + gradSum[1] * bfGrads[ni][1] ) /
				e->area ) / 4 ;
				
			retVal += pow( gradAbs, (p-2)/2.0 ) * (bfGrads[nj][0] * bfGrads[ni][0] +
			bfGrads[nj][1] * bfGrads[ni][1]) ;
		}
		else{
			retVal = bfGrads[nj][0] * bfGrads[ni][0] + bfGrads[nj][1] *
				bfGrads[ni][1] ;
		}

//		retVal = retVal / ( 4 * e->area ) ;			
		retVal = retVal * aVars->pcCoeff[ e->id ] / ( 4 * e->area ) ;
	}
	else if( params->testCase == 29 || params->testCase == 32 ||
		params->testCase == 35 || params->testCase == 38 ||
		params->testCase == 54 ){
		double p = params->plExp ; //The exponent in the p-Laplacian
		double gradSum[2] = {0.0, 0.0} ; //The sum of the gradients of the
							//approximation on the current element
		double gradAbs ; //The absolute value of the gradient of the
						 //approximation on the current grid level
		
		for( i=0 ; i < 3 ; i++ ){
			gradSum[0] += bfGrads[i][0] * aVals[i] ;
			gradSum[1] += bfGrads[i][1] * aVals[i] ;
		}
		
		//Calculate the square of the absolute value of the gradient of the
		//approximation on the current grid level
		gradAbs = (gradSum[0] * gradSum[0] + gradSum[1] * gradSum[1]) /
			(4 * pow( e->area, 2 )) ;
		
		if( p > 2 ){
			retVal = (p-2) * pow( gradAbs, (p-4.0)/2.0 ) * ( ( gradSum[0] *
				bfGrads[nj][0] + gradSum[1] * bfGrads[nj][1] ) / e->area ) *
				( ( gradSum[0] * bfGrads[ni][0] + gradSum[1] * bfGrads[ni][1] ) /
				e->area ) / 4 ;
				
			retVal += pow( gradAbs, (p-2)/2.0 ) * (bfGrads[nj][0] * bfGrads[ni][0] +
			bfGrads[nj][1] * bfGrads[ni][1]) ;
		}
		else{
			retVal = bfGrads[nj][0] * bfGrads[ni][0] + bfGrads[nj][1] * 
				bfGrads[ni][1] ;
		}

//		retVal = retVal / ( 4 * e->area ) ;			
		retVal = retVal * aVars->pcCoeff[ e->id ] / ( 4 * e->area ) +
			(bfGrads[ni][0] * bfGrads[nj][0] + bfGrads[ni][1] * bfGrads[nj][1] )
			/ (4 * e->area) ;
	}
	else if( params->testCase == 30 || params->testCase == 33 ||
		params->testCase == 36 || params->testCase == 39 ||
		params->testCase == 55 ){
		double p = params->plExp ; //The exponent in the p-Laplacian
		double gradSum[2] = {0.0, 0.0} ; //The sum of the gradients of the
							//approximation on the current element
		double gradAbs ; //The absolute value of the gradient of the
						 //approximation on the current grid level
						 
		for( i=0 ; i < 3 ; i++ ){
			gradSum[0] += bfGrads[i][0] * aVals[i] ;
			gradSum[1] += bfGrads[i][1] * aVals[i] ;
		}
		
		//Calculate the square of the absolute value of the gradient of the
		//approximation on the current grid level
		gradAbs = (gradSum[0] * gradSum[0] + gradSum[1]*gradSum[1]) / 
			(4 * pow( e->area, 2 )) ;
			
		if( p > 2 ){
			retVal = (p-2) * pow( gradAbs, (p-4.0)/2.0 ) * ( ( gradSum[0] *
				bfGrads[nj][0] + gradSum[1] * bfGrads[nj][1] ) / e->area ) *
				( ( gradSum[0] * bfGrads[ni][0] + gradSum[1] * bfGrads[ni][1] ) /
				e->area ) / 4 ;
				
			retVal += pow( gradAbs, (p-2)/2.0 ) * (bfGrads[nj][0] * bfGrads[ni][0] +
			bfGrads[nj][1] * bfGrads[ni][1]) ;
		}
		else{
			retVal = bfGrads[nj][0] * bfGrads[ni][0] + bfGrads[nj][1] *
				bfGrads[ni][1] ;
		}
	
		retVal = retVal / ( 4 * e->area ) +
			(bfGrads[ni][0] * bfGrads[nj][0] + bfGrads[ni][1] * bfGrads[nj][1] )
			/ (4 * e->area) ;
			
		retVal *=  aVars->pcCoeff[ e->id ] ;
	}
	
	//If the problem is time dependent then we multiply by the size of the time
	//step. A term from the mass matrix also needs to be added, but this is
	//most easily done in the method that calls this one
	if( is_time_dependent( params->testCase ) &&
		!is_richards_eq( params->testCase ) )
		retVal *= params->tdCtxt.weightFact ;
	
	//Return the calculated value. If no value has been calculated then zero is
	//returned by default, as this is the value that 'retVal' is initialised to
	return retVal ;
}

/** Calculates the symmetric part of the derivative operator. At the moment this
 *	is only applicable for the implementation of the porous medium equation in
 *	test case 12
 * 	\param [in] ni	Identifier of a vertex on element \e e. This identifies
 *					the row in the Jacobian to update
 *	\param [in] nj	Identifier of a vertex on element \e e. This identifies
 *					the column in the Jacobian to update
 *	\param [in] e	The element over which to get the conribution to the
 *					symmetric part of the derivative
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation, which may
 *						be used in the calucluation of the element contribution
 *						to the Jacobian
 *	\return \p double value of the contribution from an element to the
 *			symmetric part of the Jacobian matrix
 */
double exact_deriv_spart( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars )
{
	double retVal ; //The value to return from the method
	int i ; //Loop counter
	const Node *node ; //A node on the grid
	double r, s ;
	double bfGrads[3][2], aVals[3] ; //Stores the values of the approximation
									 //and the derivative of the basis functions
									 //on the current element
	
	//Put the correct values in the temporary variables 'aVals'
	for( i=0 ; i < 3 ; i++ ){
		node = &(grid->nodes[ e->nodeIDs[i] ]) ;
		//If the current node is a boundary node then set the value of the
		//approximation to be zero. This is assuming homogeneous Dirichlet
		//boundary conditions
		if( node->type == NODE_DIR_BNDRY )
			aVals[i] = bfGrads[i][0] = bfGrads[i][1] = 0.0 ;
		else{
			aVals[i] = aVars->approx[grid->mapG2A[node->id]] ;
			//The gradients that are assigned to the basis functions are scaled
			//by a factor of 2*tArea, which is taken into account later on in
			//this function
			bfGrads[i][0] = b( i, e, grid->nodes ) ;
			bfGrads[i][1] = c( i, e, grid->nodes ) ;
		}
	}
	
	//Set the default return value to zero
	retVal = 0.0 ;
	
	if( params->testCase == 3 || params->testCase == 5 || params->testCase == 7
		|| params->testCase == 9 ){
		r = aVals[1] + aVals[2] ;
		s = r + aVals[0] ;
		s = aVals[0] * s + aVals[1] * r + pow( aVals[2], 2 ) ;
		
		retVal = (bfGrads[ni][0] * bfGrads[nj][0] + bfGrads[ni][1] *
			bfGrads[nj][1]) * (1 + params->alpha * s / 6.0 ) / (e->area * 4.0) ;
	}
	else if( params->testCase == 12 || params->testCase == 10 ||
		params->testCase == 11 || params->testCase == 52 ){
		//Work out the coefficient values
		r = aVals[1] + aVals[2] ;
		s = aVals[0] + r ;
		
		for( i=2 ; i <= params->powU ; i++ ){
			r = aVals[1] * r + pow( aVals[2], i ) ;
			s = aVals[0] * s + r ;
		}
		//Set the return value
		retVal = (bfGrads[ni][0] * bfGrads[nj][0] +
			bfGrads[ni][1] * bfGrads[nj][1]) *
			s / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ;
	}
	
	//If the problem is time dependent then we multiply by the size of the time
	//step. A term from the mass matrix also needs to be added, but this is
	//most easily done in the method that calls this one
	if( is_time_dependent( params->testCase ) )
		retVal *= params->tdCtxt.weightFact ;
	
	return retVal ;
}

/** If the analytic derivative is not known then this method returns the numeric
 *	derivative of the operator on element \e e where the derivative of equation
 *	corresponding to node identified by \e ni is taken with respect to the node
 *	identified by \e nj.
 * 	\param [in] ni	Identifier of a vertex on element \e e. This identifies
 *					the row in the Jacobian to update
 *	\param [in] nj	Identifier of a vertex on element \e e. This identifies
 *					the column in the Jacobian to update
 *	\param [in] e	The element over which to get the conribution to the
 *					derivative
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation, which may
 *						be used in the calucluation of the element contribution
 *						to the Jacobian
 *	\return \p double value of the contribution to an element of the Jacobian
 *			matrix from element \e e
 */
double numeric_deriv_term( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars )
{
	double retVal ; //The value to be returned by the function
	int aInds[3] ; //The indices into the approximation for the nodes on the
				   //element
	const RichardsEqCtxt *ctxt ; //Placeholder for the context containing
								 //information regarding the Richards equation
	double gradSum[2] ; //The sum of the gradients of the basis functions
						//multiplied by the nodal values
	double hCondSum ; //The sum of the nodal values of the hydraulic
					  //conductivity
	double hCondDSum ; //The sum of the nodal values of the derivative of the
					   //hydraulic conductivity
	
	double as[3] ; //Stores the values of the approximation at the nodes of the
				   //current element
	CVector2D gs[3] ; //Stores the values of the recovered gradients at the
					 //nodes of the current element
	Vector2D tmpGrad ; //Stores the value of the derivative of the basis
					   //function at the given node
					  
	double term, pTerm ; //Store the values of the operator over the element for
						 //the given approximation and the perturbed value,
						 //respectively
	const Node *node ; //Placeholder for a node on the grid
	
	int aInd ; //Stores the index into the approximation for a node on the grid
	int i ; //Loop counter
	
	//Set the return value to be zero by default
	retVal = 0.0 ;
	
	//Check what test case we need to approximate the gradient for
	if( params->recoverGrads && (params->testCase == 2 ||
		params->testCase == 4 || params->testCase == 6 ||
		params->testCase == 8 )){
		//Store the value of the gradient at node 'nj' for future use
		tmpGrad.x = linear_bf_gradx( nj, e, grid->nodes ) ;
		tmpGrad.y = linear_bf_grady( nj, e, grid->nodes ) ;
		
		//Store the values of the approximation and the recovered gradients
		for( i=0 ; i < 3 ; i++ ){
			node = &(grid->nodes[ e->nodeIDs[i] ]) ;
			//I assume that the boundary data is stored in the approximation as
			//well, so that I don't need to check where it comes from
			as[i] = aVars->approx[ grid->mapG2A[ node->id ] ] ;
			
			//Copy the values that are stored in the recovered gradients into
			//a local variable
			gs[i].x = aVars->recGrads[ e->nodeIDs[i] ].x ;
			gs[i].y = aVars->recGrads[ e->nodeIDs[i] ].y ;
			gs[i].cnt = aVars->recGrads[ e->nodeIDs[i] ].cnt ;
		}
		
		//Now calculate the value of the operator applied at ni, nj
		term = 0.0 ;
		for( i=0 ; i < 3 ; i++ )
			term += as[i] * recover_stiff_term_ln( ni, i, e, as, gs, grid,
				params ) ;
			
		//Perturb the value of the approximation at node 'nj'
		as[nj] += M_EPS ;
		//Perturb the value of the recovered gradient at node 'nj'. As the
		//recovered gradient is an average of surrounding nodes, the value of
		//the recovered gradient is updated at each node 
		for( i=0 ; i < 3 ; i++ ){
			if( i == nj ){
				gs[i].x += M_EPS * tmpGrad.x ;
				gs[i].y += M_EPS * tmpGrad.y ;
			}
			else{
				gs[i].x += 2.0 * M_EPS * tmpGrad.x / gs[i].cnt ;
				gs[i].y += 2.0 * M_EPS * tmpGrad.y / gs[i].cnt ;
			}
		}
		
		//Calcualte the value of the operator applied at ni, nj, but with the
		//perturbed value this time
		pTerm = 0.0 ;
		for( i=0 ; i < 3 ; i++ )
			pTerm += as[i] * recover_stiff_term_ln( ni, i, e, as, gs, grid,
				params ) ;
			
		//Set the return value
		retVal = ( pTerm - term ) / M_EPS ;
	}
	else if( is_richards_eq( params->testCase ) && params->reCtxt.useCelia ){
		//Set a placeholder to the context storing information regarding the
		//Richards equation
		ctxt = &(params->reCtxt) ;

		//Store the value of the sum of the hydraulic conductivity at
		//the element nodes
		hCondSum = 0.0 ;
		for( i = 0 ; i < 3 ; i++ ){
			aInds[i] = grid->mapG2A[ e->nodeIDs[ i ] ] ;
			hCondSum += aVars->reHCond[ aInds[i] ] ;
		}

		retVal = params->tdCtxt.weightFact *
			( b( nj, e, grid->nodes ) * b( ni, e, grid->nodes ) +
			c( nj, e, grid->nodes ) * c( ni, e, grid->nodes ) ) *
			hCondSum / (12 * e->area) ;
			
		//Calculate the contribution to the entry in the Jacobian matrix from
		//the terms with time derivative
		if( ni == nj ){
			retVal += e->area * ( aVars->reTheta[ aInds[ni] ] +
				(aVars->reTheta[ aInds[ (ni+1)%3 ] ] +
				aVars->reTheta[ aInds[ (ni+2)%3 ] ] ) / 3 ) / 10.0 ;
		}
		else{
			retVal += e->area * ( ( aVars->reTheta[ aInds[nj] ] +
				aVars->reTheta[ aInds[ni] ] ) / 30.0 +
				aVars->reTheta[ aInds[ 3 - (ni + nj) ] ] / 60.0 ) ;
		}
	}
	else if( is_richards_eq( params->testCase ) ){
		//Set a placeholder to the context storing information regarding the
		//Richards equation
		ctxt = &(params->reCtxt) ;
		
		//Store the sum of the gradients of the basis functions times the nodal
		//values of the approximation, scaled by a factor of the element area.
		//Also store the value of the sum of the hydraulic conductivity at
		//the element nodes
		gradSum[0] = 0.0 ;
		gradSum[1] = 2*e->area ;
		hCondSum = 0.0 ;
		for( i = 0 ; i < 3 ; i++ ){
			aInds[i] = grid->mapG2A[ e->nodeIDs[ i ] ] ;
			gradSum[0] += aVars->approx[aInds[i]] * b( i, e, grid->nodes ) ;
			gradSum[1] += aVars->approx[aInds[i]] * c( i, e, grid->nodes ) ;
			hCondSum += aVars->reHCond[ aInds[i] ] ;
		}
		
		//Calculate the weighted summation of the nodal values of the derivative
		//of the hydraulic conductivity
		hCondDSum = aVars->reHCondDeriv[nj] + ( aVars->reHCondDeriv[ (nj+1)%3 ]
			+ aVars->reHCondDeriv[ (nj+2)%3 ] )  / 2.0 ;
			
		//Calculate all the contribution to the entry in the Jacobian matrix
		//from the terms with spatial derivative
		retVal = params->tdCtxt.weightFact * ( ( b( ni, e, grid->nodes ) *
			gradSum[0] + c( ni, e, grid->nodes ) * gradSum[1] ) * hCondDSum / 24
			+ ( b( nj, e, grid->nodes ) * b( ni, e, grid->nodes ) +
			c( nj, e, grid->nodes ) * c( ni, e, grid->nodes ) ) *
			hCondSum / 12 ) / e->area ;
			
		//Calculate the contribution to the entry in the Jacobian matrix from
		//the terms with time derivative
		if( ni == nj ){
			retVal += e->area * ( aVars->reTheta[ aInds[ni] ] +
				(aVars->reTheta[ aInds[ (ni+1)%3 ] ] +
				aVars->reTheta[ aInds[ (ni+2)%3 ] ] ) / 3 ) / 10.0 ;
		}
		else{
			retVal += e->area * ( ( aVars->reTheta[ aInds[nj] ] +
				aVars->reTheta[ aInds[ni] ] ) / 30.0 +
				aVars->reTheta[ aInds[ 3 - (ni + nj) ] ] / 60.0 ) ;
		}
	}
	else{
		//We are not performing gradient recovery, the calculation of the
		//approximation to the derivative is performed in the same way
		
		//Get the indices into the approximation for the given nodes
		aInd = grid->mapG2A[ e->nodeIDs[ nj ] ] ;
		
		//Calculate the term without the perturbed approximation
		term = 0.0 ;
		for( i=0 ; i < 3 ; i++ )
			term += aVars->approx[ grid->mapG2A[ e->nodeIDs[ i ] ] ] *
				exact_stiff_term( ni, i, e, aVars->approx, aVars, grid,
				params ) ;
		
		//Perturb the approximation at node nj
		aVars->approx[aInd] += M_EPS ;
		
		//Calculate the perturbed term
		pTerm = 0.0 ;
		
		for( i=0 ; i<3 ; i++ )
			pTerm += aVars->approx[ grid->mapG2A[ e->nodeIDs[ i ] ] ] *
				exact_stiff_term( ni, i, e, aVars->approx, aVars, grid,
				params ) ;
				
		//Reset the value of the approximation at node nj
		aVars->approx[aInd] -= M_EPS ;
		
		//Use the perturbed and non-perturbed value to estimate the value of
		//the gradient at the current point
		retVal = ( pTerm - term ) / M_EPS ;
	}
	
	//Return the calcualted value, or zero if no calculations have been
	//performed
	return retVal ;
}

/** Returns the symmetric part of the derivative, approximated using numeric
 *	differentiation and / or integration
 * 	\param [in] ni	Identifier of a vertex on element \e e. This identifies
 *					the row in the Jacobian to update
 *	\param [in] nj	Identifier of a vertex on element \e e. This identifies
 *					the column in the Jacobian to update
 *	\param [in] e	The element over which to get the conribution to the
 *					symmetric part of the derivative
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation, which may
 *						be used in the calucluation of the element contribution
 *						to the Jacobian
 *	\return \p double value of the contribution from an element to the
 *			symmetric part of the Jacobian matrix
 */
double numeric_deriv_spart( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars )
{
	int i ; //Loop counter
	double retVal = 0.0 ; //Stores the value to return from the function
	int aInds[3] ; //Indices into the approximation for the nodes on the grid
	double hCondSum ; //Sum of the hydraulic conductivity nodal values
	
	hCondSum = 0.0 ;
	for( i=0 ; i < 3 ; i++ ){
		//Store the indices into the approximation for the nodes on the
		//element
		aInds[i] = grid->mapG2A[ e->nodeIDs[i] ] ;
		//Sum up the nodal values of the hydraulic conductivity
		hCondSum += aVars->reHCond[ aInds[i] ] ;
	}
	
	if( is_richards_eq( params->testCase ) ){
		//Calculate the contribution to the symmetric part of the Jacobian from
		//the terms with spatial derivative
		retVal = params->tdCtxt.weightFact * 
			( b( nj, e, grid->nodes ) * b( ni, e, grid->nodes ) +
			c( nj, e, grid->nodes ) * c( ni, e, grid->nodes ) ) *
			hCondSum / (12*e->area) ;
			
		//Calculate the contribution to the entry in the Jacobian matrix from
		//the terms with time derivative
		if( ni == nj ){
			retVal += e->area * ( aVars->reTheta[ aInds[ni] ] +
				(aVars->reTheta[ aInds[ (ni+1)%3 ] ] +
				aVars->reTheta[ aInds[ (ni+2)%3 ] ] ) / 3 ) / 10.0 ;
		}
		else{
			retVal += e->area * ( ( aVars->reTheta[ aInds[nj] ] +
				aVars->reTheta[ aInds[ni] ] ) / 30.0 +
				aVars->reTheta[ aInds[ 3 - (ni + nj) ] ] / 60.0 ) ;
		}
	}
	
	return retVal ;
}

/** Calculates the contribution from an element and given basis functions to the
 *	derivative of an operator on the grid, and adds this to variables passed in
 * 	\param [in] ni	Identifier of a vertex on element \e e. This identifies
 *					the row in the Jacobian to update
 *	\param [in] nj	Identifier of a vertex on element \e e. This identifies
 *					the column in the Jacobian to update
 *	\param [in] e	The element over which to get the conribution to the
 *					derivative
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation, which may
 *						be used in the calucluation of the element contribution
 *						to the Jacobian
 *	\param [out] jac	The Jacobian of the operator. An elementwise
 *						contribution is added to this in this method
 *	\param [out] spart	The symmetric part of the derivative of the operator. An
 *						elementwise contribution is added to this in this method
 */
void set_numeric_deriv_and_spart( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars, double *jac, double *spart )
{
	int i ; //Loop counter
	int k ; //Index into the jacobian and symmetric part to which to add the
			//contribution
	double contrib = 0.0 ; //Stores the contribution to the jacobian and the
						   //symmetric part
					
	//Get the index into the matrices to add the contribution to
	k = grid->rowStartInds[ grid->mapG2A[ e->nodeIDs[ni] ] ] +
		connected( &(grid->nodes[ e->nodeIDs[ni] ]),
			&(grid->nodes[ e->nodeIDs[nj] ]) ) ;
			
	if( is_richards_eq( params->testCase ) ){
		int aInds[3] ; //The indices into the approximation on the grid for the
				   //nodes of the element
		double gradSum[2] ; //The sum of the gradients of the basis functions on
							//and element
		double hCondSum ; //The sum of the nodal values of the hydraulic
						  //conductivity
		double hCondDSum ; //The sum of the nodal values of the derivative of the
						   //hydraulic conductivity
						   
		//Sum up the gradients of the nodal basis functions times the nodal values,
		//scaled by a factor of the area of the node
		gradSum[0] = 0.0 ;
		gradSum[1] = 2*e->area ;
		hCondSum = 0.0 ;
		for( i=0 ; i < 3 ; i++ ){
			//Store the indices into the approximation for the nodes on the
			//element
			aInds[i] = grid->mapG2A[ e->nodeIDs[i] ] ;
		
			gradSum[0] += b( i, e, grid->nodes ) * aVars->approx[ aInds[i] ] ;
			gradSum[1] += c( i, e, grid->nodes ) * aVars->approx[ aInds[i] ] ;
		
			hCondSum += aVars->reHCond[ aInds[i] ] ;
		}
		//Calculate the contribution to the symmetric part of the Jacobian from
		//the terms with spatial derivative
		contrib = params->tdCtxt.weightFact *
			( b( nj, e, grid->nodes ) * b( ni, e, grid->nodes ) +
			c( nj, e, grid->nodes ) * c( ni, e, grid->nodes ) ) *
			hCondSum / (12*e->area) ;
			
		//Calculate the contribution to the entry in the Jacobian matrix from
		//the terms with time derivative
		if( ni == nj ){
			contrib += e->area * ( aVars->reTheta[ aInds[ni] ] +
				(aVars->reTheta[ aInds[ (ni+1)%3 ] ] +
				aVars->reTheta[ aInds[ (ni+2)%3 ] ] ) / 3 ) / 10.0 ;
		}
		else{
			contrib += e->area * ( ( aVars->reTheta[ aInds[nj] ] +
				aVars->reTheta[ aInds[ni] ] ) / 30.0 +
				aVars->reTheta[ aInds[ 3 - (ni + nj) ] ] / 60.0 ) ;
		}
		
		//The contribution so far contains elements only from the symmetric
		//part of the matrix, so add this to the symmetric part
		spart[k] += contrib ;
		
		//Add on the contribution from the non-symmetric term in the calculation
		//of the derivative
		//Calculate the weighted summation of the nodal values of the derivative
		//of the hydraulic conductivity
		hCondDSum = aVars->reHCondDeriv[nj] + ( aVars->reHCondDeriv[ (nj+1)%3 ]
			+ aVars->reHCondDeriv[ (nj+2)%3 ] )  / 2.0 ;
			
		//Calculate all the contribution to the entry in the Jacobian matrix
		//from the terms with spatial derivative
		contrib += params->tdCtxt.weightFact * ( b( ni, e, grid->nodes ) *
			gradSum[0] + c( ni, e, grid->nodes ) * gradSum[1] ) * hCondDSum /
			(24 * e->area) ;
			
		//Update the full Jacobian matrix
		jac[k] += contrib ;
	}
	else if( params->testCase == 52 ){
		const Node *node ; //A node on the grid
		double r, s, x, t ;
		double bfGrads[3][2], aVals[3] ; //Stores the values of the approximation
										 //and the derivative of the basis functions
										 //on the current element
		double tmp[2] ;
		int inds[3] ;
	
		//Put the correct values in the temporary variables 'aVals'
		for( i=0 ; i < 3 ; i++ ){
			node = &(grid->nodes[ e->nodeIDs[i] ]) ;
			//If the current node is a boundary node then set the value of the
			//approximation to be zero. This is assuming homogeneous Dirichlet
			//boundary conditions
//			if( node->type == NODE_DIR_BNDRY )
//				aVals[i] = bfGrads[i][0] = bfGrads[i][1] = 0.0 ;
//			else{
				aVals[i] = aVars->approx[grid->mapG2A[node->id]] ;
				//The gradients that are assigned to the basis functions are scaled
				//by a factor of 2*tArea, which is taken into account later on in
				//this function
				bfGrads[i][0] = b( i, e, grid->nodes ) ;
				bfGrads[i][1] = c( i, e, grid->nodes ) ;
//			}
		}
		
		r = aVals[1] + aVals[2] ;
		s = aVals[0] + r ;
		
		for( i=2 ; i <= params->powU ; i++ ){
			r = aVals[1] * r + pow( aVals[2], i ) ;
			s = aVals[0] * s + r ;
		}
		contrib = (bfGrads[ni][0] * bfGrads[nj][0] +
			bfGrads[ni][1] * bfGrads[nj][1]) *
			s / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ;
			
		spart[k] += contrib * aVars->pcCoeff[ e->id ] *
			params->tdCtxt.weightFact ;
		
		tmp[0] = tmp[1] = 0.0 ;	
		for( i=0 ; i < 3 ; i++ ){
			inds[i] = (nj + i) %3 ;
			tmp[0] += aVals[i] * bfGrads[i][0] ;
			tmp[1] += aVals[i] * bfGrads[i][1] ;
		}
		t = x = 1.0 ;
		for( i=1 ; i <= params->powU - 1 ; i++ ){
			x = aVals[inds[1]] * x + pow( aVals[inds[0]], i ) * (i+1) ;
			t = aVals[inds[2]] * t + x ;
		}
		
		contrib = (contrib + (tmp[0] * bfGrads[ni][0] + tmp[1] * bfGrads[ni][1]) *
			t / (2 * e->area * (params->powU + 1) * (params->powU + 2)) ) *
			aVars->pcCoeff[ e->id ] ;
			
		jac[k] += contrib * aVars->pcCoeff[ e->id ] * params->tdCtxt.weightFact ;
	}
}

/** Calculates the symmetric part and the full Jacobian matrix for the case
 *	where we need both. This is when GMRES with symmetric preconditioning is to
 *	to be used. This includes the lumping of the mass matrix terms (i.e. those
 *	associated with the time derivative)
 * 	\param [in] ni	Identifier of a vertex on element \e e. This identifies
 *					the row in the Jacobian to update
 *	\param [in] nj	Identifier of a vertex on element \e e. This identifies
 *					the column in the Jacobian to update
 *	\param [in] e	The element over which to get the conribution to the
 *					derivative
 *	\param [in] grid	The grid on which to operate
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation, which may
 *						be used in the calucluation of the element contribution
 *						to the Jacobian
 *	\param [out] jac	The Jacobian of the operator. An elementwise
 *						contribution is added to this in this method
 *	\param [out] spart	The symmetric part of the derivative of the operator. An
 *						elementwise contribution is added to this in this method
 */
void set_numeric_deriv_and_spart_lumped_mass( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars, double *jac, double *spart )
{
	int i ; //Loop counter
	int k ; //Index into the jacobian and symmetric part to which to add the
			//contribution
	int diagInd ; //The index of the diagonal entry in the Jacobian matrix,
				  //corresponding to the column on which we are operating
	double contrib = 0.0 ; //Stores the contribution to the jacobian and the
						   //symmetric part
	int aInds[3] ; //The indices into the approximation on the grid for the
				   //nodes of the element
	double gradSum[2] ; //The sum of the gradients of the basis functions on
						//and element
	double hCondSum ; //The sum of the nodal values of the hydraulic
					  //conductivity
	double hCondDSum ; //The sum of the nodal values of the derivative of the
					   //hydraulic conductivity
					
	//Get the index into the matrices to add the contribution to
	k = grid->rowStartInds[ grid->mapG2A[ e->nodeIDs[ni] ] ] +
		connected( &(grid->nodes[ e->nodeIDs[ni] ]),
			&(grid->nodes[ e->nodeIDs[nj] ]) ) ;
			
	//Get the index for the diagonal for the column on which we are operating
	diagInd = grid->rowStartInds[ grid->mapG2A[ e->nodeIDs[ nj ] ] ] ;
	
	//Sum up the gradients of the nodal basis functions times the nodal values,
	//scaled by a factor of the area of the node
	gradSum[0] = 0.0 ;
	gradSum[1] = 2*e->area ;
	hCondSum = 0.0 ;
	for( i=0 ; i < 3 ; i++ ){
		//Store the indices into the approximation for the nodes on the
		//element
		aInds[i] = grid->mapG2A[ e->nodeIDs[i] ] ;
		
		gradSum[0] += b( i, e, grid->nodes ) * aVars->approx[ aInds[i] ] ;
		gradSum[1] += c( i, e, grid->nodes ) * aVars->approx[ aInds[i] ] ;
		
		hCondSum += aVars->reHCond[ aInds[i] ] ;
	}
	
	if( is_richards_eq( params->testCase ) ){
		//Calculate the contribution to the entry in the Jacobian matrix from
		//the terms with time derivative
		if( ni == nj ){
			contrib = e->area * ( aVars->reTheta[ aInds[ni] ] +
				(aVars->reTheta[ aInds[ (ni+1)%3 ] ] +
				aVars->reTheta[ aInds[ (ni+2)%3 ] ] ) / 3 ) / 10.0 ;
		}
		else{
			contrib = e->area * ( ( aVars->reTheta[ aInds[nj] ] +
				aVars->reTheta[ aInds[ni] ] ) / 30.0 +
				aVars->reTheta[ aInds[ 3 - (ni + nj) ] ] / 60.0 ) ;
		}
		
		//Add the contribution to the column in the symmetric part and the full
		//Jacobian
		spart[diagInd] += contrib ;
		jac[diagInd] += contrib ;
		
		//Calculate the contribution to the symmetric part of the Jacobian from
		//the terms with spatial derivative
		contrib = params->tdCtxt.weightFact *
			( b( nj, e, grid->nodes ) * b( ni, e, grid->nodes ) +
			c( nj, e, grid->nodes ) * c( ni, e, grid->nodes ) ) *
			hCondSum / (12*e->area) ;
		
		//The contribution so far contains elements only from the symmetric
		//part of the matrix, so add this to the symmetric part
		spart[k] += contrib ;
		
		//Add on the contribution from the non-symmetric term in the calculation
		//of the derivative
		//Calculate the weighted summation of the nodal values of the derivative
		//of the hydraulic conductivity
		hCondDSum = aVars->reHCondDeriv[nj] + ( aVars->reHCondDeriv[ (nj+1)%3 ]
			+ aVars->reHCondDeriv[ (nj+2)%3 ] )  / 2.0 ;
			
		//Calculate all the contribution to the entry in the Jacobian matrix
		//from the terms with spatial derivative
		contrib += params->tdCtxt.weightFact * ( b( ni, e, grid->nodes ) *
			gradSum[0] + c( ni, e, grid->nodes ) * gradSum[1] ) * hCondDSum /
			(24 * e->area) ;
			
		//Update the full Jacobian matrix
		jac[k] += contrib ;
	}
}

/** Sets the contributions to the Jacobian matrix when lumping of the mass
 *	matrix contribution is performed
 *	\param [in] ni	Identifier of a vertex on element \e e. This identifies
 *					the row in the Jacobian to update
 *	\param [in] nj	Identifier of a vertex on element \e e. This identifies
 *					the column in the Jacobian to update
 *	\param [in] e	The element on which to calculate the contribution to the
 *					Jacobian matrix
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation on the
 *						current grid
 *	\param [out] jac	The Jacobian matrix, to which a contribution is added
 *						in this method
 */
void set_lumped_mass_mat_jac( int ni, int nj, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars, double *jac )
{
	int i ; //Loop counter
	int diagInd, k ; //Index into the jacobian where to add the contribution
	double contrib = 0.0 ; //Stores the contribution to the jacobian
	
	int aInds[3] ; //The indices into the approximation on the grid for the
				   //nodes of the element
	double gradSum[2] ; //The sum of the gradients of the basis functions on
						//and element
	double hCondSum ; //The sum of the nodal values of the hydraulic
					  //conductivity
	double hCondDSum ; //The sum of the nodal values of the derivative of the
					   //hydraulic conductivity
		
	//diagInd will store the index of the diagonal onto which to put the mass
	//matrix contribution. Lumping of the mass matrix involves putting all the
	//contributions from a column onto the diagonal, so we are interested in
	//the index for the entry in the column here
	diagInd = grid->rowStartInds[ grid->mapG2A[ e->nodeIDs[nj] ] ] ;
	//Get the index where the non-mass matrix contributions are to be placed
	k = grid->rowStartInds[ grid->mapG2A[ e->nodeIDs[ni] ] ] +
		connected( &(grid->nodes[ e->nodeIDs[ni] ]),
			&(grid->nodes[ e->nodeIDs[nj] ]) ) ;


	if( is_richards_eq( params->testCase ) && params->reCtxt.useCelia ){
		hCondSum = 0.0 ;
		for( i = 0 ; i < 3 ; i++ ){
			aInds[i] = grid->mapG2A[ e->nodeIDs[ i ] ] ;
			gradSum[0] += aVars->approx[aInds[i]] * b( i, e, grid->nodes ) ;
			gradSum[1] += aVars->approx[aInds[i]] * c( i, e, grid->nodes ) ;
			hCondSum += aVars->reHCond[ aInds[i] ] ;
		}
		//Calculate the contribution to the Jacobian from the time derivative
		//and put this on the diagonal for the column being considered
		if( ni == nj ){
			contrib = e->area * ( aVars->reTheta[ aInds[ni] ] +
				(aVars->reTheta[ aInds[ (ni+1)%3 ] ] +
				aVars->reTheta[ aInds[ (ni+2)%3 ] ] ) / 3 ) / 10.0 ;
		}
		else{
			contrib = e->area * ( ( aVars->reTheta[ aInds[nj] ] +
				aVars->reTheta[ aInds[ni] ] ) / 30.0 +
				aVars->reTheta[ aInds[ 3 - (ni + nj) ] ] / 60.0 ) ;
		}
		
		jac[diagInd] += contrib ;
		
		//Now calculate the contribution to the (possibly) off-diagonal entries
		contrib = params->tdCtxt.weightFact *
			( b( nj, e, grid->nodes ) * b( ni, e, grid->nodes ) +
			c( nj, e, grid->nodes ) * c( ni, e, grid->nodes ) ) *
			hCondSum / (12 * e->area) ;
			
		jac[k] += contrib ;
	}
	else if( is_richards_eq( params->testCase ) ){
		gradSum[0] = 0.0 ;
		gradSum[1] = 2*e->area ;
		hCondSum = 0.0 ;
		for( i = 0 ; i < 3 ; i++ ){
			aInds[i] = grid->mapG2A[ e->nodeIDs[ i ] ] ;
			gradSum[0] += aVars->approx[aInds[i]] * b( i, e, grid->nodes ) ;
			gradSum[1] += aVars->approx[aInds[i]] * c( i, e, grid->nodes ) ;
			hCondSum += aVars->reHCond[ aInds[i] ] ;
		}
		
		//Calculate the contribution to the Jacobian from the time derivative
		//and put this on the diagonal for the column being considered
		if( ni == nj ){
			contrib = e->area * ( aVars->reTheta[ aInds[ni] ] +
				(aVars->reTheta[ aInds[ (ni+1)%3 ] ] +
				aVars->reTheta[ aInds[ (ni+2)%3 ] ] ) / 3 ) / 10.0 ;
		}
		else{
			contrib = e->area * ( ( aVars->reTheta[ aInds[nj] ] +
				aVars->reTheta[ aInds[ni] ] ) / 30.0 +
				aVars->reTheta[ aInds[ 3 - (ni + nj) ] ] / 60.0 ) ;
		}
		
		jac[diagInd] += contrib ;
		
		//Now calculate the contribution to the (possibly) off-diagonal entries
		
		//Calculate the weighted summation of the nodal values of the derivative
		//of the hydraulic conductivity
		hCondDSum = aVars->reHCondDeriv[nj] + ( aVars->reHCondDeriv[ (nj+1)%3 ]
			+ aVars->reHCondDeriv[ (nj+2)%3 ] )  / 2.0 ;
		
		//Calculate all the contribution to the entry in the Jacobian matrix
		//from the terms with spatial derivative
		contrib = params->tdCtxt.weightFact * ( ( b( ni, e, grid->nodes ) *
			gradSum[0] + c( ni, e, grid->nodes ) * gradSum[1] ) * hCondDSum / 24
			+ ( b( nj, e, grid->nodes ) * b( ni, e, grid->nodes ) +
			c( nj, e, grid->nodes ) * c( ni, e, grid->nodes ) ) *
			hCondSum / 12 ) / e->area ;
			
		jac[k] += contrib ;
	}
}

/** Adds the contribution to the Jacobian matrix in the case when mass matrix
 *	lumping is performed for the given node and element
 *	\param [in] ni	The node (using local numbering) on the element for which to
 *					calculate the contribution
 *	\param [in] e	The element over which to calculate the contribution
 *	\param [in] grid	The grid on which we are currently operating
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] aVars	Variables associated with the approximation on the
 *						current grid
 *	\param [out] jDiags	The diagonal of the Jacobian matrix. This is updated in
 *						this method
 */
void set_lumped_mass_mat_jac_diags( int ni, const Element *e,
	const GridParameters *grid, const AlgorithmParameters *params,
	const ApproxVars *aVars, double *jDiags )
{
	int j ; //Loop counter
	double tmp ; //An intermediate value in the calculations
	int aInds[3] ; //The indices into the approximation for the given nodes
	int ind ; //Index into the approximation for a given node
	double hCondSum = 0.0 ; //The sum of the hydraulic conductivity over the
							//nodes of the element
	double hCondDSum = 0.0 ;
	double gradSum[2] ;

	//Loop over the nodes in the element
	for( j=0 ; j < 3 ; j++ ){
		//Store the indices into the approximations
		aInds[j] = grid->mapG2A[ e->nodeIDs[j] ] ;
		hCondSum += aVars->reHCond[ aInds[j] ] ;
	}
	
	//Calculate the contribution to the diagonal from the time derivative term
	tmp = 10 * aVars->reTheta[ aInds[ni] ] ;
	
	for( j=1 ; j < 3 ; j++ ){
		ind = aInds[ (ni+j)%3 ] ;
		tmp += 5 * aVars->reTheta[ ind ] ;
	}
	
	//Set the contribution to the Jacobian from the term associated with the
	//time derivative
	jDiags[ aInds[ni] ] += e->area * tmp / 60.0 ;
	
	//Update the contribution to the Jacobian from the term associated with the
	//spatial derivative
	if( is_richards_eq( params->testCase ) && params->reCtxt.useCelia ){
		//If we are using the model by Celia, et al. we do not have a term
		//related to the derivative of the hydraulic conductivity
		jDiags[ aInds[ni] ] += params->tdCtxt.weightFact *
			( pow( b( ni, e, grid->nodes ), 2 ) +
			pow( c( ni, e, grid->nodes ), 2 ) ) * hCondSum / (12 * e->area) ;
	}
	else if( is_richards_eq( params->testCase ) ){
		//Calculate the contribution to the gradient for each basis function on
		//the element
		gradSum[0] = 0.0 ;
		gradSum[1] = 2 * e->area ;
		for( j=0 ; j < 3 ; j++ ){
			hCondDSum += aVars->reHCondDeriv[ aInds[j] ] ;
			gradSum[0] += b( j, e, grid->nodes ) ;
			gradSum[1] += c( j, e, grid->nodes ) ;
		}
		
		//If we are not using the model by Celia et al. then we take the full
		//derivative of the nonlinear operator in the Jacobian matrix
		jDiags[ aInds[ni] ] += params->tdCtxt.weightFact *
			( ( b( ni, e, grid->nodes ) * gradSum[0] + c( ni, e, grid->nodes ) *
			gradSum[1] ) * hCondDSum / 24 + ( b( ni, e, grid->nodes ) *
			b( ni, e, grid->nodes ) + c( ni, e, grid->nodes ) *
			c( ni, e, grid->nodes ) ) * hCondSum / 12 ) / e->area ;
	}
}

/** Indicates whether or not we are able to calculate the symmetric part of the
 *	derivative of an operator for a given test case. A value zero indicates
 *	false, else true
 *	\param [in] testCase	The test case for which to check
 *	\return \p boolean value indicating whether or not there is a symmetric part
 *			in the Jacobian matrix
 */
bool has_spart( const AlgorithmParameters *params )
{
	
	if( params->testCase == 10 || params->testCase == 7 ||
		(is_richards_eq( params->testCase ) && !params->reCtxt.useCelia ) ||
		( params->testCase >= 40 && params->testCase <= 52 ) )
		return true ;
	
	return false ;
}

/** Calculates the contribution to the right hand side from boundary terms for
 *	the given element and basis function centered at node \e nodeInd
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] e		The element on which we are operating
 *	\param [in] nodeInd	The index of the node on the element. This is in the
 *						range (0,1,2)
 *	\param [in] node	The node to which the index \e nodeInd points
 *	\return \p double value of the contribution to the right hand side from the
 *			boundary term on the given element for the given basis function
 */
double boundary_rhs_contribution( const AlgorithmParameters *params,
	const GridParameters *grid, const Element *e, int nodeInd,
	const Node *node )
{
	double retVal ; //The return value
	int next ; //The next index on the element
	int j ;
	const Node *compNode ;
	
	//Set the default return value
	retVal = 0.0 ;
	
	//If we are not performing nonlinear multigrid and we are on the finest grid
	//level return zero
	if( !(is_non_linear( params->testCase ) && params->nlMethod == METHOD_NLMG)
		&& params->currLevel != params->fGridLevel )
		return 0.0 ;

	if( params->testCase == 13 && node->y == 1.0 ){
		for( j=1 ; j < 3 ; j++ ){
			//Get the internal index of the next node
			next = (nodeInd + j)%3 ;
			compNode = &(grid->nodes[ e->nodeIDs[ next ] ]) ;
			//Check if the node to compare with is on the same boundary
			if( compNode->type != NODE_INTERIOR && node->y == compNode->y ){
				//We don't need to calculate much as the function to integrate is
				//a scaling of a linear basis function.
				retVal += grid->gridSpacing ;
			}
		}
	}
	else if( (params->testCase == 14 || params->testCase == 15 ||
		params->testCase == 25 || params->testCase == 27) && node->x == 0.0 ){
		for( j=1 ; j < 3 ; j++ ){
			next = (nodeInd +j)%3 ;
			compNode = &(grid->nodes[ e->nodeIDs[ next ]]) ;
			//Check if the node to compare with is on the same boundary
			if( compNode->type != NODE_INTERIOR && node->x == compNode->x ){
				//The function to integrate is a scaling of the linear basis
				//function. The value that we multiply by is the flux into the
				//boundary
				retVal += params->tdCtxt.tStepSize * grid->gridSpacing *
					params->reCtxt.flowIn / 2.0 ;
			}
		}
	}
	else if( params->testCase == 23 && node->x == 0.0 ){
		for( j=1 ; j < 3 ; j++ ){
			next = (nodeInd + j)%3 ;
			compNode = &(grid->nodes[ e->nodeIDs[ next ] ]) ;
			//Check if the node to compare with is on the same boundary
			if( compNode->type != NODE_INTERIOR && node->x == compNode->x )
				//The function to integrate is a scaling of the linear basis
				//function. The value that we multiply by is the flux into
				//the domain
				retVal += params->tdCtxt.tStepSize * grid->gridSpacing *
					params->reCtxt.flowIn / 2.0 ;
		}
	}
	else if( params->testCase == 23 && node->x == 100.0 ){
		for( j=1 ; j < 3 ; j++ ){
			next = (nodeInd + j)%3 ;
			compNode = &(grid->nodes[ e->nodeIDs[ next ] ]) ;
			//Check if the node to compare with is on the same boundary
			if( compNode->type != NODE_INTERIOR && node->x == compNode->x )
				//The function to integrate is a scaling of the linear basis
				//function. The value that we multiply by is the flux out of
				//the domain
				retVal -= params->tdCtxt.tStepSize * grid->gridSpacing *
					params->reCtxt.flowOut / 2.0 ;
		}
	}
	
	return retVal ;
}

/** Checks whether the boundary type stored in the grid object match the
 *	expectation for the given test case. A warning message is displayed if they
 *	do not match, and a value of zero returned
 *	\param [in] grid	The grid on which we are operating
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\return \p int value indicating whether the boundary type of the grid
 *			matches the expected boundary type or not. A value 0 indicates they
 *			do not match, else they do.
 */
int check_boundary_type( const GridParameters *grid,
	const AlgorithmParameters *params )
{
	int i, j ; //Loop counter
	enum NODE_TYPE bndType[4] ; //Stores the expected boundary type
	
	//Set default values for the boundary types
	for( i=0 ; i < 4 ; i++ )
		bndType[i] = NODE_DIR_BNDRY ;

	//Test case 13 only has a Dirichlet boundary on the bottom
	if( params->testCase == 13 )
		bndType[0] = bndType[2] = bndType[3] = NODE_NEUM_BNDRY ;
	else if( params->testCase == 14 || params->testCase == 15 ||
		params->testCase == 25 || params->testCase == 27 )
		bndType[0] = bndType[1] = bndType[3] = NODE_NEUM_BNDRY ;
	else if( params->testCase == 16 )
		bndType[1] = bndType[3] = NODE_NEUM_BNDRY ;
	else if( params->testCase == 17 || params->testCase == 18 ||
		params->testCase == 22 || params->testCase == 24 )
		bndType[0] = bndType[2] = bndType[3] = NODE_NEUM_BNDRY ;
	else if( params->testCase == 19 || params->testCase == 20 ||
		params->testCase == 21 || params->testCase == 26 )
		bndType[0] = bndType[1] = bndType[2] = NODE_NEUM_BNDRY ;
	else if( params->testCase == 23 )
		bndType[0] = bndType[1] = bndType[2] = bndType[3] = NODE_NEUM_BNDRY ;
		
	for( i=0 ; i < 4 ; i++ ){
		if( grid->bndType[i] != bndType[i] ){
			printf( "***********\n" ) ;
			printf( "WARNING: Grid boundary type does not match " ) ;
			printf( "expected boundary type\n" ) ;
			printf( "Expected type: " ) ;
			for( j=0 ; j < 3 ; j++ ){
				if( bndType[j] == NODE_NEUM_BNDRY ) printf( "N, " ) ;
				else printf( "D, " ) ;
			}
			if( bndType[3] == NODE_NEUM_BNDRY ) printf( "N\nGiven Type: " ) ;
			else printf( "D\nGiven Type: " ) ;
			for( j=0 ; j < 3 ; j++ ){
				if( grid->bndType[j] == NODE_NEUM_BNDRY ) printf( "N, " ) ;
				else printf( "D, " ) ;
			}
			if( grid->bndType[3] == NODE_NEUM_BNDRY )
				printf( "N\n***********\n" ) ;
			else
				printf( "D\n***********\n" ) ;
			return 0 ;
		}
	}
	
	//If we get to here it means that all the boundaries are as expected, and
	//we do not need to print anything. We return success, signalled by a
	//nonzero value
	return 1 ;
}

/** Indicator as to whether the given test case solves a problem involving a
 *	Richards equation or not
 *	\param [in] testCase	The test case to check if it solves a Richards
 *							equation type problem
 *	\return \p boolean indicating whether the given test case solves a Richards
 *			equation type problem or not
 */
bool is_richards_eq( int testCase )
{
	//If the test case solves a Richards equation return true
	if( testCase == 14 || testCase == 15 || testCase == 16 || testCase == 17 ||
		testCase == 18 || testCase == 19 || testCase == 20 || testCase == 21 ||
		testCase == 22 || testCase == 23 || testCase == 24 || testCase == 25 ||
		testCase == 26 || testCase == 27 || testCase == 101 || testCase == 102 )
		return true ;
		
	//Return false by default
	return false ;
}

/** Indicator as to whether the given test case solves a problem which is a
 *	\f$p\f$-Laplacian
 *	\param [in] testCase	The identifier of the test case to check the
 *							equation problem type
 *	\return \p boolean indicating whether the given test case solves a
 *			\f$p\f$-Laplacian type problem or not
 */
bool is_p_laplacian( int testCase ){
	if( (testCase >= 28 && testCase <= 39) ||
		(testCase >= 53 && testCase <= 55) )
		return true ;
		
	return false ;
}

/** Indicator as to whether the given test case has a piecewise constant
 *	coefficient that needs to be calculated on an elementwise basis
 *	\param [in] testCase	The identifier of the test case to check
 *	\return \p boolean indicating whether the given test case uses a piecewise
 *			constant coefficient or not
 */
bool has_pc_coeff( int testCase ){
	if( is_p_laplacian( testCase ) || (testCase >= 40 && testCase <= 58) )
		return true ;
	
	return false ;
}

/** Returns the value of the piecewise constant coefficient function for a test
 *	test case given the \f$x\f$- and \f$y\f$-coordinates of the point on the
 *	grid
 *	\param [in] testCase	Indicator for the test case for which to get the
 *							value of the piecewise constant function
 *	\param [in] x	\f$x\f$-coordinate of the point on the grid to consider
 *	\param [in] y	\f$y\f$-coordinate of the point on the grid to consider
 */
double get_coeff_val( const AlgorithmParameters *params, double x, double y ){
	double alpha = 0.0; //The value of the piecewise constant function to return
	
	if( params->testCase == 28 || params->testCase == 29 ||
		params->testCase == 30 || params->testCase == 40 ||
		params->testCase == 41 || params->testCase == 42 ){
			alpha = (x < 0.5) ? params->alpha : 1.0 ;
	}
	else if( params->testCase == 31 || params->testCase == 32 ||
		params->testCase == 33 || params->testCase == 43 ||
		params->testCase == 44 || params->testCase == 45 ){
		alpha = ( x < 0.265625 ) ? params->alpha : 1.0 ;
	}
	else if( params->testCase == 34 || params->testCase == 35 ||
		params->testCase == 36 || params->testCase == 46 ||
		params->testCase == 47 || params->testCase == 48 ){
		if( x >= 0.25 && x < 0.75 && y >= 0.25 && y < 0.75 )
			alpha = 1.0 ;
		else
			alpha = params->alpha ;
	}
	else if( params->testCase == 37 || params->testCase == 38 ||
		params->testCase == 39 || params->testCase == 49 ||
		params->testCase == 50 || params->testCase == 51 ){
		if( ( (x >= 0.109375 && x < 0.328125) ||
			(x >= 0.671875 && x < 0.890625) ) && ( (y >= 0.109375 &&
			y < 0.328125) || (y >= 0.671875 && y < 0.890625) ) ){
			alpha = 1.0 ;
		}
		else
			alpha = params->alpha ;
	}
	else if( params->testCase == 52 ){
		alpha = (x < -params->initRad) ? params->alpha : 1.0 ;
	}
	
	return alpha ;
}

/** Sets the values of the coefficient function for a \f$p\f$-Laplacian problem.
 *	This assumes that the coefficient is piecewise constant on an element, and
 *	that the current grid resolves all discontinuities in the coefficient
 *	\param [in] params	Parameters defining how the algorithm is to be executed
 *	\param [in] grid	The grid on which we are currently operating
 *	\param [out] coeff	The coefficient function to populate with values for
 *						each element on the grid
 */
void set_pc_coeff_vals( const AlgorithmParameters *params,
	const GridParameters *grid, double *coeff )
{
	int i ; //Loop counter
	XYBounds elBox ; //A bounding box for the given element
	
	if( params->testCase == 28 || params->testCase == 29 ||
		params->testCase == 30 || params->testCase == 40 ||
		params->testCase == 41 || params->testCase == 42 ){
		//For this test case we have the coefficient is 1.0 for x < 0.5, and
		//100.0 otherwise
		for( i=0 ; i < grid->numElements ; i++ ){ //For each element
			set_element_bounds( &(grid->elements[i]), grid->nodes, &elBox ) ;
			//The coefficient value on the current element is defined by the
			//minimum x-value on the current element
			if( elBox.minX < 0.5 )
				coeff[i] = 1.0 ;
			else
				coeff[i] = params->alpha ;
		}
	}
	else if( params->testCase == 31 || params->testCase == 32 ||
		params->testCase == 33 || params->testCase == 43 ||
		params->testCase == 44 || params->testCase == 45 ){
		
		for( i=0 ; i < grid->numElements ; i++ ){
			set_element_bounds( &(grid->elements[i]), grid->nodes, &elBox ) ;
			//The coefficient value on the current element (assuming that the
			//grid resolves all discontinuities) is defined by the minimum
			//x-value on the current element
			if( elBox.minX < 17/64.0 )
				coeff[i] = params->alpha ;
			else
				coeff[i] = 1.0 ;
		}
		
//		double coeffX ; //The x-coordinate of the line at which the
//						//disccontinuity in the coefficient takes place
//		bool orientation ; //The orienation of the triangular element. False
//						   //indicates that the hypotenuse is on the lower side
//						   //of the triangle. True indicates that the hypotenuse
//						   //is on the upper side of the triangle
//		double distX ; //The distance that the discontinuity in the coefficient
//					   //is from a boundary of the current element
//		double h = grid->gridSpacing ; //The grid spacing on the current grid
//						
//		coeffX = 17 / 64.0 ;
//		//Loop over the elements on the grid
//		for( i=0 ; i < grid->numElements ; i++ ){
//			e = &(grid->elements[i]) ;
//			set_element_bounds( e, grid->nodes, &elBox ) ;
//			
//			//If the maximum x-coordinate is less than or equal to the
//			//x-coordinate of the discontinuity, or the minimum x-coordinate is
//			//greater than or equal to the x-coordinate of the discontinuity set
//			//a constant value on the element
//			if( elBox.maxX <= coeffX ){
//				coeff[i] = params->alpha ;
//				continue ;
//			}
//			else if( elBox.minX >= coeffX ){
//				coeff[i] = 1.0 ;
//				continue ;
//			}
//			
//			//If we get to here the coefficient intersects the given element
//			//vertically. We consider two case for the different orientations
//			//of the regular triangular elements
//			orientation = ( grid->nodes[ e->nodeIDs[0] ].y ==
//				grid->nodes[ e->nodeIDs[1] ].y) ;
//				
//			if( orientation ){
//				//The hypotenuse is on the upper side of the triangle. We set
//				//the variable distX to be the distance of the discontinuity
//				//from the right hand side of the triangle
//				distX = elBox.maxX - coeffX ;
//				//Set the volume fraction of the coefficient value not equal to
//				//one
//				coeff[i] = pow( h-distX, 2 ) / (2.0 * e->area) ;
//				//Calculate the contribution to the integral from the
//				//current element
//				coeff[i] = params->alpha * coeff[i] + (1-coeff[i]) ;
//			}
//			else{
//				//The hypotenuse is on the lower side of the triangle. We set
//				//the variable distX to be the distance of the discontinuity
//				//from the left hand side of the triangle
//				distX = coeffX - elBox.minX ;
//				//Set the volume fraction of the coefficient value equal to one
//				coeff[i] = pow( h- distX, 2) / ( 2.0 * e->area ) ;
//				//Calculate the contribution to the integral from the current
//				//element
//				coeff[i] = coeff[i] + (1-coeff[i]) * params->alpha ;
//			}
//		}
	}
	else if( params->testCase == 34 || params->testCase == 35 ||
		params->testCase == 36 || params->testCase == 46 ||
		params->testCase == 47 || params->testCase == 48 ){
		for( i=0 ; i < grid->numElements ; i++ ){
			set_element_bounds( &(grid->elements[i]), grid->nodes, &elBox ) ;
			if( elBox.minX >= 0.25 && elBox.minX < 0.75 && elBox.minY >= 0.25 &&
				elBox.minY < 0.75 )
				coeff[i] = 1.0 ;
			else
				coeff[i] = params->alpha ;
		}
	}
	else if( params->testCase == 37 || params->testCase == 38 ||
		params->testCase == 39 || params->testCase == 49 ||
		params->testCase == 50 || params->testCase == 51 ){
		for( i=0 ; i < grid->numElements ; i++ ){
			set_element_bounds( &(grid->elements[i]), grid->nodes, &elBox ) ;
			//Assuming that the discontinuities in coefficient are resolved on
			//this grid level then the value of the piecewise constant
			//coefficient is determined by the minimum x- and y-coordinates of
			//the element
			if( ( (elBox.minX >= 0.109375 && elBox.minX < 0.328125) ||
				(elBox.minX >= 0.671875 && elBox.minX < 0.890625) ) &&
				( (elBox.minY >= 0.109375 && elBox.minY < 0.328125) ||
				(elBox.minY >= 0.671875 && elBox.minY < 0.890625) ) ){
				coeff[i] = 1.0 ;
			}
			else
				coeff[i] = params->alpha ;
		}
		
//		double distX[2], distY[2] ; //The distance from the boundary of the
//							  //element in the x- and y-direction of the part
//							  //with coefficient value 1
//		double coeffW, coeffH ; //The width and the height of the area with
//								//coefficient value 1 in the element
//		double coeffRegion[4] ; //The bounding box of the region of the
//								//coefficient in which we are interested. The
//								//values are the x or y coordinates of the
//								//left, bottom, right and top boundaries, in
//								//that order
//		double h ; //The grid spacing on the current grid
//								
//		double l[2], r[2] ; //The left and right boundaries of the boxes
//								//on the domain for these test cases
//		double b[2], t[2] ; //The top and bottom boundaries of the boxes
//								//on the domain for these test cases
//		bool orientation ; //The orienation of the triangular element. False
//						   //indicates that the hypotenuse is on the lower side
//						   //of the triangle. True indicates that the hypotenuse
//						   //is on the upper side of the triangle
//								
//		l[0] = b[0] = 0.109375 ;
//		l[1] = b[1] = 0.671875 ;
//		r[0] = t[0] = 0.328125 ;
//		r[1] = t[1] = 0.890625 ;
//		for( i=0 ; i < grid->numElements ; i++ ){
//			//Get the element on the current grid
//			e = &(grid->elements[i]) ;
//			//Get the bounding box for the elements
//			set_element_bounds( e, grid->nodes, &elBox ) ;
//			
//			if( elBox.maxX <= l[0] ||
//				(elBox.minX >= r[0] && elBox.maxX <= l[1]) ||
//				elBox.minX >= r[1] || elBox.maxY <= b[0] ||
//				(elBox.minY >= t[0] && elBox.maxY <= b[1]) ||
//				elBox.minY >= t[1] ){
//				//If no part of the element is in a region with coefficient one
//				//then set the value to be the coefficient value specified in
//				//the algorithm parameters
//				coeff[i] = params->alpha ;
//				continue ; //Go to the next item in the loop
//			}
//			else if( ( ( elBox.minX >= l[0] && elBox.maxX <= r[0] ) || 
//				( elBox.minX >= l[1] && elBox.maxX <= r[1] ) ) &&
//				( ( elBox.minY >= b[0] && elBox.maxY <= t[0] ) ||
//				( elBox.minY >= b[1] && elBox.maxY <= t[1] ) ) ){
//				//The element is completely contained in the region of
//				//coefficient value one
//				coeff[i] = 1.0 ;
//				continue ; //Go to the next item in the loop
//			}
//			
//			//We now know that the element straddles the boundary of an area
//			//of coefficient value one. We do not deal with the case where an
//			//element can be in two different regions of coefficient value one
//			//as I restrict the grids to be fine enough for this to not happen
//			
//			//Figure out which area we are in
//			if( elBox.maxX < l[1] ){
//				//Left hand column
//				coeffRegion[0] = l[0] ;
//				coeffRegion[2] = r[0] ;
//				if( elBox.maxY < b[1] ){ //Bottom box
//					coeffRegion[1] = b[0] ;
//					coeffRegion[3] = t[0] ;
//				}
//				else{ //Top box
//					coeffRegion[1] = b[1] ;
//					coeffRegion[3] = t[1] ;
//				}
//			}
//			else{
//				//Right hand column
//				coeffRegion[0] = l[1] ;
//				coeffRegion[2] = r[1] ;
//				if( elBox.maxY < b[1] ){ //Bottom box
//					coeffRegion[1] = b[0] ;
//					coeffRegion[3] = t[0] ;
//				}
//				else{ //Top box
//					coeffRegion[1] = b[1] ;
//					coeffRegion[3] = t[1] ;
//				}
//			}
//			
//			//Now we know which box we are in we would like to know the
//			//orientation of the triangle. If the y value doesn't change between
//			//the first two nodes on the grid then we have the hypotenuse on
//			//the upper side of the triangular element
//			orientation = (grid->nodes[ e->nodeIDs[0] ].y ==
//				grid->nodes[ e->nodeIDs[1] ].y) ;
//			h = grid->gridSpacing ;
//			
//			if( orientation ){
//				//The hypotenuse is on the upper side of the triangular element
//				//Get the distance from the right boundary for the region of
//				//coefficient value one
//				if( coeffRegion[2] >= elBox.maxX ) distX[1] = 0.0 ;
//				else distX[1] = elBox.maxX - coeffRegion[2] ;
//				
//				//Get the distance from the bottom boundary for the region of
//				//coefficient value one
//				if( coeffRegion[1] <= elBox.minY ) distY[0] = 0.0 ;
//				else distY[0] = coeffRegion[1] - elBox.minY ;
//				
//				//If the distance in x and y from the boundary sum to more than
//				//the grid spacing this indicates that the region of coefficient
//				//value one does not intersect this element
//				if( distY[0] + distX[1] >= h ){
//					coeff[i] = params->alpha ;
//					continue ;
//				}
//				
//				//Get the width of the region of coefficient value one in the 
//				//current element, as well as the distance from the left of the
//				//element of the region
//				if( coeffRegion[0] <= elBox.minX ){
//					//If the region extends to the left of the element we find
//					//the point at which the boundary intersects the diagonal
//					//of the element
//					coeffW = h - distX[1] - distY[0] ;
//					distX[0] = distY[0] ;
//				}
//				else{
//					//If the region does not extend to the left of the minimum
//					//x-coordinate we check to see if the bottom boundary is
//					//completely contained in the element
//					distX[0] = coeffRegion[0] - elBox.minX ;
//					if( distY[0] - distX[0] > 0 ){
//						//The bottom boundary is not contained in the element
//						coeffW = h - distX[1] - distY[0] ;
//						distX[0] = distY[0] ;
//					}
//					else
//						coeffW = h - distX[0] - distX[1] ;
//				}
//				
//				//Get the height of the region of coefficient value one in the
//				//current element, as well as the distance from the top of the
//				//element of the region
//				if( coeffRegion[3] >= elBox.maxY ){
//					//If the region extends to the top of the element we
//					//find at which point it intersects the diagonal
//					coeffH = h - distX[1] - distY[0] ;
//					distY[1] = distX[1] ;
//				}
//				else{
//					//If the region does not extend to the top of the maximum
//					//x-coordinate we check to see if the right boundary is
//					//completely contained in the element
//					distY[1] = elBox.maxY - coeffRegion[3] ;
//					if( distX[1] - distY[1] > 0 ){
//						//The right boundary is not contained in the element
//						coeffH = h - distX[1] - distY[0] ;
//						distY[1] = distX[1] ;
//					}
//					else
//						coeffH = h - distY[0] - distY[1] ;
//				}
//				
//				//We now have all the information that we need to calculate
//				//the area of the region of coefficient value one in the
//				//element
//				if( distX[0] == 0.0 ){
//					//Calculate the volume fraction of the region with
//					//coefficient value not one
//					coeff[i] = (distX[1] * coeffH + pow(distY[1],2) / 2) /
//						e->area ;
//					//Calculate the contribution to the integral from the
//					//coefficient function on the current element
//					coeff[i] = params->alpha * coeff[i] + (1-coeff[i]) ;
//					continue ;
//				}
//				else if( distY[1] == 0.0 ){
//					//Calculate the volume fraction of the region with
//					//coefficient value not one
//					coeff[i] = (distY[0] * coeffW + pow(distX[0],2) / 2 ) /
//						e->area ;
//					//Calculate the contribution to the integral from the
//					//coefficient function on the current element
//					coeff[i] = params->alpha * coeff[i] + (1-coeff[i]) ;
//					continue ;
//				}
//				
//				//Check if the entire region of coefficient value one is
//				//contained in the current element
//				if( coeffW + coeffH + distX[1] + distY[0] <= h ){
//					//Calculate the volume fraction of the region with
//					//coefficient value one
//					coeff[i] = coeffW * coeffH / e->area ;
//					//Calculate the contribution to the integral from the
//					//coefficient function on the current element
//					coeff[i] = coeff[i] + (1-coeff[i])*params->alpha ;
//				}
//				else{
//					//Calculate the volume fraction of the region without
//					//coefficient value one
//					coeff[i] = ( (pow(distX[0],2) + pow(distY[1],2)) / 2 +
//						(distX[1] + coeffW) * distY[0] + distX[1] * coeffH ) /
//						e->area ;
//					//Calculate the contribution to the integral from the
//					//coefficient function on the current element
//					coeff[i] = params->alpha * coeff[i] + (1-coeff[i]) ;
//				}
//			}
//			else{
//				//The hypotenuse is on the lower side of the triangular element
//				//Get the distance from the left boundary that the coefficient
//				//is
//				if( coeffRegion[0] <= elBox.minX ) distX[0] = 0.0 ;
//				else distX[0] = coeffRegion[0] - elBox.minX ;
//				
//				//Get the distance from the top boundary that the coefficient is
//				if( coeffRegion[3] >= elBox.maxY ) distY[1] = 0.0 ;
//				else distY[1] = elBox.maxY - coeffRegion[3] ;
//				
//				//If the distance in x and y from the boundary sum to more than
//				//the grid spacing this indicates that the region of coefficient
//				//value one does not intersect this element
//				if( distX[0] + distY[1] >= h ){
//					coeff[i] = params->alpha ;
//					continue ;
//				}
//				
//				//Get the width of the region of coefficient value one in the 
//				//current element, as well as the distance from the right of the
//				//element of the region
//				if( coeffRegion[2] >= elBox.maxX ){
//					//If the region extends to the right of the element we find
//					//the point at which the boundary intersects the diagonal
//					//of the element
//					coeffW = h - distX[0] - distY[1] ;
//					distX[1] = distY[1] ;
//				}
//				else{
//					//If the region does not extend to the right of the maximum
//					//x-coordinate we check to see if the top boundary is
//					//completely contained in the element
//					distX[1] = elBox.maxX - coeffRegion[2] ;
//					if( distY[1] - distX[1] > 0 ){
//						//The bottom boundary is not contained in the element
//						coeffW = h - distX[0] - distY[1] ;
//						distX[1] = distY[1] ;
//					}
//					else
//						coeffW = h - distX[0] - distX[1] ;
//				}
//				
//				//Get the height of the region of coefficient value one in the
//				//current element, as well as the distance from the bottom of
//				//the element of the region
//				if( coeffRegion[1] <= elBox.minY ){
//					//If the region extends to the bottom of the element we
//					//find at which point it intersects the diagonal
//					coeffH = h - distX[0] - distY[1] ;
//					distY[0] = distX[0] ;
//				}
//				else{
//					//If the region does not extend to the bottom of the minimum
//					//x-coordinate we check to see if the left boundary is
//					//completely contained in the element
//					distY[0] = coeffRegion[1] - elBox.minY ;
//					if( distX[0] - distY[0] > 0 ){
//						//The right boundary is not contained in the element
//						coeffH = h - distX[0] - distY[1] ;
//						distY[0] = distX[0] ;
//					}
//					else
//						coeffH = h - distY[0] - distY[1] ;
//				}
				
//				if( i == 450 ){
//					//Print the element nodes
//					printf( "Nodes: %d, %d, %d\n", e->nodeIDs[0], e->nodeIDs[1], e->nodeIDs[2] ) ;
//					printf( "h: %.18lf\n", h ) ;
//					//Print the distance from the vertical boundaries
//					printf( "distX: %.18lf, %.18lf\n", distX[0], distX[1] ) ;
//					//Print the width of the coefficient in the element
//					printf( "coeffW: %.18lf\n", coeffW ) ;
//					//Print the sum of the values in the x-direction
//					printf( "x-sum: %.18lf\n", coeffW + distX[0] + distX[1] ) ;
//					//Print the distance from the horizontal boundaries
//					printf( "distY: %.18lf, %.18lf\n", distY[0], distY[1] ) ;
//					//Print the height of the coefficient in the element
//					printf( "coeffH: %.18lf\n", coeffH ) ;
//					//Print the sum of the values in the y-direction
//					printf( "y-sum: %.18lf\n", coeffH + distY[0] + distY[1] ) ;
//					//Print the bounding box
//					printf( "elBox: %.6lf, %.6lf, %.6lf, %.6lf\n",
//						elBox.minX, elBox.minY, elBox.maxX, elBox.maxY ) ;
//					//Print the coefficient region
//					printf( "Coeff Region: %.6lf, %.6lf, %.6lf, %.6lf\n",
//						coeffRegion[0], coeffRegion[1], coeffRegion[2],
//						coeffRegion[3] ) ;
//				}
				
//				//We now have all the information that we need to calculate
//				//the area of the region of coefficient value one in the
//				//element
//				if( distX[1] == 0.0 ){
//					//Calculate the volume fraction of the region with
//					//coefficient value not one
//					coeff[i] = ( distX[0] * coeffH + pow(distY[0],2) /2 ) /
//						e->area ;
//					//Calculate the contribution to the integral from the
//					//current element
//					coeff[i] = coeff[i] * params->alpha + (1-coeff[i]) ;
//					continue ;
//				}
//				else if( distY[0] == 0.0 ){
//					//Calculate the volume fraction of the region with
//					//coefficient value not one
//					coeff[i] = ( (distY[1]*coeffW) + pow( distX[1], 2) / 2 ) /
//						e->area ;
//					//Calculate the contribution to the integral from the
//					//current element
//					coeff[i] = coeff[i] * params->alpha + (1-coeff[i]) ;
//					continue ;
//				}
//				
//				//Check if the entire region of coefficient value one is
//				//contained in the current element
//				if( distX[0] + coeffW + distY[1] + coeffH <= h ){
//					//Calculate the volume fraction of the region with
//					//coefficient value one
//					coeff[i] = coeffW * coeffH / e->area ;
//					//Calculate the contribution to the integral from the
//					//current element
//					coeff[i] = coeff[i] + (1-coeff[i]) * params->alpha ;
//				}
//				else{
//					//Calculate the volume fraction of the region with
//					//coefficient value not one
//					coeff[i] = ( (pow(distX[1],2) + pow(distY[0],2)) / 2 +
//						(distX[0] + coeffW) * distY[1] + distX[0] * coeffH ) /
//						e->area ;
//					//Calculate the contribution to the integral from the
//					//current element
//					coeff[i] = coeff[i] * params->alpha + (1-coeff[i]) ;
//				}
//			}
//		}
	}
	else if( params->testCase == 52 ){
		for( i=0 ; i < grid->numElements ; i++ ){
			set_element_bounds( &(grid->elements[i]), grid->nodes, &elBox ) ;
			if( elBox.minX < -params->initRad )
				coeff[i] = params->alpha ;
			else
				coeff[i] = 1.0 ;
				
		}
	}
	else if( params->testCase >= 53 && params->testCase <= 58 ){
		const CoeffRegions *reg ; //Placeholder for the regions of different
								  //piecewise constant coefficient
		double x, y ; //The x- and y-coordinate of the bottom left hand corner
					  //of a region of piecewise constant coefficient
		bool set ; //Indicates whether a value was set or not
		int regCnt ; //Loop counter
		
		//Set a placeholder to the variable storing information on the different
		//coefficient regions on the domain
		reg = &(params->coeffReg) ;
		
		//Loop through the elements on the grid
		for( i=0 ; i < grid->numElements ; i++ ){
			set = false ;
			set_element_bounds( &(grid->elements[i]), grid->nodes, &elBox ) ;
			
			//Loop through the number of coefficient regions
			for( regCnt = 0 ; regCnt < reg->numRegions ; regCnt++ ){
				x = reg->cornerPoints[regCnt].x ;
				y = reg->cornerPoints[regCnt].y ;
				//If the element is contained in the current region of
				//coefficient then set the value, and indicate that the value
				//of the coefficient has been set
				if( elBox.minX >= x && elBox.minX < x + reg->width[regCnt] &&
					elBox.minY >= y && elBox.minY < y + reg->height[regCnt] ){
					coeff[i] = reg->val[regCnt] ;
					set = true ;
					break ;
				}
			}
			
			//If we get to here we check if the value has been set. If not
			//we set it to be equal to the value of the parameter alpha
			if( !set )
				coeff[i] = params->alpha ;
		}
	}
}








